package drawboard;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import java.lang.reflect.Method;

/**
 * Created by kirubakaranj on 6/22/2017.
 */

public class Mycustomdialog extends Dialog {

    private Context context;

    public Mycustomdialog(Context context) {
        super(context);
        this.context = context;
        ///requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {

            try
            {
                if(!hasFocus)
                {
                    Object service  = context.getSystemService("statusbar");
                    Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                    Method collapse = statusbarManager.getMethod("collapse");
                    collapse .setAccessible(true);
                    collapse .invoke(service);
                }
            }
            catch(Exception ex)
            {
            }
           /* Intent closeDialogs = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialogs);*/
        }
    }
}
