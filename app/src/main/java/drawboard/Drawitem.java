package drawboard;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kirubakaranj on 4/5/2017.
 */

public class Drawitem implements Serializable {
    ArrayList<String> movex;
    ArrayList<String> movey;
    ArrayList<String> linex;
    ArrayList<String> liney;
    String socketerase;
    String pickeropen, pickerclose,newdraw,setsave,bitmap,undo;
    Context context;
    int color;
    float brush;

    public float getBrush() {
        return brush;
    }




    public String getUndo() {
        return undo;
    }


    public String getBitmap() {
        return bitmap;
    }


    public String getSetsave() {
        return setsave;
    }

    public String getNewDraw() {
        return newdraw;
    }


    public int getColor() {
        return color;
    }


    public String getPickeropen() {
        return pickeropen;
    }

    public String getPickerclose() {
        return pickerclose;
    }


    public String getSocketerase() {
        return socketerase;
    }


    public void setSocketerase(String socketerase) {
        this.socketerase = socketerase;
    }
    public Drawitem(Context context){
        this.context=context;
    }


    public Drawitem(ArrayList<String> movex, ArrayList<String> movey, ArrayList<String> linex, ArrayList<String> liney,String socketerase,String pickeropen,String pickerclose,int color,String newdraw,String setsave,String bitmap,String undo,Float brush){
        this.movex=movex;
        this.movey=movey;
        this.linex=linex;
        this.liney=liney;
        this.socketerase=socketerase;
        this.pickeropen=pickeropen;
        this.pickerclose=pickerclose;
        this.color=color;
        this.newdraw=newdraw;
        this.setsave=setsave;
        this.bitmap=bitmap;
        this.undo=undo;
        this.brush=brush;
    }

    public ArrayList<String> getMovex(){
        return movex;
    }
    public ArrayList<String> getMovey() {
        return movey;
    }


    public ArrayList<String> getLinex() {
        return linex;
    }

    public ArrayList<String> getLiney() {
        return liney;
    }


}
