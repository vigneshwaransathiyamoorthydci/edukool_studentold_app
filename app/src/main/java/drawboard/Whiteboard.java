package drawboard;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.gesture.Gesture;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import Utils.Utils;
import Utils.*;
import books.ImageActivity;
import connection.Client;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

/**
 * This is the activity class that serves as the main entry point to the 
 * app's user interface. This class contains the onCreate() method along
 * with some other methods. The onCreate() method is called by the system 
 * when the application is launched from the device home screen. 
 * This class is defined to be used as the main activity in the Android 
 * manifest file, AndroidManifest.xml, which is at the root of 
 * the project directory. In the manifest, this class is declared with an
 * <intent-filter> that includes the MAIN action and LAUNCHER category.
 * If either the MAIN action or LAUNCHER category are not declared for one 
 * of the activities, then the application icon will not appear in the Home 
 * screen's list of app's.
 * @author PrateekMehrotra
 *
 */
@SuppressLint("NewApi")
public class Whiteboard extends Activity implements OnClickListener,ColorPickerDialog.OnColorChangedListener,OnGesturePerformedListener {
	Client mClient = null;
	private CustomViewForDrawing drawView;
	private ImageButton currPaint;
	private ImageView drawBtn;
	private float smallBrush, mediumBrush, largeBrush;
	private ImageView eraseBtn, newBtn, saveBtn, colorPickerBtn, colorFillBtn, gestureBtn;
	private ImageView btn_undo, btn_redo;
	GestureOverlayView gestures;
	private static int RESULT_LOAD_IMAGE = 1;
	ImageView imageView;
	private static final String COLOR_PREFERENCE_KEY = "color";
	GestureLibrary mLibrary;
	//boolean gestureDetected = false;
	int i=0;
	SharedPreferences pref;
	ArrayList<String> movex;
	ArrayList<String> movey;
	ArrayList<String> linex;
	ArrayList<String> liney;
	Drawitem item;
	ImageView home;
	//ImageView sync;
	ImageView lock;
	ImageView handrise;
	ImageView cursor;
	ImageView projection;
	int color;
	float brushsize;
	String socketerase, pickeropen,pickerclose,newdraw,setsave, bitmap,undo;
	Mycustomdialog dialog;
	String address;
	String filename;
	ServerSocket serverSocket;
	String notepath;
public  static boolean synwhiteboard;
	BroadcastReceiver movepage;
	 MyDialog dialog1;


	private static final int COLOR_PICKER_DIALOG_ID = 1;
	private static final Integer[] COLORS = { Color.BLACK, Color.DKGRAY,
			Color.BLUE, Color.GREEN, Color.CYAN, Color.RED, Color.MAGENTA,
			0xFFFF6800, Color.YELLOW, Color.LTGRAY, Color.GRAY,  };
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//System.out.println("oncreate");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.whiteboard);
		/*try {
			getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
		}
		catch (Exception e)
		{

		}*/

		if(Service.isAirplaneModeOn(this))
		{

			Service.Showalert(this);

		}
		pref = getSharedPreferences("student", MODE_PRIVATE);
		drawView = (CustomViewForDrawing) findViewById(R.id.drawing);
		//LinearLayout paintLayout = (LinearLayout) findViewById(R.id.paint_colors);
		home= (ImageView) findViewById(R.id.home);
		//sync= (ImageView) findViewById(R.id.sync);
		//lock= (ImageView) findViewById(R.id.lock);
		handrise= (ImageView) findViewById(R.id.gesture);
		cursor= (ImageView) findViewById(R.id.cursor);
        projection= (ImageView) findViewById(R.id.projection);
		/*try{currPaint = (ImageButton)paintLayout.getChildAt(0);
			currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));}
		catch(NullPointerException e){}*/

		//Brush initialization
	//	pref=getSharedPreferences("student",MODE_PRIVATE);
		smallBrush = getResources().getInteger(R.integer.small_size);
		mediumBrush = getResources().getInteger(R.integer.medium_size);
		largeBrush = getResources().getInteger(R.integer.large_size);

		notepath=pref.getString("notes","");
		drawBtn = (ImageView) findViewById(R.id.draw_btn);
		drawBtn.setOnClickListener(this);

		drawView.setSizeForBrush(mediumBrush);

		eraseBtn = (ImageView)findViewById(R.id.erase_btn);
		eraseBtn.setOnClickListener(this);

		saveBtn = (ImageView)findViewById(R.id.save_btn);
		saveBtn.setOnClickListener(this);

		newBtn = (ImageView)findViewById(R.id.new_btn);
		newBtn.setOnClickListener(this);

		btn_undo = (ImageView) findViewById(R.id.undo);
		btn_undo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i("UndoTag","Inside on click of undo");
				drawView.onClickUndo();
			}
		});

		/*btn_redo=(ImageView) findViewById(R.id.redo);
		btn_redo.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Log.i("RedoTag","Inside on click of redo");
				drawView.onClickRedo();
			}
		});
*/


		/*ActionBar actionBar = getActionBar();
		actionBar.show();
*/
		colorPickerBtn = (ImageView) findViewById(R.id.color_picker);
		colorPickerBtn.setOnClickListener(this);

		//	mLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures); if(!mLibrary.load()){ finish(); }
		//	gestures = (GestureOverlayView) findViewById(R.id.gestures);
		//	gestures.addOnGesturePerformedListener(this);

		/*colorFillBtn = (ImageView) findViewById(R.id.color_fill);
		colorFillBtn.setOnClickListener(this);*/
		//Gesture Overlay View Change 03/10/2013
		gestureBtn = (ImageView) findViewById(R.id.imp);
		gestureBtn.setOnClickListener(this);


		item=new Drawitem(movex,movey,linex,liney,socketerase,pickeropen,pickerclose,color,newdraw,setsave,bitmap,undo,brushsize);

		movex=new ArrayList<String>();
		movey=new ArrayList<String>();
		linex=new ArrayList<String>();
		liney=new ArrayList<String>();

		home.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		/*sync.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toast();
			}
		});*/

		/*lock.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
toast();
			}
		});*/
		handrise.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


				if(!pref.getBoolean("hand",false)) {
					new connectTask().execute();
					if(pref.getBoolean("classtime",false))
					{
						Toast.makeText(Whiteboard.this,"Handraise sent successfully",Toast.LENGTH_SHORT).show();

					}
				}else
				{
					Utils.lockpoopup(Whiteboard.this,"Handraise","Your teacher has blocked the handraise capability.");
				}
				//String stringToSend = "Signal@" + Common.StudentName + "@" + Common.RollNumber+"@"+
			//	toast();
			}
		});
		cursor.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toast();
			}
		});
		projection.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toast();
			}
		});
		/*final	ExecutorService es = Executors.newCachedThreadPool();
		ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
		ses.scheduleAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				es.submit(new Runnable()
				{
					@Override
					public void run()
					{

					}
				});
			}
		}, 0, 1, TimeUnit.SECONDS);*/


		if(synwhiteboard) {


			drawBtn.setEnabled(false);
			eraseBtn.setEnabled(false);
			saveBtn.setEnabled(false);
			newBtn.setEnabled(false);
			colorPickerBtn.setEnabled(false);
			btn_undo.setEnabled(false);
			home.setEnabled(false);
			gestureBtn.setEnabled(false);
Toast.makeText(Whiteboard.this,"Your teacher has opted for whiteboard synchronization.",Toast.LENGTH_SHORT).show();

			MyClientTask myClientTask = new MyClientTask(
					address,
					Integer.parseInt("8080"));
			myClientTask.execute();
			address = "192.168.1.68";
		}
	}

	public class connectTask extends AsyncTask<String,String, Client> {

		@Override
		protected Client doInBackground(String... message) {

			//we create a Client object and
			SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(Whiteboard.this);
			String rollno="",schoolid="",stdid="",fname="";
			Cursor stdcursor=obj.retrive("tblStudent");
			while(stdcursor.moveToNext()){
				rollno= stdcursor.getString(stdcursor.getColumnIndex("RollNo"));
				schoolid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("SchoolID")));
				stdid= String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("StudentID")));
				fname= stdcursor.getString(stdcursor.getColumnIndex("FirstName"));

			}
			String s="";
String stringToSend = "Signal@" + fname + "@" +stdid+"@"+rollno+"@"+pref.getString("ip","");
			//String s="invS@Connect@IPAddress@RollNumber";
		//	String s="invS@connect@"+getIpAddress()+"@"+rollno+"@"+schoolid+"@"+fname+"@"+stdid+"@"+currentdate1();
			mClient = new Client(new Client.OnMessageReceived() {
				@Override

				public void messageReceived(String message) {


					try {


					} catch (Exception e) {
						e.printStackTrace();
					}


				}
			},pref,stringToSend);
			mClient.run();

			return null;
		}

	}

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue=0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
			if(curBrightnessValue>20) {
				float brightness = curBrightnessValue / (float) 255;
				WindowManager.LayoutParams lp = getWindow().getAttributes();


				lp.screenBrightness = brightness;
				getWindow().setAttributes(lp);
			}
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
       /* float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


		movepage=new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {

				/*Intent in=new Intent("filemove");
				in.putExtra("page",file[1].toString());
				sendBroadcast(in);*/
				try

				{
					if(intent.getExtras().getBoolean("close",false))
					{
						Toast.makeText(Whiteboard.this,"close",Toast.LENGTH_LONG).show();

						finish();
					}
					else {
						/*Toast.makeText(MuPDFActivity.this,"closenot",Toast.LENGTH_LONG).show();
*/
						// mDocView.setDisplayedViewIndex(Integer.parseInt(intent.getStringExtra("page")));

					}

				}
				catch (Exception e)
				{

				}

			}
		};
		IntentFilter in=new IntentFilter("filemove");
		registerReceiver(movepage,in);
    }
	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(movepage);
	}
    public class SocketServerThread extends Thread {

		static final int SocketServerPORT = 8080;
		int count = 0;

		@Override
		public void run() {
			try {
				serverSocket = new ServerSocket(1995);
				Whiteboard.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						//Toast.makeText(getApplicationContext(),"I am waiting"+serverSocket.getLocalPort(),Toast.LENGTH_LONG).show();
                        /*info.setText("I'm waiting here: "
                                + serverSocket.getLocalPort());*/
					}
				});

				while (true) {
					final Socket socket = serverSocket.accept();
					count++;
				//	drawView.setSocket(socket);

					// message += "#" + count + " from " + socket.getInetAddress()
					//           + ":" + socket.getPort() + "\n";
					//ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream()); //Error Line!
					try {
						/*Object object = objectInput.readObject();
						item =  (Drawitem) object;*/
						Runnable runclint = new Receivefile(socket);
						Thread recf = new Thread(runclint);
						recf.start();

						// System.out.println(titleList.get(1));
					} catch (Exception e) {
						System.out.println("The title list has not come from the server");
						e.printStackTrace();
					}

				/*	Whiteboard.this.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							//  msg.setText(message);
							i++;
							if(i<=4)
								Toast.makeText(getApplicationContext(),"client"+socket.getInetAddress()+","+socket.getPort(),Toast.LENGTH_LONG).show();

							try{
								drawView.setSizeForBrush(item.getBrush());
								drawView.setPrevBrushSize(item.getBrush());

								if(item.getUndo().equals("1")) {
									//	Toast.makeText(getApplicationContext(),"undotrue",Toast.LENGTH_LONG).show();
									drawView.onClickUndo();
								}else{
									//	Toast.makeText(getApplicationContext(),"undofalse",Toast.LENGTH_LONG).show();
								}
								//Toast.makeText(getApplicationContext(),"ss"+item.getSocketerase(),Toast.LENGTH_LONG).show();
								if(item.getSocketerase().equals("1")){
									//	Toast.makeText(getApplicationContext(),"sockettrue",Toast.LENGTH_LONG).show();
									drawView.setErase(true);
								}else if(item.getSocketerase().equals("0")){
									if(i<4){
										//	Toast.makeText(getApplicationContext(),"socketfalse",Toast.LENGTH_LONG).show();
										i++;}
								}
								if(item.getPickeropen().equals("1")){

									showDialog(COLOR_PICKER_DIALOG_ID);
								}else if(item.getPickeropen().equals("0")){

									try{
										dialog.dismiss();}
									catch(NullPointerException e){e.printStackTrace();}
								}

								if(item.getPickerclose().equals("1")){

									//	Toast.makeText(getApplicationContext(),"pickerclose",Toast.LENGTH_LONG).show();
								}

								drawView.setPrimColor(item.getColor());

								if(item.getNewDraw().equals("1")){
									if(i<4){
										Toast.makeText(getApplicationContext(),"newDraw",Toast.LENGTH_LONG).show();
										i++;
									}
									newDrawing();
								}

								if(item.getSetsave().equals("1")){
									Manualsave();
								}

								if(item.getBitmap()!=null){
									drawView.setUpDrawing();
									drawView.setSizeForBrush(mediumBrush);
									Bitmap socketbitmap= StringToBitMap(item.getBitmap());
									Drawable socketdrawable = new BitmapDrawable(getResources(),socketbitmap);

									//drawView.newStart();
									drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
									drawView.setBackgroundColor(Color.WHITE);
									drawView.newStart();
									drawView.setSizeForBrush(mediumBrush);
									drawView.smoothStrokes=false;

									drawView.setBackground(socketdrawable);
									// drawView.invalidate();
								}


								for(int i=0;i<item.getMovex().size();i++){
									Log.d("cmoveto",item.getMovex().get(i)+","+item.getMovey().get(i));
								}
								for(int b=0;b<item.getLinex().size();b++){
									Log.d("clineto",item.getLinex().get(b)+","+item.getLiney().get(b));
								}
								drawView.draw(item.getMovex(),item.getMovey(),item.getLinex(),item.getLiney());
								//item.getMovex().clear();
								//	item.getMovey().clear();
								//	item.getLinex().clear();
								//	item.getLiney().clear();
//
							}catch(NullPointerException e){}



						}
					});*/

					//   SocketServerReplyThread socketServerReplyThread = new SocketServerReplyThread(socket, count);
					//     socketServerReplyThread.run();

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	void toast()
	{
		Toast.makeText(getApplicationContext(),"Under Construction",Toast.LENGTH_LONG).show();
	}


	public class MyClientTask extends AsyncTask<Void, Void, Void> {

		String dstAddress;
		int dstPort;
		String response = "";


		MyClientTask(String addr, int port){
			dstAddress = addr;
			dstPort = port;
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			Socket socket = null;
			/*Whiteboard.this.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					//  msg.setText(message);
					if(i<8){
						Toast.makeText(getApplicationContext(),"client is connecting",Toast.LENGTH_LONG).show();}i++;
				}
			});*/

			try {
				///socket = new Socket(address, 8080);
				Thread socketServerThread = new Thread(new SocketServerThread());
				socketServerThread.start();

			/*	ObjectInputStream objectInput = new ObjectInputStream(socket.getInputStream()); //Error Line!
				try {
					Object object = objectInput.readObject();
					item =  (Drawitem) object;

					// System.out.println(titleList.get(1));
				} catch (ClassNotFoundException e) {
					System.out.println("The title list has not come from the server");
					e.printStackTrace();
				}
*/

			} /*catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "UnknownHostException: " + e.toString();
			}*/ catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				response = "IOException: " + e.toString();
			}/*finally{
				if(socket != null){
					try {
						Log.d("closesocketed","d");
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}*/
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			super.onPostExecute(result);




		}





	}

	public Bitmap StringToBitMap(String encodedString){
		try{
			byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
			Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
			return bitmap;
		}catch(Exception e){
			e.getMessage();
			return null;
		}
	}

	public void Manualsave(String filename){
		drawView.setDrawingCacheEnabled(true);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		drawView.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 100, baos);
		Date date = new Date();
		Format formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss", Locale.US);
		String fileName = filename + ".png";
		if (Environment.getExternalStorageState()
				.equals(Environment.MEDIA_MOUNTED)) {
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File(pref.getString("notes",""));
			//boolean directoryCreated = dir.mkdirs();

			File file = new File(dir, fileName);

			FileOutputStream f;
			try {
				f = new FileOutputStream(file);
				f.write(baos.toByteArray());
				f.flush();
				f.close();
				Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT)
						.show();
				// Trigger gallery refresh on photo save
				MediaScannerConnection.scanFile(
						getApplicationContext(),
						new String[]{file.toString()},
						null,
						new MediaScannerConnection.OnScanCompletedListener() {
							public void onScanCompleted(String path, Uri uri) {
							}
						});
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "Save Failed!",
						Toast.LENGTH_SHORT).show();
			}
		}
		drawView.destroyDrawingCache();
	}
	/**
	 * This method is called when a gesture is input from the gesture
	 * view. This method defines the logic for gesture matching using
	 * prediction scores returned by the Gesture Builder API.
	 */
	@Override
	public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture)
	{
		ArrayList<Prediction> predictions = mLibrary.recognize(gesture);
		btn_undo.setVisibility(View.GONE);
		btn_redo.setVisibility(View.GONE);
		if(predictions.size() > 0 && predictions.get(0).score > 1.0)
		{
			String result = predictions.get(0).name;

			if("new".equalsIgnoreCase(result))
			{
				drawView.setBackgroundColor(Color.WHITE);
				drawView.setVisibility(View.VISIBLE);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);
				makeToast("Gesture Detected and Views Switched");
				//drawView.newStart();
				newDrawing();
				Toast.makeText(this, "new document",Toast.LENGTH_SHORT).show();
				//gestureDetected = true;
			}
			else if("smooth".equalsIgnoreCase(result))
			{
				//	gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);

				drawView.setVisibility(View.VISIBLE);
				drawView.smoothStrokes=true;
				makeToast("Smoothening enabled!");
				//gestureDetected = true;
			}
			else if("curve".equalsIgnoreCase(result))
			{
				//		gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				drawView.setVisibility(View.VISIBLE);
				drawView.smoothStrokes=false;
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);
				makeToast("Curve enabled, smoothening disabled!");

				//gestureDetected = true;
			}
			else if("save".equalsIgnoreCase(result))
			{ 	//Gesture Overlay View Change 03/10/2013
				//		gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				//drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
				drawView.setVisibility(View.VISIBLE);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);

				makeToast("Gesture Detected and Views Switched");
				saveDrawing();
				Toast.makeText(this, "Saving the document", Toast.LENGTH_SHORT).show();
				//gestureDetected = true;
			}
			//Gesture Overlay View Change 03/10/2013
			else if("erase".equalsIgnoreCase(result)){
				//gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				drawView.setVisibility(View.VISIBLE);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);

				makeToast("Gesture Detected and Views Switched");
				eraseDraw();
				makeToast("Select Eraser Size!");
				//gestureDetected = true;
			}
			else if("open".equalsIgnoreCase(result)){
				makeToast("Gesture Detected and Views Switched");
				//onActivityResult(1, resultCode, data);
				makeToast("Refreshing...");
				Intent i = new Intent(
						Intent.ACTION_PICK, Images.Media.EXTERNAL_CONTENT_URI);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);

				startActivityForResult(i, RESULT_LOAD_IMAGE);

			}
			else if("undo".equalsIgnoreCase(result)){
				//	gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				drawView.setVisibility(View.VISIBLE);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);

				makeToast("Gesture Detected and Views Switched");
				drawView.onClickUndo();
			}
		/*	else if("redo".equalsIgnoreCase(result)){
				//	gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				drawView.setVisibility(View.VISIBLE);
				btn_undo.setVisibility(View.VISIBLE);
				btn_redo.setVisibility(View.VISIBLE);

				makeToast("Gesture Detected and Views Switched");
				drawView.onClickRedo();
			}*/

			else if("colorpicker".equalsIgnoreCase(result)){
				//	gestures.setVisibility(View.GONE);
				drawView.setBackgroundColor(Color.WHITE);
				drawView.setVisibility(View.VISIBLE);

				int color = PreferenceManager.getDefaultSharedPreferences(
						Whiteboard.this).getInt(COLOR_PREFERENCE_KEY,
						Color.WHITE);
				new ColorPickerDialog(Whiteboard.this, Whiteboard.this,
						color).show();
				makeToast("Choose color, center tap to select");

			}
			System.out.println(predictions.size() + " " + predictions.get(0).score);
		}
	}

	/**
	 * This method is called first when the eraser functionality is activated 
	 * by the user. This method is responsible for displaying a dialog box
	 * showing various eraser sizes for the user to choose from. This method 
	 * also defines the OnClickListener functions for each of the eraser sizes.
	 */
	public void eraseDraw(){
		System.out.println("when erase is clicked");
		final Mycustomdialog brushDialog = new Mycustomdialog(this);
		brushDialog.setTitle("Eraser size:");
		brushDialog.setContentView(R.layout.select_brush);
		//size buttons
		ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
		smallBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				drawView.setErase(true);
				drawView.setSizeForBrush(smallBrush);
				brushDialog.dismiss();
			}
		});
		ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
		mediumBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				drawView.setErase(true);
				drawView.setSizeForBrush(mediumBrush);
				brushDialog.dismiss();
			}
		});
		ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
		largeBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				drawView.setErase(true);
				drawView.setSizeForBrush(largeBrush);
				brushDialog.dismiss();
			}
		});
		brushDialog.show();

	}
	//Gesture Overlay View Change 03/10/2013
	//Overloaded for saving images when edited through gesture overlay
	/**
	 * This method is called when the user enables the save functionality.
	 * This method pops a dialog box asking a confirmation from the user of
	 * the desired functionality. If the user selects "yes" then saveDrawing()
	 * method is called by the "yes" button event listener to save the current 
	 * image into device gallery.
	 * @param bitmap Bitmap of current image
	 */
	public void saveDrawing(final Bitmap bitmap)
	{
		AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
		saveDialog.setTitle("Save drawing");
		saveDialog.setMessage("Save drawing to device Gallery?");
		saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){
				String url = Images.Media.insertImage(getContentResolver(), bitmap, "title", ".png");
			}
		});
		saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which){
				dialog.cancel();
			}
		});
		saveDialog.show();
	}

	/**
	 * This method is called when the new drawing functionality is 
	 * enabled by the user. This function pops a dialog box to get 
	 * a confirmation on the desired functionality. If "yes" is 
	 * selected from the dialog box, the canvas is cleared, the
	 * previous drawing is removed, and the canvas object is
	 * re-initialized to its original state.
	 */
	public void newDrawing()
	{
		drawView.setBackgroundColor(Color.WHITE);
		drawView.newStart();
		drawView.setSizeForBrush(smallBrush);
		drawView.smoothStrokes=false;

	}

	/**
	 * This method is called when the user selects "yes" from the save
	 * dialog box. This method saves the current image on which the user 
	 * is working to the device gallery.
	 */
	public void saveDrawing()
	{

		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
			System.out.println("external available");
			System.out.println(getExternalFilesDir(null));
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
			System.out.println("external available but readonly");
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			//  to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
		saveDialog.setTitle("Save drawing");
		saveDialog.setMessage("Save drawing to device Gallery?");
		saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//save drawing
				Bitmap bitmap;
				//View v1 = findViewById(R.id.drawing);
				View v1 = drawView;
				v1.setDrawingCacheEnabled(true);
				bitmap = Bitmap.createBitmap(v1.getDrawingCache());
				String url = Images.Media.insertImage(getContentResolver(), bitmap, "title", ".png");
				v1.setDrawingCacheEnabled(false);

			}
		});

		saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		saveDialog.show();
	}

	/**
	 * This method is called when the user desires to change the color
	 * to paint with. This method then sets the color object of the
	 * view to the newly selected color.
	 * @param color Desired Color 
	 */
	@Override
	public void colorChanged(int color) {

		PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(
				COLOR_PREFERENCE_KEY, color).commit();
		Integer i = new Integer(color);
		System.out.println("~~~~~~~~" + i.toHexString(color));
		System.out.println(Color.parseColor("#" + i.toHexString(color)));
		//String cc=i.toHexString(color);
		drawView.setColor("#" + i.toHexString(color));
	}
	public void onDestroy() {

		super.onDestroy();
		if(serverSocket!=null)
		{
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//	es.shutdown();
		//	ses.shutdown();
	}


	/**
	 * This method is called to initialize the options menu in
	 * the action bar.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		System.out.println("oncreateoptionsmenu");
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode!=0)
		{
			//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$"  + " " + new Integer(resultCode).toString() + " " + "$$$$$$$$$$$$$$$$$$");
			String picturePath=null;

			if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				picturePath = cursor.getString(columnIndex);
				cursor.close();
			}

			//System.out.println("<<<<<<<" + " " + picturePath + " " + "<<<<");
			// String picturePath contains the path of selected Image
			//imageView = (ImageView) findViewById(R.id.new_view);

			drawView.setUpDrawing();
			drawView.setSizeForBrush(mediumBrush);
			Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
			Drawable d = new BitmapDrawable(getResources(),bitmap);
			//drawView.newStart();
			drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
			drawView.setBackgroundColor(Color.WHITE);
			drawView.newStart();
			drawView.setSizeForBrush(mediumBrush);
			drawView.smoothStrokes=false;

			drawView.setBackground(d);

		}
		else
		{
			drawView.setVisibility(View.VISIBLE);
		}
	}

	private void initiatePopupWindow(final Context context, final ByteArrayOutputStream baos) {
		//final PopupWindow pwindo;

		try {
// We need to get the instance of the LayoutInflater
			/*LayoutInflater inflater = (LayoutInflater) Whiteboard.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.screenpopup1,
					(ViewGroup) findViewById(R.id.popup_element));
*/
			final MyDialog dialog = new MyDialog(context);
			dialog.setContentView(R.layout.screenpopup1);
			//dialog.setTitle("Title...");
			/*pwindo = new PopupWindow(layout, 450, 130, true);
			pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);*/
			/*pwindo.setOnDismissListener(new PopupWindow.OnDismissListener() {
				@Override
				public void onDismiss() {

				}
			});

*/

			/*if(pwindo.isOutsideTouchable())
			{
				pwindo.dismiss();
			}*/

			/*pwindo.setTouchInterceptor(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {

				}
			});*/
			/*pwindo.setTouchInterceptor(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					return false;
				}
			});*/
			/*pwindo.setTouchInterceptor(new ViewTreeObserver.OnWindowFocusChangeListener() {
				@Override
				public void onWindowFocusChanged(boolean hasFocus) {

				}
			});*/
			final EditText name=(EditText)dialog.findViewById(R.id.name);

			Button btnconnect = (Button) dialog.findViewById(R.id.ok);
			btnconnect.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					filename=name.getText().toString();
					if(!filename.equalsIgnoreCase("")) {

						String fileName = filename + ".png";

						if (Environment.getExternalStorageState()
								.equals(Environment.MEDIA_MOUNTED)) {
							File sdCard = Environment.getExternalStorageDirectory();
							File dir = new File(notepath);
							//boolean directoryCreated = dir.mkdirs();

							File file = new File(dir, fileName);

							FileOutputStream f;
							try {
								f = new FileOutputStream(file);
								f.write(baos.toByteArray());
								f.flush();
								f.close();
								Toast.makeText(getApplicationContext(), "Your note is saved successfully.", Toast.LENGTH_SHORT)
										.show();
								// Trigger gallery refresh on photo save
								MediaScannerConnection.scanFile(
										getApplicationContext(),
										new String[]{file.toString()},
										null,
										new MediaScannerConnection.OnScanCompletedListener() {
											public void onScanCompleted(String path, Uri uri) {
											}
										});
							} catch (Exception e) {
								e.printStackTrace();
								Toast.makeText(getApplicationContext(), "Save Failed!",
										Toast.LENGTH_SHORT).show();
							}
							dialog.dismiss();
						}

					}


				}
			});

			Button btncancel = (Button) dialog.findViewById(R.id.cancel);
			btncancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();
         //onWindowFocusChanged(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void confirmSave() {
		drawView.setDrawingCacheEnabled(true);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		drawView.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 100, baos);
		//saveWhiteboardImage(this, baos);
		initiatePopupWindow(this, baos);
		drawView.destroyDrawingCache();
	}
	public static void saveWhiteboardImage(final Context context, final ByteArrayOutputStream baos) {
		new AlertDialog.Builder(context)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setTitle("Confirm canvas save")
				.setMessage("Do you want to save the canvas?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Date date = new Date();
						Format formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss", Locale.US);
						String fileName = formatter.format(date) + ".png";
						if (Environment.getExternalStorageState()
								.equals(Environment.MEDIA_MOUNTED)) {
							File sdCard = Environment.getExternalStorageDirectory();
							File dir = new File(sdCard.getAbsolutePath() + "/Whiteboard");
							boolean directoryCreated = dir.mkdirs();

							File file = new File(dir, fileName);

							FileOutputStream f;
							try {
								f = new FileOutputStream(file);
								f.write(baos.toByteArray());
								f.flush();
								f.close();
								Toast.makeText(context, "Saved", Toast.LENGTH_SHORT)
										.show();
								// Trigger gallery refresh on photo save
								MediaScannerConnection.scanFile(
										context,
										new String[]{file.toString()},
										null,
										new MediaScannerConnection.OnScanCompletedListener() {
											public void onScanCompleted(String path, Uri uri) {
											}
										});
							} catch (Exception e) {
								e.printStackTrace();
								Toast.makeText(context, "Save Failed!",
										Toast.LENGTH_SHORT).show();
							}
						}
					}
				})
				.setNegativeButton("No", null)
				.show();
	}

	public void fileimport(String filename)
	{

		File sdCard = Environment.getExternalStorageDirectory();
	//	File dir = new File(sdCard.getAbsolutePath() + "/Whiteboard");
		File dir = new File(pref.getString("notes",""));

		boolean directoryCreated = dir.mkdirs();
		File file = new File(dir, filename+".png");
		if(file.exists())
		{
			drawView.setUpDrawing();
			drawView.setSizeForBrush(mediumBrush);
			Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
			Drawable d = new BitmapDrawable(getResources(),bitmap);
			//drawView.newStart();
			drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
			drawView.setBackgroundColor(Color.WHITE);
			drawView.newStart();
			drawView.setSizeForBrush(mediumBrush);
			drawView.smoothStrokes=false;

			drawView.setBackground(d);

		}
		else {
			Toast.makeText(getApplicationContext(),"File not Found",Toast.LENGTH_LONG).show();
		}
	}


	/**
	 * This method is used to return the current status of the drawView
	 * @return drawView the current status of the drawView
	 */
	public CustomViewForDrawing getDrawView()
	{
		return drawView;
	}

	/**
	 * This method is called to display a toast message on device
	 * screen.
	 * @param message
	 */
	public void makeToast(String message) {
		// with jam obviously
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	/**
	 * This function is called when the user desires to change
	 * the brush thickness that is being used to draw on  the 
	 * canvas. This function pops a dialog box asking the user
	 * to select a particular brush thickness. When the user 
	 * makes a selection, the event listener for each brush
	 * thickness is defined in this method which sets the 
	 * brush thickness object with the newly chosen brush 
	 * thickness.
	 * @param view
	 */
	//user clicks on paint
	public void paintClicked(View view){
		System.out.println("paintclicked");
		drawView.setErase(false);
		drawView.setSizeForBrush(drawView.getPrevBrushSize());

		if(view!=currPaint){
			ImageButton imgView = (ImageButton) view;
			String color = view.getTag().toString();

			drawView.setColor(color);

			imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
			currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint));
			currPaint = (ImageButton)view;
		}
	}

	/**
	 * This function defines all the event listeners for the 
	 * icons in the main layout of the application.
	 */
	@Override
	public void onClick(View v)
	{
		//drawView.setVisibility(View.VISIBLE);
		//imageView.setVisibility(View.GONE);
		System.out.println("onclick");
		// TODO Auto-generated method stub
		if(v.getId()==R.id.draw_btn)
		{
			drawBtn.setImageResource(R.drawable.unpaint_br);//for change
			eraseBtn.setImageResource(R.drawable.eraser);
			btn_undo.setImageResource(R.drawable.undo);
			saveBtn.setImageResource(R.drawable.save);
			gestureBtn.setImageResource(R.drawable.importimage);
			colorPickerBtn.setImageResource(R.drawable.colorpicker);

			//draw button clicked
			final Mycustomdialog brushDialog = new Mycustomdialog(this);
			brushDialog.setTitle("Brush size:");
			brushDialog.setContentView(R.layout.select_brush);

			//Different buttons for varying size brushes; implement gestures in it
			ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
			smallBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					//drawView.setErase(false);
					drawView.setErase(false);
					drawView.setSizeForBrush(smallBrush);
					drawView.setPrevBrushSize(smallBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
			mediumBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					drawView.setErase(false);
					drawView.setSizeForBrush(mediumBrush);
					drawView.setPrevBrushSize(mediumBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
			largeBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					drawView.setErase(false);
					drawView.setSizeForBrush(largeBrush);
					drawView.setPrevBrushSize(largeBrush);
					brushDialog.dismiss();
				}
			});
			//show and wait for user interaction
			brushDialog.show();

		}
		else if(v.getId()==R.id.erase_btn){
			//for image change
			drawBtn.setImageResource(R.drawable.paint_br);
			eraseBtn.setImageResource(R.drawable.unerase);
			btn_undo.setImageResource(R.drawable.undo);
			saveBtn.setImageResource(R.drawable.save);
			gestureBtn.setImageResource(R.drawable.importimage);
			colorPickerBtn.setImageResource(R.drawable.colorpicker);



			//switch to erase - choose size
			System.out.println("when erase is clicked");
			final Mycustomdialog brushDialog = new Mycustomdialog(this);
			brushDialog.setTitle("Eraser size:");
			brushDialog.setContentView(R.layout.select_brush);
			//size buttons
			ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
			smallBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					drawView.setErase(true);
					drawView.setSizeForBrush(smallBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
			mediumBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					drawView.setErase(true);
					drawView.setSizeForBrush(mediumBrush);
					brushDialog.dismiss();
				}
			});
			ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
			largeBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v) {
					drawView.setErase(true);
					drawView.setSizeForBrush(largeBrush);
					brushDialog.dismiss();
				}
			});
			brushDialog.show();
		}
		else if(v.getId()==R.id.save_btn)
		{
			//for image change
			drawBtn.setImageResource(R.drawable.paint_br);
			eraseBtn.setImageResource(R.drawable.eraser);
			btn_undo.setImageResource(R.drawable.undo);
			saveBtn.setImageResource(R.drawable.unsave);
			gestureBtn.setImageResource(R.drawable.importimage);
			colorPickerBtn.setImageResource(R.drawable.colorpicker);
			confirmSave();
		}
		else if(v.getId()==R.id.new_btn)
		{
			newDrawing();
		}
		else if(v.getId()==R.id.color_picker)
		{
			//for image change
			drawBtn.setImageResource(R.drawable.paint_br);
			eraseBtn.setImageResource(R.drawable.eraser);
			btn_undo.setImageResource(R.drawable.undo);
			saveBtn.setImageResource(R.drawable.save);
			gestureBtn.setImageResource(R.drawable.importimage);
			colorPickerBtn.setImageResource(R.drawable.uncolorpicker);

			showDialog(COLOR_PICKER_DIALOG_ID);
		}
		//Gesture Overlay View Change 03/10/2013
		/*else if(v.getId()==R.id.color_fill)
		{
			boolean smoothStatus = drawView.smoothStrokes;
			drawView.setUpDrawing();
			drawView.smoothStrokes = smoothStatus;
			drawView.setSizeForBrush(mediumBrush);
			drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
			drawView.getCanvas().drawColor(0, PorterDuff.Mode.SRC);
			int color = drawView.getColor();
			drawView.getCanvas().drawColor(color);
			drawView.invalidate();
			//paintClicked(drawView);

		}*/
		//Gesture Overlay View Change 03/10/2013
		else if(v.getId()==R.id.imp)
		{
			//for image change
			drawBtn.setImageResource(R.drawable.paint_br);
			eraseBtn.setImageResource(R.drawable.eraser);
			btn_undo.setImageResource(R.drawable.undo);
			saveBtn.setImageResource(R.drawable.save);
			gestureBtn.setImageResource(R.drawable.unimport);
			colorPickerBtn.setImageResource(R.drawable.colorpicker);

			makeToast("Refreshing...");
		//	Intent i = new Intent(
		//			Intent.ACTION_PICK, Images.Media.EXTERNAL_CONTENT_URI);

		//	startActivityForResult(i, RESULT_LOAD_IMAGE);
			File f = new File(pref.getString("notes",""));
			getfile(f);

			ImportWindow(Whiteboard.this);

		}
	}
	ArrayList<String> pathList;
	public ArrayList<String> getfile(File dir) {

		pathList = new ArrayList<>();

		File listFile[] = dir.listFiles();
		if (listFile != null && listFile.length > 0) {
			for (int i = 0; i < listFile.length; i++) {
				if (listFile[i].getName().endsWith(".png")
						|| listFile[i].getName().endsWith(".jpg")
						|| listFile[i].getName().endsWith(".jpeg")
						|| listFile[i].getName().endsWith(".gif"))
				{
					//String temp = listFile.getPath().substring(0, file.getPath().lastIndexOf('/'));
				//	fileList.add(temp);
					;
					Log.e("imagename",listFile[i].getAbsolutePath());
					pathList.add(listFile[i].getAbsolutePath());
				}
			}
		}
		return pathList;
	}

	Importadapter adapter;
	 PopupWindow pwindo;

	private void ImportWindow(final Context context) {

		try {
// We need to get the instance of the LayoutInflater
			/*LayoutInflater inflater = (LayoutInflater) Whiteboard.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.import_popup,
					(ViewGroup) findViewById(R.id.popup_element));
			pwindo = new PopupWindow(layout, 650, 530, true);
			pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);*/
			 dialog1=new MyDialog(context);
			dialog1.setContentView(R.layout.import_popup);

			GridView gridView = (GridView)dialog1.findViewById(R.id.gridView);


			adapter = new Importadapter(Whiteboard.this,
					R.layout.booklistitem, pathList);

            gridView.setAdapter(adapter);



			Button btncancel = (Button) dialog1.findViewById(R.id.cancel);
			btncancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog1.dismiss();
				}
			});
			dialog1.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

   public void setImportedImaged(String img){

	  // pwindo.dismiss();
	   try {
		   dialog1.dismiss();
	   }
	   catch (Exception e)
	   {

	   }
	   drawView.setUpDrawing();
	   drawView.setSizeForBrush(mediumBrush);
	   Bitmap bitmap = BitmapFactory.decodeFile(img);
	   Drawable d = new BitmapDrawable(getResources(),bitmap);
	   //drawView.newStart();
	   drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
	   drawView.setBackgroundColor(Color.WHITE);
	   drawView.newStart();
	   drawView.setSizeForBrush(mediumBrush);
	   drawView.smoothStrokes=false;

	   drawView.setBackground(d);

   }
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {

			case COLOR_PICKER_DIALOG_ID:
				dialog = new Mycustomdialog(this);
				dialog.setTitle("Choose colour");
				dialog.setContentView(R.layout.color_picker);
				GridView gridView = (GridView) dialog
						.findViewById(R.id.color_picker_gridview);
				gridView.setAdapter(new ArrayAdapter<Integer>(this, 0, COLORS) {
					@Override
					public View getView(int position, View convertView,
										ViewGroup parent) {
						final int color = COLORS[position];
						View colorView = new View(Whiteboard.this);
						colorView.setMinimumWidth(45);
						colorView.setMinimumHeight(45);
						colorView.setBackgroundColor(color);
						return colorView;
					}
				});
				gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View v,
											int position, long id) {
						drawView.setPrimColor(COLORS[position]);
						dialog.dismiss();
					}
				});
				return dialog;
			default:
				return super.onCreateDialog(id);
		}}

	private String getMessageFromTheConnection(Socket connection) {
		String message = null;
		try {
			InputStreamReader isr = new InputStreamReader(new BufferedInputStream(connection.getInputStream()));
			StringBuilder process = new StringBuilder();
			synchronized (this) {
				while (true) {
					try {
						Log.e("reading","reading");
						int character = isr.read();
						Log.e("character",""+character);
						if (!(character == 13 || character == -1)) {
							process.append((char) character);
						}
						else
						{
							message = process.toString();
							///connection.close();
							return  message;
							//break;
						}
						message=process.toString();
						Log.e("reading","reading"+message);
					} catch (Exception e) {
						Log.e("catch","reading"+process.toString());

						e.printStackTrace();
					}
				}
				//Log.e("ending","end");

                /*  Log.e("messaging","msg"+message);
                  messagelistener.onMessageReceived(message);*/
			}


		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return message;
	}


	private  class Receivefile implements Runnable {
		//private Socket socket;

		String fname;
		long flen;
		String ipaddress;
		Socket getwhiteboard;

		Receivefile(Socket sock) {
			getwhiteboard=sock;



			/*fname=filename;
			flen=filelenth;
			ipaddress=ip;*/
		}

		@Override
		public void run() {



			try {

				final String message=getMessageFromTheConnection(getwhiteboard);

				Whiteboard.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						//  msg.setText(message);
						i++;
						/*if(i<=4)
							Toast.makeText(getApplicationContext(),"client"+socket.getInetAddress()+","+socket.getPort(),Toast.LENGTH_LONG).show();
*/
						//String whiteboard = movex.toString() + "@" + movey.toString() + "@" + linex.toString() + "@" + liney.toString() + "@" + "" + getColor() + "@" + newdraw + "@" + setsave;

						/*String splitstring[]=message.split("\\@");
						Log.e("splitstring",splitstring[0].toString().replaceAll("\\[", "").replaceAll("\\]",""));
						String x=splitstring[0].toString().replaceAll("\\[", "").replaceAll("\\]","");
						String y=splitstring[1].toString().replaceAll("\\[", "").replaceAll("\\]","");
						String x1=splitstring[2].toString().replaceAll("\\[", "").replaceAll("\\]","");
						String y1=splitstring[3].toString().replaceAll("\\[", "").replaceAll("\\]","");
						ArrayList<String> movex = new ArrayList<String>(Arrays.asList(x.trim().split(",")));
						ArrayList<String> movey = new ArrayList<String>(Arrays.asList(y.trim().split(",")));

						ArrayList<String> linex = new ArrayList<String>(Arrays.asList(x1.trim().split(",")));

						ArrayList<String> liney = new ArrayList<String>(Arrays.asList(y1.trim().split(",")));


						try{


							drawView.draw(movex,movey,linex,liney);

//
						}catch(NullPointerException e){}*/
						synchronized (this) {

							if (message.contains("color")) {
								String color[] = message.split(":");
								if (color[1].equalsIgnoreCase("-1")) {
									drawView.setErase(true);
								} else {
									drawView.setErase(false);
									drawView.setSizeForBrush(Float.parseFloat(color[3]));
									drawView.setPrevBrushSize(Float.parseFloat(color[3]));
									drawView.setPrimColor(Integer.parseInt(color[2]));
								}
							} else if (message.contains("undo")) {
								drawView.onClickUndo();
							}
							else if(message.contains("close"))
							{
								drawBtn.setEnabled(true);
								eraseBtn.setEnabled(true);
								saveBtn.setEnabled(true);
								newBtn.setEnabled(true);
								colorPickerBtn.setEnabled(true);
								btn_undo.setEnabled(true);
								home.setEnabled(true);
								gestureBtn.setEnabled(true);
								home.setEnabled(true);
                                synwhiteboard=false;
								Toast.makeText(Whiteboard.this,"Your teacher has removed the whiteboard synchronization.",Toast.LENGTH_SHORT).show();
								try {
									serverSocket.close();
								} catch (IOException e) {
									e.printStackTrace();
								}

							}
							else if(message.contains("currentFile"))
							{
								String filepath[]=message.split(":");
								fileimport(filepath[1]);
							}
							else if(message.contains("save"))
							{
								String  filenamesplit[]= message.split(":");
								Manualsave(filenamesplit[1]);

							}
							else if (message.contains("#")) {

								String linpath[] = message.split("#");

								ArrayList<String> movex = new ArrayList<String>();
								ArrayList<String> movey = new ArrayList<String>();

								ArrayList<String> linex = new ArrayList<String>();

								ArrayList<String> liney = new ArrayList<String>();


								for (int i = 0; i < linpath.length; i++) {
									if (i == 0) {
										String getppoint[] = linpath[i].split(":");
										movex.add(getppoint[1]);
										movey.add(getppoint[2]);
										linex.add(getppoint[1]);
										liney.add(getppoint[2]);
									} else {
										String getppoint[] = linpath[i].split(":");
										//movex.add(getppoint[1]);
										//movey.add(getppoint[2]);
										linex.add(getppoint[1]);
										liney.add(getppoint[2]);
									}
								}
								drawView.draw(movex, movey, linex, liney);
							}


						}
					}
				});

			} catch (Exception e) {
				e.printStackTrace();
			}

			//msg.onMessageReceived("file recieved");

		}
	}

	@Override
	public void onBackPressed() {
		// super.onBackPressed();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		System.out.println("....window focus changed..");
		Log.e("hia","hia");
		super.onWindowFocusChanged(hasFocus);
		try
		{
			if(!hasFocus)
			{
				Object service  = getSystemService("statusbar");
				Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
				Method collapse = statusbarManager.getMethod("collapse");
				collapse .setAccessible(true);
				collapse .invoke(service);
			}
		}
		catch(Exception ex)
		{
		}
	}

}