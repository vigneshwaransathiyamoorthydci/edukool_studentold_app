package calendar;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.util.ArrayList;


import com.dci.edukool.student.SqliteOpenHelperDemo;

public class CaldroidSampleCustomFragment extends CaldroidFragment {

	@Override
	public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		SqliteOpenHelperDemo db=new SqliteOpenHelperDemo(getActivity().getApplicationContext());
       ArrayList<Calendarpojo> calendarlist =db.retriveCalendar();

		return new CaldroidSampleCustomAdapter(getActivity(), month, year,
				getCaldroidData(), extraData,calendarlist);
	}

}
