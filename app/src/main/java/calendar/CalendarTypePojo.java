package calendar;

/**
 * Created by kirubakaranj on 6/5/2017.
 */

public class CalendarTypePojo {

    int eventtypeid;
    String eventtypename;
    String eventtypeDescription;
    String eventtypeimage;

    public int getEventtypeid() {
        return eventtypeid;
    }

    public void setEventtypeid(int eventtypeid) {
        this.eventtypeid = eventtypeid;
    }

    public String getEventtypename() {
        return eventtypename;
    }

    public void setEventtypename(String eventtypename) {
        this.eventtypename = eventtypename;
    }

    public String getEventtypeDescription() {
        return eventtypeDescription;
    }

    public void setEventtypeDescription(String eventtypeDescription) {
        this.eventtypeDescription = eventtypeDescription;
    }

    public String getEventtypeimage() {
        return eventtypeimage;
    }

    public void setEventtypeimage(String eventtypeimage) {
        this.eventtypeimage = eventtypeimage;
    }



}
