package calendar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.io.File;
import java.util.ArrayList;


import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

/**
 * Custom list adapter, implementing BaseAdapter
 */
public class FirstListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Calendarpojo> items;
    SqliteOpenHelperDemo obj;

    public FirstListAdapter(Context context, ArrayList<Calendarpojo> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.category_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Calendarpojo currentItem = (Calendarpojo) getItem(position);
        try {
            if (currentItem.getCategoryIcon() != null)
                setbackground(viewHolder.cateicon, currentItem.getCategoryIcon());
        }
        catch(Exception e){ }


     /*  if(currentItem.getCategory().equalsIgnoreCase("holiday")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mholiday);
        }
        if(currentItem.getCategory().equalsIgnoreCase("Competition")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mcompetition);
        }
        if(currentItem.getCategory().equalsIgnoreCase("Event")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mevent);
        }
        if(currentItem.getCategory().equalsIgnoreCase("Examination")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mexam);
        }
        if(currentItem.getCategory().equalsIgnoreCase("Assignment")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.assignment);
        }*/


        if(currentItem.getCategory().equalsIgnoreCase("holiday")){
            viewHolder.stdname.setText("Holiday");
        }
        if(currentItem.getCategory().equalsIgnoreCase("Competition")){
            viewHolder.stdname.setText("Competition");
        }
        if(currentItem.getCategory().equalsIgnoreCase("Event")){
            viewHolder.stdname.setText("Event");
        }
        if(currentItem.getCategory().equalsIgnoreCase("Examination")){
            viewHolder.stdname.setText("Examination");
        }
        if(currentItem.getCategory().equalsIgnoreCase("Assignment")){
            viewHolder.stdname.setText("Assignment");
        }


      //  viewHolder.stdname.setText(currentItem.get());

      /*  viewHolder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //showPopup(msg.getText().toString(),stdname.getText().toString(),content.getText().toString(),dat.getText().toString());
            }
        });
*///if (profile != null) setbackground(profileimage, profile);

        return convertView;
    }

    //ViewHolder inner class
    private class ViewHolder {

        TextView comid;
        TextView msg, stdname, content, dat;
        RelativeLayout rel;
        ImageView cateicon;

        public ViewHolder(View view) {

            stdname = (TextView) view.findViewById(R.id.category);
            content = (TextView) view.findViewById(R.id.id);
            cateicon = (ImageView) view.findViewById(R.id.cateicon);
           // rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }
    void setbackground(ImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

}