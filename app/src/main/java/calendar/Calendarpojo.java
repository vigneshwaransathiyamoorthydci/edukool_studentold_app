package calendar;

/**
 * Created by kirubakaranj on 5/26/2017.
 */

public class Calendarpojo {
    String eventname;
    String eventDesc;
    String eventstart;
    String eventend;
    String category;
    String categoryIcon;
    int eventid;
    int categoryID;

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getEventstart() {
        return eventstart;
    }

    public void setEventstart(String eventstart) {
        this.eventstart = eventstart;
    }

    public String getEventend() {
        return eventend;
    }

    public void setEventend(String eventend) {
        this.eventend = eventend;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }




    public int getEventid() {
        return eventid;
    }

    public void setEventid(int eventid) {
        this.eventid = eventid;
    }
}
