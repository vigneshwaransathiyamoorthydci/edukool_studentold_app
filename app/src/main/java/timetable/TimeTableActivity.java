package timetable;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import Utils.Utils;
import Utils.*;
import calendar.CalendarTypePojo;
import helper.Rooms;
import helper.RoundedImageView;
import com.dci.edukool.student.Batchpojo;
import com.dci.edukool.student.BuildConfig;
import com.dci.edukool.student.Masterpojo;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpen;
import com.dci.edukool.student.SqliteOpenHelperDemo;
import com.dci.edukool.student.Staffpojo;


public class TimeTableActivity extends Activity {
   Button er;
    LinearLayout lin;

    String currentday,profile,clas,sec,ac,firstname,lastname;
    ArrayList<Subject> period=new ArrayList<>();
    ArrayList<Subject> mon=new ArrayList<>();
    ArrayList<Subject> tue=new ArrayList<>();
    ArrayList<Subject> wed=new ArrayList<>();
    ArrayList<Subject> thu=new ArrayList<>();
    ArrayList<Subject> firday=new ArrayList<>();
    ArrayList<Subject> satday=new ArrayList<>();
    ArrayList<Subject> sunday=new ArrayList<>();
    RoundedImageView profileimage;
    TextView batchname,attendance;
    ArrayList<Subject>common;
    ImageView back,down;

    SharedPreferences.Editor edit;
    SqliteOpenHelperDemo obj;
    TextView mon_text,tue_text,wed_text,thurs_text,fri_text,sat_text,sun_text,dat,stdname;
    int batchid;

    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    SharedPreferences pref;
    Utils utils;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.timetable);
        lin= (LinearLayout) findViewById(R.id.linearLayout1);
        profileimage= (RoundedImageView) findViewById(R.id.profileimage);
        back= (ImageView) findViewById(R.id.back);
        down= (ImageView) findViewById(R.id.down);

        mon_text=(TextView)findViewById(R.id.mon);
        tue_text=(TextView)findViewById(R.id.tue);
        wed_text=(TextView)findViewById(R.id.wed);
        thurs_text=(TextView)findViewById(R.id.thur);
        fri_text=(TextView)findViewById(R.id.fri);
        sat_text=(TextView)findViewById(R.id.sat);
        sun_text=(TextView)findViewById(R.id.sun);
        dat=(TextView)findViewById(R.id.dat);
        batchname= (TextView) findViewById(R.id.tex);
        attendance= (TextView) findViewById(R.id.classname);
        stdname= (TextView) findViewById(R.id.stdname);

        currentday=getCurrentDay();

        utils=new Utils(TimeTableActivity.this);
        obj= new SqliteOpenHelperDemo(getApplicationContext());

        Cursor stdcur=obj.retrive("tblStudent");
        while(stdcur.moveToNext()){
            profile= stdcur.getString(stdcur.getColumnIndex("PhotoFilename"));
            batchid= stdcur.getInt(stdcur.getColumnIndex("BatchID"));
            firstname= stdcur.getString(stdcur.getColumnIndex("FirstName"));
            lastname= stdcur.getString(stdcur.getColumnIndex("LastName"));
        }

        try {
            stdname.setText(firstname.toUpperCase() + " " + lastname.toUpperCase());
        }catch(Exception e){}

        setbackground(profileimage, profile);

        Cursor batcursor=obj.retriveClassName();
        while(batcursor.moveToNext()){
            clas=batcursor.getString(batcursor.getColumnIndex("ClassName"));
            sec= batcursor.getString(batcursor.getColumnIndex("SectionName"));

        }

        Cursor bc=obj.retrivevalue(batchid);
        while(bc.moveToNext()){
            ac=bc.getString(bc.getColumnIndex("AcademicYear"));
        }

        batchname.setText(clas+" "+sec+" "+ac);


        dat.setText(headerdate());

        if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }



        pref = getSharedPreferences("student", MODE_PRIVATE);

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String osversion = Build.VERSION.RELEASE;
//                String devname = android.os.Build.MODEL;
                String appversion= BuildConfig.VERSION_NAME;

                String login_str="UserName:"+pref.getString("username","")+"|Password:"+pref.getString("password","")+"|Function:StudentLogin|Update:Yes"+"|DeviceType:Android|AppVersion:"+appversion+"|MACAddress:"+"|OSVersion:"+osversion+"|GCMKey:"+"|DeviceID:"+"|AppID:"+"|IMEINumber:";
                login.clear();
                byte[] data ;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if(utils.hasConnection()) {
                        login.clear();
                        login.add(new BasicNameValuePair("WS", base64_register));

                        Load_Login_WS load_plan_list = new Load_Login_WS(TimeTableActivity.this, login);
                        load_plan_list.execute();
                    }
                    else
                    {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        new AsyncTask<Void,Void,Void>()
        {

            @Override
            protected Void doInBackground(Void... params) {
                period.clear();
                mon.clear();
                tue.clear();
                wed.clear();
                thu.clear();
                firday.clear();
                parseXML();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                ArrayList<ArrayList<Subject>>all=new ArrayList<ArrayList<Subject>>();
                all.add(period);
                all.add(mon);
                all.add(tue);
                all.add(wed);
                all.add(thu);
                all.add(firday);

                for(int y=0;y<period.size();y++) {
                    Subject sub = new Subject();
                    sub.setSubjectname("");
                    satday.add(sub);

                }
                all.add(satday);

                for(int y=0;y<period.size();y++) {
                    Subject sub = new Subject();
                    sub.setSubjectname("");
                    sunday.add(sub);

                }
                all.add(sunday);


                for(int i=0; i<all.size(); i++)
                {
                    final View hiddenInfo = getLayoutInflater().inflate(
                            R.layout.dynamicview, null, false);
                    LinearLayout hor= (LinearLayout) hiddenInfo.findViewById(R.id.dynlin);
                    for(int j=0;j<all.get(i).size();j++)
                    {
                        final View hiddenInfo2 = getLayoutInflater().inflate(
                                R.layout.dyntext, null, false);
                        TextView text= (TextView) hiddenInfo2.findViewById(R.id.dyn);
                        text.setText(all.get(i).get(j).getSubjectname());

                        if(currentday.equalsIgnoreCase("Monday")){
                          if(i==1){
                          text.setBackgroundResource(R.drawable.yellow_border);
                          mon_text.setBackgroundResource(R.drawable.yellow_border);}
                        }
                        else if(currentday.equalsIgnoreCase("Tuesday")){
                            if(i==2) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                tue_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        }
                        else  if(currentday.equalsIgnoreCase("Wednesday")){
                            if(i==3) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                wed_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        }
                        else  if(currentday.equalsIgnoreCase("Thursday")){
                            if(i==4) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                thurs_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        }
                        else if(currentday.equalsIgnoreCase("Friday")){
                            if(i==5) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                fri_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        }
                        else  if(currentday.equalsIgnoreCase("Saturday")){
                            if(i==6) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                sat_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        }


                        hor.addView(hiddenInfo2);
                    }
                    lin.addView(hor);
                }

            }
        }.execute();


    }
    public static String headerdate()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public static String headerday()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("ddd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    public String getCurrentDay()
    {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:

                return "Sunday";
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";

            // etc.
        }
        return null;
    }
    public static String currentdate()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.timetable, menu);
        return true;
    }
    int batid;
    private void parseXML() {
      String timetable="";

        Cursor stdcursor=obj.retrive("tblStudent");
        while(stdcursor.moveToNext()){
            batid= stdcursor.getInt(stdcursor.getColumnIndex("BatchID"));

        }


        Cursor xmlcursor=obj.retriveTimeTable(batid);
        while(xmlcursor.moveToNext()){
            timetable= xmlcursor.getString(xmlcursor.getColumnIndex("TimeTable"));
            Log.e("timetable",timetable+",");
            }
        try {
            InputStream is = new FileInputStream(timetable);/*assetManager.open("1493791668.xml");*/
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            ItemXMLHandler myXMLHandler = new ItemXMLHandler();
            xr.setContentHandler(myXMLHandler);
            Log.d("inpurt",is.toString());
            InputSource inStream = new InputSource(is);
            xr.parse(inStream);

            is.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        // your code.
    }

    class ItemXMLHandler extends DefaultHandler {

        Boolean currentElement = false;
        String currentValue = "";

        Subject sub;

        @Override
        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {

            currentElement = true;
            currentValue = "";
            if (localName.equals("Time")) {
                sub=new Subject();
            }
            else if (localName.equals("Mon")) {
                common=new ArrayList<>();
            }
            else if (localName.equals("Tue")) {
                common=new ArrayList<>();
                     }
            else if (localName.equals("Wed")) {
                common=new ArrayList<>();
            }
            else if (localName.equals("Thu")) {
                common=new ArrayList<>();
            }
            else if (localName.equals("Fri")) {
                common=new ArrayList<>();
            }



        }

        // Called when tag closing
        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {

            currentElement = false;

            /** set value */
            if (localName.equalsIgnoreCase("period")) {
                Subject sub=new Subject();
                sub.setSubjectname(currentValue);
                period.add(sub);
            }
            else if(localName.equals("subject"))
            {
                Subject sub=new Subject();
                sub.setSubjectname(currentValue);
                common.add(sub);
            }

            else if (localName.equalsIgnoreCase("Mon")) {
                mon.addAll(common);
                common.clear();

            }

            else if (localName.equalsIgnoreCase("Tue")) {
                tue.addAll(common);
                common.clear();

            }

            else if (localName.equalsIgnoreCase("Wed")) {
                wed.addAll(common);
                common.clear();

            }

            else if (localName.equalsIgnoreCase("Thu")) {
                thu.addAll(common);
                common.clear();

            }

            else if (localName.equalsIgnoreCase("Fri")) {
                firday.addAll(common);
                common.clear();
               /* sub.setSubjectname(currentValue);
                period.add(sub);*/
            }

        }

        // Called to get tag characters
        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {

            if (currentElement) {
                currentValue = currentValue +  new String(ch, start, length);
            }

        }

    }




    //for download Update

    class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog dia;

        public Load_Login_WS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(TimeTableActivity.this);
            dia.setMessage("DOWNLOADING...");
            dia.setCancelable(false);
            dia.show();


        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service sr = new Service(TimeTableActivity.this);
                jsonResponseString = sr.getLogin(login,
                        Url.base_url);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            new secondasync(jsonResponse).execute();

            // new LoginActivity.secondasync(jsonResponse).execute();

        }
    }



    class secondasync extends AsyncTask<String, String, String> {
        String jsonResponse;
        ProgressDialog dia;

        public secondasync(String jsonResponse){this.jsonResponse=jsonResponse;}

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(TimeTableActivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();}
        @Override
        protected String doInBackground(String... params) {

            try {
                JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                String statusCode = jObj.getString("StatusCode");

                // if (status.toString().equalsIgnoreCase("Success")) {
                if (statusCode.toString().equalsIgnoreCase("200")) {


                    ArrayList<Masterpojo> masterlist = new ArrayList<Masterpojo>();

                    for (int i = 0; i < jObj.getJSONObject("masterInfo").getJSONArray("Subjects").length(); i++) {
                        int mid;
                        try{
                            mid=Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));}
                        catch(Exception e){mid=0;}

                        Cursor c=obj.retriveIsMasterID(mid);
                        Masterpojo masterpojo = new Masterpojo();
                        if(c.getCount()>0==false){

                            masterpojo.setSubid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));
                            masterpojo.setSubjectname(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectName"));
                            masterpojo.setSubjectdesc(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectDescription"));
                            masterpojo.setCreatedBy(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedBy"));
                            masterpojo.setCreateddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedDate"));
                            masterpojo.setModifiedby(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedBy"));
                            masterpojo.setModifieddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedDate"));
                            masterpojo.setStatus(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("Status"));
                            masterpojo.setSchoolid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SchoolID"));

                            masterlist.add(masterpojo);

                        }

                        if(c.getCount()>0){

                            masterpojo.setSubid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));
                            masterpojo.setSubjectname(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectName"));
                            masterpojo.setSubjectdesc(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectDescription"));
                            masterpojo.setCreatedBy(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedBy"));
                            masterpojo.setCreateddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedDate"));
                            masterpojo.setModifiedby(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedBy"));
                            masterpojo.setModifieddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedDate"));
                            masterpojo.setStatus(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("Status"));
                            masterpojo.setSchoolid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SchoolID"));
                            obj.UpdateMasterInfo(masterpojo);
                        }
                    }

                    if(masterlist.size()!=0)
                        obj.InsertMasterInfo(masterlist);

                    obj.UpdateSubjectFromMaster();

                    JSONArray roomsarray= jObj.getJSONObject("masterInfo").getJSONArray("Rooms");

                    for(int r=0; r<roomsarray.length(); r++)
                    {

                        Rooms room=new Rooms();
                        JSONObject roomobject=roomsarray.getJSONObject(r);
                        int rid;
                        try{
                            rid=Integer.parseInt(roomobject.getString("ID"));}
                        catch(Exception e){rid=0;}

                        Cursor c= obj.retriveIsRoomid(rid);

                        if(c.getCount()>0==false) {
                            try {
                                room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);
                            } catch (Exception e) {
                                room.setId(0);
                            }
                            room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                            room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                            room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");
                            obj.addmasterroom(room);
                        }
                        if(c.getCount()>0){
                            try {
                                room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);
                            } catch (Exception e) {
                                room.setId(0);
                            }
                            room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                            room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                            room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");
                            obj.UpdateRoom(room);
                        }
                    }





                    for (int i = 0; i < jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").length(); i++) {

                        CalendarTypePojo calendarTypePojo = new CalendarTypePojo();
                        int chkid;
                        try{
                            chkid=Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID"));
                        }catch(Exception e){
                            chkid=0;
                        }
                        Cursor c=obj.retriveIsCalenderTypeId(chkid);

                        if(c.getCount()>0==false) {
                            try {
                                calendarTypePojo.setEventtypeid(Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID")));
                            } catch (Exception e) {
                                calendarTypePojo.setEventtypeid(0);
                            }

                            calendarTypePojo.setEventtypename(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Name"));
                            calendarTypePojo.setEventtypeDescription(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Description"));


                            String imagefile = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").substring(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").lastIndexOf("/") + 1, jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").length());

                            try {
                                downloadfile(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image"), pref.getString("calender", ""), imagefile);
                            } catch (Exception e) {
                            }
                            String imagename = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image");
                            String local_path = pref.getString("calender", "") + "/" + imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());
                            calendarTypePojo.setEventtypeimage(local_path);


                            obj.InsertCalenderType(calendarTypePojo);
                        }

                        if(c.getCount()>0){

                            try {
                                calendarTypePojo.setEventtypeid(Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID")));
                            } catch (Exception e) {
                                calendarTypePojo.setEventtypeid(0);
                            }

                            calendarTypePojo.setEventtypename(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Name"));
                            calendarTypePojo.setEventtypeDescription(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Description"));


                            String imagefile = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").substring(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").lastIndexOf("/") + 1, jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").length());

                            try {
                                downloadfile(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image"), pref.getString("calender", ""), imagefile);
                            } catch (Exception e) {
                            }
                            String imagename = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image");
                            String local_path = pref.getString("calender", "") + "/" + imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());
                            calendarTypePojo.setEventtypeimage(local_path);
                            obj.UpdateCalenderType(calendarTypePojo);
                        }


                    }



                    //student info



                    SqliteOpenHelperDemo obj = new SqliteOpenHelperDemo(getApplicationContext());


                    ContentValues cv = new ContentValues();
                    try {
                        cv.put("StudentID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ID")));
                    }catch(Exception e){
                        cv.put("StudentID",0);
                    }
                    cv.put("AdmissionNumber", jObj.getJSONObject("studentInfo").getString("AdmissionNumber"));
                    cv.put("DOA", jObj.getJSONObject("studentInfo").getString("AdmissionDate"));

                    String subarray = jObj.getJSONObject("studentInfo").getJSONArray("Room").toString();
                    subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                    subarray = subarray.replace("\"", "");
                    if(subarray.equalsIgnoreCase("")) {
                        cv.put("Room", "0");
                    }else {
                        cv.put("Room", subarray);
                    }
                    cv.put("FirstName", jObj.getJSONObject("studentInfo").getString("FirstName"));
                    cv.put("LastName", jObj.getJSONObject("studentInfo").getString("LastName"));
                    cv.put("DOB", jObj.getJSONObject("studentInfo").getString("DateOfBirth"));
                    cv.put("Gender", jObj.getJSONObject("studentInfo").getString("Gender"));
                    cv.put("Phone_1", jObj.getJSONObject("studentInfo").getString("Phone"));
                    cv.put("Email", jObj.getJSONObject("studentInfo").getString("Email"));
                    cv.put("FatherName", jObj.getJSONObject("studentInfo").getString("FatherName"));
                    cv.put("MotherName", jObj.getJSONObject("studentInfo").getString("MotherName"));
                    cv.put("GuardianMobileNumber", jObj.getJSONObject("studentInfo").getString("GuardianPhone"));
                    try
                    {
                        String s=jObj.getJSONObject("studentInfo").getString("RollNumber");
                        if(Integer.parseInt(s)>0)
                        {
                            cv.put("RollNo", jObj.getJSONObject("studentInfo").getString("RollNumber"));

                        }
                    }
                    catch (Exception e)
                    {
                        cv.put("RollNo", "0");

                    }
                    try{
                        cv.put("ClassID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ClassId")));}
                    catch(Exception e){}
                    try{
                        cv.put("BatchID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("Batch")));}
                    catch(Exception e){}
                    try{
                        cv.put("SchoolID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("SchoolID")));}
                    catch(Exception e){}
                    //cv.put("PhotoFilename", jObj.getJSONObject("studentInfo").getString("ProfileImage"));
                    cv.put("StudentLoginID", jObj.getJSONObject("studentInfo").getString("StudentLoginID"));




                    SqliteOpen pojo = new SqliteOpen();

                    //for school table
                    if(jObj.getJSONObject("schoolInfo").getString("ID").equalsIgnoreCase("")) {
                        pojo.setSchoolid("0");
                    }else{
                        pojo.setSchoolid(jObj.getJSONObject("schoolInfo").getString("ID"));
                    }

                    pojo.setSchoolName(jObj.getJSONObject("schoolInfo").getString("Name"));
                    pojo.setAcronym(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"));

                    String acrid = jObj.getJSONObject("studentInfo").getString("ID");
                    String bac = jObj.getJSONObject("schoolInfo").getString("BackgroundImage");
                    String schoolfolder = jObj.getJSONObject("schoolInfo").getString("SchoolAcronym");
                    String namefolder = jObj.getJSONObject("studentInfo").getString("FirstName");
                    String stdid = jObj.getJSONObject("studentInfo").getString("ID");

                    File file1 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Assets");
                    file1.mkdirs();

                    // File file2 = new File(Environment.getExternalStorageDirectory(), schoolfolder+"/Users");
                    //   file2.mkdirs();
                    File file0 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid);
                    file0.mkdirs();
                    File file3 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Evaluation");
                    File file4 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material");
                    File file5 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Notes");
                    File file6 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Calendar");
                    File file7 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material" + "/Academic");
                    File file8 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material" + "/Reference");

                    edit.putString("academic", file7.getAbsolutePath());
                    edit.putString("reference", file8.getAbsolutePath());
                    edit.putString("notes", file5.getAbsolutePath());

                    edit.putString("calender",file6.getAbsolutePath());
                    edit.commit();

                    file3.mkdirs();
                    file4.mkdirs();
                    file5.mkdirs();
                    file6.mkdirs();
                    file7.mkdirs();
                    file8.mkdirs();

                    // createFolder(schoolfolder,jObj.getJSONObject("studentInfo").getString("FirstName"),jObj.getJSONObject("schoolInfo").getString("Logo"),jObj.getJSONObject("studentInfo").getString("ProfileImage"),bac);
                    String schoollogo = jObj.getJSONObject("schoolInfo").getString("Logo");
                    String stdprofile = jObj.getJSONObject("studentInfo").getString("ProfileImage");


                    String stdimagedb = file1.getAbsolutePath() + "/" + stdprofile.substring(stdprofile.lastIndexOf("/") + 1, stdprofile.length());
                    String logoimagedb = file1.getAbsolutePath() + "/" + "logo_" + schoollogo.substring(schoollogo.lastIndexOf("/") + 1, schoollogo.length());
                    String backimagedb = file1.getAbsolutePath() + "/" + "background_" + bac.substring(bac.lastIndexOf("/") + 1, bac.length());
                    cv.put("PhotoFilename", stdimagedb);
                    pojo.setSchoolLogo(logoimagedb);
                    pojo.setBackgroundimage(backimagedb);
                    try {
                        DownloadTask1(schoollogo, file1.getAbsolutePath());
                    } catch (Exception e) {
                    }
                    try {
                        DownloadTask2(bac, file1.getAbsolutePath());
                    } catch (Exception e) {
                    }
                    try {
                        DownloadTask(stdprofile, file1.getAbsolutePath(), stdimagedb, logoimagedb, backimagedb);
                    } catch (Exception e) {

                    }

                    //for classes update
                    if(jObj.getJSONObject("Classes").getString("ID").equalsIgnoreCase("")){
                        pojo.setClassID("0");
                    }else {
                        pojo.setClassID(jObj.getJSONObject("Classes").getString("ID"));
                    }

                    pojo.setClassName(jObj.getJSONObject("Classes").getString("ClassName"));
                    pojo.setSectionName(jObj.getJSONObject("Classes").getString("Section"));
                    pojo.setGrade(jObj.getJSONObject("Classes").getString("Grade"));

                    if(jObj.getJSONObject("Classes").getString("SchoolID").equalsIgnoreCase("")) {
                        pojo.setSchoolID("0");
                    }else{
                        pojo.setSchoolID(jObj.getJSONObject("Classes").getString("SchoolID"));
                    }

                    if(jObj.getJSONObject("Classes").getString("Department").equalsIgnoreCase("")){
                        pojo.setDepartmentID("0");
                    }else {
                        pojo.setDepartmentID(jObj.getJSONObject("Classes").getString("Department"));
                    }

                    edit.putString("wifissid",jObj.getJSONObject("Classes").getString("InternetSSID"));
                    edit.putString("wifipassword",jObj.getJSONObject("Classes").getString("InternetPassword"));
                    edit.putString("classwifissid",jObj.getJSONObject("Classes").getString("InternetSSID"));
                    edit.putString("classwifipassword",jObj.getJSONObject("Classes").getString("InternetPassword"));
                    edit.commit();

                    pojo.setInternetSSID(jObj.getJSONObject("Classes").getString("InternetSSID"));
                    pojo.setInternetPassword(jObj.getJSONObject("Classes").getString("InternetPassword"));
                    pojo.setInternetType(jObj.getJSONObject("Classes").getString("InternetType"));
                    pojo.setPushName(jObj.getJSONObject("Classes").getString("IpushName"));




////////////for STudent
                    Cursor scur=null;
                    try{
                        scur=obj.retriveIsStudentId(Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ID")));
                    }catch(Exception e){
                        scur=obj.retriveIsStudentId(0);
                    }
                    if(scur.getCount()>0){
                        obj.UpdateStudent(cv,jObj.getJSONObject("studentInfo").getString("ID"));
                    }else{
                        obj.InsertStudent(cv);
                    }
                    scur.close();

////for School
                    Cursor schcur=null;
                    try{
                        schcur=obj.retriveIsSchool(Integer.parseInt(jObj.getJSONObject("schoolInfo").getString("ID")));
                    }catch(Exception e){
                        schcur=obj.retriveIsSchool(0);
                    }
                    if(schcur.getCount()>0){
                        obj.UpdateSchool(pojo);
                    }else{
                        obj.InsertSchool(pojo);
                    }
                    schcur.close();

/////for Class
                    Cursor clascur=null;
                    Log.e("classid",jObj.getJSONObject("Classes").getString("ID"));
                    try {
                        clascur = obj.retriveIsClass(Integer.parseInt(jObj.getJSONObject("Classes").getString("ID")));
                    }catch(Exception e){
                        clascur = obj.retriveIsClass(0);
                    }


                    if(clascur.getCount()>0){
                        obj.UpdateClass(pojo);
                    }else{
                        obj.InsertClass(pojo);
                    }
                    clascur.close();




                    //for staffInfo Update

                    ArrayList<Staffpojo> stafflist = new ArrayList<Staffpojo>();
                    for (int i = 0; i < jObj.getJSONArray("staffInfo").length(); i++) {
                        Staffpojo staffpojo = new Staffpojo();

                        Cursor stfcur=null;
                        try{
                            stfcur=obj.retriveIsStaff(Integer.parseInt(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID")));
                        }catch(Exception e){
                            stfcur=obj.retriveIsStaff(0);
                        }
                        if(stfcur.getCount()>0) {


                            if(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID").equalsIgnoreCase("")){
                                staffpojo.setStaffid("0");
                            }else{
                                staffpojo.setStaffid(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID"));
                            }

                            staffpojo.setDOJ(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfJoining"));
                            staffpojo.setFirstName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FirstName"));
                            staffpojo.setLastName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("LastName"));
                            staffpojo.setGender(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Gender"));
                            staffpojo.setDOB(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfBirth"));
                            staffpojo.setMaritalStatusID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MaritalStatus"));
                            staffpojo.setSpouseName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("SpouseName"));
                            staffpojo.setFatherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FatherName"));
                            staffpojo.setMotherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MotherName"));
                            staffpojo.setPhone(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("PhoneNumber"));
                            staffpojo.setEmail(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Email"));
                            staffpojo.setPhotoFilename(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ProfileImage"));
                            if(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory").equalsIgnoreCase("")){
                                staffpojo.setStaffCategoryID("0");
                            }else{
                                staffpojo.setStaffCategoryID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory"));
                            }

                            stafflist.add(staffpojo);

                            obj.UpdateStaff(stafflist);
                        }

                        if(stfcur.getCount()>0==false) {



                            if(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID").equalsIgnoreCase("")){
                                staffpojo.setStaffid("0");
                            }else{
                                staffpojo.setStaffid(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID"));
                            }

                            staffpojo.setDOJ(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfJoining"));
                            staffpojo.setFirstName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FirstName"));
                            staffpojo.setLastName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("LastName"));
                            staffpojo.setGender(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Gender"));
                            staffpojo.setDOB(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfBirth"));
                            staffpojo.setMaritalStatusID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MaritalStatus"));
                            staffpojo.setSpouseName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("SpouseName"));
                            staffpojo.setFatherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FatherName"));
                            staffpojo.setMotherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MotherName"));
                            staffpojo.setPhone(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("PhoneNumber"));
                            staffpojo.setEmail(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Email"));
                            staffpojo.setPhotoFilename(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ProfileImage"));
                            if(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory").equalsIgnoreCase("")){
                                staffpojo.setStaffCategoryID("0");
                            }else{
                                staffpojo.setStaffCategoryID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory"));
                            }

                            stafflist.add(staffpojo);
                            obj.InsertStaff(stafflist);
                        }

                        stfcur.close();

                    }








                    //forBatchtable

                    ArrayList<Batchpojo> batchlist = new ArrayList<Batchpojo>();
                    for (int i = 0; i < jObj.getJSONObject("Classes").getJSONArray("Batches").length(); i++) {
                        Batchpojo batchpojo = new Batchpojo();

                        Cursor batcursor=null;
                        try{
                            batcursor=obj.retriveIsBatch(Integer.parseInt(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID")));
                        }catch(Exception e){
                            batcursor=obj.retriveIsBatch(0);
                        }

                        if(batcursor.getCount()>0) {


                            if(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                batchpojo.setBatchId("0");
                            }else{
                                batchpojo.setBatchId(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID"));
                            }

                            if(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator").equalsIgnoreCase("")) {
                                batchpojo.setCoordinatingStaffID("0");
                            }else{
                                batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            }
                            //batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            batchpojo.setAcademicYear(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("AcademicYear"));
                            batchpojo.setBatchclassid(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Class"));
                            batchpojo.setBatchStartDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("StartDate"));
                            batchpojo.setBatchEndDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("EndDate"));
                            //batchpojo.setTimeTable(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchpojo.setSubjects(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Subjects"));

                            String timetable = jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable");
                            String timedb = file0.getAbsolutePath() + "/" + timetable.substring(timetable.lastIndexOf("/") + 1, timetable.length());
                            batchpojo.setTimeTable(timedb);

                            otherTask(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"), file0.getAbsolutePath());

                            //XmltoFolder(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"),jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchlist.add(batchpojo);

                            obj.UpdateBatch(batchlist);
                        }


                        if(batcursor.getCount()>0==false) {


                            if(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                batchpojo.setBatchId("0");
                            }else{
                                batchpojo.setBatchId(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID"));
                            }

                            if(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator").equalsIgnoreCase("")) {
                                batchpojo.setCoordinatingStaffID("0");
                            }else{
                                batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            }
                            //batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            batchpojo.setAcademicYear(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("AcademicYear"));
                            batchpojo.setBatchclassid(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Class"));
                            batchpojo.setBatchStartDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("StartDate"));
                            batchpojo.setBatchEndDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("EndDate"));
                            //batchpojo.setTimeTable(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchpojo.setSubjects(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Subjects"));

                            String timetable = jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable");
                            String timedb = file0.getAbsolutePath() + "/" + timetable.substring(timetable.lastIndexOf("/") + 1, timetable.length());
                            batchpojo.setTimeTable(timedb);

                            otherTask(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"), file0.getAbsolutePath());

                            //XmltoFolder(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"),jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchlist.add(batchpojo);
                            obj.InsertBatch(batchlist);

                        }
                        batcursor.close();
                    }//forloop




                }//200


            }
            catch(Exception e){
                Log.e("Exceptionsql",e.toString());
            }

            return null;}


        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if (dia.isShowing())
                dia.cancel();


            Toast.makeText(getApplicationContext(),"Updated",Toast.LENGTH_LONG).show();
        }

    }




    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue=0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }
        /*float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;*/
       // getWindow().setAttributes(lp);
    }

    void setbackground(ImageView view, String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }






    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("ImageFileeError: ", e.getMessage());
        }


    }


    void DownloadTask1(String urls,String filepath)
    {

        int count;
        try {
            String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            filename="logo_"+filename;
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            //    Log.e("Error: ", e.getMessage());
        }


    }


    void DownloadTask2(String urls,String filepath)
    {

        int count;
        try {
            String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            filename="background_"+filename;
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            //   Log.e("Error: ", e.getMessage());
        }


    }

    public void  DownloadTask(String urls,String filepath,String stdimagedb,String logoimagedb,String backimagedb){


        int count;
        try {
            String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;

                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
        }


    }


    public void otherTask(String urls,String filepath){


        int count;
        try {
            String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());

            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();


            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);
            // Output stream
            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
        }



    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }

}