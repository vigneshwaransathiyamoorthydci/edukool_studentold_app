package messaging;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.dci.edukool.student.R;

import drawboard.MyDialog;

/**
 * Custom list adapter, implementing BaseAdapter
 */
public class SecondListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Item> items;
    boolean[] checkBoxState;
    Map<Integer, Item> map    = new HashMap<Integer, Item>();;

    public SecondListAdapter(Context context, ArrayList<Item> items) {
        this.context = context;
        this.items = items;
        checkBoxState=new boolean[items.size()];
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder=null;

        if (convertView == null) {

                convertView = LayoutInflater.from(context).inflate(R.layout.layout_second_listitem, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.check.setOnCheckedChangeListener(null);
        }

        Item currentItem = (Item) getItem(position);

       viewHolder.check.setTag(position);
        viewHolder.title.setText(currentItem.getTitle());
        viewHolder.content.setText(currentItem.getContent());
        viewHolder.dateof.setText(formatdate(currentItem.getDateof()));
        viewHolder.staffid.setText(""+currentItem.getSenderid());
        viewHolder.subjectid.setText(""+currentItem.getSubjectid());


        viewHolder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(((CheckBox)v).isChecked())
                    checkBoxState[position]=true;
                else
                    checkBoxState[position]=false;
                int posit=(int)v.getTag();
            //    Toast.makeText(context,"position"+posit,Toast.LENGTH_LONG).show();

              Item selecteditem = (Item) getItem(posit);

                if(((CheckBox)v).isChecked()){

                    map.put(posit,selecteditem);
                    checkBoxState[position]=true;
                    ((CommunicationActivity) context).setMap(selecteditem, position);


               }
               else if(!((CheckBox)v).isChecked()){

                    checkBoxState[position]=false;
                    ((CommunicationActivity) context).setMap1(selecteditem,position);

                   // Toast.makeText(context,"not",Toast.LENGTH_LONG).show();
                }

            }
        });



        viewHolder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView title1 = (TextView) v.findViewById(R.id.msg);

                TextView content1 = (TextView) v.findViewById(R.id.content);
                TextView dat1 = (TextView) v.findViewById(R.id.dat);
                showPopup(title1.getText().toString(),content1.getText().toString(),dat1.getText().toString());
            }
        });
        return convertView;
    }

    //ViewHolder inner class
    private class ViewHolder {
        CheckBox check;
        TextView title,content,dateof,staffid,subjectid;
        RelativeLayout rel;

        public ViewHolder(View view) {
            check = (CheckBox) view.findViewById(R.id.check);
            title = (TextView) view.findViewById(R.id.msg);
            content = (TextView) view.findViewById(R.id.content);
            dateof = (TextView) view.findViewById(R.id.dat);
            staffid = (TextView) view.findViewById(R.id.staffid);
            subjectid = (TextView) view.findViewById(R.id.subid);
            rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }
    public String formatdate(String fdate)
    {
        String datetime=null;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat d= new SimpleDateFormat("dd-MMM-yyyy hh:mm aa");
        try {
            Date convertedDate = inputFormat.parse(fdate);
            datetime = d.format(convertedDate);

        }catch (ParseException e)
        {

        }
        return  datetime;


    }

    public void showPopup(String msg,String cont,String dat) {
       /* View popupView = LayoutInflater.from(context).inflate(R.layout.message_popup, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, 550, 300);
        popupWindow.showAtLocation(popupView, Gravity.CENTER,0,0);*/
        final MyDialog dialog=new MyDialog(context);
                dialog.setContentView(R.layout.message_popup);
        TextView datpop=(TextView)dialog.findViewById(R.id.dat_pop);

        TextView msgpop=(TextView)dialog.findViewById(R.id.msg_pop);
        TextView contpop=(TextView)dialog.findViewById(R.id.content_pop);

        datpop.setText(dat);

        msgpop.setText(msg);
        contpop.setText(cont);

        Button btnDismiss = (Button) dialog.findViewById(R.id.close);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
dialog.show();
        //popupWindow.showAsDropDown(popupView, 0, 0);
    }

}
