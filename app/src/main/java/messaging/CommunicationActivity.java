package messaging;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


import Utils.Service;
import connection.Acknowled;
import helper.RoundedImageView;

import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

/**
 * Created by kirubakaranj on 5/16/2017.
 */

public class CommunicationActivity extends Activity {


    ListView itemsListView;
    ListView secondlistview;
    ImageView back;
    Button but_acknowledge;
    TextView studentname, classname,dat;
    RoundedImageView profileimage;
  SqliteOpenHelperDemo obj;
    SecondListAdapter secondadapter;
    SharedPreferences pref;
    ArrayList<Item> secondlist = new ArrayList<>();
   String clas,sec,ac,stdname,profile;
    int batchid;
    Map<Integer, Item>  map= new HashMap<Integer, Item>();;
    Map<Integer, String>  SubidMap= new HashMap<Integer, String>();
    ArrayList<Item> list;
    RadioButton all,acknowledged;
    Boolean ack=false;
    String ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.communication);
        itemsListView  = (ListView)findViewById(R.id.list_view_items);
        all=(RadioButton)findViewById(R.id.radioButton1);
        acknowledged=(RadioButton)findViewById(R.id.radioButton2);
        but_acknowledge=(Button)findViewById(R.id.acknowledge);

        studentname=(TextView)findViewById(R.id.stdnam);
        dat=(TextView)findViewById(R.id.dat);
        classname=(TextView)findViewById(R.id.class_name);
        back=(ImageView)findViewById(R.id.back);

        profileimage=(RoundedImageView)findViewById(R.id.profileimage);

        map.clear();
        ack=false;
        if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }
        pref = getSharedPreferences("student", MODE_PRIVATE);
        ip=pref.getString("ip","");

        obj=new SqliteOpenHelperDemo(this);

        setbackground(profileimage, "");



      //  staffclass.setText(pref.getString("classname",""));
       // classname.setText("CLASS : "+pref.getString("classname",""));


        Cursor batcursor = obj.retriveClassName();
        while (batcursor.moveToNext()) {
            clas = batcursor.getString(batcursor.getColumnIndex("ClassName"));
            sec = batcursor.getString(batcursor.getColumnIndex("SectionName"));

        }

        Cursor tc = obj.retrive("tblStudent");
        while (tc.moveToNext()) {
            batchid = Integer.parseInt(tc.getString(tc.getColumnIndex("BatchID")));
            stdname = tc.getString(tc.getColumnIndex("FirstName"));
            profile = tc.getString(tc.getColumnIndex("PhotoFilename"));
        }


        Cursor bc = obj.retrivevalue(batchid);
        while (bc.moveToNext()) {
            ac = bc.getString(bc.getColumnIndex("AcademicYear"));
        }

        classname.setText("CLASS :"+clas+" "+sec+" "+ac);
        studentname.setText(stdname.toUpperCase());
        dat.setText("DATE : "+headerdate());

        if (profile != null) setbackground(profileimage, profile);

        ////firstlistview
        list = new ArrayList<>();

        Cursor subcursor=obj.retrive("tblSubject");

        while(subcursor.moveToNext()){

                Item item=new Item();
                item.setSubjectid(subcursor.getInt(subcursor.getColumnIndex("SubjectID")));
                item.setSubjectname(subcursor.getString(subcursor.getColumnIndex("SubjectName")));
                list.add(item);
        }




        CustomListAdapter adapter = new CustomListAdapter(this,list);
        itemsListView.setAdapter(adapter);

        itemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView subid = (TextView) view.findViewById(R.id.subjectid);
            //    Toast.makeText(getApplicationContext(), "click" + subid.getText().toString(), Toast.LENGTH_LONG).show();
                secondlist = new ArrayList<>();
                secondlist.clear();
                SubidMap.clear();
                SubidMap.put(0,subid.getText().toString());

                Cursor recur=null;
                if(!ack) {
                     recur = obj.retriveMessage(Integer.parseInt(subid.getText().toString()));
                }else if(ack){
                     recur = obj.retriveIssent(Integer.parseInt(subid.getText().toString()));
                }

                Log.d("count", recur.getCount() + "");



                if(recur.getCount()>0){
                recur.moveToLast();
                Item ite = new Item();
                ite.setMsgid((recur.getInt(recur.getColumnIndex("MsgID"))));
                ite.setSubjectid((recur.getInt(recur.getColumnIndex("SubjectID"))));
                ite.setSenderid((recur.getInt(recur.getColumnIndex("Sender"))));
                ite.setReceiverid((recur.getInt(recur.getColumnIndex("Receiver"))));
                ite.setTitle((recur.getString(recur.getColumnIndex("Title"))));
                ite.setContent((recur.getString(recur.getColumnIndex("Content"))));
                ite.setDateof((recur.getString(recur.getColumnIndex("DateOfCommunication"))));
                secondlist.add(ite);

                while (recur.moveToPrevious()) {
                    Item items = new Item();
                    items.setMsgid((recur.getInt(recur.getColumnIndex("MsgID"))));
                    items.setSubjectid((recur.getInt(recur.getColumnIndex("SubjectID"))));
                    items.setSenderid((recur.getInt(recur.getColumnIndex("Sender"))));
                    items.setReceiverid((recur.getInt(recur.getColumnIndex("Receiver"))));
                    items.setTitle((recur.getString(recur.getColumnIndex("Title"))));
                    items.setContent((recur.getString(recur.getColumnIndex("Content"))));
                    items.setDateof((recur.getString(recur.getColumnIndex("DateOfCommunication"))));
                    secondlist.add(items);
                }
                //  secondadapter.notifyDataSetChanged();
                secondadapter = new SecondListAdapter(CommunicationActivity.this, secondlist);

                secondlistview.setAdapter(secondadapter);
            }else{
                    secondadapter = new SecondListAdapter(CommunicationActivity.this, secondlist);

                    secondlistview.setAdapter(secondadapter);
                }





            }
        });









        //second listview
         secondlistview  = (ListView)findViewById(R.id.listsecond);
    Log.d("firstsubid",""+list.get(0).getSubjectid());

        Cursor c=obj.retriveMessage(list.get(0).getSubjectid());
        if(c.getCount()>0) {
            c.moveToLast();
            Item ite = new Item();
            ite.setMsgid((c.getInt(c.getColumnIndex("MsgID"))));
            ite.setSubjectid((c.getInt(c.getColumnIndex("SubjectID"))));
            ite.setSenderid((c.getInt(c.getColumnIndex("Sender"))));
            ite.setReceiverid((c.getInt(c.getColumnIndex("Receiver"))));
            ite.setTitle((c.getString(c.getColumnIndex("Title"))));
            ite.setContent((c.getString(c.getColumnIndex("Content"))));
            ite.setDateof((c.getString(c.getColumnIndex("DateOfCommunication"))));
            secondlist.add(ite);

            while (c.moveToPrevious()) {
                Item items = new Item();
                items.setMsgid((c.getInt(c.getColumnIndex("MsgID"))));
                items.setSubjectid((c.getInt(c.getColumnIndex("SubjectID"))));
                items.setSenderid((c.getInt(c.getColumnIndex("Sender"))));
                items.setReceiverid((c.getInt(c.getColumnIndex("Receiver"))));
                items.setTitle((c.getString(c.getColumnIndex("Title"))));
                items.setContent((c.getString(c.getColumnIndex("Content"))));
                items.setDateof((c.getString(c.getColumnIndex("DateOfCommunication"))));
                secondlist.add(items);
            }
            c.close();

            secondadapter = new SecondListAdapter(this, secondlist);

            secondlistview.setAdapter(secondadapter);

        }


        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             ack=false;
                SelectAll();
            }
        });

        acknowledged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            ack=true;
                SelectAck();
            }
        });
        but_acknowledge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

       if(map.size()!=0) {
           for (Map.Entry<Integer, Item> entry : map.entrySet()) {
           sendAcknowledge(entry.getValue().getMsgid(), entry.getValue().getReceiverid(), entry.getValue().getSubjectid());
           Log.d("mapvalue", entry.getValue().getMsgid() + "");
        //  Toast.makeText(getApplicationContext(),"value"+","+entry.getValue().getTitle(),Toast.LENGTH_LONG).show();
             }
       }else{
           Toast.makeText(getApplicationContext(),"Select atlest one",Toast.LENGTH_LONG).show();
       }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               CommunicationActivity.this.finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }
       /* float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
    }

    public void setMap(Item d , int position){
        // Toast.makeText(getApplicationContext(),"hai"+i,Toast.LENGTH_LONG).show();

        //this.map.remove(
        this.map.remove(position);
        this.map.put(position,d);
        String id="";

        for(Map.Entry<Integer, Item> entry: this.map.entrySet()) {
           // Toast.makeText(getApplicationContext(),"value"+","+entry.getValue().getTitle(),Toast.LENGTH_LONG).show();
        }

    }
    public void setMap1( Item d ,int position){
        // Toast.makeText(getApplicationContext(),"hai"+i,Toast.LENGTH_LONG).show();

        //this.map.remove(
        this.map.remove(position);
        for(Map.Entry<Integer, Item> entry: this.map.entrySet()) {
        //    Toast.makeText(getApplicationContext(),"value"+","+entry.getValue().getTitle(),Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        // your code.
    }
  public void SelectAll(){
      secondlist.clear();
      Cursor c;

      if(SubidMap.get(0)==null){
          c=obj.retriveMessage(list.get(0).getSubjectid());
      }else{
          c=obj.retriveMessage(Integer.parseInt(SubidMap.get(0)));
      }


      if(c.getCount()>0) {
          c.moveToLast();
          Item ite = new Item();
          ite.setMsgid((c.getInt(c.getColumnIndex("MsgID"))));
          ite.setSubjectid((c.getInt(c.getColumnIndex("SubjectID"))));
          ite.setSenderid((c.getInt(c.getColumnIndex("Sender"))));
          ite.setReceiverid((c.getInt(c.getColumnIndex("Receiver"))));
          ite.setTitle((c.getString(c.getColumnIndex("Title"))));
          ite.setContent((c.getString(c.getColumnIndex("Content"))));
          ite.setDateof((c.getString(c.getColumnIndex("DateOfCommunication"))));
          secondlist.add(ite);

          while (c.moveToPrevious()) {
              Item items = new Item();
              items.setMsgid((c.getInt(c.getColumnIndex("MsgID"))));
              items.setSubjectid((c.getInt(c.getColumnIndex("SubjectID"))));
              items.setSenderid((c.getInt(c.getColumnIndex("Sender"))));
              items.setReceiverid((c.getInt(c.getColumnIndex("Receiver"))));
              items.setTitle((c.getString(c.getColumnIndex("Title"))));
              items.setContent((c.getString(c.getColumnIndex("Content"))));
              items.setDateof((c.getString(c.getColumnIndex("DateOfCommunication"))));
              secondlist.add(items);
          }
          c.close();

          secondadapter = new SecondListAdapter(this, secondlist);

          secondlistview.setAdapter(secondadapter);

      }
  }

  public void SelectAck(){
      secondlist.clear();
      Cursor recur;

      if(SubidMap.get(0)==null){
          recur = obj.retriveIssent(  list.get(0).getSubjectid());
      }else{
          recur = obj.retriveIssent(Integer.parseInt(SubidMap.get(0)));
      }



      if(recur.getCount()>0){
          recur.moveToLast();
          Item ite = new Item();
          ite.setMsgid((recur.getInt(recur.getColumnIndex("MsgID"))));
          ite.setSubjectid((recur.getInt(recur.getColumnIndex("SubjectID"))));
          ite.setSenderid((recur.getInt(recur.getColumnIndex("Sender"))));
          ite.setReceiverid((recur.getInt(recur.getColumnIndex("Receiver"))));
          ite.setTitle((recur.getString(recur.getColumnIndex("Title"))));
          ite.setContent((recur.getString(recur.getColumnIndex("Content"))));
          ite.setDateof((recur.getString(recur.getColumnIndex("DateOfCommunication"))));
          secondlist.add(ite);

          while (recur.moveToPrevious()) {
              Item items = new Item();
              items.setMsgid((recur.getInt(recur.getColumnIndex("MsgID"))));
              items.setSubjectid((recur.getInt(recur.getColumnIndex("SubjectID"))));
              items.setSenderid((recur.getInt(recur.getColumnIndex("Sender"))));
              items.setReceiverid((recur.getInt(recur.getColumnIndex("Receiver"))));
              items.setTitle((recur.getString(recur.getColumnIndex("Title"))));
              items.setContent((recur.getString(recur.getColumnIndex("Content"))));
              items.setDateof((recur.getString(recur.getColumnIndex("DateOfCommunication"))));
              secondlist.add(items);
          }
          //  secondadapter.notifyDataSetChanged();
          secondadapter = new SecondListAdapter(CommunicationActivity.this, secondlist);

          secondlistview.setAdapter(secondadapter);
      }else{
          secondadapter = new SecondListAdapter(CommunicationActivity.this, secondlist);

          secondlistview.setAdapter(secondadapter);
      }



  }
    public void sendAcknowledge(int msgid,int recid,int subid) {
        try {

        final JSONObject sendmessage = new JSONObject();
        sendmessage.put("Action", "ACK");
        sendmessage.put("MsgID", ""+msgid);
        sendmessage.put("Status", "3");
        sendmessage.put("ReceiverID", ""+recid);
        sendmessage.put("SubjectID", ""+subid);
        sendmessage.put("IPAddress", ip);
            Log.e("ip",ip);

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {


                Acknowled ack = new Acknowled(new Acknowled.OnMessageReceived() {
                    @Override

                    public void messageReceived(String message) {


                        try {


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }, pref, "ACK@"+sendmessage.toString());
                ack.run();
                return null;
            }
        }.execute();
    }catch(Exception e){}


    }

    void setbackground(RoundedImageView view, String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }
    public static String headerdate()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }

}
