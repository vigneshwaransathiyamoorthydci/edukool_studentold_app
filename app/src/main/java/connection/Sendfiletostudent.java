package connection;

import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by iyyapparajr on 4/17/2017.
 */
public class Sendfiletostudent implements Runnable {

Socket sendsocket;

Sendfiletostudent(Socket soc)

{
    sendsocket=soc;
}

    @Override
    public void run() {


            File file = new File(
                    Environment.getExternalStorageDirectory(),
                    "signature.png");

            byte[] bytes = new byte[(int) file.length()];
            BufferedInputStream bis;
            try {
                bis = new BufferedInputStream(new FileInputStream(file));

                DataInputStream dis = new DataInputStream(bis);
                dis.readFully(bytes, 0, bytes.length);
                OutputStream os = sendsocket.getOutputStream();

                //Sending file name and file size to the server
                DataOutputStream dos = new DataOutputStream(os);
                dos.writeUTF(file.getName()
                );
                dos.writeLong(bytes.length);

                dos.write(bytes, 0, bytes.length);

                dos.flush();

                //Sending file data to the server
                os.write(bytes, 0, bytes.length);
                os.flush();

                //Closing socket
                os.close();
                dos.close();


                final String sentMsg = "File sent to: " + sendsocket.getInetAddress();
                sendsocket.close();


            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


}
