package helper;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by iyyapparajr on 4/4/2017.
 */
public class Student implements Serializable {

    String Studentname;
    String studentipaddress;
    Bitmap Studentbitmap;
    byte[] bytes;

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getStudentname() {
        return Studentname;
    }

    public void setStudentname(String studentname) {
        Studentname = studentname;
    }

    public String getStudentipaddress() {
        return studentipaddress;
    }

    public void setStudentipaddress(String studentipaddress) {
        this.studentipaddress = studentipaddress;
    }

    public Bitmap getStudentbitmap() {
        return Studentbitmap;
    }

    public void setStudentbitmap(Bitmap studentbitmap) {
        Studentbitmap = studentbitmap;
    }
}
