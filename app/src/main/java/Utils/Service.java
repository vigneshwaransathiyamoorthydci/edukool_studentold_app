package Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class Service {
	Context ctx;
	String VIDEOURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=20&playlistId=PLYn3ScEptDEYS1shhxJcAQ_tdEd1KY0LU&key=AIzaSyAtmoryOxB_ZrOHNjiUKY0-q6I64pUhM8E";
	String TRAILERURL = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=10&playlistId=PLYn3ScEptDEZsEcSqD9U-DQJRjRPvLmnr&key=AIzaSyAtmoryOxB_ZrOHNjiUKY0-q6I64pUhM8E";
	static String response;
	JSONObject json;
	static HttpResponse httpResponse;
	public static String responserstring;
	static InputStream inputstream;
	static String json_ar;
	int i = 0;

	public Service(Context ctx) {
		this.ctx = ctx;
	}

	public String getjsonmethodofarray(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}
	public String getCountries_list(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}

	public String getStates_list(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}


	public String getLoginId_check(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));

			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}

	String convertStreamToString(java.io.InputStream is) {
		try {
			return new java.util.Scanner(is).useDelimiter("\\A").next();
		} catch (java.util.NoSuchElementException e) {
			return "";
		}
	}
	public String  getLogin(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			Log.d("vignesh", "http" + convertStreamToString(httpPost.getEntity().getContent()));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {

			return json_ar;

		}
	}


	public String profile_edit_contact_info(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}

	
	public String getUserDetails(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}


	public String skills_details(List<NameValuePair> select, String url)
			throws JSONException {
		try {
			json_ar = null;
			httpResponse = null;
			responserstring = "";
			HttpClient httpClient = new DefaultHttpClient();

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("WSH",
					"U2VjcmV0OkVLQXBwfFBhc3N3b3JkOiMkZWZySHloYTY0Nw==");
			httpPost.setEntity(new UrlEncodedFormEntity(select));
			HttpParams httpParameters = httpPost.getParams();
			int timeoutConnection = 60 * 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 60 * 10000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			inputstream = httpEntity.getContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 500) {
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(inputstream, "iso-8859-1"), 8);
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inputstream.close();
					response = sb.toString();
					Log.e("tage", response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				json_ar = new String(response);
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			} else {
				responserstring = ""
						+ httpResponse.getStatusLine().getStatusCode();
				return json_ar;
			}
		} catch (Exception e) {
			return json_ar;
		}
	}

	public static boolean isAirplaneModeOn(Context context) {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1){
        /* API 17 and above */
			return Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
		} else {
        /* below */
			return Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
		}
	}

	public static void Showalert(Context context)
	{

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("AirplaneMode");
		builder.setMessage("Please disable AirplaneMode.");
		builder.setPositiveButton("OK", null);
		AlertDialog dialog = builder.show();
		TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
		TextView titleView = (TextView)dialog.findViewById(context.getResources().getIdentifier("alertTitle", "id", "android"));
		if (titleView != null) {
			titleView.setGravity(Gravity.CENTER);
		}
		dialog.show();

	}
}
