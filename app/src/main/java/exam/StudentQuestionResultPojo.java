package exam;

/**
 * Created by pratheeba on 5/9/2017.
 */
public class StudentQuestionResultPojo {


    public int getResponseID() {
        return ResponseID;
    }

    public void setResponseID(int responseID) {
        ResponseID = responseID;
    }

    public int getExamResponseID() {
        return ExamResponseID;
    }

    public void setExamResponseID(int examResponseID) {
        ExamResponseID = examResponseID;
    }

    public int getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(int questionID) {
        QuestionID = questionID;
    }

    public String getStudentAnswer() {
        return StudentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        StudentAnswer = studentAnswer;
    }

    public int getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        IsCorrect = isCorrect;
    }

    public int getMarkForAnswer() {
        return MarkForAnswer;
    }

    public void setMarkForAnswer(int markForAnswer) {
        MarkForAnswer = markForAnswer;
    }

    public int getObtainedScore() {
        return ObtainedScore;
    }

    public void setObtainedScore(int obtainedScore) {
        ObtainedScore = obtainedScore;
    }
    int ResponseID;
    int ExamResponseID;

    int QuestionID;
    String StudentAnswer;
    int IsCorrect;
    int MarkForAnswer;
    int ObtainedScore;


    public StudentQuestionResultPojo(int ResponseID, int ExamResponseID, int QuestionID, String StudentAnswer, int IsCorrect, int MarkForAnswer, int ObtainedScore){
        this.ResponseID = ResponseID;
        this.ExamResponseID = ExamResponseID;
        this.QuestionID = QuestionID;
        this.StudentAnswer = StudentAnswer;
        this.IsCorrect = IsCorrect;
        this.MarkForAnswer = MarkForAnswer;
        this.ObtainedScore = ObtainedScore;

    }

    public StudentQuestionResultPojo(){

    }

}
