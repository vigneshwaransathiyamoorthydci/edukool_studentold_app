package exam;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import Utils.Service;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Utils.*;
import Utils.Utils;
import helper.RoundedImageView;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

/**
 * Created by pratheeba on 5/15/2017.
 */
public class SelfEvaluationActivity extends Activity{
    String studentname,profilename,PortalUserID;
    String StudentID;
    String RollNo;
    TextView stdname;
    RoundedImageView profileimage;
    ImageView download_exam;
    ArrayList<NameValuePair> selfasses = new ArrayList<NameValuePair>();
    Utils utils;
    ImageView back;
    private ImageView results,view_questions_list;
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID ;
    String ExamCategory ;
    String ExamCode ;
    int QuestionID;
    String ExamDescription;
    String BatchIDVal ;
    String ExamDurationVal;
    int BatchID ;
    int ExamDuration ;
    int Mark;
    String SubjectIDVal;
    String storagepath;
    String Subject ;
    int ObtainedMark ;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    int SubjectID;
    String ExamTypeIDVal ;
    int NegativeMark;
    ProgressDialog dia;
    int IsCorrect;
    String MarkForAnswer ;
    String ExamType ;
    String ExamDate ;
    int AspectID;
    int ExamTypeID;
    int TopicID;
    String TopicIDVal,Topic,AspectIDVal,Aspect,QuestionNumberVal;
    int CorrectAnswerVal;
    String CorrectAnswer;
    String StudentAnswer;
    String ExamIDVal;
    int QuestionNumber;
    DatabaseHandler  db;
    String question;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.self_evaluation_home_layout);
        utils=new Utils(SelfEvaluationActivity.this);
        download_exam= (ImageView) findViewById(R.id.download_exams);
        results = (ImageView) findViewById(R.id.results);
        view_questions_list= (ImageView) findViewById(R.id.view_questions_list);
        stdname= (TextView)findViewById(R.id.studentname);
        profileimage= (RoundedImageView)findViewById(R.id.profileimage);
        back= (ImageView)findViewById(R.id.back);
        db=new DatabaseHandler(this);
        SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());
        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext()){
            studentname= tc.getString(tc.getColumnIndex("FirstName"));
            StudentID = tc.getString(tc.getColumnIndex("StudentID"));
            RollNo       = tc.getString(tc.getColumnIndex("RollNo"));
            profilename= tc.getString(tc.getColumnIndex("PhotoFilename"));
            PortalUserID = tc.getString(tc.getColumnIndex("StudentLoginID"));
        }
        pref=getSharedPreferences("student", MODE_PRIVATE);
        edit=pref.edit();
        stdname.setText(studentname.toUpperCase());
        if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }
        if(profilename!=null)setbackground(profileimage,profilename);
        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SelfEvaluationActivity.this, SelfEvaluationTotalExamList.class);
                    intent.putExtra("RecentExamID", ExamID);
                    intent.putExtra("RecentExamIDValue", "RecentExamID");

                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                   finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        view_questions_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SelfEvaluationActivity.this, StudentSelfAssesmentList.class);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        download_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login_str="UserName:"+PortalUserID+"|StudentId:"+StudentID+"|Function:DownloadSelfEvaluation";
                // String login_str="";
                selfasses.clear();
                byte[] data ;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if(utils.hasConnection()) {
                        selfasses.clear();
                        selfasses.add(new BasicNameValuePair("WS", base64_register));

                        SelfEvaluationWS load_plan_list = new SelfEvaluationWS(SelfEvaluationActivity.this, selfasses);
                        load_plan_list.execute();
                    }
                    else
                    {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        });

    }


    void setbackground(ImageView view, String filepath)
    {
        try
        {


        File imgFile=new File(filepath);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
            view.setImageBitmap(myBitmap);

        }
        }
        catch (Exception e)
        {

        }
    }


    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onResume();
    }

    class SelfEvaluationWS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public SelfEvaluationWS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(SelfEvaluationActivity.this);
           dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service  sr = new Service(SelfEvaluationActivity.this);
                jsonResponseString = sr.getLogin(selfasses,
                        Url.base_url);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(final String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {



                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success")) {
                    new  AsyncTask<Void, Void,Void>()
                    {


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dia=new ProgressDialog(SelfEvaluationActivity.this);
                            dia.setMessage("DOWNLOADING");
                            dia.setCancelable(false);
                            dia.show();
                        }

                        @Override
                        protected Void doInBackground(Void... params) {

                            try
                            {


                                Log.e("examdetails,","details");

                                JSONArray Exam_arr=jObj.getJSONObject("examDetails").getJSONArray("exam");

                                for (int i = 0; i < Exam_arr.length(); i++) {
                                    JSONObject objexam=Exam_arr.getJSONObject(i);
                                    ExamIDVal = objexam.getString("ExamID");

                                    ExamID = Integer.parseInt(ExamIDVal);
                                    ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                                    ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                                    ExamCategory = objexam.getString("ExamCategory");
                                    ExamCode = objexam.getString("ExamCode");
                                    ExamDescription = objexam.getString("ExamDescription");
                                    BatchIDVal = objexam.getString("BatchID");
                                    ExamDurationVal = objexam.getString("ExamDuration");
                                    try {
                                        BatchID = Integer.parseInt(BatchIDVal);
                                    }
                                    catch (Exception e){
                                        BatchID = 0;
                                        e.printStackTrace();
                                    }
                                    ExamDuration = Integer.parseInt(ExamDurationVal);

                                    SubjectIDVal = objexam.getString("SubjectID");
                                    Subject = objexam.getString("Subject");


                                    try {
                                        SubjectID = Integer.parseInt(SubjectIDVal);
                                    }
                                    catch (Exception e){
                                        SubjectID = 0;
                                        e.printStackTrace();
                                    }
                                    ExamTypeIDVal = objexam.getString("ExamTypeID");

                                    ExamType = objexam.getString("ExamType");

                                    // ExamDate = objexam.getString("ExamDate");


                                    try {
                                        ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                                    }
                                    catch (Exception e){
                                        ExamTypeID = 0;
                                        e.printStackTrace();
                                    }
                   /* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*/
                                    // String Questions = objexam.getString("Questions");

                                    int ExamSequence =0;
                                    int SchoolID =0;
                                    int IsResultPublished =0;
                                    int ExamShelfID =0;
                                    int ClassID =0;
                                    int TimeTaken = 0;
                                    int TotalScore =0;
                                    String DateAttended ="01/06/2017 10:00:00";

                                    boolean isExamIdexist = db.CheckIsIDAlreadyInDBorNot(ExamIDVal);
                                    if(isExamIdexist){

                                        db.downloadUpdateExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                                BatchID, IsResultPublished,
                                                ExamShelfID,
                                                TimeTaken,
                                                DateAttended,
                                                TotalScore));

                                        System.out.println("isExamIdexist"+ExamIDVal+isExamIdexist);
                                    }
                                    else{
                                        System.out.println("isExamIdexist"+ExamIDVal+isExamIdexist);
                                        db.addExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                                BatchID, IsResultPublished,
                                                ExamShelfID,
                                                TimeTaken,
                                                DateAttended,
                                                TotalScore));

                                    }

                                    List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

                                    JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                                    QuizWrapper newItemObject = null;

                                    for(int j = 0; j < jsonArrayquestion.length(); j++){
                                        JSONObject jsonChildNode = null;
                                        try {
                                            jsonChildNode = jsonArrayquestion.getJSONObject(j);
                                            String QuestionIDval = jsonChildNode.getString("QuestionID");
                                            QuestionID = Integer.parseInt(QuestionIDval);
                                            TopicIDVal = jsonChildNode.getString("TopicID");
                                            if(TopicIDVal.equalsIgnoreCase("")){
                                                TopicID=1;
                                            }
                                            else{
                                                TopicID = Integer.parseInt(TopicIDVal);

                                            }

                                            // int TopicID=1;
                                            Topic = jsonChildNode.getString("Topic");
                                            AspectIDVal = jsonChildNode.getString("AspectID");
                                            if(AspectIDVal.equalsIgnoreCase("")){
                                                AspectID=1;
                                            }
                                            else{
                                                AspectID = Integer.parseInt(AspectIDVal);

                                            }
                                            Aspect = jsonChildNode.getString("Aspect");
                                            QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                                            QuestionNumber = Integer.parseInt(QuestionNumberVal);
                                            question= jsonChildNode.getString("Question");
                                            JSONArray options = jsonChildNode.getJSONArray("Options");
                                            CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                                            CorrectAnswerVal = 1;
                                            MarkVal = jsonChildNode.getString("Mark");
                                            Mark = Integer.parseInt(MarkVal);

                                            NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                                            NegativeMark = Integer.parseInt(NegativeMarkVal);
                                            String Created_on = "01/06/2017 10:00:00";
                                            String ModifiedOn = "01/06/2017 10:00:00";
                                            StudentAnswer ="";
                                            newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal,NegativeMark,Mark);
                                            String Evaluation = pref.getString("Evaluation", "0");
                                            File username=new File(Evaluation+"/"+ExamID);
                                            username.mkdir();
                                            if(question.contains("img")){
                                                String questionImg = testX(question,username.getAbsolutePath());
                                                question=questionImg;
                                            }

                                            for(int z = 0; z < options.length(); z++){
                                                String[] s = new String[options.length()];
                                                String storagepath = testX(options.get(z).toString(),username.getAbsolutePath());
                                                if(storagepath.contains(Evaluation)){
                                                    options.put(z,storagepath);
                                                }
                                            }
                                            if(isExamIdexist){

                                                if(options.length()>3){
                                                    db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(), CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }

                                                else if(options.length()>2){
                                                    db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "", CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }

                                                else if(options.length()>1){
                                                    db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }


                                                System.out.println("isExamIdexist" + ExamIDVal + isExamIdexist);
                                            }
                                            else{
                                                System.out.println("isExamIdexist" + ExamIDVal + isExamIdexist);
                                                if(options.length()>3){
                                                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(),CorrectAnswer , Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }

                                                else if(options.length()>2){
                                                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }

                                                else if(options.length()>1){
                                                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "",CorrectAnswer , Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }

                                            }

                                            List<ExamDetails> examdetailsList = db.getAllExamsDetails();
                                            List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                                            Calendar calendar = Calendar.getInstance();
                                            SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                            String strDate = mdformat.format(calendar.getTime());

                                            for (ExamDetails cn : examdetailsList) {
                                                String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                                                // Writing Contacts to log
                                                Log.d("Exam2: ", log);
                                            }

                                            for (QuestionDetails cn : questiondetailsList) {
                                                String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                                                // Writing Contacts to log
                                                Log.d("Question2: ", log);
                                            }

                                            jsonObject.add(newItemObject);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    // Adding child data

                                    // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                                    //jsonObject.add(newItemObject);

                                }

                            }
                            catch (Exception e)
                            {
                                System.out.println(e.toString() + "zcx");

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if(dia.isShowing())
                                dia.cancel();
                            new validateUserTask(jsonResponse).execute("");

                            Toast.makeText(getApplicationContext(),"Exam Downloaded Successfully", Toast.LENGTH_LONG).show();
                           // finish();

                        }
                    }.execute();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Invalid Credentials",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                if(dia.isShowing())
                    dia.cancel();
                System.out.println(e.toString() + "zcx");
            }

        }
    }




    public String testX(String hjdsf, String path) {
        Pattern p = Pattern.compile("<img.+?src=[\\\\\\\\\\\\\\\"'](.+?)[\\\\\\\\\\\\\\\"'].+?>");
        Matcher m = p.matcher(hjdsf);
        String test="";
        if (m.find()) {

            System.out.println(m.group(1).substring(1));
            String filename = m.group(1).substring(1).substring(0, m.group(1).substring(1).lastIndexOf('.'));
            // prints http://www.01net.com/images/article/mea/150.100.790233.jpg

            String batchfile=m.group(1).substring(1).substring(m.group(1).substring(1).lastIndexOf("/") + 1, m.group(1).substring(1).length());

            downloadfile(m.group(1).substring(1), path, batchfile);
            storagepath= path+"/"+batchfile;

            test = hjdsf.replaceAll(m.group(1).substring(1),storagepath);

            System.out.println(test+"test");
            //pathArray.add(test);
        }


        return test;

    }



    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    private class validateUserTask extends AsyncTask<String, Void, String> {
        String response,statusres;

        validateUserTask(String statusres){
            this.statusres=statusres;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {


                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                  //  String userid = "Function:StudentSyncComplete|StudentID:" + stdid + "|UserName:" + stdlogin_id + "|Type:content";
                    String userid="UserName:"+PortalUserID+"|StudentID:"+StudentID+"|Type:"+"exam"+"|Function:StudentSyncComplete";

                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(SelfEvaluationActivity.this);

                        res = sr.getLogin(postParameters,
                                Url.base_url);
                        // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                        //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                }
                else{
                    Log.e("error","ss");
                }

            }
            catch(Exception e){
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
           // Toast.makeText(getApplicationContext(),"ok"+result,Toast.LENGTH_LONG).show();


        }//close onPostExecute
    }// close validateUserTask


    /*public downLoadExamSync (){

    }*/



    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }


}
