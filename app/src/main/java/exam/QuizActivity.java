package exam;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import connection.Client;
import drawboard.MyDialog;
import helper.RoundedImageView;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

public class QuizActivity extends Activity {
    private TextView quizQuestion,questionoutofall,timer_text,json_creation,todaydate;
    private RadioGroup radioGroup;
    private RadioButton optionOne;
    private RadioButton optionTwo;
    private RadioButton optionThree;
    private RadioButton optionFour;
   /* TextView textopt1;
    TextView textopt2;

    TextView textopt3;

    TextView textopt4;*/

    private int currentQuizQuestion;
    private int quizCount;
    static JSONArray quesList,arrayqueslist;
    private MyDialog dialog;
    private TextView headerText;
    private TextView bodyText,notsaved,stuname;
    private Button closeButton, yesBtn;
    private Button valuate, revisit,submit;
    String StudentAnswer;
    String DateAttended;
    String TotalScoreVal;
    static JSONArray examDetailList;
    private int mQuestionIndex = 0;
    private QuizWrapper firstQuestion;
    JSONArray options;
    private List<QuizWrapper> parsedObject;
    String TopicIDVal,Topic,AspectIDVal,Aspect,QuestionNumberVal;
    int QuestionNumber;
    int TopicID;
    int AspectID;
    String question;
    String TimeTakenVal;
    int Mark;
    Client mClient=null;
    SharedPreferences pref;
    String studentResponse;
    int NegativeMark;
    int tatal_marksVal =0;
    int tatal_maxMark =0;

    int CorrectAnswerVal;
    int IsCorrect;
    String MarkForAnswer ;
    int ObtainedMark ;
   String startDate;
    long endDate;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    long maxTimeInMilliseconds = 3*25*8*1000;// in your case
    String ExamIDVal;
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID ;
    String ExamCategory ;
    String ExamCode ;
    int QuestionID;
    String ExamDescription;
    String BatchIDVal ;
    String ExamDurationVal;
    int BatchID ;
    int ExamDuration ;
    TextView subject,download_txt;
    String SubjectIDVal;
    String storagepath;
    String Subject ;
    TextView tatal_marks;
    int SubjectID;
    String ExamTypeIDVal ;
    int ioptionto;
    String ExamType ;
    SharedPreferences.Editor edit;
    int quesSize;
    String ExamIDResult;
    String ExamDate, studentname,profilename ;
    int StudentID;

    int ExamTypeID;
    TextView stdname,maxmark,negative_mark;
   RoundedImageView profileimage;
    String CorrectAnswer;

    HashMap<Integer,NoAnswerPojo>collapsestring=new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.activity_quiz);
        collapsestring.clear();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ioptionto=0;
        ArrayUtils.selectedAnswersList.clear();
        quizQuestion = (TextView)findViewById(R.id.quiz_question);
        timer_text= (TextView)findViewById(R.id.timer_text);
        questionoutofall= (TextView)findViewById(R.id.questionoutofall);
        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        optionOne = (RadioButton)findViewById(R.id.radio0);
        optionTwo = (RadioButton)findViewById(R.id.radio1);
        optionThree = (RadioButton)findViewById(R.id.radio2);
        json_creation = (TextView)findViewById(R.id.json_creation);
        tatal_marks  = (TextView)findViewById(R.id.tatal_marks);

        optionFour = (RadioButton)findViewById(R.id.radio3);
        /*textopt1= (TextView) findViewById(R.id.optiontext1);
        textopt2= (TextView) findViewById(R.id.optiontext2);

        textopt3= (TextView) findViewById(R.id.optiontext3);

        textopt4= (TextView) findViewById(R.id.optiontext4);
*/

        todaydate= (TextView)findViewById(R.id.todaydate);
        stdname= (TextView)findViewById(R.id.studentname);
        maxmark= (TextView)findViewById(R.id.maxmark);
        negative_mark= (TextView)findViewById(R.id.negative_mark);
        profileimage= (RoundedImageView)findViewById(R.id.profileimage);
        submit= (Button)findViewById(R.id.submit);
        final Button previousButton = (Button)findViewById(R.id.previousquiz);
       final Button nextButton = (Button)findViewById(R.id.nextquiz);
        previousButton.setVisibility(View.INVISIBLE);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
        String strDate = mdformat.format(calendar.getTime());
        submit.setVisibility(View.GONE);
        DatabaseHandler db = new DatabaseHandler(QuizActivity.this);
        pref=getSharedPreferences("student",MODE_PRIVATE);
        edit=pref.edit();


        SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());
        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext()){
            studentname= tc.getString(tc.getColumnIndex("FirstName"));
            profilename= tc.getString(tc.getColumnIndex("PhotoFilename"));
        }

        stdname.setText(studentname.toUpperCase());
        if(profilename!=null)setbackground(profileimage,profilename);


// textView is the TextView view that should display it
        todaydate.setText(strDate);
       // Utils.setTextviewtypeface(2, todaydate);


  /*      new CountDownTimer(300000, 1000) {

            public void onTick(long millisUntilFinished) {

                timer_text.setText(""+(millisUntilFinished/60)+":"+(millisUntilFinished%60));// manage it accordign to you

                //timer_text.setText("seconds remaining: " + millisUntilFinished / 1000);
                Utils.setTextviewtypeface(2, timer_text);
            }

            public void onFinish() {
                finish();


                Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_LONG).show();
                timer_text.setText("done!");
                Utils.setTextviewtypeface(2, timer_text);
                for (int j = 0; j < ArrayUtils.selectedAnswersList.size(); j++) {
                    String selectedAnswersList = ArrayUtils.selectedAnswersList.get(j);
                    System.out.println(selectedAnswersList);
                }

                ArrayUtils.selectedAnswersList.clear();
            }
        }.start();*/

        try {



            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            String Examquestion= bundle.getString("Examquestion");
             int ExamID= bundle.getInt("ExamID");
             ExamIDResult = ExamID+"";

            JSONObject quesObj;
            if(Examquestion.contains("Examquestion")){
                if (Examquestion.length() > 0) {
                   /* quesObj = new JSONObject(Examquestion);
                    quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                    int totalquestions = quesList.length();*/

                   // System.out.println("totalquestions" + totalquestions);
                    parsedObject = returnParsedJsonObjectTeacher(Examquestion);
                    if(parsedObject == null){
                        return;
                    }
                    quizCount = parsedObject.size();
                    questionoutofall.setText("1/"+quizCount);
                    if(quizCount==1){
                        nextButton.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);
                    }
                    else{
                        nextButton.setVisibility(View.VISIBLE);
                    }
                    firstQuestion = parsedObject.get(0);
                    String formatted;
                    if(firstQuestion.getQuestion().startsWith("<p>")&&firstQuestion.getQuestion().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";

                        formatted=firstQuestion.getQuestion().substring(starttag.length(),firstQuestion.getQuestion().length()-endtag.length());
                        formatted=formatted.replace("<br>","");
                    }
                    else
                    {
                        formatted=firstQuestion.getQuestion();
                        formatted=formatted.replace("<br>","");

                    }
                    //quizQuestion.setText("1) "+ Html.fromHtml(formatted));
                    JSONArray possibleAnswers = firstQuestion.getAnswers();

                    String starttag="<p>";
                    String endtag="</p>";
                    String option1=possibleAnswers.get(0).toString();
                    option1=option1.replace("<br>","");
                    String option2=possibleAnswers.get(1).toString();
                    option2=option2.replace("<br>","");



                    String option3=possibleAnswers.get(2).toString();
                    option3=option3.replace("<br>","");

                    String option4=possibleAnswers.get(3).toString();
                    option4=option4.replace("<br>","");

                    if(option2.startsWith("<p>")&&option2.endsWith("</p>"))
                    {
                        option2=option2.substring(starttag.length(),option2.length()-endtag.length());
                    }
                    if(option3.startsWith("<p>")&&option3.endsWith("</p>"))
                    {
                        option3=option3.substring(starttag.length(),option3.length()-endtag.length());

                    }
                    if(option4.startsWith("<p>")&&option4.endsWith("</p>"))
                    {
                        option4=option4.substring(starttag.length(),option4.length()-endtag.length());

                    }
                    if(option1.startsWith("<p>")&&option1.endsWith("</p>"))
                    {
                        option1=option1.substring(starttag.length(),option1.length()-endtag.length());

                    }

                   /* optionOne.setText(Html.fromHtml(option1));
                    optionTwo.setText(Html.fromHtml(option2));
                    optionThree.setText(Html.fromHtml(option3));
                    optionFour.setText(Html.fromHtml(option4));*/

                    URLImageParser question = new URLImageParser(quizQuestion, this);
                    URLImageParser p = new URLImageParser(optionOne, this);
                    URLImageParser p1 = new URLImageParser(optionTwo, this);
                    URLImageParser p2 = new URLImageParser(optionThree, this);
                    URLImageParser p3 = new URLImageParser(optionFour, this);
                    tatal_marks.setText("Marks :"+ firstQuestion.getMark()+"");
                    negative_mark.setText("Negative Marks :"+firstQuestion.getNeg()+"");
                    try {
                        Spanned questionSpan = Html.fromHtml(firstQuestion.getQuestion(), question, null);
                        if(firstQuestion.getQuestion().contains("img")) {
                            quizQuestion.setText(questionSpan);
                        }
                        else{
                            quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                        }
                    }
                    catch (Exception e){
                        quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                        e.printStackTrace();
                    }
                    try {
                        Spanned htmlSpan = Html.fromHtml(option1, p, null);
                        if(option1.contains("img")){
                            optionOne.setText(htmlSpan);

                        }
                        else{
                            optionOne.setText(Html.fromHtml(htmlSpan.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionOne.setText(possibleAnswers.get(0).toString());

                        e.printStackTrace();
                    }
                    try {
                    Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);

                        if(option2.contains("img")){
                            optionTwo.setText(htmlSpan1);

                        }
                        else{
                            optionTwo.setText(Html.fromHtml(htmlSpan1.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionTwo.setText(possibleAnswers.get(1).toString());
                        e.printStackTrace();
                    }
                    try {
                    Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
                        if(option3.contains("img")){
                            optionThree.setText(htmlSpan2);

                        }
                        else{
                            optionThree.setText(Html.fromHtml(htmlSpan2.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionThree.setText(possibleAnswers.get(2).toString());
                        e.printStackTrace();
                    }
                    try {
                    Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);
                        if(option4.contains("img")){
                            optionFour.setText(htmlSpan3);

                        }
                        else{
                            optionFour.setText(Html.fromHtml(htmlSpan3.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionFour.setText(possibleAnswers.get(3).toString());
                        e.printStackTrace();
                    }









                   /* optionOne.setText(Html.fromHtml(possibleAnswers.get(0).toString()));
                    optionTwo.setText(Html.fromHtml(possibleAnswers.get(1).toString()));
                    optionThree.setText(Html.fromHtml(possibleAnswers.get(2).toString()));
                    optionFour.setText(Html.fromHtml(possibleAnswers.get(3).toString()));*/
                    ioptionto = ioptionto+1;
                    optionOne.setId(ioptionto);
                    ioptionto = ioptionto+1;

                    optionTwo.setId(ioptionto);
                    ioptionto = ioptionto+1;

                    optionThree.setId(ioptionto);
                    ioptionto = ioptionto+1;

                    optionFour.setId(ioptionto);



                    // examDetailList = quesObj.getJSONArray("ED");
                } else {
                    quesList = new JSONArray();
                    // examDetailList = new JSONArray();
                }
            }
            else{

                List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(ExamID);
                List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(ExamID);

                for (ExamDetails cn : examdetailsList) {
                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    Log.d("Exam2: ", log);
                    ExamID=  cn.getExamID();
                    ExamCategoryID =  cn.getExamCategoryID();
                    ExamCategory = cn.getExamCategoryName();
                    ExamCode = cn.getExamCode();
                    ExamDescription =  cn.getExamDescription();
                    BatchID = cn.getBatchID();
                    ExamDuration = cn.getExamDuration();
                    SubjectID = cn.getSubjectID();
                    Subject =  cn.getSubject();
                    ExamTypeID =  cn.getExamTypeID();
                    ExamType = cn.getExamType();
                    ExamDate =cn.getExamDate();

                }
                startTimer(ExamDuration*1000, 1000);
                JSONObject parent = new JSONObject();
                JSONObject parent1 = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                JSONArray jsonArray1 = new JSONArray();
                JSONObject list2 = new JSONObject();
                try {
                    list2.put("ExamID",ExamID).toString();
                    list2.put("ExamCategoryID", ExamCategoryID).toString();
                    list2.put("ExamCategory", ExamCategory).toString();
                    list2.put("ExamCode", ExamCode).toString();
                    list2.put("ExamDescription", ExamDescription).toString();
                    list2.put("BatchID", BatchID).toString();
                    list2.put("ExamDuration", ExamDuration).toString();
                    list2.put("SubjectID", SubjectID).toString();
                    list2.put("Subject", Subject).toString();
                    list2.put("ExamTypeID", ExamTypeID).toString();
                    list2.put("ExamType", ExamType).toString();
                    list2.put("ExamDate", ExamDate).toString();
                }
                catch (Exception e){
                    e.printStackTrace();
                }

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question3: ", log);


                    String optionstr1="";
                    String optionstr2="";

                    String optionstr3="";
                    String optionstr4="";

                    if(cn.getOptionA().startsWith("<p>")&&cn.getOptionA().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";
                        optionstr1=cn.getOptionA().substring(starttag.length(),cn.getOptionA().length()-endtag.length());
                    }
                    else
                    {
                        optionstr1=cn.getOptionA();
                    }

                    if(cn.getOptionB().startsWith("<p>")&&cn.getOptionB().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";
                        optionstr2=cn.getOptionB().substring(starttag.length(),cn.getOptionB().length()-endtag.length());
                    }
                    else
                    {
                        optionstr2=cn.getOptionB();
                    }

                    if(cn.getOptionC().startsWith("<p>")&&cn.getOptionC().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";
                        optionstr3=cn.getOptionC().substring(starttag.length(),cn.getOptionC().length()-endtag.length());
                    }
                    else
                    {
                        optionstr3=cn.getOptionC();
                    }

                    if(cn.getOptionD().startsWith("<p>")&&cn.getOptionD().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";
                        optionstr4=cn.getOptionD().substring(starttag.length(),cn.getOptionD().length()-endtag.length());
                    }
                    else
                    {
                        optionstr4=cn.getOptionD();
                    }
                    /*if(cn.getOptionA().startsWith("<p>")&&cn.getOptionA().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";
                        optionstr1=cn.getOptionA().substring(starttag.length(),cn.getOptionA().length()-endtag.length());
                    }
                    else
                    {
                        optionstr1=cn.getOptionA();
                    }*/


                    JSONArray optionArray = new JSONArray();

                    optionArray.put(optionstr1);
                    optionArray.put(optionstr2);
                    optionArray.put(optionstr3);
                    optionArray.put(optionstr4);



                    try {


                        JSONObject list1 = new JSONObject();
                        list1.put("Questions",cn.getQuestion().toString()).toString();
                        list1.put("QuestionID",cn.getQuestionID()).toString();
                        list1.put("TopicID", cn.getTopicID()).toString();
                        list1.put("Topic", cn.getTopicName().toString());
                        list1.put("AspectID", cn.getAspectID()).toString();
                        list1.put("Aspect",cn.getAspect()).toString();
                        list1.put("QuestionNumber",cn.getQuestionNumber()).toString();
                        list1.put("question", cn.getQuestion()).toString();
                        list1.put("options", optionArray);
                        list1.put("CorrectAnswer",cn.getCorrectAnswer()).toString();
                        list1.put("Mark",cn.getMark()).toString();
                        list1.put("NegativeMark",cn.getNegative_Mark()).toString();
                        jsonArray.put(list1);

                        // new connectTask().execute();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }


                }
                try {
                    list2.put("Questions", jsonArray).toString();
                    parent1.put("Exam", jsonArray1).toString();

                    jsonArray1.put(list2);

                    parent.put("status", "Success").toString();
                    parent.put("StatusCode", "200").toString();
                    parent.put("ExamDetails", parent1).toString();
                    studentResponse = parent.toString();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                Log.d("output", parent.toString());

                String senddata = "SelfEvaluation" + "@" + parent.toString();
                String jsonFormattedString = senddata;

                if (jsonFormattedString.length() > 0) {
                   /* quesObj = new JSONObject(Examquestion);
                    quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                    int totalquestions = quesList.length();*/

                    // System.out.println("totalquestions" + totalquestions);
                    parsedObject = returnParsedJsonObject(jsonFormattedString);
                    if(parsedObject == null){
                        return;
                    }
                    quizCount = parsedObject.size();
                    questionoutofall.setText("1/"+quizCount);
                    if(quizCount==1){
                        nextButton.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);
                    }
                    else{
                        nextButton.setVisibility(View.VISIBLE);
                    }
                    firstQuestion = parsedObject.get(0);
                    String formatted;
                    if(firstQuestion.getQuestion().startsWith("<p>")&&firstQuestion.getQuestion().endsWith("</p>"))
                    {
                        String starttag="<p>";
                        String endtag="</p>";
                        formatted=firstQuestion.getQuestion().substring(starttag.length(),firstQuestion.getQuestion().length()-endtag.length());

                        formatted=formatted.replace("<br>","");

                    }
                    else
                    {
                        formatted=firstQuestion.getQuestion();
                        formatted=formatted.replace("<br>","");

                    }
                    tatal_marks.setText("Marks :"+firstQuestion.getMark()+"");
                    negative_mark.setText("Negative Marks :"+firstQuestion.getNeg()+"");

                    quizQuestion.setText(Html.fromHtml("1) " + formatted));
                    JSONArray possibleAnswers = firstQuestion.getAnswers();
                    String starttag="<p>";
                    String endtag="</p>";
                    String option1=possibleAnswers.get(0).toString();
                    option1=option1.replace("<br>","");
                    String option2=possibleAnswers.get(1).toString();
                    option2=option2.replace("<br>","");



                    String option3=possibleAnswers.get(2).toString();
                    option3=option3.replace("<br>","");

                    String option4=possibleAnswers.get(3).toString();
                    option4=option4.replace("<br>","");

                    if(option2.startsWith("<p>")&&option2.endsWith("</p>"))
                    {
                        option2=option2.substring(starttag.length(),option2.length()-endtag.length());
                    }
                    if(option3.startsWith("<p>")&&option3.endsWith("</p>"))
                    {
                        option3=option3.substring(starttag.length(),option3.length()-endtag.length());

                    }
                    if(option4.startsWith("<p>")&&option4.endsWith("</p>"))
                    {
                        option4=option4.substring(starttag.length(),option4.length()-endtag.length());

                    }
                    if(option1.startsWith("<p>")&&option1.endsWith("</p>"))
                    {
                        option1=option1.substring(starttag.length(),option1.length()-endtag.length());

                    }


                    URLImageParser question = new URLImageParser(quizQuestion, getApplicationContext());
                    URLImageParser p = new URLImageParser(optionOne, getApplicationContext());
                    URLImageParser p1 = new URLImageParser(optionTwo, getApplicationContext());
                    URLImageParser p2 = new URLImageParser(optionThree, getApplicationContext());
                    URLImageParser p3 = new URLImageParser(optionFour, getApplicationContext());

                    Spanned questionSpan = Html.fromHtml(firstQuestion.getQuestion(), question, null);
                    Spanned htmlSpan = Html.fromHtml(option1, p, null);
                    Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);
                    Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
                    Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);

                    if(option1.contains("img")){
                        optionOne.setText(htmlSpan);

                    }
                    else{
                        optionOne.setText(Html.fromHtml(htmlSpan.toString()).toString());

                    }
                    if(option2.contains("img")){
                        optionTwo.setText(htmlSpan1);

                    }
                    else{
                        optionTwo.setText(Html.fromHtml(htmlSpan1.toString()).toString());

                    }
                    if(option3.contains("img")){
                        optionThree.setText(htmlSpan2);

                    }
                    else{
                        optionThree.setText(Html.fromHtml(htmlSpan2.toString()).toString());

                    }
                    if(option4.contains("img")){
                        optionFour.setText(htmlSpan3);

                    }
                    else{
                        optionFour.setText(Html.fromHtml(htmlSpan3.toString()).toString());

                    }

                    if(firstQuestion.getQuestion().contains("img")) {
                        quizQuestion.setText(questionSpan);
                    }
                    else{
                        quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                    }


                    // examDetailList = quesObj.getJSONArray("ED");
                } else {
                    quesList = new JSONArray();
                    // examDetailList = new JSONArray();
                }

                //  loadQuestions();

            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

        new connectTask1().execute();


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                    int radioSelected = radioGroup.getCheckedRadioButtonId();
                    int userSelection = getSelectedAnswer(radioSelected);
                int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));

                //  int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                Log.d("userSelection: ", String.valueOf(userSelection));
                     if(radioSelected== 0){


                        // HashMap<Integer,String>answerandquestion=new HashMap<Integer, String>();
                         collapsestring.remove(firstQuestion.getId());
                         NoAnswerPojo nsp = new NoAnswerPojo();
                         nsp.setQuestionNumber(currentQuizQuestion+1+"");
                         nsp.setSelectedAnswer(String.valueOf(index + 1));
                         collapsestring.put(firstQuestion.getId(),nsp);

                        /* if(ArrayUtils.selectedAnswersList.size()>0) {
                             ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));
                             if(ArrayUtils.selectedAnswersList.size()-1>currentQuizQuestion)
                                 ArrayUtils.selectedAnswersList.remove(currentQuizQuestion + 1);

                         }
                         else if (ArrayUtils.selectedAnswersList.size() == 0) {
                             ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));

                         }
                         else {
                             ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf("-1"));

                         }*/
                       //  ArrayUtils.selectedAnswersList.add("-1");
                     }
                   else{


                         collapsestring.remove(firstQuestion.getId());
                         NoAnswerPojo nsp = new NoAnswerPojo();
                         nsp.setQuestionNumber(currentQuizQuestion+1+"");
                         nsp.setSelectedAnswer(String.valueOf(index + 1));

                         collapsestring.put(firstQuestion.getId(), nsp);


                         //  ArrayUtils.selectedAnswersList.add(String.valueOf(userSelection-1));

                        /* if(ArrayUtils.selectedAnswersList.size()>=currentQuizQuestion)
                         ArrayUtils.selectedAnswersList.remove(currentQuizQuestion);*/
                        /* if(ArrayUtils.selectedAnswersList.size()>0) {
                             ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));
                             if(ArrayUtils.selectedAnswersList.size()-1>currentQuizQuestion)
                             ArrayUtils.selectedAnswersList.remove(currentQuizQuestion + 1);

                         }
                         else if (ArrayUtils.selectedAnswersList.size() == 0) {
                             ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));

                         }
                         else {
                             ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf("-1"));

                         }*/
                         System.out.println(currentQuizQuestion + "currentQuizQuestion");
                         System.out.println(String.valueOf(userSelection-1)+"String.valueOf(userSelection-1)");
                     }

                    // Reading all contacts
              /*      Log.d("Reading: ", "Reading all contacts..");
                    List<StudentTeacherExam> contacts = db.getAllContacts();

                    for (StudentTeacherExam cn : contacts) {
                        String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
                        // Writing Contacts to log
                        Log.d("Name: ", log);*/

                   /* if(ArrayUtils.selectedAnswersList.get(currentQuizQuestion).equalsIgnoreCase("1") ){
                        optionOne.setChecked(true);
                    }
                    else if(ArrayUtils.selectedAnswersList.get(currentQuizQuestion).equalsIgnoreCase("2")){
                        optionTwo.setChecked(true);
                    }
                    else if(ArrayUtils.selectedAnswersList.get(currentQuizQuestion).equalsIgnoreCase("3")){
                        optionThree.setChecked(true);
                    }
                    else if(ArrayUtils.selectedAnswersList.get(currentQuizQuestion).equalsIgnoreCase("4")){
                        optionFour.setChecked(true);
                    }
                    else if(ArrayUtils.selectedAnswersList.get(currentQuizQuestion).equalsIgnoreCase("0")){

                    }*/

                radioGroup.check(0);
                currentQuizQuestion++;
                questionoutofall.setText(currentQuizQuestion+1+"/"+quizCount);
              //  Utils.setTextviewtypeface(2, questionoutofall);





               // if(ArrayUtils.selectedAnswersList.size()>0&&ArrayUtils.selectedAnswersList.size()>currentQuizQuestion) {

               // }


                if(quizCount!=1) {
                    previousButton.setVisibility(View.VISIBLE);

                }

                    if (currentQuizQuestion >= quizCount) {
                        nextButton.setVisibility(View.GONE);
                        submit.setVisibility(View.VISIBLE);

                        Toast.makeText(QuizActivity.this, "End of the Quiz Questions", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        if (currentQuizQuestion + 1 == quizCount) {
                            //  nextButton.setVisibility(View.GONE);
                            nextButton.setVisibility(View.GONE);
                            submit.setVisibility(View.VISIBLE);
                        }


                        firstQuestion = parsedObject.get(currentQuizQuestion);
                       String getselsectedstring;
                        NoAnswerPojo no = collapsestring.get(firstQuestion.getId());
                        if (no==null) {
                            getselsectedstring = "-1";


                        if (getselsectedstring == null) {
                            getselsectedstring = "-1";

                        } else if (getselsectedstring.equalsIgnoreCase("null")) {
                            getselsectedstring = "-1";
                        }
                    }
                        else{
                            getselsectedstring = no.getSelectedAnswer();
                        }
                        if (getselsectedstring.equalsIgnoreCase("1")) {
                            optionOne.setChecked(true);
                            optionTwo.setChecked(false);
                            optionThree.setChecked(false);
                            optionFour.setChecked(false);
                        } else if (getselsectedstring.equalsIgnoreCase("2")) {
                            optionTwo.setChecked(true);
                            optionOne.setChecked(false);
                            optionThree.setChecked(false);
                            optionFour.setChecked(false);

                        } else if (getselsectedstring.equalsIgnoreCase("3")) {
                            optionThree.setChecked(true);
                            optionOne.setChecked(false);
                            optionTwo.setChecked(false);
                            optionFour.setChecked(false);

                        } else if (getselsectedstring.equalsIgnoreCase("4")) {
                            optionFour.setChecked(true);
                            optionTwo.setChecked(false);
                            optionThree.setChecked(false);
                            optionOne.setChecked(false);

                        } else {
                            optionFour.setChecked(false);
                            optionTwo.setChecked(false);
                            optionThree.setChecked(false);
                            optionOne.setChecked(false);

                        }

                        String formatted;
                        if(firstQuestion.getQuestion().startsWith("<p>")&&firstQuestion.getQuestion().endsWith("</p>"))
                        {
                            String starttag="<p>";
                            String endtag="</p>";
                            formatted=firstQuestion.getQuestion().substring(starttag.length(),firstQuestion.getQuestion().length()-endtag.length());
                            formatted=formatted.replace("<br>","");

                        }
                        else
                        {
                            formatted=firstQuestion.getQuestion();
                            formatted=formatted.replace("<br>","");

                        }
                        tatal_marks.setText("Marks :"+firstQuestion.getMark()+"");
                        negative_mark.setText("Negative Marks :"+firstQuestion.getNeg()+"");

                        quizQuestion.setText(Html.fromHtml(currentQuizQuestion+1+") "+formatted));
                    //    Utils.setTextviewtypeface(2, quizQuestion);

                        //  String[] possibleAnswers = firstQuestion.getAnswers().split(",");
                       // uncheckedRadioButton();
                        try {
                            JSONArray possibleAnswers = firstQuestion.getAnswers();
                          //  Utils.setTextviewtypeface(2, optionFour);

                            String starttag="<p>";
                            String endtag="</p>";
                            String option1=possibleAnswers.get(0).toString();
                            option1=option1.replace("<br>","");
                            String option2=possibleAnswers.get(1).toString();
                            option2=option2.replace("<br>","");



                            String option3=possibleAnswers.get(2).toString();
                            option3=option3.replace("<br>","");

                            String option4=possibleAnswers.get(3).toString();
                            option4=option4.replace("<br>","");

                            if(option2.startsWith("<p>")&&option2.endsWith("</p>"))
                            {
                                option2=option2.substring(starttag.length(),option2.length()-endtag.length());
                            }
                            if(option3.startsWith("<p>")&&option3.endsWith("</p>"))
                            {
                                option3=option3.substring(starttag.length(),option3.length()-endtag.length());

                            }
                            if(option4.startsWith("<p>")&&option4.endsWith("</p>"))
                            {
                                option4=option4.substring(starttag.length(),option4.length()-endtag.length());

                            }
                            if(option1.startsWith("<p>")&&option1.endsWith("</p>"))
                            {
                                option1=option1.substring(starttag.length(),option1.length()-endtag.length());

                            }
                            URLImageParser question = new URLImageParser(quizQuestion, getApplicationContext());
                            URLImageParser p = new URLImageParser(optionOne, getApplicationContext());
                            URLImageParser p1 = new URLImageParser(optionTwo, getApplicationContext());
                            URLImageParser p2 = new URLImageParser(optionThree, getApplicationContext());
                            URLImageParser p3 = new URLImageParser(optionFour, getApplicationContext());
                            tatal_marks.setText("Marks :"+firstQuestion.getMark()+"");
                            negative_mark.setText("Negative Marks :"+firstQuestion.getNeg()+"");

                            try {
                                Spanned questionSpan = Html.fromHtml(firstQuestion.getQuestion(), question, null);
                                if(firstQuestion.getQuestion().contains("img")) {
                                    quizQuestion.setText(questionSpan);
                                }
                                else{
                                    quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                                }
                            }
                            catch (Exception e){
                                quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                                e.printStackTrace();
                            }
                            try {
                                Spanned htmlSpan = Html.fromHtml(option1, p, null);
                                if(option1.contains("img")){
                                    optionOne.setText(htmlSpan);

                                }
                                else{
                                    optionOne.setText(Html.fromHtml(htmlSpan.toString()).toString());

                                }
                            }
                            catch (Exception e){
                                optionOne.setText(possibleAnswers.get(0).toString());

                                e.printStackTrace();
                            }
                            try {
                                Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);

                                if(option2.contains("img")){
                                    optionTwo.setText(htmlSpan1);

                                }
                                else{
                                    optionTwo.setText(Html.fromHtml(htmlSpan1.toString()).toString());

                                }
                            }
                            catch (Exception e){
                                optionTwo.setText(possibleAnswers.get(1).toString());
                                e.printStackTrace();
                            }
                            try {
                                Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
                                if(option3.contains("img")){
                                    optionThree.setText(htmlSpan2);

                                }
                                else{
                                    optionThree.setText(Html.fromHtml(htmlSpan2.toString()).toString());

                                }
                            }
                            catch (Exception e){
                                optionThree.setText(possibleAnswers.get(2).toString());
                                e.printStackTrace();
                            }
                            try {
                                Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);
                                if(option4.contains("img")){
                                    optionFour.setText(htmlSpan3);

                                }
                                else{
                                    optionFour.setText(Html.fromHtml(htmlSpan3.toString()).toString());

                                }
                            }
                            catch (Exception e){
                                optionFour.setText(possibleAnswers.get(3).toString());
                                e.printStackTrace();
                            }



                        }
                        catch (Exception e){

                            e.printStackTrace();
                        }

                    }


            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    ArrayUtils.questionNumber.clear();
                    for ( Map.Entry<Integer, NoAnswerPojo> entry : collapsestring.entrySet()) {
                       /// St key = entry.getKey();
                        NoAnswerPojo pojo = entry.getValue();
                        if(pojo.getSelectedAnswer().equalsIgnoreCase("0"))
                        {
                            ArrayUtils.questionNumber.add(Integer.parseInt(pojo.getQuestionNumber()));
                        }
                        // do something with key and/or tab
                    }


                    Collections.sort( ArrayUtils.questionNumber, new Comparator<Integer>() {
                        @Override
                        public int compare(Integer lhs, Integer rhs) {
                            return lhs-rhs;
                        }
                    });

                  /*  for (int i=0; i<collapsestring.size(); i++ )
                    {
                    }*/
                    Intent intent = getIntent();
                    Bundle bundle = intent.getExtras();
                    String Examquestion= bundle.getString("Examquestion");
                    int ExamID= bundle.getInt("ExamID");
                    ExamIDResult = ExamID+"";

                    JSONObject quesObj;
                    if(Examquestion.contains("Examquestion")){
                        if (Examquestion.length() > 0) {

                            // finish();
                            int radioSelected = radioGroup.getCheckedRadioButtonId();
                            int userSelection = getSelectedAnswer(radioSelected);
                            int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));

                            //  int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                            Log.d("userSelection: ", String.valueOf(userSelection));
                            if (radioSelected == 0) {


                                collapsestring.remove(firstQuestion.getId());
                                NoAnswerPojo nsp = new NoAnswerPojo();
                                nsp.setQuestionNumber(currentQuizQuestion + 1 + "");
                                nsp.setSelectedAnswer(String.valueOf(index + 1));

                                collapsestring.put(firstQuestion.getId(), nsp);

                                String getselsectedstring;
                                NoAnswerPojo no = collapsestring.get(firstQuestion.getId());
                                if (no==null) {
                                    getselsectedstring = "-1";


                                    if (getselsectedstring == null) {
                                        getselsectedstring = "-1";

                                    } else if (getselsectedstring.equalsIgnoreCase("null")) {
                                        getselsectedstring = "-1";
                                    }
                                }
                                else{
                                    getselsectedstring = no.getSelectedAnswer();
                                }

                                if (getselsectedstring.equalsIgnoreCase("1")) {
                                    optionOne.setChecked(true);
                                    optionTwo.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionFour.setChecked(false);
                                } else if (getselsectedstring.equalsIgnoreCase("2")) {
                                    optionTwo.setChecked(true);
                                    optionOne.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionFour.setChecked(false);

                                } else if (getselsectedstring.equalsIgnoreCase("3")) {
                                    optionThree.setChecked(true);
                                    optionOne.setChecked(false);
                                    optionTwo.setChecked(false);
                                    optionFour.setChecked(false);

                                } else if (getselsectedstring.equalsIgnoreCase("4")) {
                                    optionFour.setChecked(true);
                                    optionTwo.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionOne.setChecked(false);

                                } else {
                                    optionFour.setChecked(false);
                                    optionTwo.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionOne.setChecked(false);

                                }


                               /* if (ArrayUtils.selectedAnswersList.size() > 0) {
                                    ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf("-1"));
                                    if (ArrayUtils.selectedAnswersList.size() - 1 > currentQuizQuestion)
                                        ArrayUtils.selectedAnswersList.remove(currentQuizQuestion + 1);

                                }  else if (ArrayUtils.selectedAnswersList.size() == 0) {
                                    ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));

                                }
                                else {
                                    ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf("-1"));

                                }*/
                                //  ArrayUtils.selectedAnswersList.add("-1");
                            } else {
                                //  ArrayUtils.selectedAnswersList.add(String.valueOf(userSelection-1));

                        /* if(ArrayUtils.selectedAnswersList.size()>=currentQuizQuestion)
                         ArrayUtils.selectedAnswersList.remove(currentQuizQuestion);*/
                               /* if (ArrayUtils.selectedAnswersList.size() > 0) {
                                    ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));
                                    if (ArrayUtils.selectedAnswersList.size() - 1 > currentQuizQuestion)
                                        ArrayUtils.selectedAnswersList.remove(currentQuizQuestion + 1);

                                }
                               else if (ArrayUtils.selectedAnswersList.size() == 0) {
                                    ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(index+1));

                                }

                                else {
                                    ArrayUtils.selectedAnswersList.add(currentQuizQuestion, String.valueOf(1 - 1));

                                }*/
                                collapsestring.remove(firstQuestion.getId());
                                NoAnswerPojo nsp = new NoAnswerPojo();
                                nsp.setQuestionNumber(currentQuizQuestion + 1 + "");
                                nsp.setSelectedAnswer(String.valueOf(index + 1));
                                collapsestring.put(firstQuestion.getId(), nsp);

                                String getselsectedstring;
                                NoAnswerPojo no = collapsestring.get(firstQuestion.getId());
                                if (no==null) {
                                    getselsectedstring = "-1";


                                    if (getselsectedstring == null) {
                                        getselsectedstring = "-1";

                                    } else if (getselsectedstring.equalsIgnoreCase("null")) {
                                        getselsectedstring = "-1";
                                    }
                                }
                                else{
                                    getselsectedstring = no.getSelectedAnswer();
                                }

                                if (getselsectedstring.equalsIgnoreCase("1")) {
                                    optionOne.setChecked(true);
                                    optionTwo.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionFour.setChecked(false);
                                } else if (getselsectedstring.equalsIgnoreCase("2")) {
                                    optionTwo.setChecked(true);
                                    optionOne.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionFour.setChecked(false);

                                } else if (getselsectedstring.equalsIgnoreCase("3")) {
                                    optionThree.setChecked(true);
                                    optionOne.setChecked(false);
                                    optionTwo.setChecked(false);
                                    optionFour.setChecked(false);

                                } else if (getselsectedstring.equalsIgnoreCase("4")) {
                                    optionFour.setChecked(true);
                                    optionTwo.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionOne.setChecked(false);

                                } else {

                                    optionFour.setChecked(false);
                                    optionTwo.setChecked(false);
                                    optionThree.setChecked(false);
                                    optionOne.setChecked(false);

                                }

                                System.out.println(currentQuizQuestion + "currentQuizQuestion");
                                System.out.println(String.valueOf(userSelection - 1) + "String.valueOf(userSelection-1)");
                            }
                            stratExamCompletedPopup("TeacherConducted");

                        }
                    }

                    else{
                        {
                            radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
                            // finish();
                            int radioSelected = radioGroup.getCheckedRadioButtonId();
                            int userSelection = getSelectedAnswer(radioSelected);
                            int index = radioGroup.indexOfChild(findViewById(radioGroup.getCheckedRadioButtonId()));

                            //  int correctAnswerForQuestion = firstQuestion.getCorrectAnswer();
                            Log.d("userSelection: ", String.valueOf(userSelection));

                            collapsestring.remove(firstQuestion.getId());
                            NoAnswerPojo nsp = new NoAnswerPojo();
                            nsp.setQuestionNumber(currentQuizQuestion + 1 + "");
                            nsp.setSelectedAnswer(String.valueOf(index + 1));
                            collapsestring.put(firstQuestion.getId(), nsp);

                            String getselsectedstring;
                            NoAnswerPojo no = collapsestring.get(firstQuestion.getId());
                            if (no==null) {
                                getselsectedstring = "-1";


                                if (getselsectedstring == null) {
                                    getselsectedstring = "-1";

                                } else if (getselsectedstring.equalsIgnoreCase("null")) {
                                    getselsectedstring = "-1";
                                }
                            }
                            else{
                                getselsectedstring = no.getSelectedAnswer();
                            }

                            if (getselsectedstring.equalsIgnoreCase("1")) {
                                optionOne.setChecked(true);
                                optionTwo.setChecked(false);
                                optionThree.setChecked(false);
                                optionFour.setChecked(false);
                            } else if (getselsectedstring.equalsIgnoreCase("2")) {
                                optionTwo.setChecked(true);
                                optionOne.setChecked(false);
                                optionThree.setChecked(false);
                                optionFour.setChecked(false);

                            } else if (getselsectedstring.equalsIgnoreCase("3")) {
                                optionThree.setChecked(true);
                                optionOne.setChecked(false);
                                optionTwo.setChecked(false);
                                optionFour.setChecked(false);

                            } else if (getselsectedstring.equalsIgnoreCase("4")) {
                                optionFour.setChecked(true);
                                optionTwo.setChecked(false);
                                optionThree.setChecked(false);
                                optionOne.setChecked(false);

                            } else {
                                optionFour.setChecked(false);
                                optionTwo.setChecked(false);
                                optionThree.setChecked(false);
                                optionOne.setChecked(false);

                            }

                            stratExamCompletedPopup("Self-Evaluation");

                        }
                    }
                }

            }

        });


        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextButton.setText("Next");
                currentQuizQuestion--;
                submit.setVisibility(View.INVISIBLE);
                if(currentQuizQuestion==0){
                    previousButton.setVisibility(View.INVISIBLE);
                }
                nextButton.setVisibility(View.VISIBLE);

                if(currentQuizQuestion < 0){
                   return;
                }
               // uncheckedRadioButton();
                firstQuestion = parsedObject.get(currentQuizQuestion);
                questionoutofall.setText(currentQuizQuestion+1+"/"+quizCount);
                String formatted;
                if(firstQuestion.getQuestion().startsWith("<p>")&&firstQuestion.getQuestion().endsWith("</p>"))
                {
                    String starttag="<p>";
                    String endtag="</p>";
                    formatted=firstQuestion.getQuestion().substring(starttag.length(),firstQuestion.getQuestion().length()-endtag.length());
                    formatted=formatted.replace("<br>","");
                }
                else
                {
                    formatted=firstQuestion.getQuestion();
                    formatted=formatted.replace("<br>","");

                }

                tatal_marks.setText("Marks :"+firstQuestion.getMark()+"");
                negative_mark.setText("Negative Marks :"+firstQuestion.getNeg()+"");


                quizQuestion.setText(Html.fromHtml(currentQuizQuestion + 1+") "+formatted));



                //quizQuestion.setText(firstQuestion.getQuestion());

                try {
                    JSONArray possibleAnswers = firstQuestion.getAnswers();

                    String starttag="<p>";
                    String endtag="</p>";
                    String option1=possibleAnswers.get(0).toString();
                    option1=option1.replace("<br>","");
                    String option2=possibleAnswers.get(1).toString();
                    option2=option2.replace("<br>","");



                    String option3=possibleAnswers.get(2).toString();
                    option3=option3.replace("<br>","");

                    String option4=possibleAnswers.get(3).toString();
                    option4=option4.replace("<br>","");

                    if(option2.startsWith("<p>")&&option2.endsWith("</p>"))
                    {
                        option2=option2.substring(starttag.length(),option2.length()-endtag.length());
                    }
                    if(option3.startsWith("<p>")&&option3.endsWith("</p>"))
                    {
                        option3=option3.substring(starttag.length(),option3.length()-endtag.length());

                    }
                    if(option4.startsWith("<p>")&&option4.endsWith("</p>"))
                    {
                        option4=option4.substring(starttag.length(),option4.length()-endtag.length());

                    }
                    if(option1.startsWith("<p>")&&option1.endsWith("</p>"))
                    {
                        option1=option1.substring(starttag.length(),option1.length()-endtag.length());

                    }

                    URLImageParser question = new URLImageParser(quizQuestion, getApplicationContext());
                    URLImageParser p = new URLImageParser(optionOne, getApplicationContext());
                    URLImageParser p1 = new URLImageParser(optionTwo, getApplicationContext());
                    URLImageParser p2 = new URLImageParser(optionThree, getApplicationContext());
                    URLImageParser p3 = new URLImageParser(optionFour, getApplicationContext());
                    tatal_marks.setText("Marks :"+firstQuestion.getMark()+"");
                    negative_mark.setText("Negative Marks :"+firstQuestion.getNeg()+"");

                    try {
                        Spanned questionSpan = Html.fromHtml(firstQuestion.getQuestion(), question, null);
                        if(firstQuestion.getQuestion().contains("img")) {
                            quizQuestion.setText(questionSpan);
                        }
                        else{
                            quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                        }
                    }
                    catch (Exception e){
                        quizQuestion.setText(Html.fromHtml(firstQuestion.getQuestion().toString()).toString());

                        e.printStackTrace();
                    }
                    try {
                        Spanned htmlSpan = Html.fromHtml(option1, p, null);
                        if(option1.contains("img")){
                            optionOne.setText(htmlSpan);

                        }
                        else{
                            optionOne.setText(Html.fromHtml(htmlSpan.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionOne.setText(possibleAnswers.get(0).toString());

                        e.printStackTrace();
                    }
                    try {
                        Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);

                        if(option2.contains("img")){
                            optionTwo.setText(htmlSpan1);

                        }
                        else{
                            optionTwo.setText(Html.fromHtml(htmlSpan1.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionTwo.setText(possibleAnswers.get(1).toString());
                        e.printStackTrace();
                    }
                    try {
                        Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
                        if(option3.contains("img")){
                            optionThree.setText(htmlSpan2);

                        }
                        else{
                            optionThree.setText(Html.fromHtml(htmlSpan2.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionThree.setText(possibleAnswers.get(2).toString());
                        e.printStackTrace();
                    }
                    try {
                        Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);
                        if(option4.contains("img")){
                            optionFour.setText(htmlSpan3);

                        }
                        else{
                            optionFour.setText(Html.fromHtml(htmlSpan3.toString()).toString());

                        }
                    }
                    catch (Exception e){
                        optionFour.setText(possibleAnswers.get(3).toString());
                        e.printStackTrace();
                    }




                }
                catch (Exception e){

                    e.printStackTrace();
                }

                String getstring;
                NoAnswerPojo no = collapsestring.get(firstQuestion.getId());
                if (no==null) {
                    getstring = "-1";


                    if (getstring == null) {
                        getstring = "-1";

                    } else if (getstring.equalsIgnoreCase("null")) {
                        getstring = "-1";
                    }
                }
                else{
                    getstring = no.getSelectedAnswer();
                }                if(getstring.equalsIgnoreCase("1")){
                    optionOne.setChecked(true);
                    optionTwo.setChecked(false);
                    optionThree.setChecked(false);
                    optionFour.setChecked(false);
                }
                 else if(getstring.equalsIgnoreCase("2")){
                     optionTwo.setChecked(true);
                    optionOne.setChecked(false);
                    optionThree.setChecked(false);
                    optionFour.setChecked(false);

                }
                 else if(getstring.equalsIgnoreCase("3")){
                     optionThree.setChecked(true);
                    optionOne.setChecked(false);
                    optionTwo.setChecked(false);
                    optionFour.setChecked(false);

                }
                 else if(getstring.equalsIgnoreCase("4")){
                     optionFour.setChecked(true);
                    optionTwo.setChecked(false);
                    optionThree.setChecked(false);
                    optionOne.setChecked(false);

                }
                else{
                    optionFour.setChecked(false);
                    optionTwo.setChecked(false);
                    optionThree.setChecked(false);
                    optionOne.setChecked(false);

                }


                        }
        });
    }


    void setbackground(ImageView view, String filepath)
    {
        try
        {


        File imgFile=new File(filepath);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
            view.setImageBitmap(myBitmap);

        }
        }
        catch (Exception e)
        {

        }
    }
   public void onBackPressed() {

    }

    public void startTimer(final long finish, long tick)
    {
         new CountDownTimer(finish, tick) {

            public void onTick(long millisUntilFinished)
            {
                long remainedSecs = millisUntilFinished/1000;


                timer_text.setText(""+("00:"+remainedSecs/60)+":"+(remainedSecs%60));// manage it accordign to you
               // Utils.setTextviewtypeface(2, timer_text);

            }

            public void onFinish()
            {
                timer_text.setText("00:00:00");
              //  cancel();

                finish();


                Toast.makeText(getApplicationContext(), "Session Expired", Toast.LENGTH_LONG).show();
                timer_text.setText("done!");
               // Utils.setTextviewtypeface(2, timer_text);
                for (int j = 0; j < ArrayUtils.selectedAnswersList.size(); j++) {
                    String selectedAnswersList = ArrayUtils.selectedAnswersList.get(j);
                    System.out.println(selectedAnswersList);
                }

                ArrayUtils.selectedAnswersList.clear();

            }
        }.start();
    }
    public void stratExamCompletedPopup(final String examType) {
        dialog = new MyDialog(this);
       // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.exam_submitted_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        valuate = (Button) dialog.findViewById(R.id.button2);
        revisit = (Button) dialog.findViewById(R.id.button3);
        stuname= (TextView) dialog.findViewById(R.id.stuname);
        stuname.setText(studentname);
        notsaved= (TextView) dialog.findViewById(R.id.notsaved);
        DatabaseHandler db = new DatabaseHandler(QuizActivity.this);

        if(ArrayUtils.questionNumber.isEmpty()){
            notsaved.setText("Are you sure you want to submit ? ");
        }
        else{
            notsaved.setText("You have not given any answer for question number : "+  ArrayUtils.questionNumber.toString()+" "+"What you would like to do...!!");
        }

        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View arg0) {
                dialog.dismiss();
                int  obtainedScoreVal = 0;
                int  IsCorrectAnswer = 0;
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
                Date date = new Date();

                DatabaseHandler db = new DatabaseHandler(QuizActivity.this);
                db.updateExamDetails(new ExamDetails(examType,ExamID,111,dateFormat.format(date)));
                List<QuestionDetails> questiondetailsListusingID = db.getAllExamQuestionsuseingID(ExamID);
                for (QuestionDetails cn : questiondetailsListusingID) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question2: ", log);
                    ArrayUtils.questionIDarray.add(cn.getQuestionID());
                    ArrayUtils.negativeMark.add(cn.getNegative_Mark());
                    ArrayUtils.mark.add(cn.getMark());
                    String selectedAnswersList="";
                     NoAnswerPojo noss = collapsestring.get(cn.getQuestionID());
                    selectedAnswersList=noss.getSelectedAnswer();
                    if(selectedAnswersList.equalsIgnoreCase("1")){
                        selectedAnswersList="A";
                    }
                    else if(selectedAnswersList.equalsIgnoreCase("2")){
                        selectedAnswersList="B";
                    } else if(selectedAnswersList.equalsIgnoreCase("3")){
                        selectedAnswersList="C";
                    }
                    else if(selectedAnswersList.equalsIgnoreCase("4")){
                        selectedAnswersList="D";
                    }
                    else if(selectedAnswersList.equalsIgnoreCase("0")){
                        selectedAnswersList="";
                    }
                    else{
                        selectedAnswersList="";
                    }

                    if(selectedAnswersList .equalsIgnoreCase("")) {

                        IsCorrectAnswer =0;
                        obtainedScoreVal = 0+obtainedScoreVal;

                    }

                   else if(cn.getCorrectAnswer() .equalsIgnoreCase(selectedAnswersList)) {


                        IsCorrectAnswer =1;
                        obtainedScoreVal = cn.getMark()+obtainedScoreVal;

                    }

                    else {
                        obtainedScoreVal = obtainedScoreVal-cn.getNegative_Mark();
                        IsCorrectAnswer =0;

                    }



                    //ArrayUtils.currectAnswer.add(cn.getCorrectAnswer());
                    Log.d("update ot not","" +  db.updateQuestionDetails(new QuestionDetails(cn.getQuestionID(),ExamID,QuestionNumber, selectedAnswersList, IsCorrectAnswer,dateFormat.format(date),dateFormat.format(date),obtainedScoreVal)));
                }

                ArrayUtils.questionIDarray.clear();
                ArrayUtils.negativeMark.clear();
                ArrayUtils.mark.clear();
                ArrayUtils.currectAnswer.clear();
                ArrayUtils.questionNumber.clear();
                if(examType.equalsIgnoreCase("TeacherConducted")){
                    db.updateQuestionDetailsByID(new QuestionDetails(ExamID, "TeacherConductedCompleted"));
                }
                else{
                    db.updateQuestionDetailsByID(new QuestionDetails(ExamID, "Self-EvaluationCompleted"));
                }


                List<QuestionDetails> studentans = db.getAllExamQuestionsuseingID(ExamID);

                for (QuestionDetails cn : studentans) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("studentansstudentans: ", log);




                }
                JSONObject parent = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                JSONArray jsonArray = new JSONArray();

                SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());
                Cursor tc=obj.retrive("tblStudent");
                while(tc.moveToNext()){
                    studentname= tc.getString(tc.getColumnIndex("FirstName"));
                     StudentID= tc.getInt(tc.getColumnIndex("StudentID"));
                }
                List<QuestionDetails> studentansde = db.getAllExamQuestionsuseingID(ExamID);

               for (QuestionDetails cn : studentansde) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question3: ", log);


                    try {
                        JSONObject list2 = new JSONObject();

                        list2.put("ExamResponseID", "1");
                        list2.put("ExamID", cn.getExamID());
                        list2.put("StudentID", StudentID);
                        list2.put("TimeTaken", "TimeTaken");
                        list2.put("StudentName", studentname);
                        list2.put("DateAttended", cn.getCreatedOn());
                        list2.put("TotalScore", cn.getObtainedScore());

                        JSONObject list1 = new JSONObject();
                        list1.put("StudentResponseID","1");
                        list1.put("QuestionID",cn.getQuestionID());
                        list1.put("StudentAnswer", cn.getStudentAnswer());
                        list1.put("IsCorrect",cn.getIsCorrect());
                        list1.put("MarkForAnswer",cn.getMark());
                        list1.put("ObtainedMark",cn.getObtainedScore());
                        jsonArray.put(list1);

                        list2.put("StudentResponse", jsonArray);




                        parent.put("Result", list2);
                         studentResponse = parent.toString();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }


                }
                Log.d("studentResponse", studentResponse);



                Intent intent = getIntent();
                Bundle bundle = intent.getExtras();
                String Examquestion= bundle.getString("Examquestion");
                int ExamID= bundle.getInt("ExamID");
                ExamIDResult = ExamID+"";
                if(Examquestion.contains("Examquestion")) {
                    new connectTask().execute();
                }
                else{
                    try {
                        Intent intent1 = new Intent(QuizActivity.this, SelfEvaluationTotalExamList.class);
                        intent1.putExtra("RecentExamID", ExamID);
                        intent1.putExtra("RecentExamIDValue", "RecentExamIDValue");
                        startActivity(intent1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                ArrayUtils.selectedAnswersList.clear();
                 finish();
               /* Intent intent = new Intent(QuizActivity.this, ExamCompletedActivity.class);
                startActivity(intent);*/
            }
        });
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
/*
    private void loadQuestions() throws Exception {

        String fileContent = new String();
        //Find the directory for the SD Card using the API
/*/
/*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();

//Get the text file
        File file = new File(sdcard,"question.txt");
        JSONObject quesObj;



//Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            fileContent = text.toString();

            br.close();
            System.out.println("fileContent.length()" + fileContent.length());

            if (fileContent.length() > 0) {
                quesObj = new JSONObject(fileContent);
                quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                int totalquestions = quesList.length();
                System.out.println("totalquestions" + totalquestions);
                parsedObject = returnParsedJsonObjectnon(fileContent);
                if(parsedObject == null){
                    return;
                }
                quizCount = parsedObject.size();
                questionoutofall.setText("1/"+quizCount);
                firstQuestion = parsedObject.get(0);

                quizQuestion.setText(firstQuestion.getQuestion());
                JSONArray possibleAnswers = firstQuestion.getAnswers();
                optionOne.setText(possibleAnswers.get(0).toString());
                optionTwo.setText(possibleAnswers.get(1).toString());
                optionThree.setText(possibleAnswers.get(2).toString());
                optionFour.setText(possibleAnswers.get(3).toString());

                // examDetailList = quesObj.getJSONArray("ED");
            } else {
                quesList = new JSONArray();
                // examDetailList = new JSONArray();
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
       // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
       // tv.setText(text);

    }
*/

    public class connectTask extends AsyncTask<String,String, Client> {

        @Override
        protected Client doInBackground(String... message) {

            //we create a Client object and
            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            },pref,studentResponse);
            mClient.run();

            return null;
        }

    }
    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }
    private static class Question implements Comparable<Question> {

        private String question;
        private int questionIndex;
        private String[] answers;
        private int correctAnswerIndex;

        public Question(String question, int questionIndex, String[] answers,
                        int correctAnswerIndex) {
            this.question = question;
            this.questionIndex = questionIndex;
            this.answers = answers;
            this.correctAnswerIndex = correctAnswerIndex;
        }

        public static Question fromJson(JSONObject questionObject, int questionIndex)
                throws JSONException {
            String question = questionObject.getString(JsonUtils.JSON_FIELD_QUESTION);
            JSONArray answersJsonArray = questionObject.getJSONArray(JsonUtils.JSON_FIELD_ANSWERS);
            String[] answers = new String[JsonUtils.NUM_ANSWER_CHOICES];
            for (int j = 0; j < answersJsonArray.length(); j++) {
                answers[j] = answersJsonArray.getString(j);
            }
            int correctIndex = questionObject.getInt(JsonUtils.JSON_FIELD_CORRECT_INDEX);
            return new Question(question, questionIndex, answers, correctIndex);
        }

        @Override
        public int compareTo(Question that) {
            return this.questionIndex - that.questionIndex;
        }

    }
    private List<QuizWrapper> returnParsedJsonObjectnon(String result){

        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper newItemObject = null;

        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;
            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = 1;
                String question = jsonChildNode.getString("question");
                JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                int correctAnswer = jsonChildNode.getInt("correctIndex");
                newItemObject = new QuizWrapper(id, question, answerOptions, correctAnswer,0,0);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    private List<QuizWrapper> returnParsedJsonObjectTeacher(String result){

        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper newItemObject = null;
        DatabaseHandler db = new DatabaseHandler(QuizActivity.this);


        try {
            String exam_split [] = result.split("\\@");
            JSONObject array = new JSONObject(exam_split[1]);
            String status = array.getString("status");
            String StatusCode = array.getString("StatusCode");

            //String ExamDetails = array.getString("ExamDetails");
            JSONArray Exam_arr=array.getJSONObject("ExamDetails").getJSONArray("Exam");
            // JSONArray Result_arr=array.getJSONObject("ExamDetails").getJSONArray("Result");
            quesList = shuffleJsonArray(Exam_arr);

            //  JSONArray Exam_arr = array.getJSONArray("Exam");


            if (StatusCode.equalsIgnoreCase("200")) {
                for (int i = 0; i < quesList.length(); i++) {
                    JSONObject objexam=quesList.getJSONObject(i);
                    ExamIDVal = objexam.getString("ExamID");
                    ExamID = Integer.parseInt(ExamIDVal);
                    ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                    ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                    ExamCategory = objexam.getString("ExamCategory");
                    ExamCode = objexam.getString("ExamCode");
                    ExamDescription = objexam.getString("ExamDescription");
                    BatchIDVal = objexam.getString("BatchID");
                    ExamDurationVal = objexam.getString("ExamDuration");
                    BatchID = Integer.parseInt(BatchIDVal);
                    ExamDuration = Integer.parseInt(ExamDurationVal);

                    SubjectIDVal = objexam.getString("SubjectID");
                    Subject = objexam.getString("Subject");
                    subject  = (TextView)findViewById(R.id.subject);
                    download_txt= (TextView)findViewById(R.id.download_txt);
                    download_txt.setText(Html.fromHtml(ExamDescription));
                    subject.setText(Html.fromHtml(Subject));
                    SubjectID = Integer.parseInt(SubjectIDVal);
                    ExamTypeIDVal = objexam.getString("ExamTypeID");

                    ExamType = objexam.getString("ExamType");

                    // ExamDate = objexam.getString("ExamDate");

                    ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   /* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*/
                    // String Questions = objexam.getString("Questions");
                    String optionvalue;
                    int ExamSequence =0;
                    int SchoolID =0;
                    int IsResultPublished =0;
                    int ExamShelfID =0;
                    int ClassID =0;
                    int TimeTaken = 0;
                    int TotalScore =0;
                    String DateAttended ="01/06/2017 10:00:00";
                    db.addExamDetails(new ExamDetails(ExamID,ExamCategoryID,ExamCategory,ExamCode,ExamDescription,ExamSequence,ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                            BatchID, IsResultPublished,
                            ExamShelfID,
                            TimeTaken,
                            DateAttended,
                            TotalScore));
                    JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                    arrayqueslist = shuffleJsonArray(jsonArrayquestion);

                    for(int j = 0; j < arrayqueslist.length(); j++){
                        JSONObject jsonChildNode = null;
                        try {
                            jsonChildNode = arrayqueslist.getJSONObject(j);
                            String QuestionIDval = jsonChildNode.getString("QuestionID");
                            QuestionID = Integer.parseInt(QuestionIDval);
                            TopicIDVal = jsonChildNode.getString("TopicID");
                            int TopicID = Integer.parseInt(TopicIDVal);
                            Topic = jsonChildNode.getString("Topic");
                            AspectIDVal = jsonChildNode.getString("AspectID");
                            AspectID = Integer.parseInt(AspectIDVal);
                            Aspect = jsonChildNode.getString("Aspect");
                            QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                            QuestionNumber = Integer.parseInt(QuestionNumberVal);
                            question= jsonChildNode.getString("question");
                            options = jsonChildNode.getJSONArray("options");

                            String Evaluation = pref.getString("Evaluation", "0");
                            File username=new File(Evaluation+"/"+ExamID);
                            username.mkdir();
                            if(question.contains("img")){
                                String questionImg = testX(question,username.getAbsolutePath());
                                question=questionImg;
                            }

                            for(int z = 0; z < options.length(); z++){
                                String[] s = new String[options.length()];
                                String storagepath = testX(options.get(z).toString(),username.getAbsolutePath());
                                if(storagepath.contains(Evaluation)){
                                    options.put(z,storagepath);
                                }
                            }
                            CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                            CorrectAnswerVal = 1;
                            MarkVal = jsonChildNode.getString("Mark");
                            Mark = Integer.parseInt(MarkVal);
                            //  tatal_marksVal = tatal_marksVal+Mark;
                            tatal_marksVal = Mark;
                            tatal_maxMark = tatal_maxMark+Mark;

                            NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                            NegativeMark = Integer.parseInt(NegativeMarkVal);
                            String Created_on = "01/06/2017 10:00:00";
                            String ModifiedOn = "01/06/2017 10:00:00";
                            StudentAnswer ="";

                            newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal,NegativeMark,Mark);


                             if(options.length()>3){
                                 db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(),CorrectAnswer , Mark,
                                         NegativeMark, StudentAnswer,
                                         IsCorrect,
                                         ObtainedMark,
                                         Created_on,
                                         "not update",
                                         ModifiedOn));
                             }

                            else if(options.length()>2){
                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                                        NegativeMark, StudentAnswer,
                                        IsCorrect,
                                        ObtainedMark,
                                        Created_on,
                                        "not update",
                                        ModifiedOn));
                            }

                           else if(options.length()>1){
                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "",CorrectAnswer , Mark,
                                        NegativeMark, StudentAnswer,
                                        IsCorrect,
                                        ObtainedMark,
                                        Created_on,
                                        "not update",
                                        ModifiedOn));
                            }


                            jsonObject.add(newItemObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    maxmark.setText(Html.fromHtml("Max.Marks: "+tatal_maxMark));


                    // Adding child data

                    // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                    //jsonObject.add(newItemObject);

                }


                List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question: ", log);
                }

                List<ExamDetails> examdetailsList = db.getAllExamsDetails();

                for (ExamDetails cn : examdetailsList) {
                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    Log.d("Exam: ", log);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
       /* try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;
            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = 1;
                String question = jsonChildNode.getString("question");
                JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                int correctAnswer = jsonChildNode.getInt("correctIndex");
                newItemObject = new QuizWrapper(id, question, answerOptions, correctAnswer);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
        return jsonObject;
    }
    private List<QuizWrapper> returnParsedJsonObject(String result){

        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        QuizWrapper newItemObject = null;
        DatabaseHandler db = new DatabaseHandler(QuizActivity.this);


        try {
            String exam_split [] = result.split("\\@");
            JSONObject array = new JSONObject(exam_split[1]);
            String status = array.getString("status");
            String StatusCode = array.getString("StatusCode");

            //String ExamDetails = array.getString("ExamDetails");
            JSONArray Exam_arr=array.getJSONObject("ExamDetails").getJSONArray("Exam");
           // JSONArray Result_arr=array.getJSONObject("ExamDetails").getJSONArray("Result");
            quesList = shuffleJsonArray(Exam_arr);

            //  JSONArray Exam_arr = array.getJSONArray("Exam");


            if (StatusCode.equalsIgnoreCase("200")) {
                for (int i = 0; i < quesList.length(); i++) {
                    JSONObject objexam=quesList.getJSONObject(i);
                     ExamIDVal = objexam.getString("ExamID");
                     ExamID = Integer.parseInt(ExamIDVal);
                     ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                     ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                     ExamCategory = objexam.getString("ExamCategory");
                     ExamCode = objexam.getString("ExamCode");
                     ExamDescription = objexam.getString("ExamDescription");
                     BatchIDVal = objexam.getString("BatchID");
                     ExamDurationVal = objexam.getString("ExamDuration");
                     BatchID = Integer.parseInt(BatchIDVal);
                     ExamDuration = Integer.parseInt(ExamDurationVal);

                     SubjectIDVal = objexam.getString("SubjectID");
                     Subject = objexam.getString("Subject");
                    subject  = (TextView)findViewById(R.id.subject);
                    download_txt= (TextView)findViewById(R.id.download_txt);
                    download_txt.setText(Html.fromHtml(ExamDescription));
                    subject.setText(Html.fromHtml(Subject));
                     SubjectID = Integer.parseInt(SubjectIDVal);
                     ExamTypeIDVal = objexam.getString("ExamTypeID");

                     ExamType = objexam.getString("ExamType");

                    // ExamDate = objexam.getString("ExamDate");

                     ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   /* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*/
                   // String Questions = objexam.getString("Questions");
                    String optionvalue;
                    int ExamSequence =0;
                    int SchoolID =0;
                    int IsResultPublished =0;
                    int ExamShelfID =0;
                    int ClassID =0;
                     int TimeTaken = 0;
                      int TotalScore =0;
                     String DateAttended ="01/06/2017 10:00:00";
                  /*  db.addExamDetails(new ExamDetails(ExamID,ExamCategoryID,ExamCategory,ExamCode,ExamDescription,ExamSequence,ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                            BatchID, IsResultPublished,
                            ExamShelfID,
                            TimeTaken,
                            DateAttended,
                            TotalScore));*/
                    JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                    arrayqueslist = shuffleJsonArray(jsonArrayquestion);

                    for(int j = 0; j < arrayqueslist.length(); j++){
                        JSONObject jsonChildNode = null;
                        try {
                            jsonChildNode = arrayqueslist.getJSONObject(j);
                            String QuestionIDval = jsonChildNode.getString("QuestionID");
                             QuestionID = Integer.parseInt(QuestionIDval);
                             TopicIDVal = jsonChildNode.getString("TopicID");
                             int TopicID = Integer.parseInt(TopicIDVal);
                            Topic = jsonChildNode.getString("Topic");
                            AspectIDVal = jsonChildNode.getString("AspectID");
                             AspectID = Integer.parseInt(AspectIDVal);
                            Aspect = jsonChildNode.getString("Aspect");
                            QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                           QuestionNumber = Integer.parseInt(QuestionNumberVal);
                            question= jsonChildNode.getString("question");
                            options = jsonChildNode.getJSONArray("options");
                            String Evaluation = pref.getString("Evaluation", "0");
                            File username=new File(Evaluation+"/"+ExamID);
                            username.mkdir();
                            if(question.contains("img")){
                                String questionImg = testX(question,username.getAbsolutePath());
                                question=questionImg;
                            }

                            for(int z = 0; z < options.length(); z++){
                                String[] s = new String[options.length()];
                                String storagepath = testX(options.get(z).toString(),username.getAbsolutePath());
                                if(storagepath.contains(Evaluation)){
                                    options.put(z,storagepath);
                                }
                            }
                             CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                             CorrectAnswerVal = 1;
                             MarkVal = jsonChildNode.getString("Mark");
                             Mark = Integer.parseInt(MarkVal);
                          //  tatal_marksVal = tatal_marksVal+Mark;
                            tatal_marksVal = Mark;
                            tatal_maxMark = tatal_maxMark+Mark;

                            NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                             NegativeMark = Integer.parseInt(NegativeMarkVal);
                            String Created_on = "01/06/2017 10:00:00";
                            String ModifiedOn = "01/06/2017 10:00:00";
                            StudentAnswer ="";

                            newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal,NegativeMark,Mark);
/*

                             if(options.length()>3){
                                 db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(),CorrectAnswer , Mark,
                                         NegativeMark, StudentAnswer,
                                         IsCorrect,
                                         ObtainedMark,
                                         Created_on,
                                         "not update",
                                         ModifiedOn));
                             }

                            else if(options.length()>2){
                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                                        NegativeMark, StudentAnswer,
                                        IsCorrect,
                                        ObtainedMark,
                                        Created_on,
                                        "not update",
                                        ModifiedOn));
                            }

                           else if(options.length()>1){
                                db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "",CorrectAnswer , Mark,
                                        NegativeMark, StudentAnswer,
                                        IsCorrect,
                                        ObtainedMark,
                                        Created_on,
                                        "not update",
                                        ModifiedOn));
                            }
*/

                            jsonObject.add(newItemObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    tatal_marks  = (TextView)findViewById(R.id.tatal_marks);
                    tatal_marks.setText("Marks :"+Html.fromHtml("MARKS: "+tatal_marksVal));
                    maxmark.setText(Html.fromHtml("Max.Marks: "+tatal_maxMark));
                    // Adding child data

                   // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                    //jsonObject.add(newItemObject);

                }


                List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question: ", log);
                }

                List<ExamDetails> examdetailsList = db.getAllExamsDetails();

                for (ExamDetails cn : examdetailsList) {
                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    Log.d("Exam: ", log);
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
       /* try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            jsonArray = resultObject.optJSONArray("questions");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;
            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = 1;
                String question = jsonChildNode.getString("question");
                JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                int correctAnswer = jsonChildNode.getInt("correctIndex");
                newItemObject = new QuizWrapper(id, question, answerOptions, correctAnswer);
                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
        return jsonObject;
    }



    public String testX(String hjdsf, String path) {
        Pattern p = Pattern.compile("<img.+?src=[\\\\\\\\\\\\\\\"'](.+?)[\\\\\\\\\\\\\\\"'].+?>");
        Matcher m = p.matcher(hjdsf);
        String test="";
        if (m.find()) {

            System.out.println(m.group(1).substring(1));
            String filename = m.group(1).substring(1).substring(0, m.group(1).substring(1).lastIndexOf('.'));
            // prints http://www.01net.com/images/article/mea/150.100.790233.jpg

            String batchfile=m.group(1).substring(1).substring(m.group(1).substring(1).lastIndexOf("/") + 1, m.group(1).substring(1).length());

            downloadfile(m.group(1).substring(1), path, batchfile);
            storagepath= path+"/"+batchfile;

            test = hjdsf.replaceAll(m.group(1).substring(1),storagepath);

            System.out.println(test+"test");
            //pathArray.add(test);
        }


        return test;

    }




    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    int BUFFER = 2048;

    public void zip(String _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            //  for (int i = 0; i < 1; i++) {
            Log.v("Compress", "Adding: " + _files);
            FileInputStream fi = new FileInputStream(_files);
            origin = new BufferedInputStream(fi, BUFFER);

            ZipEntry entry = new ZipEntry(_files.substring(_files.lastIndexOf("/") + 1));
            out.putNextEntry(entry);
            int count;

            while ((count = origin.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            origin.close();
            // }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private int getSelectedAnswer(int radioSelected){

        int answerSelected = 0;
        if(radioSelected == R.id.radio0){
            answerSelected = 1;
        }
        if(radioSelected == R.id.radio1){
            answerSelected = 2;
        }
        if(radioSelected == R.id.radio2){
            answerSelected = 3;
        }
        if(radioSelected == R.id.radio3){
            answerSelected = 4;
        }
        return answerSelected;
    }
    private void uncheckedRadioButton(){
        optionOne.setChecked(false);
        optionTwo.setChecked(false);
        optionThree.setChecked(false);
        optionFour.setChecked(false);
    }


    public class connectTask1 extends AsyncTask<String,String, Client> {

        @Override
        protected Client doInBackground(String... message) {
            String jsonresponse =  "Receive@studentID@StudentRoll@file/exam@"+ExamIDVal;

            //we create a Client object and
            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            },pref,jsonresponse);
            mClient.run();

            return null;
        }

    }

    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onResume();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}
