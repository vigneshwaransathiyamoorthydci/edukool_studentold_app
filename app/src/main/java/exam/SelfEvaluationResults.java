package exam;
import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Random;

import com.dci.edukool.student.R;

/**
 * Created by pratheeba on 4/26/2017.
 */
public class SelfEvaluationResults extends Activity {
    SelfTestQuestionAdapter selfevalutation_adapter;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    static JSONArray quesList;
    ListView listView,self_asses_ques_ans_list;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.self_evaluation_result_list);

        self_asses_ques_ans_list = (ListView) findViewById(R.id.listView);

        try {
            loadQuestions();
        }
        catch (Exception e){
            e.printStackTrace();
        }


    }




    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue=0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
            if(curBrightnessValue>20) {
                float brightness = curBrightnessValue / (float) 255;
                WindowManager.LayoutParams lp = getWindow().getAttributes();


                lp.screenBrightness = brightness;
                getWindow().setAttributes(lp);
            }

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

       /* float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/
    }

    private void loadQuestions() throws Exception {
        String fileContent = new String();
        //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();
//Get the text file
        File file = new File(sdcard,"question.txt");
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            fileContent = text.toString();
            br.close();
            System.out.println("fileContent.length()" + fileContent.length());
            if (fileContent.length() > 0) {
                quesObj = new JSONObject(fileContent);
                quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                int totalquestions = quesList.length();
                System.out.println("totalquestions" + totalquestions);
                JSONObject resultObject = null;
                JSONArray jsonArray = null;
                QuizWrapper newItemObject = null;
                try {
                    resultObject = new JSONObject(fileContent);
                    System.out.println("Testing the water " + resultObject.toString());
                    jsonArray = resultObject.optJSONArray("questions");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonChildNode = null;
                    try {
                        jsonChildNode = jsonArray.getJSONObject(i);
                        int id = 1;
                        String question = jsonChildNode.getString("question");
                        JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                        int correctAnswer = jsonChildNode.getInt("correctIndex");
                        try {
                            plan_list_array.add(new QuestionPojo(question, "", "", "", "", String.valueOf(correctAnswer), "1", "StudentName", "StudentRollNumber", "StudentID", "TotalNoQuestion", "StudentPhotPath",0));

                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                selfevalutation_adapter = new SelfTestQuestionAdapter(SelfEvaluationResults.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);

            } else {
                quesList = new JSONArray();
                // examDetailList = new JSONArray();
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}
