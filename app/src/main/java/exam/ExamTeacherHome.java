package exam;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.edukool.student.R;

/**
 * Created by pratheeba on 5/4/2017.
 */
public class ExamTeacherHome extends Activity {
    ImageView download_exams,worst_perfor,pie_chart,receive_and_send_ans,view_questions_list;



    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_home_layout);
        download_exams = (ImageView)findViewById(R.id.download_exams);
        worst_perfor = (ImageView)findViewById(R.id.worst_perfor);
        pie_chart = (ImageView)findViewById(R.id.pie_chart);
        receive_and_send_ans = (ImageView)findViewById(R.id.receive_and_send_ans);
        view_questions_list = (ImageView)findViewById(R.id.view_questions_list);



        download_exams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
                         }
        });

        worst_perfor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

        pie_chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

        receive_and_send_ans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

        view_questions_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ExamTeacherHome.this, TeacherViewExams.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        super.onResume();
    }
}
