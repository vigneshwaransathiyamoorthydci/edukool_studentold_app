package exam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;



/**
 * Created by pratheeba on 4/21/2017.
 */

;import java.util.ArrayList;

import com.dci.edukool.student.R;

public class SelfTestQuestionAdapter extends BaseAdapter {
    public View view;
    public int currPosition = 0;
    Context context;
    int layoutId;
    ProgressDialog mProgressDialog;
    public static final String MY_PREFS_NAME = "GENERALINFO";

    Holder holder;
    static SharedPreferences preftutorial;
    String user_id_main;
    static ArrayList<QuestionPojo> planListAdapter = new ArrayList<QuestionPojo>();

    public SelfTestQuestionAdapter(Context context, int textViewResourceId,
                          ArrayList<QuestionPojo> list) {

        this.context = context;
        SelfTestQuestionAdapter.planListAdapter = list;
        layoutId = textViewResourceId;
    }

    @Override
    public int getCount() {
        return planListAdapter.size();
    }

    @Override
    public QuestionPojo getItem(int position) {

        return planListAdapter.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        RelativeLayout layout;
        if (convertView == null) {
            // set layout view initialize the resource Id
            layout = (RelativeLayout) View.inflate(context, layoutId, null);
            holder = new Holder();

            holder.quiz_question = (TextView) layout.findViewById(R.id.quiz_question);
            holder.resultinquestion = (ImageView) layout.findViewById(R.id.resultinquestion);
            holder.option1 = (RadioButton) layout.findViewById(R.id.radio0);
            holder.option2 = (RadioButton) layout.findViewById(R.id.radio1);
            holder.option3 = (RadioButton) layout.findViewById(R.id.radio2);
            holder.option4 = (RadioButton) layout.findViewById(R.id.radio3);
/*
            Utils.setTextviewtypeface(2, holder.quiz_question);
            Utils.setTextviewtypeface(2, holder.option2);
            Utils.setTextviewtypeface(2,  holder.option1);
            Utils.setTextviewtypeface(2, holder.option3);
            Utils.setTextviewtypeface(2, holder.option4);
*/


            layout.setTag(holder);
        } else {
            layout = (RelativeLayout) convertView;
            view = layout;
            holder = (Holder) layout.getTag();
        }



        if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("A") ){
            if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("A")) {
                holder.option1.setChecked(true);
                holder.option1.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(false);
                holder.option2.setChecked(false);

            }

            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("B") ){
                holder.option2.setChecked(true);
                holder.option1.setChecked(true);
                holder.option1.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option1.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(false);


            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("C") ){
                holder.option3.setChecked(true);
                holder.option3.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option1.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setTextColor(context.getResources().getColor(R.color.green));
                holder.option3.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option1.setChecked(true);
                holder.option4.setChecked(false);
                holder.option2.setChecked(false);


            }
            else  if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("D") ){
                holder.option4.setChecked(true);
                holder.option1.setChecked(true);
                holder.option4.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option1.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option3.setChecked(false);
                holder.option2.setChecked(false);

            }

        }
        else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("B")){
            if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("A")) {
                holder.option2.setChecked(true);
                holder.option2.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option2.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(false);
                holder.option1.setChecked(true);
            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("B")){
                holder.option2.setChecked(true);
                holder.option2.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(false);
                holder.option1.setChecked(false);

            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("C")){
                holder.option3.setChecked(true);
                holder.option3.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option2.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option2.setChecked(true);
                holder.option4.setChecked(false);
                holder.option1.setChecked(false);

            }

            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("D")){
                holder.option4.setChecked(true);
                holder.option4.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option2.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option2.setChecked(true);
                holder.option3.setChecked(false);
                holder.option1.setChecked(false);

            }




        }

        else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("C")){
            if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("A")) {
                holder.option2.setChecked(false);
                holder.option3.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option3.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(true);
                holder.option4.setChecked(false);
                holder.option1.setChecked(true);
            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("B")){
                holder.option2.setChecked(true);
                holder.option3.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option3.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(true);
                holder.option4.setChecked(false);
                holder.option1.setChecked(false);

            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("C")){
                holder.option3.setChecked(true);
                holder.option3.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.green));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option2.setChecked(false);
                holder.option4.setChecked(false);
                holder.option1.setChecked(false);

            }

            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("D")){
                holder.option4.setChecked(true);
                holder.option4.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option3.setButtonDrawable(R.drawable.radio_bg);
                holder.option4.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option2.setChecked(false);
                holder.option3.setChecked(true);
                holder.option1.setChecked(false);

            }




        }

        else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("D")){
            if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("A")) {
                holder.option2.setChecked(false);
                holder.option4.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option4.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(true);
                holder.option1.setChecked(true);
            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("B")){
                holder.option2.setChecked(true);
                holder.option4.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option4.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(true);
                holder.option1.setChecked(false);

            }
            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("C")){
                holder.option3.setChecked(true);
                holder.option3.setButtonDrawable(R.drawable.radio_bg_red);
                holder.option4.setButtonDrawable(R.drawable.radio_bg);

                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.green));
                holder.option3.setTextColor(context.getResources().getColor(R.color.redcolor));
                holder.option2.setChecked(false);
                holder.option4.setChecked(true);
                holder.option1.setChecked(false);

            }

            else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("D")){
                holder.option4.setChecked(true);
                holder.option4.setButtonDrawable(R.drawable.radio_bg);
                holder.option4.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option2.setChecked(false);
                holder.option3.setChecked(false);
                holder.option1.setChecked(false);

            }




        }

       /* if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("0") ){
            holder.option1.setChecked(true);
            holder.option1.setTextColor(context.getResources().getColor(R.color.green));
            holder.option2.setTextColor(context.getResources().getColor(R.color.white));
            holder.option3.setTextColor(context.getResources().getColor(R.color.white));
            holder.option4.setTextColor(context.getResources().getColor(R.color.white));

        }
        else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("1")){
            holder.option2.setChecked(true);
            holder.option2.setTextColor(context.getResources().getColor(R.color.green));
            holder.option1.setTextColor(context.getResources().getColor(R.color.white));
            holder.option3.setTextColor(context.getResources().getColor(R.color.white));
            holder.option4.setTextColor(context.getResources().getColor(R.color.white));


        }
        else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("2")){
            holder.option3.setChecked(true);
            holder.option3.setTextColor(context.getResources().getColor(R.color.green));
            holder.option2.setTextColor(context.getResources().getColor(R.color.white));
            holder.option1.setTextColor(context.getResources().getColor(R.color.white));
            holder.option4.setTextColor(context.getResources().getColor(R.color.white));

        }

        else {
            holder.option4.setTextColor(context.getResources().getColor(R.color.green));
            holder.option1.setTextColor(context.getResources().getColor(R.color.white));
            holder.option3.setTextColor(context.getResources().getColor(R.color.white));
            holder.option2.setTextColor(context.getResources().getColor(R.color.white));

            holder.option4.setChecked(true);
        }*/

       /* if(planListAdapter.get(position).getQuestion().startsWith("<p>")&&planListAdapter.get(position).getQuestion().endsWith("</p>"))
        {
            String starttag="<p>";
            String endtag="</p>";

            String formatted=planListAdapter.get(position).getQuestion().substring(starttag.length(),planListAdapter.get(position).getQuestion().length()-endtag.length());
            formatted=formatted.replace("<br>","");
            holder.quiz_question.setText(Html.fromHtml(position+1+") "+formatted) );

        }

        else{
            holder.quiz_question.setText(Html.fromHtml(position+1+") "+planListAdapter.get(position).getQuestion()) );


        }*/
        System.out.println(planListAdapter.get(position).getCorrectAnswer()+"correct ans");
        System.out.println(planListAdapter.get(position).getSelectedAnswer() + "selected ans");

        if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase((planListAdapter.get(position).getSelectedAnswer()))){
            holder.resultinquestion.setImageResource(R.drawable.correct);
        }
        else if(planListAdapter.get(position).getSelectedAnswer().equalsIgnoreCase("")){
            if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("A")){
                holder.option1.setChecked(true);
                holder.option1.setButtonDrawable(R.drawable.radio_bg);
                holder.option1.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setChecked(false);
                holder.option4.setChecked(false);
                holder.option2.setChecked(false);

            }
           else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("B")){
                holder.option2.setChecked(true);
                holder.option2.setButtonDrawable(R.drawable.radio_bg);
                holder.option2.setTextColor(context.getResources().getColor(R.color.green));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));

                holder.option3.setChecked(false);
                holder.option4.setChecked(false);
                holder.option1.setChecked(false);

            }
            else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("C")){
                holder.option3.setChecked(true);
                holder.option3.setButtonDrawable(R.drawable.radio_bg);
                holder.option3.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));
                holder.option4.setTextColor(context.getResources().getColor(R.color.white));

                holder.option1.setChecked(false);
                holder.option4.setChecked(false);
                holder.option2.setChecked(false);

            }
            else if(planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("D")){
                holder.option4.setChecked(true);
                holder.option4.setButtonDrawable(R.drawable.radio_bg);
                holder.option4.setTextColor(context.getResources().getColor(R.color.green));
                holder.option2.setTextColor(context.getResources().getColor(R.color.white));
                holder.option3.setTextColor(context.getResources().getColor(R.color.white));
                holder.option1.setTextColor(context.getResources().getColor(R.color.white));

                holder.option3.setChecked(false);
                holder.option1.setChecked(false);
                holder.option2.setChecked(false);

            }
                holder.resultinquestion.setImageResource(R.drawable.warning);
        }

        else {
            holder.resultinquestion.setImageResource(R.drawable.wrong);
        }
        try {


            String starttag="<p>";

            String endtag="</p>";
            String option1=planListAdapter.get(position).getAnswer1();
            option1=option1.replace("<br>","");
            String option2=planListAdapter.get(position).getAnswer2();
            option2=option2.replace("<br>","");



            String option3=planListAdapter.get(position).getAnswer3();
            option3=option3.replace("<br>","");

            String option4=planListAdapter.get(position).getAnswer4();
            option4=option4.replace("<br>","");

            if(option2.startsWith("<p>")&&option2.endsWith("</p>"))
            {
                option2=option2.substring(starttag.length(),option2.length()-endtag.length());
            }
            if(option3.startsWith("<p>")&&option3.endsWith("</p>"))
            {
                option3=option3.substring(starttag.length(),option3.length()-endtag.length());

            }
            if(option4.startsWith("<p>")&&option4.endsWith("</p>"))
            {
                option4=option4.substring(starttag.length(),option4.length()-endtag.length());

            }
            if(option1.startsWith("<p>")&&option1.endsWith("</p>"))
            {
                option1=option1.substring(starttag.length(),option1.length()-endtag.length());

            }

            URLImageParser question = new URLImageParser(holder.quiz_question, context);
            URLImageParser p = new URLImageParser(holder.option1, context);
            URLImageParser p1 = new URLImageParser(holder.option2, context);
            URLImageParser p2 = new URLImageParser(holder.option3, context);
            URLImageParser p3 = new URLImageParser(holder.option4, context);

            Spanned questionSpan = Html.fromHtml(planListAdapter.get(position).getQuestion(), question, null);
            Spanned htmlSpan = Html.fromHtml(option1, p, null);
            Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);
            Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
            Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);

            if(option1.contains("img")){
                holder.option1.setText(htmlSpan);

            }
            else{
                holder.option1.setText(Html.fromHtml(htmlSpan.toString()).toString());

            }
            if(option2.contains("img")){
                holder.option2.setText(htmlSpan1);

            }
            else{
                holder.option2.setText(Html.fromHtml(htmlSpan1.toString()).toString());

            }
            if(option3.contains("img")){
                holder.option3.setText(htmlSpan2);

            }
            else{
                holder.option3.setText(Html.fromHtml(htmlSpan2.toString()).toString());

            }
            if(option4.contains("img")){
                holder.option4.setText(htmlSpan3);

            }
            else{
                holder.option4.setText(Html.fromHtml(htmlSpan3.toString()).toString());

            }

            if(planListAdapter.get(position).getQuestion().contains("img")) {
                holder.quiz_question.setText(questionSpan);
            }
            else{
                holder.quiz_question.setText(Html.fromHtml(planListAdapter.get(position).getQuestion().toString()).toString());

            }

/*
            holder.option1.setText(Html.fromHtml(planListAdapter.get(position).getAnswer1().toString()));
            holder.option2.setText(Html.fromHtml(planListAdapter.get(position).getAnswer2().toString()));
            holder.option3.setText(Html.fromHtml(planListAdapter.get(position).getAnswer3().toString() ));
            holder.option4.setText(Html.fromHtml(planListAdapter.get(position).getAnswer4().toString() ));
*/

        }
        catch (Exception e){

        }

        return layout;
    }

    public int getCurrentPosition() {
        return currPosition;
    }


    //Setting pojo for user comment
    private class Holder {
        public TextView quiz_question;
        public RadioButton option1;
        public RadioButton option2;
        public RadioButton option3;
        public RadioButton option4;
        public ImageView resultinquestion;


    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 1;
    }


}

