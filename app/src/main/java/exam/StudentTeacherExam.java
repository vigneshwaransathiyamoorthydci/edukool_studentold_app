package exam;

/**
 * Created by pratheeba on 4/24/2017.
 */
public class StudentTeacherExam {

    public int getExam_id() {
        return exam_id;
    }

    public void setExam_id(int exam_id) {
        this.exam_id = exam_id;
    }

    public String getStudent_roll_number() {
        return student_roll_number;
    }

    public void setStudent_roll_number(String student_roll_number) {
        this.student_roll_number = student_roll_number;
    }

    public String getSelected_answer() {
        return selected_answer;
    }

    public void setSelected_answer(String selected_answer) {
        this.selected_answer = selected_answer;
    }

    int exam_id;
    String student_roll_number;
    String selected_answer;
    String correct_answer;

    public String getCorrect_answer() {
        return correct_answer;
    }

    public void setCorrect_answer(String correct_answer) {
        this.correct_answer = correct_answer;
    }

    public String getOption_answer() {
        return option_answer;
    }

    public void setOption_answer(String option_answer) {
        this.option_answer = option_answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    String option_answer;
    String question;


    public StudentTeacherExam(){

    }

    public StudentTeacherExam(int exam_id, String student_roll_number, String selected_answer){
        this.exam_id = exam_id;
        this.student_roll_number = student_roll_number;
        this.selected_answer = selected_answer;
    }

    public StudentTeacherExam(int exam_id,String question, String option_answer, String correct_answer){
        this.exam_id = exam_id;
        this.question = question;
        this.option_answer = option_answer;
        this.correct_answer = correct_answer;
    }

}
