package exam;

/**
 * Created by pratheeba on 4/25/2017.
 */
public class DataModel {

    String name;
    int examId;

    public boolean isClick() {
        return click;
    }

    public void setClick(boolean click) {
        this.click = click;
    }

    boolean click;
    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    String version_number;
    String feature;

    public DataModel(String name) {
        this.name=name;

    }

    public DataModel(String name,int examId) {
        this.name=name;
        this.examId=examId;

    }

    public String getName() {
        return name;
    }



}
