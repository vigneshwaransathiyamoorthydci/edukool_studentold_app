package exam;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Utils.Service;
import helper.RoundedImageView;
import com.dci.edukool.student.R;
import com.dci.edukool.student.SqliteOpenHelperDemo;

/**
 * Created by pratheeba on 4/28/2017.
 */
public class StudentExamListActivity extends Activity{
    ArrayList<ExamPojo> examPojo;
    ExamListAdapter adapter;
    private RelativeLayout linearLayout1;
    private RelativeLayout linearLayout2;
    private RelativeLayout subjectlay;

    BroadcastReceiver questionreceiver,subjectreceiver;
    ListView listView,self_asses_ques_ans_list;
    private LinearLayout mainLinearLayout;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    SelfTestQuestionAdapter selfevalutation_adapter;
    static JSONArray quesList;
    TextView stdname;
    DatabaseHandler db;
    int ExamIDValue;
    String Question;
    String OptionA;
    String OptionB;
    String OptionC;
    String OptionD;
    String  CorrectAnswer;
    int ObtainedScore;
    String DateAttended;
    String studentname,profilename;
    String StudentID;
    GridView exam_list_grid;

    String RollNo;
    ArrayList<Integer> idlist = new ArrayList<Integer>();
    String description,descriptionval;
    private TextView headerText,obtainedscoreval,examname,dateattended;
   ImageView back;
    ArrayList<String>bookshelf;
    ArrayList<Integer>bookshelfid;
    AssesGridAdapter exam_grid_adapter;

    int batid;
    RoundedImageView profileimage;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.student_exam_list_layout);

        profileimage= (RoundedImageView) findViewById(R.id.imageView6);
        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);



        stdname= (TextView)findViewById(R.id.studentname);
        linearLayout1 = (RelativeLayout) View.inflate(this,
                R.layout.student_self_asses_list, null);
        linearLayout2 = (RelativeLayout) View.inflate(this,
                R.layout.self_asses_ques_ans_list, null);
        subjectlay = (RelativeLayout) View.inflate(this,
                R.layout.subject_self_asses_grid, null);


        exam_list_grid= (GridView)subjectlay. findViewById(R.id.gridView);

        back= (ImageView) findViewById(R.id.back);
        obtainedscoreval = (TextView) linearLayout2.findViewById(R.id.obtainedscore);
        dateattended = (TextView) linearLayout2.findViewById(R.id.dateattended);
        examname= (TextView) linearLayout2.findViewById(R.id.examname);
        bookshelf=new ArrayList<>();
        bookshelfid=new ArrayList<>();
        if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }

        examPojo= new ArrayList<>();
        db=new DatabaseHandler(this);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SqliteOpenHelperDemo obj=new SqliteOpenHelperDemo(getApplicationContext());

        Cursor stdcursor=obj.retrive("tblStudent");
        while(stdcursor.moveToNext()){
            batid= stdcursor.getInt(stdcursor.getColumnIndex("BatchID"));

        }


        Cursor subcursor=obj.retriveSubjects(batid);
        while(subcursor.moveToNext()){
            bookshelfid.add(Integer.parseInt(subcursor.getString(subcursor.getColumnIndex("SubjectID"))));
            bookshelf.add(subcursor.getString(subcursor.getColumnIndex("SubjectName")));
        }
      /*  List<QuestionDetails> questiondetailsListusingID = db.getAllExamcompleted();
        for(QuestionDetails cn : questiondetailsListusingID)
        {
            String log = " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() ;           // Writing Contacts to log
            if(idlist.contains(cn.getExamID())){

            }
            else{
                idlist.add(cn.getExamID());
            }

            //examPojo.add(new ExamPojo(cn.getTopicName(),cn.getExamID(),description));
            Log.d("Questionvdsf2: ", log);
        }*/

       /* for(int i = 0; i < idlist.size(); i++){

            description  = db.getAllExamsDetailsusingExamId(idlist.get(i),"TeacherConducted");
            DateAttended  = db.getAllExamsDateusingExamId(idlist.get(i), "TeacherConducted");



            dateattended.setText("Date Attended: "+DateAttended);

            if(description.equalsIgnoreCase("")){

            }
            else{
                examPojo.add(new ExamPojo("",idlist.get(i),description));

            }




            //dataModels.add(new DataModel(description,idlist.get(i)));

        }*/
           /* for(QuestionDetails cn : questiondetailsListusingID)
            {
                String log = " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() ;           // Writing Contacts to log
                String description  = db.getAllExamsDetailsusingExamId(cn.getExamID(),"TeacherConducted");
                Log.d("Questionvdsf2: ", log);
            }*/
        for(int i = 0; i < bookshelf.size(); i++) {
            examPojo.add(new ExamPojo("", bookshelfid.get(i), bookshelf.get(i)));

        }
        listView = (ListView) linearLayout1.findViewById(R.id.listView);
        self_asses_ques_ans_list = (ListView) linearLayout2.findViewById(R.id.listView);
        adapter= new ExamListAdapter(examPojo,getApplicationContext());
        listView.setAdapter(adapter);
        Cursor tc=obj.retrive("tblStudent");
        while(tc.moveToNext())
        {
            studentname = tc.getString(tc.getColumnIndex("FirstName"));
            StudentID   = tc.getString(tc.getColumnIndex("StudentID"));
            RollNo      = tc.getString(tc.getColumnIndex("RollNo"));
            profilename = tc.getString(tc.getColumnIndex("PhotoFilename"));
        }
        stdname.setText(studentname.toUpperCase());
        if(profilename!=null)setbackground(profileimage,profilename);
       /* listView.animate().setDuration(1000).alpha(0).
                withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        listView.setAlpha(1);
                    }
                });*/

        mainLinearLayout.addView(linearLayout1);
        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    Toast.makeText(getApplicationContext(), "onclick", Toast.LENGTH_LONG).show();
                    loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });*/
    }

    void setbackground(ImageView view, String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }


    public void loadQuestions() throws Exception {
        try {
            descriptionval  = db.getAllExamsDetailsusingExamId(ExamIDValue,"TeacherConducted");
            examname.setText(descriptionval);

            DatabaseHandler db = new DatabaseHandler(StudentExamListActivity.this);

            List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(ExamIDValue);
            for (QuestionDetails cn : questiondetailsList) {
                String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                // Writing Contacts to log
                Log.d("Question3: ", log);
                Question =cn.getQuestion();
                OptionA =cn.getOptionA();
                OptionB =cn.getOptionB();
                OptionC =cn.getOptionC();
                OptionD =cn.getOptionD();
                CorrectAnswer = cn.getCorrectAnswer();
                ObtainedScore= cn.getObtainedScore();

                obtainedscoreval.setText("Marks Obtained: "+ObtainedScore);


                try {
                    plan_list_array.add(new QuestionPojo(Question, OptionA, OptionB, OptionC, OptionD, String.valueOf(CorrectAnswer), cn.getStudentAnswer(), studentname, RollNo, StudentID, questiondetailsList.size()+"", "StudentPhotPath",0));
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }

            selfevalutation_adapter = new SelfTestQuestionAdapter(StudentExamListActivity.this, R.layout.self_asses_quesandans,
                    plan_list_array);
            self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
            mainLinearLayout.addView(linearLayout2);


        }
        catch (Exception e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }

    public void loadExamusingSubject(int subjectID) throws Exception {
        List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();

        for (QuestionDetails cn : questiondetailsList) {
            String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
            // Writing Contacts to log
            Log.d("Question: ", log);
        }
        List<ExamDetails> examdetails = db.GetAllExamFromServerTeacher(subjectID + "");

        if(examdetails.size()==0){
            Toast.makeText(getApplicationContext(), "No exams found",Toast.LENGTH_LONG).show();
        }
        else{
            for (ExamDetails cn : examdetails) {

                // dataModels.add(new DataModel(cn.getExamDescription(),cn.getExamID()));
                plan_list_array.add(new QuestionPojo(cn.getExamDescription(),cn.getExamID()));

            }
          //  mainLinearLayout.removeView(subjectlay);
            exam_grid_adapter = new AssesGridAdapter(StudentExamListActivity.this, R.layout.exams_grid_adapter,
                    plan_list_array);
            exam_list_grid.setAdapter(exam_grid_adapter);
            mainLinearLayout.addView(subjectlay);


        }


    }
    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if(curBrightnessValue>20) {
            float brightness = curBrightnessValue / (float) 255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();


            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }

        /*float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);*/

        questionreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    Bundle bundle = intent.getExtras();
                    ExamIDValue = bundle.getInt("ExamIDValue");
                    plan_list_array.clear();
                   // loadQuestions();
                    loadExamusingSubject(ExamIDValue);
                    exam_grid_adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent=new IntentFilter("Exam");
        registerReceiver(questionreceiver, intent);

        subjectreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    Bundle bundle = intent.getExtras();
                    ExamIDValue = bundle.getInt("ExamIDValue");
                    plan_list_array.clear();
                     //loadQuestions();
                    exam_grid_adapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent1=new IntentFilter("ExamSubject");
        registerReceiver(subjectreceiver,intent1);



    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}
