package exam;

/**
 * Created by pratheeba on 5/4/2017.
 */
public class QuestionDetails {


       int QuestionID;

    public int getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(int questionID) {
        QuestionID = questionID;
    }

    int ExamID;
       int TopicID;
       String TopicName;
       int AspectID;
       String Aspect;
       String Question;
       int QuestionNumber;
       String OptionA;
       String OptionB;
       String OptionC;

    public String getResultFromTeacher() {
        return ResultFromTeacher;
    }

    public void setResultFromTeacher(String resultFromTeacher) {
        ResultFromTeacher = resultFromTeacher;
    }

    String ResultFromTeacher;



    public String getTopicName() {
        return TopicName;
    }

    public void setTopicName(String topicName) {
        TopicName = topicName;
    }



    public String getAspect() {
        return Aspect;
    }

    public void setAspect(String aspect) {
        Aspect = aspect;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }


    public String getOptionA() {
        return OptionA;
    }

    public void setOptionA(String optionA) {
        OptionA = optionA;
    }

    public String getOptionB() {
        return OptionB;
    }

    public void setOptionB(String optionB) {
        OptionB = optionB;
    }

    public String getOptionC() {
        return OptionC;
    }

    public void setOptionC(String optionC) {
        OptionC = optionC;
    }

    public String getOptionD() {
        return OptionD;
    }

    public void setOptionD(String optionD) {
        OptionD = optionD;
    }

    public String getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        CorrectAnswer = correctAnswer;
    }





    public String getStudentAnswer() {
        return StudentAnswer;
    }

    public void setStudentAnswer(String studentAnswer) {
        StudentAnswer = studentAnswer;
    }


    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    String OptionD;
       String CorrectAnswer;
       int Mark;
       int Negative_Mark;

    public int getExamID() {
        return ExamID;
    }

    public void setExamID(int examID) {
        ExamID = examID;
    }

    public int getTopicID() {
        return TopicID;
    }

    public void setTopicID(int topicID) {
        TopicID = topicID;
    }

    public int getAspectID() {
        return AspectID;
    }

    public void setAspectID(int aspectID) {
        AspectID = aspectID;
    }

    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }

    public int getMark() {
        return Mark;
    }

    public void setMark(int mark) {
        Mark = mark;
    }

    public int getNegative_Mark() {
        return Negative_Mark;
    }

    public void setNegative_Mark(int negative_Mark) {
        Negative_Mark = negative_Mark;
    }

    public int getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        IsCorrect = isCorrect;
    }

    public int getObtainedScore() {
        return ObtainedScore;
    }

    public void setObtainedScore(int obtainedScore) {
        ObtainedScore = obtainedScore;
    }

    String StudentAnswer;
       int IsCorrect;
       int ObtainedScore;
       String CreatedOn;
       String ModifiedOn;


    public QuestionDetails(){

    }


    public QuestionDetails(int ExamID,String ResultFromTeacher){
        this.ExamID = ExamID;
        this.ResultFromTeacher = ResultFromTeacher;
    }


    public QuestionDetails(int QuestionNumber,int IsCorrect,int ObtainedScore){
        this.QuestionNumber = QuestionNumber;
        this.IsCorrect = IsCorrect;
        this.ObtainedScore = ObtainedScore;

    }

    public QuestionDetails(int QuestionID,int ExamID,int QuestionNumber,String StudentAnswer, int IsCorrect, String CreatedOn, String ModifiedOn ,int ObtainedScore){
        this.QuestionID =  QuestionID;
        this.ExamID = ExamID;
        this.QuestionNumber = QuestionNumber;
        this.StudentAnswer = StudentAnswer;
        this.IsCorrect = IsCorrect;
        this.CreatedOn = CreatedOn;
        this.ModifiedOn = ModifiedOn;
        this.ObtainedScore = ObtainedScore;
    }



    public QuestionDetails(int QuestionID, int ExamID,int TopicID, String TopicName,int AspectID,String Aspect ,String Question,int QuestionNumber,String OptionA,String  OptionB,String OptionC,String OptionD,String CorrectAnswer,int Mark,
                       int Negative_Mark,
                           String StudentAnswer,
                       int IsCorrect,
                       int ObtainedScore,
                       String CreatedOn,
                           String ResultFromTeacher,
                           String ModifiedOn
    ){
        this.QuestionID = QuestionID;
        this.ExamID = ExamID;
        this.TopicID = TopicID;
        this.TopicName = TopicName;
        this.AspectID = AspectID;
        this.Aspect = Aspect;
        this.Question = Question;
        this.QuestionNumber = QuestionNumber;
        this.OptionA = OptionA;
        this.OptionB = OptionB;
        this.OptionC = OptionC;
        this.OptionD = OptionD;
        this.CorrectAnswer = CorrectAnswer;
        this.Mark = Mark;
        this.Negative_Mark = Negative_Mark;
        this.StudentAnswer = StudentAnswer;
        this.IsCorrect = IsCorrect;
        this.ObtainedScore = ObtainedScore;
        this.CreatedOn = CreatedOn;
        this.ResultFromTeacher =ResultFromTeacher;
        this.ModifiedOn = ModifiedOn;

    }
    public QuestionDetails(int QuestionID, int ExamID,int TopicID, String TopicName,int AspectID,String Aspect ,String Question,int QuestionNumber,String OptionA,String  OptionB,String OptionC,String OptionD,String CorrectAnswer,int Mark,
                           int Negative_Mark,
                           String StudentAnswer,
                           int IsCorrect,
                           int ObtainedScore,
                           String CreatedOn,
                           String ModifiedOn
    ){
        this.QuestionID = QuestionID;
        this.ExamID = ExamID;
        this.TopicID = TopicID;
        this.TopicName = TopicName;
        this.AspectID = AspectID;
        this.Aspect = Aspect;
        this.Question = Question;
        this.QuestionNumber = QuestionNumber;
        this.OptionA = OptionA;
        this.OptionB = OptionB;
        this.OptionC = OptionC;
        this.OptionD = OptionD;
        this.CorrectAnswer = CorrectAnswer;
        this.Mark = Mark;
        this.Negative_Mark = Negative_Mark;
        this.StudentAnswer = StudentAnswer;
        this.IsCorrect = IsCorrect;
        this.ObtainedScore = ObtainedScore;
        this.CreatedOn = CreatedOn;
        this.ModifiedOn = ModifiedOn;

    }
}
