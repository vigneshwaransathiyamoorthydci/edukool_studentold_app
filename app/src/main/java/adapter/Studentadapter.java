package adapter;

/**
 * Created by iyyapparajr on 1/4/2017.
 */

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import com.dci.edukool.student.R;


public class Studentadapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity activity;
   // private ArrayList<Trending> posts;
    /* private ArrayList<HashMap<String, String>> photos;
     private ArrayList<HashMap<String, String>> videos;*/
    private String username, access_token;

    private FragmentManager fragmentManager;

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    //Utils utils;
    ImageLoader imageLoader;
    // Cloudinary cloudinary;

    public Studentadapter(Activity activity, FragmentManager fragmentManager,
                         /* ArrayList<Trending> posts,*/
                          RecyclerView recyclerView) {
        this.activity = activity;
       // this.posts=posts;
     //   imageLoader = Teacherapp.getInstance().getImageLoader();

       /// utils=new Utils(activity);
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return 1/*Integer.parseInt(posts.get(position).getType())*/;
    }

    @Override
    public int getItemCount() {
        return 0/*posts.size()*/;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case 1:
                viewHolder = new TextViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.studentlist, parent, false));
                break;

           /* case 2:
                viewHolder = new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_layout, parent, false));
                break;*/
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        int type = getItemViewType(position);
        switch (type) {
            case 1:
                final TextViewHolder textViewHolder = (TextViewHolder) holder;



               /* textViewHolder.likecount.setText(posts.get(position).getVlikecount());
                textViewHolder.viewcount.setText(posts.get(position).getVviewcount());
              //  textViewHolder.thumbnailimage.setTag(position);

                final Charset UTF8_CHARSET = Charset.forName("UTF-16");

                try {
                    byte ptext[] =  posts.get(position).getVtitle().getBytes(UTF8_CHARSET);
                    String value = new String(ptext,  UTF8_CHARSET);
                    textViewHolder.title.setText("" + value);
                }
                catch (Exception e)
                {

                }*/
                //    myString.getBytes("UTF-8")
             /*   utils.setTextviewtypeface(0, textViewHolder.title);
                utils.setTextviewtypeface(0, textViewHolder.viewcount);

                utils.setTextviewtypeface(0,  textViewHolder.likecount);
                textViewHolder.thumbnailimage.setErrorImageResId(R.drawable.viewico);
                textViewHolder.thumbnailimage.setImageUrl(posts.get(position).getVurl(), NetworkController.getInstance().getImageLoader());
*/
               /* textViewHolder.thumbnailimage.setImageUrl(
                     *//*   ToolsUtils.IMAGE_URL_100 + posts.get(position).get("profile_picture")+ "=s100-c"*//*
                        ,
                        NetworkController.getInstance().getImageLoader());*/

// If you are using normal ImageView
               /* imageLoader.get(posts.get((Integer.parseInt(textViewHolder.thumbnailimage.getTag().toString()))).getVurl(), new ImageLoader.ImageListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Log.e(TAG, "Image Load Error: " + error.getMessage());
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            // load image into imageview
                            textViewHolder.thumbnailimage.setImageBitmap(response.getBitmap());
                        }
                    }
                });*/

                // textViewHolder.thumbnailimage.setImageUrl(posts.get(position).getVurl(),NetworkController.getInstance().getImageLoader());
                break;

        }
    }

    public void addFooter() {
       /* HashMap<String, String> map = new HashMap<>();
        map.put("type_number", "4");
        posts.add(map);*/
       /* Trending trend =new Trending();
        trend.setType("2");
        posts.add(trend);
        notifyItemChanged(posts.size() - 1);*/
    }

    public void removeFooter() {
       /* posts.remove(posts.size() - 1);
        notifyItemRemoved(posts.size());*/
    }

    public static class TextViewHolder extends RecyclerView.ViewHolder {
        public NetworkImageView thumbnailimage;
        public TextView viewcount;
        public TextView likecount;
        public TextView daysago;
        public TextView title;

        public TextViewHolder(View itemView) {
            super(itemView);

         /*   thumbnailimage= (NetworkImageView) itemView.findViewById(R.id.thumbnailimage);
            viewcount= (TextView) itemView.findViewById(R.id.viewcounttext);
            likecount= (TextView) itemView.findViewById(R.id.likecouttext);
            daysago= (TextView) itemView.findViewById(R.id.howmuchagotext);
            title= (TextView) itemView.findViewById(R.id.titletext);*/

         /*   explore_text_profile_img = (NetworkImageView) itemView.findViewById(R.id.explore_text_profile_img);
            explore_name_text = (TextView) itemView.findViewById(R.id.explore_name_text);
            username_explore_text_item = (TextView) itemView.findViewById(R.id.username_explore_text_item);
            explore_text_time_text = (TextView) itemView.findViewById(R.id.explore_text_time_text);
            explore_message_text = (TextView) itemView.findViewById(R.id.explore_message_text);
            explore_text_like_img = (ImageView) itemView.findViewById(R.id.explore_text_like_img);
            explore_text_comment_img = (ImageView) itemView.findViewById(R.id.explore_text_comment_img);
            explore_text_like_count_text = (TextView) itemView.findViewById(R.id.explore_text_like_count_text);
            explore_text_comment_count_text = (TextView) itemView.findViewById(R.id.explore_text_comment_count_text);
            explore_text_hzm = (TextView) itemView.findViewById(R.id.explore_text_hzm);*/
        }
    }



    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }


}