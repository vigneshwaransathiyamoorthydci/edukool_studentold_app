package books;

/**
 * Created by kirubakaranj on 5/11/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;


import android.widget.Toast;


import org.ebookdroid.ui.viewer.ViewerActivity;

import com.dci.edukool.student.R;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class VideoAdapter extends ArrayAdapter<Videoname> {

    private ArrayList<Videoname> countryList;
    Context con;
    // CoolReader mactivity;

    public VideoAdapter(Context context, int textViewResourceId,
                             ArrayList<Videoname> countryList) {
        super(context, textViewResourceId, countryList);
        this.countryList = new ArrayList<Videoname>();
        con=context;
        this.countryList.addAll(countryList);
    }

    private class ViewHolder {
        ImageView bookiamge;
        TextView name,bpath;
        RelativeLayout parentlayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.booklistitem, null);

            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.studentnametext);
            holder.bpath = (TextView) convertView.findViewById(R.id.bpath);
            holder.bookiamge = (ImageView) convertView.findViewById(R.id.studntimageview);
            holder.parentlayout = (RelativeLayout) convertView.findViewById(R.id.parentlayout);
            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Videoname bookname = countryList.get(position);

        holder.name.setText(bookname.getContenttitle());
        holder.bpath.setText(bookname.getBookpath());
        holder.parentlayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView bookpath = (TextView) v.findViewById(R.id.bpath);

                TextView   bookname = (TextView) v.findViewById(R.id.studentnametext);
                String pattern1=".jpg";
                String pattern2=".png";
                String pattern3=".JPEG";
                String pattern4=".PNG";
                String pattern5=".pdf";
                String pattern6=".doc";
                if( bookpath.getText().toString().toUpperCase().endsWith(pattern5.toUpperCase())) {


                    Uri uri = Uri.parse(bookpath.getText().toString());
                    File file=new File(bookpath.getText().toString());

                    Intent intent = new Intent(Intent.ACTION_VIEW,  Uri.fromFile(file));
                    intent.setClass(con, ViewerActivity.class);
                    intent.putExtra("bookname",bookname.getText().toString());
                    con.startActivity(intent);
                   /* Uri uri = Uri.parse(bookpath.getText().toString());

                    //Uri uri = Uri.parse("file:///android_asset/" + TEST_FILE_NAME);
                    Intent intent = new Intent(con, MuPDFActivity.class);
                    intent.putExtra("linkhighlight", true);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(uri);

                    //if document protected with password
                    intent.putExtra("password", "encrypted PDF password");

                    //if you need highlight link boxes
                    intent.putExtra("linkhighlight", true);

                    //if you don't need device sleep on reading document
                    intent.putExtra("idleenabled", false);

                    //set true value for horizontal page scrolling, false value for vertical page scrolling
                    intent.putExtra("horizontalscrolling", true);

                    //document name
                    intent.putExtra("docname", bookname.getText().toString());

                    con.startActivity(intent);*/



                }else if( bookpath.getText().toString().toUpperCase().endsWith(pattern1.toUpperCase())|| bookpath.getText().toString().toUpperCase().endsWith(pattern2.toUpperCase())|| bookpath.getText().toString().toUpperCase().endsWith(pattern3.toUpperCase())|| bookpath.getText().toString().toUpperCase().endsWith(pattern4.toUpperCase())) {
                  //  Toast.makeText(con,"Under Construction",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(v.getContext(),
                            ImageActivity.class);
                    Bundle bun = new Bundle();
                    Log.d("selectimage", bookpath.getText().toString());

                    bun.putString("path", bookpath.getText().toString());
                    intent.putExtras(bun);
                    v.getContext().startActivity(intent);
                }

                else if(bookpath.getText().toString().toUpperCase().endsWith(pattern6.toUpperCase())){
                    Toast.makeText(con,"Under Construction",Toast.LENGTH_LONG).show();
                }
                else if(bookpath.getText().toString().toUpperCase().endsWith(".mp4".toUpperCase()) || bookpath.getText().toString().toUpperCase().endsWith(".3gp".toUpperCase()) || bookpath.getText().toString().toUpperCase().endsWith(".wmv".toUpperCase())){
                 //   Toast.makeText(v.getContext(), bookpath.getText().toString(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(v.getContext(),
                            VideoActivity.class);
                    Bundle bun = new Bundle();
                    Log.d("selectimage", bookpath.getText().toString());

                    bun.putString("path", bookpath.getText().toString());
                    intent.putExtras(bun);
                    v.getContext().startActivity(intent);

                }
            }
        });
        // holder.code.setText(" (" +  country.getCode() + ")");
        //  holder.name.setText(bookname.getName());

        holder.name.setTag(bookname);
        String pattern1=".jpg";
        String pattern2=".png";
        String pattern3=".JPEG";
        String pattern4=".PNG";
        String pattern5=".pdf";

      try {
          if (bookname.getBookpath().toUpperCase().contains(".mp4".toUpperCase()) || bookname.getBookpath().toUpperCase().contains(".3gp".toUpperCase()) || bookname.getBookpath().toUpperCase().contains(".wmv".toUpperCase())) {

              holder.bookiamge.setImageResource(R.drawable.videorose);
          } else if (bookname.getBookpath().toUpperCase().endsWith(pattern1.toUpperCase()) || bookname.getBookpath().toUpperCase().endsWith(pattern2.toUpperCase()) || bookname.getBookpath().toUpperCase().endsWith(pattern3.toUpperCase()) || bookname.getBookpath().toUpperCase().endsWith(pattern4.toUpperCase())) {
              holder.bookiamge.setImageResource(R.drawable.imag);
          } else if (bookname.getBookpath().toUpperCase().endsWith(pattern5.toUpperCase())) {
              holder.bookiamge.setImageResource(R.drawable.bookrose);
          }
      }catch(NullPointerException e){Log.d("nullex",e.toString());}


        return convertView;

    }

}