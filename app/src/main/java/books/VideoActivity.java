package books;

/**
 * Created by kirubakaranj on 5/2/2017.
 */

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.dci.edukool.student.R;

import java.lang.reflect.Method;

import Utils.Service;

public class VideoActivity extends AppCompatActivity {


    private VideoView videoView;
    private int position = 0;
    private MediaController mediaController;
    ImageView back;
    BroadcastReceiver movepage;
    public static boolean videoboolean;
    public static Activity videoact=null;


    String videopath;
    //public static
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videolayout);

       back=(ImageView)findViewById(R.id.exit);

        videoView = (VideoView) findViewById(R.id.videoView);
        Intent i=getIntent();
        Bundle bun=i.getExtras();
        videoact=VideoActivity.this;
        videopath= bun.getString("path");





        // Set the media controller buttons
        if (mediaController == null) {
            mediaController = new MediaController(VideoActivity.this);

            // Set the videoView that acts as the anchor for the MediaController.
            mediaController.setAnchorView(videoView);


            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
        }


        try {
            // ID of video file.
            int id = this.getRawResIdByName("isac");
            videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + id));
          videoView.setVideoURI(Uri.parse(videopath));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoView.requestFocus();


        // When the video file ready for playback.
        videoView.setOnPreparedListener(new OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VideoActivity.this.finish();
            }
        });
    }

    // Find ID corresponding to the name of the resource (in the directory raw).
    public int getRawResIdByName(String resName) {
        String pkgName = this.getPackageName();
        // Return 0 if not found.
        int resID = this.getResources().getIdentifier(resName, "raw", pkgName);
        Log.i("AndroidVideoView", "Res Name: " + resName + "==> Res ID = " + resID);
        return resID;
    }

    @Override
    protected void onResume() {
        super.onResume();

       /* float curBrightnessValue=0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
            float brightness = curBrightnessValue / (float)255;
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
*/
        movepage=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

				/*Intent in=new Intent("filemove");
				in.putExtra("page",file[1].toString());
				sendBroadcast(in);*/
                try

                {
                    finish();
                  /*  if(intent.getExtras().getBoolean("close",false))
                    {
                       // Toast.makeText(VideoActivity.this,"close",Toast.LENGTH_LONG).show();

                        finish();
                    }*/
                   /* else {
						*//*Toast.makeText(MuPDFActivity.this,"closenot",Toast.LENGTH_LONG).show();
*//*
                        // mDocView.setDisplayedViewIndex(Integer.parseInt(intent.getStringExtra("page")));

                    }*/

                }
                catch (Exception e)
                {

                }

            }
        };
        IntentFilter in=new IntentFilter("filemove");
        registerReceiver(movepage,in);
    }



    // When you change direction of phone, this method will be called.
    // It store the state of video (Current position)
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // Store current position.
        savedInstanceState.putInt("CurrentPosition", videoView.getCurrentPosition());
        videoView.pause();
    }


    // After rotating the phone. This method is called.
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Get saved position.
        position = savedInstanceState.getInt("CurrentPosition");
        videoView.seekTo(position);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(movepage);
    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia","hia");
        super.onWindowFocusChanged(hasFocus);
        try
        {
            if(!hasFocus)
            {
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);
            }
        }
        catch(Exception ex)
        {
        }
    }
}