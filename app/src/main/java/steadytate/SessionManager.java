package steadytate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

@SuppressLint("NewApi")
public class SessionManager {
    private static SessionManager sessionMgrObj = null;
    public final String KEY_SP_PASSWORD_LAST_CONNECTED = "password";
    public final String KEY_SP_SSID_LAST_CONNECTED = "ssid";
    private final String KEY_IS_FIRST_TIME_MASTER_SYNC = "firstTime";
    private final String KEY_IS_MIRRORING_ON = "isMirroringOn";
    private final String KEY_MDM_PASS_PIN = "mdm_pass_pin";
    private final String KEY_NO_OF_APKS = "noOfApk";
    private final String KEY_SP_INSTITUTION_TYPE = "institution_type";
    private final String KEY_SP_IS_DOWNLOADED = "downloaded";
    private final String KEY_SP_IS_FIRST_RUN_ON_BIRTHDAY = "isFirstRunbday";
    private final String KEY_SP_IS_SWITCH_APP = "switchApp";
    private final String KEY_SP_IS_TEACHER_CONNECTED = "isTeacherConnectedPersistance";
    private final String KEY_SP_MASTER_PREFERENCE = "masterprefs";
    private final String KEY_SP_MDM = "mdm";
    private final String KEY_SP_SSID_TYPE_CAPABILITIES = "type";
    private final String KEY_SP_STAFF_OR_STUDENT_ID = "staff_or_student_id";
    private final String KEY_SP_USER_TYPE = "user_type";
    private final String KEY_STUDENT_ID_SINGLE_LOGIN = "student_id_single_login";
    private final String KEY_VIOLATED_HOME = "home_screen_changes";
    private final String KEY_VIOLATED_STUDENT = "violated_student";
    private boolean autoEnableWifi;
    private Context context;
    private SharedPreferences sharedPreferenceObj;
    private Editor spEditorObj;

    private SessionManager(Context paramContext) {
        this.context = paramContext;
        this.sharedPreferenceObj = paramContext.getSharedPreferences("masterprefs", 1);
        this.spEditorObj = this.sharedPreferenceObj.edit();
    }

    public static SessionManager getInstance(Context paramContext) {
        if (sessionMgrObj != null)
            return sessionMgrObj;
        sessionMgrObj = new SessionManager(paramContext);
        return sessionMgrObj;
    }

    private void activateDeviceAdminSetPassword() {
        // HomeScreen.mTaskHandler.sendEmptyMessage(206);
    }

    private void deactivateDeviceAdminResetPassword() {
        //  HomeScreen.mTaskHandler.sendEmptyMessage(207);
    }

    public boolean getSpDownloadedMasterSync() {
        return this.sharedPreferenceObj.getBoolean("downloaded", false);
    }

    public String getSpInstituion_type() {
        return this.sharedPreferenceObj.getString("institution_type", "error");
    }

    public boolean getSpIsFirstRunOnBirthday() {
        return this.sharedPreferenceObj.getBoolean("isFirstRunbday", false);
    }

    public boolean getSpIsMirroringOn() {
        return this.sharedPreferenceObj.getBoolean("isMirroringOn", false);

    }

    public boolean getSpIsTeacherConnected() {
        return this.sharedPreferenceObj.getBoolean("isTeacherConnectedPersistance", false);
    }

    public String getSpMdm() {
        return this.sharedPreferenceObj.getString("mdm", "error");
    }

    public String getSpMdmPassPin() {
        return this.sharedPreferenceObj.getString("mdm_pass_pin", "825643");
    }

    public int getSpNoOfApks() {
        return this.sharedPreferenceObj.getInt("noOfApk", 0);
    }

    public String getSpPasswordLastConnected() {
        return this.sharedPreferenceObj.getString("password", "");
    }

    public String getSpSsidLastConnected() {
        return this.sharedPreferenceObj.getString("ssid", "");
    }

    public int getSpStaffOrStudent() {
        return this.sharedPreferenceObj.getInt("staff_or_student_id", 0);
    }

    public String getSpStudentIdSingleLogin() {
        return this.sharedPreferenceObj.getString("student_id_single_login", "");
    }

    public boolean getSpSwitchApp() {
        return this.sharedPreferenceObj.getBoolean("switchApp", false);
    }

    public String getSpUserType() {
        return this.sharedPreferenceObj.getString("user_type", "");
    }

    public boolean getSpViolatedStudent() {
        return this.sharedPreferenceObj.getBoolean("violated_student", false);
    }

    public boolean getSpViolatedStudentChangeHome() {
        return this.sharedPreferenceObj.getBoolean("home_screen_changes", false);
    }

    public boolean isAutoEnableWifi() {
        return this.autoEnableWifi;
    }

    public void setAutoEnableWifi(boolean paramBoolean) {
        this.autoEnableWifi = paramBoolean;
    }

    public void putSpDownloadedMasterSync(boolean paramBoolean) {
        this.spEditorObj.putBoolean("downloaded", paramBoolean);
        this.spEditorObj.commit();
    }

    public void putSpFirstRunOnBirthDay(boolean paramBoolean) {
        this.spEditorObj.putBoolean("isFirstRunbday", paramBoolean);
        this.spEditorObj.commit();
    }

    public void putSpFirstTimeMasterSync(boolean paramBoolean) {
        this.spEditorObj.putBoolean("firstTime", paramBoolean);
        this.spEditorObj.commit();
    }

    @SuppressLint("NewApi")
    public void putSpInstituion_type(String paramString) {
        this.spEditorObj.putString("institution_type", paramString);
        this.spEditorObj.apply();
        this.spEditorObj.commit();
    }

    public void putSpIsMirroringOn(boolean paramBoolean) {
        this.spEditorObj.putBoolean("isMirroringOn", paramBoolean);
        this.spEditorObj.commit();
    }

    public void putSpIsTeacherConnected(boolean paramBoolean) {
        this.spEditorObj.putBoolean("isTeacherConnectedPersistance", paramBoolean);
        if (!this.spEditorObj.commit()) {
            this.spEditorObj.putBoolean("isTeacherConnectedPersistance", paramBoolean);
            this.spEditorObj.commit();
        }
        Intent localIntent;
        if (getSpMdm().equalsIgnoreCase("N")) {

            Toast.makeText(context, "MDM", Toast.LENGTH_LONG).show();
     /* localIntent = new Intent(this.context, MoniterStudentService.class);
      if (paramBoolean)
        this.context.startService(localIntent);*/
        } else {
            return;
        }
        ///  this.context.stopService(localIntent);
    }

    public void putSpLastConnectedSsidPwdType(String paramString1, String paramString2, String paramString3) {
        this.spEditorObj.putString("ssid", paramString1);
        this.spEditorObj.putString("password", paramString2);
        this.spEditorObj.putString("type", paramString3);
        this.spEditorObj.commit();
    }

    public void putSpMdm(String paramString) {
        getSpMdm();
        this.spEditorObj.putString("mdm", paramString);
        this.spEditorObj.apply();
        this.spEditorObj.commit();
    }

    @SuppressLint("NewApi")
    public void putSpMdmPassPin(String paramString) {
        this.spEditorObj.putString("mdm_pass_pin", paramString);
        this.spEditorObj.apply();
        this.spEditorObj.commit();
    }

    public void putSpNoOfApks(int paramInt) {
        this.spEditorObj.putInt("noOfApk", paramInt);
        this.spEditorObj.commit();
    }

    @SuppressLint("NewApi")
    public void putSpStaffOrStudentIdUserTypeMdm(int paramInt, String paramString1, String paramString2) {
        this.spEditorObj.putInt("staff_or_student_id", paramInt);
        this.spEditorObj.putString("user_type", paramString1);
        this.spEditorObj.putString("mdm", paramString2);
        this.spEditorObj.apply();
        this.spEditorObj.commit();
    }

    public void putSpStudentIdSingleLogin(String paramString) {
        this.spEditorObj.putString("student_id_single_login", paramString);
        this.spEditorObj.commit();
    }

    public void putSpSwitchApp(boolean paramBoolean) {
        this.spEditorObj.putBoolean("switchApp", paramBoolean);
        this.spEditorObj.commit();
    }

    @SuppressLint("NewApi")
    public void putSpUserType(String paramString) {
        this.spEditorObj.putString("user_type", paramString);
        this.spEditorObj.apply();
        this.spEditorObj.commit();
    }

    public void putSpViolatedStudent(boolean paramBoolean) {
        this.spEditorObj.putBoolean("violated_student", paramBoolean);
        this.spEditorObj.commit();
    }

    public void putSpViolationStudentChangeHome(boolean paramBoolean) {
        this.spEditorObj.putBoolean("home_screen_changes", paramBoolean);
        this.spEditorObj.commit();
    }

    public void putSsidPwd(String paramString1, String paramString2) {
        this.spEditorObj.putString("ssid", paramString1);
        this.spEditorObj.putString("password", paramString2);
        this.spEditorObj.commit();
    }
}

/* Location:           C:\dex2jar-0.0.9.15\dex2jar-0.0.9.15\classes-dex2jar.jar
 * Qualified Name:     com.dfoeindia.one.master.student.SessionManager
 * JD-Core Version:    0.6.0
 */