package steadytate;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;

public class customviewgroup extends ViewGroup {

	public customviewgroup(Context context) {
		super(context);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		Log.v("customViewGroup", "**********Intercepted");
		return true;
	}
}
