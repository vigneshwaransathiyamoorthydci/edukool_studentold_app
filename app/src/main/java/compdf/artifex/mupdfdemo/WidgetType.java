package compdf.artifex.mupdfdemo;

public enum WidgetType {
	NONE,
	TEXT,
	LISTBOX,
	COMBOBOX
}
