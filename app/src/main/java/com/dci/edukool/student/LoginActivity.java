package com.dci.edukool.student;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import Utils.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import Utils.Utils;
import Utils.Service;
import calendar.CalendarTypePojo;
import drawboard.MyDialog;
import drawboard.Whiteboard;
import exam.DatabaseHandler;
import helper.Permission;
import helper.Rooms;
import steadytate.Password;

/**
 * Created by iyyapparajr on 4/17/2017.
 */
public class LoginActivity extends Activity {

    String ExamIDVal;
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID;
    String ExamCategory;
    String ExamCode;
    int QuestionID;
    String ExamDescription;
    String BatchIDVal;
    String ExamDurationVal;
    int BatchID;
    int ExamDuration;
    int Mark;
    String SubjectIDVal;
    String Subject;
    int ObtainedMark;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    int SubjectID;
    String ExamTypeIDVal;
    int NegativeMark;
    int IsCorrect;
    String MarkForAnswer;
    String ExamType;
    String ExamDate;
    int AspectID;
    int ExamTypeID;
    int TopicID;
    String TopicIDVal, Topic, AspectIDVal, Aspect, QuestionNumberVal;
    int CorrectAnswerVal;
    String CorrectAnswer;
    String StudentAnswer;
    DatabaseHandler db;
    int QuestionNumber;
    String question;


    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    private final Handler handler = new Handler();
    ArrayList<User> arrayOfUsers;
    UsersAdapter adapter;
    ListView listView;
   // private PopupWindow pwindo, listwindo;
    StringBuilder sb = new StringBuilder();
    String wifipass = "";


    EditText username;
    EditText password;
    TextView signin;
    RelativeLayout touch;
    ImageView wifi;
    // Utils utils;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    Utils utils;
    ProgressDialog dia;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY[1]);
        }
        catch (Exception e)
        {

        }*/
        setContentView(R.layout.loginlayouttemp);
        username = (EditText) findViewById(R.id.usernameedittext);
        password = (EditText) findViewById(R.id.passwordedittext);
        db = new DatabaseHandler(this);
        Permission.verifyStoragePermissions(this);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                99);
        utils = new Utils(LoginActivity.this);
        touch = (RelativeLayout) findViewById(R.id.rellayouts);
        signin = (TextView) findViewById(R.id.signinbiutton);
        wifi = (ImageView) findViewById(R.id.wifi);
        pref = getSharedPreferences("student", MODE_PRIVATE);
        edit = pref.edit();
        utils.setEdittexttypeface(2, username);
        utils.setEdittexttypeface(2, password);
        utils.setTextviewtypeface(2, signin);
       /* if(Service.isAirplaneModeOn(this))
        {

            Service.Showalert(this);

        }*/
        if(Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(LoginActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
           // disablePullNotificationTouch();
        }
        else {
            //disablePullNotificationTouch();
        }
           /* Intent intent = new Intent(Activity.this, Service.class);
            startService(intent);*//**//**//*
        }*/
       // disablePullNotificationTouch();
        //username.setText("EK-002-S1496651957");
        // password.setText("EK-002-S1496651957");

        // username.setText("EK-003-S1496834110");
        // password.setText("EK-003-S1496834110");

        /*username.setText("EK-002-S1496652144");
        password.setText("EK-002-S1496652144");*/
        //Password.get(LoginActivity.this).setPassword();
        //Password.get(LoginActivity.this).setPassword("jegan");

        if (pref.getBoolean("login", false)) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
            /*startActivity(new Intent(LoginActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();*/
        }
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (username.getText().toString().isEmpty()) {
                    username.setError("User ID not empty");
                } else if (password.getText().toString().isEmpty()) {
                    password.setError("Password not empty");
                } else {
                    String osversion = Build.VERSION.RELEASE;
                    //String devname = android.os.Build.MODEL;
                    String appversion = BuildConfig.VERSION_NAME;

                    String login_str = "UserName:" + username.getText().toString() + "|Password:" + password.getText().toString() + "|Function:StudentLogin|DeviceType:Android|AppVersion:" + appversion + "|MACAddress:" + "|OSVersion:" + osversion + "|Update:No" + "|GCMKey:" + "|DeviceID:" + "|AppID:" + "|IMEINumber:";
                    login.clear();
                    byte[] data;
                    try {
                        data = login_str.getBytes("UTF-8");
                        String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                        if (utils.hasConnection()) {
                            login.clear();
                            login.add(new BasicNameValuePair("WS", base64_register));

                            Load_Login_WS load_plan_list = new Load_Login_WS(LoginActivity.this, login);
                            load_plan_list.execute();
                        } else {
                            utils.Showalert();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }

               /* edit.putString("name",username.getText().toString());
                edit.commit();
                Intent in=new Intent(LoginActivity.this,MainActivity.class);
                startActivity(in);
                finish();*/
            }
        });
        touch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.hideSoftKeyboard(LoginActivity.this);
            }
        });

        wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Listpopup();
            }
        });

      /*  mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        receiverWifi = new WifiReceiver();
        registerReceiver(receiverWifi, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        if(mainWifi.isWifiEnabled()==false)
        {
            mainWifi.setWifiEnabled(true);
        }*/
        arrayOfUsers = new ArrayList<User>();
        for (int i = 0; i < 6; i++)
            arrayOfUsers.add(new User("Nathan", "San Diego"));


    }

    private void Listpopup() {
      /*  LayoutInflater inflater = (LayoutInflater) LoginActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_popup,
                (ViewGroup) findViewById(R.id.listparent));
        listwindo = new PopupWindow(layout, 600, 600, true);
        listwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);*/
        final MyDialog layout=new MyDialog(LoginActivity.this);
        layout.setContentView(R.layout.list_popup);


        listView = (ListView) layout.findViewById(R.id.list);
        final Button close = (Button) layout.findViewById(R.id.close);

        arrayOfUsers = new ArrayList<User>();
        //   for(int i=0;i<6;i++)
        arrayOfUsers.add(new User("", ""));

        adapter = new UsersAdapter(this, arrayOfUsers);
        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(adapter);

        doInback();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = adapter.getItem(position);
                initiatePopupWindow(user.name);
                // Toast.makeText(getApplicationContext(),"position"+user.name,Toast.LENGTH_LONG).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layout.dismiss();
            }
        });
        layout.show();

    }

    private void initiatePopupWindow(final String ssid) {
        try {
// We need to get the instance of the LayoutInflater
           /* LayoutInflater inflater = (LayoutInflater) LoginActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 450, 130, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);*/
            final MyDialog layout=new MyDialog(LoginActivity.this);
            layout.setContentView(R.layout.screen_popup);


            final EditText password = (EditText) layout.findViewById(R.id.edit);

            Button btnconnect = (Button) layout.findViewById(R.id.button1);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    wifipass = password.getText().toString();
                    if (!wifipass.equalsIgnoreCase("")) {
                        WifiConfiguration wc = new WifiConfiguration();
                        wc.SSID = String.format("\"%s\"", ssid);
                        wc.preSharedKey = String.format("\"%s\"", wifipass);
                        wc.status = WifiConfiguration.Status.ENABLED;
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                        int netId = mainWifi.addNetwork(wc);
                        mainWifi.disconnect();
                        mainWifi.enableNetwork(netId, true);
                        mainWifi.reconnect();
                        doInback();
                    }

                    layout.dismiss();
                }
            });

            Button btncancel = (Button) layout.findViewById(R.id.button2);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layout.dismiss();
                }
            });
            layout.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void doInback() {

        handler.postDelayed(new Runnable() {

            @SuppressLint("WifiManagerLeak")
            @Override
            public void run() {
                // TODO Auto-generated method stub

                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                receiverWifi = new WifiReceiver();
                registerReceiver(receiverWifi, new IntentFilter(
                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                if (mainWifi.isWifiEnabled() == false) {
                    mainWifi.setWifiEnabled(true);
                }

                mainWifi.startScan();
                //  String ssid=getCurrentSsid(getApplicationContext(),mainWifi);

                // doInback();
            }
        }, 1000);

    }

    class secondasync extends AsyncTask<String, String, String> {
        String jsonResponse;

        public secondasync(String jsonResponse) {
            this.jsonResponse = jsonResponse;
        }

        ProgressDialog dia;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(LoginActivity.this);
            dia.setMessage("Downloading...");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {


            // Log.d("JsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                String statusCode = jObj.getString("StatusCode");

                if (status.toString().equalsIgnoreCase("Success")) {
                    if (statusCode.toString().equalsIgnoreCase("200")) {


                        String staffname = jObj.getJSONObject("studentInfo").getString("FirstName");

                        edit.putString("name", staffname);
                        edit.putBoolean("login", true);
                        edit.commit();

                        SqliteOpenHelperDemo obj = new SqliteOpenHelperDemo(getApplicationContext());


                        ContentValues cv = new ContentValues();
                        try {
                            cv.put("StudentID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ID")));
                        } catch (Exception e) {
                        }
                        cv.put("AdmissionNumber", jObj.getJSONObject("studentInfo").getString("AdmissionNumber"));
                        cv.put("DOA", jObj.getJSONObject("studentInfo").getString("AdmissionDate"));

                        String subarray = jObj.getJSONObject("studentInfo").getJSONArray("Room").toString();
                        subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                        subarray = subarray.replace("\"", "");
                        if (subarray.equalsIgnoreCase("")) {
                            cv.put("Room", "0");
                        } else {
                            cv.put("Room", subarray);
                        }
                        cv.put("FirstName", jObj.getJSONObject("studentInfo").getString("FirstName"));
                        cv.put("LastName", jObj.getJSONObject("studentInfo").getString("LastName"));
                        cv.put("DOB", jObj.getJSONObject("studentInfo").getString("DateOfBirth"));
                        cv.put("Gender", jObj.getJSONObject("studentInfo").getString("Gender"));
                        cv.put("Phone_1", jObj.getJSONObject("studentInfo").getString("Phone"));
                        cv.put("Email", jObj.getJSONObject("studentInfo").getString("Email"));
                        cv.put("FatherName", jObj.getJSONObject("studentInfo").getString("FatherName"));
                        cv.put("MotherName", jObj.getJSONObject("studentInfo").getString("MotherName"));
                        cv.put("GuardianMobileNumber", jObj.getJSONObject("studentInfo").getString("GuardianPhone"));
                        try {
                            String s = jObj.getJSONObject("studentInfo").getString("RollNumber");
                            if (Integer.parseInt(s) > 0) {
                                cv.put("RollNo", jObj.getJSONObject("studentInfo").getString("RollNumber"));

                            }
                        } catch (Exception e) {
                            cv.put("RollNo", "0");

                        }
                        try {
                            cv.put("ClassID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ClassId")));
                        } catch (Exception e) {
                        }
                        try {
                            cv.put("BatchID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("Batch")));
                        } catch (Exception e) {
                        }
                        try {
                            cv.put("SchoolID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("SchoolID")));
                        } catch (Exception e) {
                        }
                        //cv.put("PhotoFilename", jObj.getJSONObject("studentInfo").getString("ProfileImage"));
                        cv.put("StudentLoginID", jObj.getJSONObject("studentInfo").getString("StudentLoginID"));


                        SqliteOpen pojo = new SqliteOpen();

                        //for school table
                        pojo.setSchoolid(jObj.getJSONObject("schoolInfo").getString("ID"));
                        pojo.setSchoolName(jObj.getJSONObject("schoolInfo").getString("Name"));
                        pojo.setAcronym(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"));
                        // pojo.setSchoolLogo(jObj.getJSONObject("schoolInfo").getString("Logo"));
                        //pojo.setBackgroundimage(jObj.getJSONObject("schoolInfo").getString("BackgroundImage"));
                        String acrid = jObj.getJSONObject("studentInfo").getString("ID");
                        String bac = jObj.getJSONObject("schoolInfo").getString("BackgroundImage");
                        String schoolfolder = jObj.getJSONObject("schoolInfo").getString("SchoolAcronym");
                        String namefolder = jObj.getJSONObject("studentInfo").getString("FirstName");
                        String stdid = jObj.getJSONObject("studentInfo").getString("ID");

                        File file1 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Assets");
                        file1.mkdirs();

                        // File file2 = new File(Environment.getExternalStorageDirectory(), schoolfolder+"/Users");
                        //   file2.mkdirs();
                        File file0 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid);
                        file0.mkdirs();
                        File file3 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Evaluation");
                        File file4 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material");
                        File file5 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Notes");
                        File file6 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Calendar");
                        File file7 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material" + "/Academic");
                        File file8 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material" + "/Reference");

                        edit.putString("academic", file7.getAbsolutePath());
                        edit.putString("reference", file8.getAbsolutePath());
                        edit.putString("notes", file5.getAbsolutePath());
                        edit.putString("timetable", file0.getAbsolutePath());
                        edit.putString("calender", file6.getAbsolutePath());
                        edit.putString("Evaluation", file3.getAbsolutePath());
                        edit.commit();

                        file3.mkdirs();
                        file4.mkdirs();
                        file5.mkdirs();
                        file6.mkdirs();
                        file7.mkdirs();
                        file8.mkdirs();

                        // createFolder(schoolfolder,jObj.getJSONObject("studentInfo").getString("FirstName"),jObj.getJSONObject("schoolInfo").getString("Logo"),jObj.getJSONObject("studentInfo").getString("ProfileImage"),bac);
                        String schoollogo = jObj.getJSONObject("schoolInfo").getString("Logo");
                        String stdprofile = jObj.getJSONObject("studentInfo").getString("ProfileImage");


                        String stdimagedb = file1.getAbsolutePath() + "/" + stdprofile.substring(stdprofile.lastIndexOf("/") + 1, stdprofile.length());
                        String logoimagedb = file1.getAbsolutePath() + "/" + "logo_" + schoollogo.substring(schoollogo.lastIndexOf("/") + 1, schoollogo.length());
                        String backimagedb = file1.getAbsolutePath() + "/" + "background_" + bac.substring(bac.lastIndexOf("/") + 1, bac.length());
                        cv.put("PhotoFilename", stdimagedb);
                        pojo.setSchoolLogo(logoimagedb);
                        pojo.setBackgroundimage(backimagedb);
                        try {
                            DownloadTask1(schoollogo, file1.getAbsolutePath());
                        } catch (Exception e) {
                        }
                        try {
                            DownloadTask2(bac, file1.getAbsolutePath());
                        } catch (Exception e) {
                        }
                        try {
                            DownloadTask(stdprofile, file1.getAbsolutePath(), stdimagedb, logoimagedb, backimagedb);
                        } catch (Exception e) {

                        }
                        //for classes table
                        pojo.setClassID(jObj.getJSONObject("Classes").getString("ID"));
                        pojo.setClassName(jObj.getJSONObject("Classes").getString("ClassName"));
                        pojo.setSectionName(jObj.getJSONObject("Classes").getString("Section"));
                        pojo.setGrade(jObj.getJSONObject("Classes").getString("Grade"));
                        pojo.setSchoolID(jObj.getJSONObject("Classes").getString("SchoolID"));
                        pojo.setDepartmentID(jObj.getJSONObject("Classes").getString("Department"));

                        edit.putString("wifissid", jObj.getJSONObject("Classes").getString("InternetSSID"));
                        edit.putString("wifipassword", jObj.getJSONObject("Classes").getString("InternetPassword"));
                        edit.putString("classwifissid", jObj.getJSONObject("Classes").getString("InternetSSID"));
                        edit.putString("classwifipassword", jObj.getJSONObject("Classes").getString("InternetPassword"));
                        edit.commit();

                        pojo.setInternetSSID(jObj.getJSONObject("Classes").getString("InternetSSID"));
                        pojo.setInternetPassword(jObj.getJSONObject("Classes").getString("InternetPassword"));
                        pojo.setInternetType(jObj.getJSONObject("Classes").getString("InternetType"));
                        pojo.setPushName(jObj.getJSONObject("Classes").getString("IpushName"));


                        //for batches table
                        ArrayList<Batchpojo> batchlist = new ArrayList<Batchpojo>();
                        for (int i = 0; i < jObj.getJSONObject("Classes").getJSONArray("Batches").length(); i++) {
                            Batchpojo batchpojo = new Batchpojo();
                            if (jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                batchpojo.setBatchId("0");
                            } else {
                                batchpojo.setBatchId(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID"));
                            }

                            if (jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator").equalsIgnoreCase("")) {
                                batchpojo.setCoordinatingStaffID("0");
                            } else {
                                batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            }
                            //batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            batchpojo.setAcademicYear(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("AcademicYear"));
                            batchpojo.setBatchclassid(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Class"));
                            batchpojo.setBatchStartDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("StartDate"));
                            batchpojo.setBatchEndDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("EndDate"));
                            //batchpojo.setTimeTable(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchpojo.setSubjects(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Subjects"));

                            String timetable = jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable");
                            String timedb = file0.getAbsolutePath() + "/" + timetable.substring(timetable.lastIndexOf("/") + 1, timetable.length());
                            batchpojo.setTimeTable(timedb);

                            otherTask(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"), file0.getAbsolutePath());

                            //XmltoFolder(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"),jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchlist.add(batchpojo);
                        }

                        ArrayList<Masterpojo> masterlist = new ArrayList<Masterpojo>();

                        for (int i = 0; i < jObj.getJSONObject("masterInfo").getJSONArray("Subjects").length(); i++) {

                            Masterpojo masterpojo = new Masterpojo();
                            masterpojo.setSubid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));
                            masterpojo.setSubjectname(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectName"));
                            masterpojo.setSubjectdesc(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectDescription"));
                            masterpojo.setCreatedBy(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedBy"));
                            masterpojo.setCreateddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedDate"));
                            masterpojo.setModifiedby(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedBy"));
                            masterpojo.setModifieddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedDate"));
                            masterpojo.setStatus(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("Status"));
                            masterpojo.setSchoolid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SchoolID"));

                            masterlist.add(masterpojo);
                        }
                        JSONArray roomsarray = jObj.getJSONObject("masterInfo").getJSONArray("Rooms");

                        for (int r = 0; r < roomsarray.length(); r++) {
                            Rooms room = new Rooms();
                            JSONObject roomobject = roomsarray.getJSONObject(r);
                            try {
                                room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);
                            } catch (Exception e) {
                                room.setId(0);
                            }
                            room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                            room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                            room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");
                            obj.addmasterroom(room);
                        }

                        ArrayList<CalendarTypePojo> CalendarTypelist = new ArrayList<CalendarTypePojo>();

                        for (int i = 0; i < jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").length(); i++) {

                            CalendarTypePojo calendarTypePojo = new CalendarTypePojo();
                            try {
                                calendarTypePojo.setEventtypeid(Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID")));
                            } catch (Exception e) {
                            }
                            calendarTypePojo.setEventtypename(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Name"));
                            calendarTypePojo.setEventtypeDescription(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Description"));


                            String imagefile = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").substring(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").lastIndexOf("/") + 1, jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").length());

                            try {
                                downloadfile(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image"), file6.getAbsolutePath(), imagefile);
                            } catch (Exception e) {
                            }
                            String imagename = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image");
                            String local_path = file6.getAbsolutePath() + "/" + imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());
                            calendarTypePojo.setEventtypeimage(local_path);


                            CalendarTypelist.add(calendarTypePojo);
                        }
                        //obj.insertCalendarType(CalendarTypelist);


                  /*  ArrayList<CalendarPojo> calendarlist = new ArrayList<CalendarPojo>();

                        for (int i = 0; i < jObj.getJSONArray("Calender").length(); i++) {

                            CalendarPojo calendarPojo = new CalendarPojo();
                            calendarPojo.setEventID(jObj.getJSONArray("Calender").getJSONObject(i).getString("ID"));
                            calendarPojo.setEventName(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventName"));
                            calendarPojo.setEventDescription(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventDescription"));
                            calendarPojo.setEventStartDate(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventStartDate"));
                            calendarPojo.setEventEndDate(jObj.getJSONArray("Calender").getJSONObject(i).getString("EventEndDate"));
                            calendarPojo.setCategory(jObj.getJSONArray("Calender").getJSONObject(i).getString("Category"));
                            calendarPojo.setCategoryID(Integer.parseInt(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryID")));

                            String calenderImage=jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage");
                            String calfile = jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").substring(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").lastIndexOf("/") + 1, jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage").length());
                            String cal_imagepath = file6.getAbsolutePath() + "/" + calenderImage.substring(calenderImage.lastIndexOf("/") + 1, calenderImage.length());
                            try {
                                downloadfile(jObj.getJSONArray("Calender").getJSONObject(i).getString("CategoryImage"), file6.getAbsolutePath(), calfile);
                            } catch (Exception e) {
                            }

                            calendarPojo.setCategoryIcon(cal_imagepath);
                            calendarlist.add(calendarPojo);
                        }
*/
/*
                       ArrayList<ContentPojo> Contentlist = new ArrayList<ContentPojo>();
                        for (int i = 0; i < jObj.getJSONArray("contentDetails").length(); i++) {
                            ContentPojo contentpojo = new ContentPojo();
                            contentpojo.setContentid(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ID"));
                            contentpojo.setContentDescription(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentTitle"));
                            contentpojo.setContentType(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentType"));
                            contentpojo.setCatalog(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog"));
                            contentpojo.setVaporize(jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Vaporize"));
                            contentpojo.setSubject(jObj.getJSONArray("contentDetails").getJSONObject(i).getJSONArray("Subject").toString());


                            String urls = jObj.getJSONArray("contentDetails").getJSONObject(i).getString("ContentFileName");


                            if (jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog").equals("Academic")) {

                                otherTask(urls, file7.getAbsolutePath());

                                String content_filename = file7.getAbsolutePath() + "/" + urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                                contentpojo.setContentFilename(content_filename);

                            } else if (jObj.getJSONArray("contentDetails").getJSONObject(i).getString("Catalog").equals("Reference")) {

                                otherTask(urls, file8.getAbsolutePath());


                                String content_filename1 = file8.getAbsolutePath() + "/" + urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                                contentpojo.setContentFilename(content_filename1);
                            }


                            Contentlist.add(contentpojo);

                        }*/
                        // JSONArray Exam_arr = jObj.getJSONObject("evaluationExams").getJSONArray("exam");

                        //for (int i = 0; i < Exam_arr.length(); i++)
                        //
                        /*{
                            JSONObject objexam = Exam_arr.getJSONObject(i);
                            ExamIDVal = objexam.getString("ExamID");
                            ExamID = Integer.parseInt(ExamIDVal);
                            ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                            try{
                            ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);}
                            catch(Exception e){}
                            ExamCode = objexam.getString("ExamCode");
                            ExamCategory = objexam.getString("ExamCategory");
                            ExamDescription = objexam.getString("ExamDescription");
                            BatchIDVal = objexam.getString("BatchID");
                            ExamDurationVal = objexam.getString("ExamDuration");

                            try {
                                BatchID = Integer.parseInt(BatchIDVal);
                            }catch(Exception e){
                                BatchID = 0;
                            }
                            try {
                                ExamDuration = Integer.parseInt(ExamDurationVal);
                            }catch(Exception e){}
                            SubjectIDVal = objexam.getString("SubjectID");
                            Subject = objexam.getString("Subject");

                            SubjectID = Integer.parseInt(SubjectIDVal);
                            ExamTypeIDVal = objexam.getString("ExamTypeID");

                            ExamType = objexam.getString("ExamType");

                            // ExamDate = objexam.getString("ExamDate");
                           try {
                               ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                           }catch(Exception e){}
                   *//* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*//*
                            // String Questions = objexam.getString("Questions");

                            int ExamSequence = 0;
                            int SchoolID = 0;
                            int IsResultPublished = 0;
                            int ExamShelfID = 0;
                            int ClassID = 0;
                            int TimeTaken = 0;
                            int TotalScore = 0;
                            String DateAttended = "01/06/2017 10:00:00";
                            db.addExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                    BatchID, IsResultPublished,
                                    ExamShelfID,
                                    TimeTaken,
                                    DateAttended,
                                    TotalScore));
                            List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

                            JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                            QuizWrapper newItemObject = null;

                            for (int j = 0; j < jsonArrayquestion.length(); j++) {
                                JSONObject jsonChildNode = null;
                                try {
                                    jsonChildNode = jsonArrayquestion.getJSONObject(j);
                                    String QuestionIDval = jsonChildNode.getString("QuestionID");
                                    try{
                                    QuestionID = Integer.parseInt(QuestionIDval);}catch(Exception e){}
                                    TopicIDVal = jsonChildNode.getString("TopicID");
                                    if (TopicIDVal.equalsIgnoreCase("")) {
                                        TopicID = 1;
                                    } else {
                                        TopicID = Integer.parseInt(TopicIDVal);

                                    }

                                    // int TopicID=1;
                                    Topic = jsonChildNode.getString("Topic");
                                    AspectIDVal = jsonChildNode.getString("AspectID");
                                    if (AspectIDVal.equalsIgnoreCase("")) {
                                        AspectID = 1;
                                    } else {
                                        AspectID = Integer.parseInt(AspectIDVal);

                                    }
                                    Aspect = jsonChildNode.getString("Aspect");
                                    QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                                    try {
                                        QuestionNumber = Integer.parseInt(QuestionNumberVal);
                                    }catch(Exception e){}
                                    question = jsonChildNode.getString("Question");
                                    JSONArray options = jsonChildNode.getJSONArray("Options");
                                    CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                                    CorrectAnswerVal = 1;
                                    MarkVal = jsonChildNode.getString("Mark");
                                    try {
                                        Mark = Integer.parseInt(MarkVal);
                                    }catch(Exception e){}
                                    NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                                    try {
                                        NegativeMark = Integer.parseInt(NegativeMarkVal);
                                    }catch(Exception e){}
                                    String Created_on = "01/06/2017 10:00:00";
                                    String ModifiedOn = "01/06/2017 10:00:00";
                                    StudentAnswer = "";
                                    newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                                    if (options.length() > 3) {
                                        db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(), CorrectAnswer, Mark,
                                                NegativeMark, StudentAnswer,
                                                IsCorrect,
                                                ObtainedMark,
                                                Created_on,
                                                "",
                                                ModifiedOn));
                                    } else if (options.length() > 2) {
                                        db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "", CorrectAnswer, Mark,
                                                NegativeMark, StudentAnswer,
                                                IsCorrect,
                                                ObtainedMark,
                                                Created_on,
                                                "",
                                                ModifiedOn));
                                    } else if (options.length() > 1) {
                                        db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                NegativeMark, StudentAnswer,
                                                IsCorrect,
                                                ObtainedMark,
                                                Created_on,
                                                "",
                                                ModifiedOn));
                                    }
                                    List<ExamDetails> examdetailsList = db.getAllExamsDetails();
                                    List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                                    Calendar calendar = Calendar.getInstance();
                                    SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                    String strDate = mdformat.format(calendar.getTime());

                                    for (ExamDetails cn : examdetailsList) {
                                        String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID() + " ,BatchID: " + cn.getBatchID() + " ,IsResultPublished: " + cn.getIsResultPublished() + " ,ExamShelfID: " + cn.getExamShelfID() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                                        // Writing Contacts to log
                                      //  Log.d("Exam2: ", log);
                                    }

                                    for (QuestionDetails cn : questiondetailsList) {
                                        String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                                        // Writing Contacts to log
                                       // Log.d("Question2: ", log);
                                    }

                                    jsonObject.add(newItemObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            // Adding child data

                            // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                            //jsonObject.add(newItemObject);

                        }*/


                        ArrayList<Staffpojo> stafflist = new ArrayList<Staffpojo>();
                        for (int i = 0; i < jObj.getJSONArray("staffInfo").length(); i++) {
                            Staffpojo staffpojo = new Staffpojo();
                            staffpojo.setStaffid(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID"));
                            staffpojo.setDOJ(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfJoining"));
                            staffpojo.setFirstName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FirstName"));
                            staffpojo.setLastName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("LastName"));
                            staffpojo.setGender(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Gender"));
                            staffpojo.setDOB(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfBirth"));
                            staffpojo.setMaritalStatusID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MaritalStatus"));
                            staffpojo.setSpouseName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("SpouseName"));
                            staffpojo.setFatherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FatherName"));
                            staffpojo.setMotherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MotherName"));
                            staffpojo.setPhone(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("PhoneNumber"));
                            staffpojo.setEmail(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Email"));
                            staffpojo.setPhotoFilename(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ProfileImage"));
                            staffpojo.setStaffCategoryID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory"));
                            stafflist.add(staffpojo);
                        }

                        obj.insert(cv, pojo, batchlist, masterlist, stafflist, CalendarTypelist);

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                if (dia.isShowing())
                    dia.cancel();
//              Log.d("thisis",e.toString());
                System.out.println(e.toString() + "thisis");
            }


            return null;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if (dia.isShowing())
                dia.cancel();

        }

    }

    class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public Load_Login_WS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(LoginActivity.this);
            dia.setMessage("GET IN");
            dia.setCancelable(false);
            dia.show();


        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service sr = new Service(LoginActivity.this);
                jsonResponseString = sr.getLogin(login,
                        Url.base_url);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if (dia.isShowing())
                dia.cancel();
            // Log.d("JsonResponse", "Login" + jsonResponse);
            try {
                JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                String statusCode = jObj.getString("StatusCode");

                if (status.toString().equalsIgnoreCase("Success")) {
                    if (statusCode.toString().equalsIgnoreCase("200")) {

                        edit.putString("username", username.getText().toString());
                        edit.putString("password", password.getText().toString());
                        edit.commit();

                    }
                }
            } catch (Exception e) {
            }

            new secondasync(jsonResponse).execute();


        }
    }


    public void DownloadTask(String urls, String filepath, String stdimagedb, String logoimagedb, String backimagedb) {


        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;

                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
        }


    }


    void DownloadTask1(String urls, String filepath) {

        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());
            filename = "logo_" + filename;
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            //    Log.e("Error: ", e.getMessage());
        }


    }


    void DownloadTask2(String urls, String filepath) {

        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());
            filename = "background_" + filename;
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            //   Log.e("Error: ", e.getMessage());
        }


    }

    public void otherTask(String urls, String filepath) {


        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());

            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();


            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);
            // Output stream
            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
        }


    }

    public class otherTask extends AsyncTask<String, Void, Bitmap> {

        String urls, filepath;

        public otherTask(String urls, String filepath) {
            this.urls = urls;
            this.filepath = filepath;
        }

        @Override
        protected Bitmap doInBackground(String... useles) {


            int count;
            try {
                String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());

                URL url = new URL(urls);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream

                OutputStream output = new FileOutputStream(filepath
                        + "/" + filename);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
            }


            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // mChart.setImageBitmap(result);              // how do I pass a reference to mChart here ?

        }
    }


    @Override
    protected void onPause() {
        synchronized (this) {
            if (receiverWifi != null)
                unregisterReceiver(receiverWifi);
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
       // Password.get(LoginActivity.this).setPassword("jeganraj").lock();

        try {
            if (receiverWifi != null)
                unregisterReceiver(receiverWifi);
        } catch (Exception e) {

        }
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        registerReceiver(receiverWifi, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        super.onResume();
    }

    public static String getCurrentSsid(Context context, WifiManager wifiManager) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            //wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }
        }
        return ssid;
    }


    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {

            arrayOfUsers.clear();

            ArrayList<String> connections = new ArrayList<String>();
            ArrayList<Float> Signal_Strenth = new ArrayList<Float>();

            sb = new StringBuilder();
            List<ScanResult> wifiList;
            wifiList = mainWifi.getScanResults();
            for (int i = 0; i < wifiList.size(); i++) {
                connections.add(wifiList.get(i).SSID);
            }

            //  adapter.clear();
            // Log.d("con-size", connections.size() + "");
            if (connections.size() == 1) {
                doInback();
            }
            for (int y = 0; y < connections.size(); y++) {


                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                String mode = "Not connected";
                String compare = null;
                //   Log.d("connectedssid", ssid);
                //      Log.d("listssid",connections.get(y));
                //    Log.e("ssidiyyappa","iyyappa"+ssid);

                if (ssid != null) {
                    compare = ssid.replace("\"", "");
                }


                if (ssid != null) {
                    if (compare.equalsIgnoreCase(connections.get(y))) {
                        mode = "connected";
                    } else {
                        mode = "Not connected";
                    }
                }

                User newUser = new User("" + connections.get(y), mode);
                arrayOfUsers.add(newUser);
                //adapter.add(newUser);


            }
            for (int i = 0; i < arrayOfUsers.size(); i++) {


                //    Log.e("size",""+arrayOfUsers.size());
                if (arrayOfUsers.get(i).getHometown().equalsIgnoreCase("Connected")) {
                    int j = i;
                    if (i == 0) {
                        break;
                    } else {
                        User use = arrayOfUsers.get(i);
                        arrayOfUsers.remove(i);
                        arrayOfUsers.add(0, use);
                        break;
                    }
                }

                    /*for(int j=i+1; j<arrayOfUsers.size(); j++)
                    {

                    }*/
            }

            adapter.notifyDataSetChanged();


        }
    }

    void downloadfile(String urls, String filepath, String filename) {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("ImageFileeError: ", e.getMessage());
        }


    }

    private void disablePullNotificationTouch() {
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));
        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (20 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.RGBX_8888;
        customViewGroup view = new customViewGroup(this);
        manager.addView(view, localLayoutParams);
    }

    @Override
    public void onBackPressed() {
    }


    class customViewGroup extends ViewGroup {

        public customViewGroup(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
        }

        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {

            Log.v("customViewGroup", "**********Intercepted");
            return true;
        }
    }
}

       /* //Add this class in your project
         class customViewGroup extends ViewGroup {

            public customViewGroup(Context context) {
                super(context);
            }

            @Override
            protected void onLayout(boolean changed, int l, int t, int r, int b) {
            }

            @Override
            public boolean onInterceptTouchEvent(MotionEvent ev) {

                Log.v("customViewGroup", "**********Intercepted");
                return true;
            }

        }*/

