package com.dci.edukool.student;

/**
 * Created by kirubakaranj on 5/6/2017.
 */

public class Batchpojo {



    //for batches table
    String BatchId,CoordinatingStaffID,AcademicYear,Batchclassid,BatchStartDate,BatchEndDate,TimeTable,Subjects;

    public String getSubjects() {
        return Subjects;
    }

    public void setSubjects(String subjects) {
        Subjects = subjects;
    }


    public String getBatchId() {
        return BatchId;
    }

    public void setBatchId(String batchId) {
        BatchId = batchId;
    }

    public String getCoordinatingStaffID() {
        return CoordinatingStaffID;
    }

    public void setCoordinatingStaffID(String coordinatingStaffID) {
        CoordinatingStaffID = coordinatingStaffID;
    }

    public String getAcademicYear() {
        return AcademicYear;
    }

    public void setAcademicYear(String academicYear) {
        AcademicYear = academicYear;
    }

    public String getBatchclassid() {
        return Batchclassid;
    }

    public void setBatchclassid(String batchclassid) {
        Batchclassid = batchclassid;
    }

    public String getBatchStartDate() {
        return BatchStartDate;
    }

    public void setBatchStartDate(String batchStartDate) {
        BatchStartDate = batchStartDate;
    }

    public String getBatchEndDate() {
        return BatchEndDate;
    }

    public void setBatchEndDate(String batchEndDate) {
        BatchEndDate = batchEndDate;
    }

    public String getTimeTable() {
        return TimeTable;
    }

    public void setTimeTable(String timeTable) {
        TimeTable = timeTable;
    }



}
