package com.dci.edukool.student;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

import helper.Permission;
import services.AppCheckServices;
import steadytate.Password;
import steadytate.SteadyStateDeviceAdminReceiver;
import steadytate.customviewgroup;
import wificonnectivity.WifiBase;

/**
 * Created by abimathi on 11-May-17.
 */
public class Splash extends Activity {
    DevicePolicyManager mDPM;
    ComponentName mAdminName;
    protected static final int REQUEST_ENABLE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

      /*  requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        setContentView(R.layout.splash);
      //  startService(new Intent(Splash.this, AppCheckServices.class));//uncomment
        Permission.verifyStoragePermissions(this);
        if (Build.VERSION.SDK_INT >=23) {
            if (Settings.System.canWrite(Splash.this)) {
                // Do stuff here
            }
            else {
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            } else {
             // disablenotification();
                new Timer().schedule(new TimerTask() {
                    public void run() {
                        runOnUiThread(new Runnable() {
                            public void run() {
                             //   Password.get(Splash.this).setPassword("").lock();

                                startActivity(new Intent(Splash.this, LoginActivity.class));
                                finish();
                            }
                        });
                    }
                }, 2000);
            }
            // disablePullNotificationTouch();
        } else {
          //  disablenotification();
            new Timer().schedule(new TimerTask() {
                public void run() {
                    runOnUiThread(new Runnable() {
                        public void run() {
                          //  Password.get(Splash.this).setPassword("").lock();

                            startActivity(new Intent(Splash.this, LoginActivity.class));
                            finish();
                        }
                    });
                }
            }, 2000);
        }
            //disablePullNotificationTouch();
           /* Intent intent = new Intent(Activity.this, Service.class);
            startService(intent);*//*
        }*/

      /*  new Timer().schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        startActivity(new Intent(Splash.this, LoginActivity.class));
                        finish();
                    }
                });
            }
        }, 2000);*/
            mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            mAdminName = new ComponentName(this, SteadyStateDeviceAdminReceiver.class);

   /*     getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        Toast.makeText(Splash.this, "welcome", Toast.LENGTH_SHORT).show();


                       hideSystemUI();


                    }
                });*/

            if (!mDPM.isAdminActive(mAdminName)) {
                // try to become active � must happen here in this activity, to get result
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                        mAdminName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Additional text explaining why this needs to be added.");
                startActivityForResult(intent, REQUEST_ENABLE);
            } else {
                // Already is a device administrator, can do security operations now.
                //mDPM.lockNow();
            }
            //int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
      /*  if(getTopStatusBarHeight(Splash.this)!=0) {*/

            //}

       /* new Timer().schedule(new TimerTask() {
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        startActivity(new Intent(Splash.this, Brightness.class));
                        finish();
                    }
                });
            }
        }, 2000);*/


        }


   /* @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);*//*
    }*//*
    }*/
    public static int getTopStatusBarHeight(Activity activity) {

        Rect rectangle = new Rect();
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        /*Rect rect = new Rect();

        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);

        return rect.top;*/

        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result =activity. getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia", "hia");
        super.onWindowFocusChanged(hasFocus);
     //   hideSystemUI();
        try
        {
            if(!hasFocus)
            {
               // Toast.makeText(getApplicationContext(),"focusloose",Toast.LENGTH_SHORT).show();
               // showSystemUI();
                //hideSystemUI();
                Object service  = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapseStatusBar = null;
                try {

                    // Prior to API 17, the method to call is 'collapse()'
                    // API 17 onwards, the method to call is `collapsePanels()`

                    if (Build.VERSION.SDK_INT > 16) {
                        collapseStatusBar = statusbarManager .getMethod("collapsePanels");
                    } else {
                        collapseStatusBar = statusbarManager .getMethod("collapse");
                    }
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }

                collapseStatusBar.setAccessible(true);
                collapseStatusBar .invoke(service);
               /* Method collapse = statusbarManager.getMethod("collapse");
                collapse .setAccessible(true);
                collapse .invoke(service);*/
            }
            else
            {
              //  Toast.makeText(getApplicationContext(),"focusgainged",Toast.LENGTH_SHORT).show();

                // hideSystemUI();
            }
        }
        catch(Exception ex)
        {
        }

        try{

                /*if (hasFocus) {
                    getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }else
                {
                    getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }*/
        }
        catch (Exception e)
        {

        }
    }

    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
void disablenotification()
{
    WindowManager manager = ((WindowManager) getApplicationContext()
            .getSystemService(Context.WINDOW_SERVICE));

    WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
    localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
    localLayoutParams.gravity = Gravity.TOP;
    localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

            // this is to enable the notification to recieve touch events
            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

            // Draws over status bar
            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

    localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
    localLayoutParams.height = (int) (25 * getResources()
            .getDisplayMetrics().scaledDensity);
    localLayoutParams.format = PixelFormat.TRANSPARENT;

    customviewgroup view = new customviewgroup(this);

    manager.addView(view, localLayoutParams);
    View decorView = getWindow().getDecorView();
    // Hide the status bar.
    int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
    decorView.setSystemUiVisibility(uiOptions);
}


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1234)
        {
          //  disablenotification();
            new Timer().schedule(new TimerTask() {
                public void run() {
                    runOnUiThread(new Runnable() {
                        public void run() {
                          //  Password.get(Splash.this).setPassword("").lock();
                        startActivity(new Intent(Splash.this, LoginActivity.class));
                        finish();
                        }
                    });
                }
            }, 2000);

        }
    }


}
