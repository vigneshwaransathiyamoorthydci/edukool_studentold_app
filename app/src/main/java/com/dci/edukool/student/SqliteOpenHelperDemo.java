package com.dci.edukool.student;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import calendar.CalendarTypePojo;
import calendar.Calendarpojo;
import helper.Rooms;

/**
 * Created by kirubakaranj on 5/5/2017.
 */

public class SqliteOpenHelperDemo extends SQLiteOpenHelper
{
   // SqliteOpen pojo;
    public SqliteOpenHelperDemo(Context ct)
    {
        super(ct,"mproject",null,1);
    }



    @Override
    public void onCreate(SQLiteDatabase arg0) {

        String tablestd="create table tblStudent(StudentID integer primary key not null, AdmissionNumber text, DOA datetime, FirstName text, LastName text, DOB datetime, Gender text, Phone_1 text, Phone_2 text, Email text, FatherName text, MotherName text, GuardianMobileNumber text," +
                "RollNo text, Room text, ClassID integer, BatchID int, SchoolID int, AcademicYear text, Guardians text, PhotoFilename text, PortalUserID int, StudentLoginID text, Version int," +
                "StaffID int, CreditPoints int, ConnectionStatus int, ManualAttendance int, NoOfPresent int, Credits int, CreatedOn datetime, ModifiedOn datetime)";

                arg0.execSQL(tablestd);

        String tableschool="create table tblSchool(SchoolID integer primary key not null, SchoolCategoryID text, SchoolName text, Acronym text, SchoolLogo text, backgroundimage text, CityID int, StateID int, CountryID int, CreatedOn Datetime, ModifiedOn Datetime)";

                arg0.execSQL(tableschool);

        String tableClasses="create table tblClasses(ClassID integer primary key not null, ClassName text, SectionName text, Grade text, ClassCode text, SchoolID int, DepartmentID int, StaffID int, InternetSSID text, InternetPassword text, InternetType text, PushName text, CreatedOn datetime, ModifiedOn datetime)";

                arg0.execSQL(tableClasses);

        String BatchClasses="create table tblBatch(BatchID integer primary key not null, CoordinatingStaffID int, AcademicYear text, SchoolID int, ClassID int, StaffID int, BatchStartDate datetime, BatchEndDate datetime, TimeTable text, IsActive int, CreatedOn datetime, ModifiedOn datetime,Subject text)";

                arg0.execSQL(BatchClasses);

        String Masterinfo="create table Masterinfo(ID integer primary key not null, SubjectName text, SubjectDescription text, CreatedBy text, CreatedDate datetime, ModifiedBy text, ModifiedDate datetime, Status text, SchoolID text)";

        arg0.execSQL(Masterinfo);

        String tableSubject="create table tblSubject(SubjectID integer , SubjectName text, SchoolID int, StaffID int, ClassID int, BatchID int, CreatedOn datetime, ModifiedOn datetime)";

        arg0.execSQL(tableSubject);

        String tableCalendar="create table tblCalendar(EventID integer primary key not null, EventName text, EventDescription text, EventStartDate text, EventEndDate text, CreatedBy int, Category text, CategoryID integer, CategoryIcon text, CreatedOn datetime, ModifiedBy int, ModifiedOn datetime, Status int)";

        arg0.execSQL(tableCalendar);

        String tablecontent="create table tblContent(ContentID integer primary key not null, Subject text, Catalog text, ContentDescription text, Author text, ContentTypeID int, ContentCatalogType text, ContentFilename text, BookShelfID int, Vaporize text, IsEncrypted int, EncryptionSalt text, SchoolID text, BatchID int, StaffID int, CreatedOn datetime, ModifiedOn datetime)";

        arg0.execSQL(tablecontent);

        String tableStaff="create table tblStaff(StaffID integer primary key not null, EmployementNumber text, DOJ datetime, FirstName text, MiddleName text, LastName text, Gender text, DOB datetime, MaritalStatusID text, SpouseName text, FatherName text, MotherName text, Phone text, Email text, PhotoFilename text, StaffCategoryID int, SchoolID text, DeptartmentID int, DesignationID int, PortalUserID text, PortalLoginID text, CreatedOn datetime, ModifiedOn datetime)";

        arg0.execSQL(tableStaff);

//extra table
        String tableContentType="create table tblContentType(ContentTypeID integer primary key not null, ContentType text, SchoolID int, StaffID int, BatchID int, UploadRights text, CreatedOn datetime, ModifiedOn datetime)";

        arg0.execSQL(tableContentType);

        String tablebookshelf="create table tblBookShelf(BookShelfID integer primary key not null, BookShelfName text)";

        arg0.execSQL(tablebookshelf);

        String tableContentbatch="create table tblContentBatch(ContentBatchID integer primary key not null, ContentID int, ContentTypeID int, SchoolID int, BatchID int, SujectID int, StaffID int, CreatedOn datetime, ModifiedOn datetime)";

        arg0.execSQL(tableContentbatch);

        String tableCommunication="create table tblCommunication(CommunicationID integer primary key AUTOINCREMENT, MsgID integer, Sender integer, Receiver integer, DateOfCommunication datetime, Title text, Content text, BatchID integer, CreatedOn datetime, UpdatedOn datetime, StaffID integer, SchoolID integer, Status integer, IsAckSent integer, IsSent integer,SubjectID integer)";

        arg0.execSQL(tableCommunication);

        String tablSequence="create table tblSequence(TableName text, SequenceNumber int)";

        arg0.execSQL(tablSequence);

        String calendetyper="CREATE TABLE \"tblcalendartype\"\n" +
                "(\n" +
                "CategoryID integer,\n" +
                "Name varchar,\n" +
                "Description varchar,\n" +

                "Image varchar" +

                ")";
        arg0.execSQL(calendetyper);

        String rooms="CREATE TABLE \"tblrooms\"\n" +
                "(\n" +
                "ID integer,\n" +
                "RoomName varchar,\n" +
                "SSID varchar,\n" +

                "Password varchar" +

                ")";
        arg0.execSQL(rooms);

       /* String tableCalendar=  "CREATE TABLE tblCalendar(EventID integer primary key not null, EventName text, EventDescription text, EventStartDate datetime, EventEndDate datetime, CreatedBy integer, Category text, CategoryIcon text, CreatedOn datetime, ModifiedBy integer, ModifiedOn datetime, Status integer)";

        db.execSQL(tableCalendar);
*/
        ///"select MsgID, SubjectID, Receiver, Title, Content, DateOfCommunication from tblCommunication where SubjectID="+id, null);



    }
    public void addmasterroom(Rooms room)
    {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ID", room.getId());
        values.put("RoomName" , room.getRoomsname());
        values.put("SSID", room.getSsid());
        values.put("Password",room.getPassword());
        //values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
        db.insert("tblrooms", null, values);
        db.close(); // Closing database connection
    }
    public Rooms getroomdata(int id)
    {
        String selectQuery = "select *from tblrooms where ID='"+id+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Rooms rooms=new Rooms();

        if (cursor.moveToFirst()) {
            rooms.setId(cursor.getInt(0));
            rooms.setRoomsname(cursor.getString(1));
            rooms.setSsid(cursor.getString(2));
            rooms.setPassword(cursor.getString(3));
            return  rooms;


        }
        return rooms;
       /* do {
            helper.Calendarpojo pojo=new helper.Calendarpojo();
            pojo.setCalendarid(cursor.getInt(0));
            pojo.setCalendarname(cursor.getString(1));
            pojo.setCalendardescription(cursor.getString(2));
            pojo.setCalendarimage(cursor.getString(3));

        }while (cursor.moveToNext());*/





    }
    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        arg0.execSQL("drop table if exists tblStudent");
        arg0.execSQL("drop table if exists tblSchool");
        arg0.execSQL("drop table if exists tblClasses");
        arg0.execSQL("drop table if exists tblBatch");
        arg0.execSQL("drop table if exists Masterinfo");
        arg0.execSQL("drop table if exists tblSubject");
        arg0.execSQL("drop table if exists tblCalendar");
        arg0.execSQL("drop table if exists tblContent");
        arg0.execSQL("drop table if exists tblStaff");
        arg0.execSQL("drop table if exists tblcalendartype");

        arg0.execSQL("drop table if exists tblContentType");//extra
        arg0.execSQL("drop table if exists tblBookShelf");
        arg0.execSQL("drop table if exists tblContentBatch");
        arg0.execSQL("drop table if exists tblCommutblSequencenication");
        arg0.execSQL("drop table if exists tblSequence");
        onCreate(arg0);
    }

   public void InsertReceiveCalendar(ContentValues cv){
       SQLiteDatabase con=this.getWritableDatabase();
       con.insert("tblCalendar",null, cv);
   }

    void insertCommunication(ContentValues cv){
       SQLiteDatabase con=this.getWritableDatabase();
       con.insert("tblCommunication",null, cv);
   }
  public Cursor  retriveIssent(int id){
        SQLiteDatabase con=this.getWritableDatabase();
    Cursor c=con.rawQuery("select * from tblCommunication where IsAckSent=1 AND SubjectID="+id, null);
    return c;
    }
    public void UpdateMap(int msgid,ContentValues values){
        SQLiteDatabase con=this.getWritableDatabase();
        con.update("tblCommunication",
                values,
                "MsgID" + " = ?",
                new String[]{""+msgid});
        con.close();
    }

    void insert(ContentValues cv,SqliteOpen pojo,ArrayList<Batchpojo> batchlist,ArrayList<Masterpojo> masterlist,  ArrayList<Staffpojo> stafflist, ArrayList<CalendarTypePojo> Calendartypelist)
    {
      //  this.pojo=pojo;

       SQLiteDatabase con=this.getWritableDatabase();
        con.insert("tblStudent",null, cv);

        //for school table
        ContentValues schoolcv=new ContentValues();
        schoolcv.put("SchoolID",Integer.parseInt(pojo.getSchoolid()));
        schoolcv.put("SchoolName",pojo.getSchoolName());
        schoolcv.put("Acronym",pojo.getAcronym());
        schoolcv.put("SchoolLogo",pojo.getSchoolLogo());
        schoolcv.put("backgroundimage",pojo.getBackgroundimage());
        con.insert("tblSchool",null,schoolcv);

        //for classes table
        ContentValues classcv=new ContentValues();
        classcv.put("ClassID",Integer.parseInt(pojo.getClassID()));   //int
        classcv.put("ClassName",pojo.getClassName());
        classcv.put("SectionName",pojo.getSectionName());
        classcv.put("Grade",pojo.getGrade());
        classcv.put("SchoolID",Integer.parseInt(pojo.getSchoolID()));  //int
        classcv.put("DepartmentID",Integer.parseInt(pojo.getDepartmentID()));  //int
        classcv.put("InternetSSID",pojo.getInternetSSID());
        classcv.put("InternetPassword",pojo.getInternetPassword());
        classcv.put("InternetType",pojo.getInternetType());
        classcv.put("PushName",pojo.getPushName());
        con.insert("tblClasses",null,classcv);


        //for batch table
        for(int i=0;i<batchlist.size();i++) {
            ContentValues batcv = new ContentValues();
            batcv.put("BatchID",Integer.parseInt(batchlist.get(i).getBatchId()));
            batcv.put("CoordinatingStaffID",Integer.parseInt(batchlist.get(i).getCoordinatingStaffID()));
            batcv.put("AcademicYear",batchlist.get(i).getAcademicYear());
            batcv.put("SchoolID",Integer.parseInt(pojo.getSchoolID()));
            batcv.put("ClassID",Integer.parseInt(batchlist.get(i).getBatchclassid()));
            batcv.put("BatchStartDate",batchlist.get(i).getBatchStartDate());
            batcv.put("BatchEndDate",batchlist.get(i).getBatchEndDate());
            batcv.put("TimeTable",batchlist.get(i).getTimeTable());
            batcv.put("Subject",batchlist.get(i).getSubjects());
            con.insert("tblBatch",null,batcv);

        }

        //for master info table
        for(int i=0;i<masterlist.size();i++) {
            ContentValues mastercv = new ContentValues();
            mastercv.put("ID",Integer.parseInt(masterlist.get(i).getSubid()));
            mastercv.put("SubjectName",masterlist.get(i).getSubjectname());
            mastercv.put("SubjectDescription",masterlist.get(i).getSubjectdesc());
            mastercv.put("CreatedBy",masterlist.get(i).getCreatedBy());
            mastercv.put("CreatedDate",masterlist.get(i).getCreateddate());
            mastercv.put("ModifiedBy",masterlist.get(i).getModifiedby());
            mastercv.put("ModifiedDate",masterlist.get(i).getModifieddate());
            mastercv.put("Status",masterlist.get(i).getStatus());
            mastercv.put("SchoolID",masterlist.get(i).getSchoolid());
            con.insert("Masterinfo",null,mastercv);
        }

        // for subject table
        for(int i=0;i<batchlist.size();i++) {
            String subarray = batchlist.get(i).getSubjects();
            //  subarray=String str = "[Chrissman-@1]";
            subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
            subarray = subarray.replace("\"", "");
            String[] separated = subarray.split(",");

            for(int c=0;c<separated.length;c++){
                for(int y=0;y<masterlist.size();y++){
                    if(separated[c].equals(masterlist.get(y).getSubid())){
                        ContentValues subjectcv = new ContentValues();
                        subjectcv.put("SubjectID",Integer.parseInt(separated[c]));
                        subjectcv.put("SubjectName",masterlist.get(y).getSubjectname());
                        subjectcv.put("SchoolID",Integer.parseInt(masterlist.get(y).getSchoolid()));
                        subjectcv.put("ClassID",Integer.parseInt(pojo.getClassID()));
                        subjectcv.put("BatchID",Integer.parseInt(batchlist.get(i).getBatchId()));
                        subjectcv.put("CreatedOn",masterlist.get(y).getCreateddate());
                        subjectcv.put("ModifiedOn",masterlist.get(y).getModifieddate());
                        con.insert("tblSubject",null,subjectcv);
                    }
                }
            }

        }



      /*  //calendar table

        for(int i=0;i<calendarlist.size();i++) {
            ContentValues calendarcv = new ContentValues();
            calendarcv.put("EventID",Integer.parseInt(calendarlist.get(i).getEventID()));
            calendarcv.put("EventName",calendarlist.get(i).getEventName());
            calendarcv.put("EventDescription",calendarlist.get(i).getEventDescription());
            calendarcv.put("EventStartDate",calendarlist.get(i).getEventStartDate());
            calendarcv.put("EventEndDate",calendarlist.get(i).getEventEndDate());
            calendarcv.put("Category",calendarlist.get(i).getCategory());
            calendarcv.put("CategoryID",calendarlist.get(i).getCategoryID());
            calendarcv.put("CategoryIcon",calendarlist.get(i).getCategoryIcon());
            con.insert("tblCalendar",null,calendarcv);
        }
*/


        //for staff table

       for(int i=0;i<stafflist.size();i++) {
            ContentValues staffcv = new ContentValues();
            staffcv.put("StaffID",Integer.parseInt(stafflist.get(i).getStaffid()));
            staffcv.put("DOJ",stafflist.get(i).getDOJ());
            staffcv.put("FirstName",stafflist.get(i).getFirstName());
            staffcv.put("LastName",stafflist.get(i).getLastName());
            staffcv.put("Gender",stafflist.get(i).getGender());
            staffcv.put("DOB",stafflist.get(i).getDOB());
            staffcv.put("MaritalStatusID",stafflist.get(i).getMaritalStatusID());
            staffcv.put("SpouseName",stafflist.get(i).getSpouseName());
            staffcv.put("FatherName",stafflist.get(i).getFatherName());
            staffcv.put("MotherName",stafflist.get(i).getMotherName());
            staffcv.put("Phone",stafflist.get(i).getPhone());
            staffcv.put("Email",stafflist.get(i).getEmail());
            staffcv.put("SchoolID",pojo.getSchoolid());
            staffcv.put("PhotoFilename",stafflist.get(i).getPhotoFilename());
            staffcv.put("StaffCategoryID",Integer.parseInt(stafflist.get(i).getStaffCategoryID()));
              con.insert("tblStaff",null,staffcv);
        }

       /* for(int i=0;i<Contentlist.size();i++) {
        ContentValues contentcv = new ContentValues();
        contentcv.put("ContentID",Integer.parseInt(Contentlist.get(i).getContentid()));
        contentcv.put("Subject",Contentlist.get(i).getSubject());
        contentcv.put("ContentDescription",Contentlist.get(i).getContentDescription());
        contentcv.put("Catalog",Contentlist.get(i).getCatalog());
        contentcv.put("ContentCatalogType",Contentlist.get(i).getContentType());
        contentcv.put("ContentFilename",Contentlist.get(i).getContentFilename());
        contentcv.put("Vaporize",Contentlist.get(i).getVaporize());
        con.insert("tblContent",null,contentcv);
    }*/

    //for calendar type table

        for(int i=0;i<Calendartypelist.size();i++) {
            ContentValues calendar_typecv = new ContentValues();
            calendar_typecv.put("CategoryID",Calendartypelist.get(i).getEventtypeid());
            calendar_typecv.put("Name",Calendartypelist.get(i).getEventtypename());
            calendar_typecv.put("Description",Calendartypelist.get(i).getEventtypeDescription());
            calendar_typecv.put("Image",Calendartypelist.get(i).getEventtypeimage());
            con.insert("tblcalendartype",null,calendar_typecv);
        }

    }

    //update MasterInfo
    public Cursor retriveIsMasterID(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from Masterinfo where ID="+id, null);
        return c;
    }

    public void InsertMasterInfo(ArrayList<Masterpojo> masterlist) {

        SQLiteDatabase con=this.getWritableDatabase();

        //for master info table
        for (int i = 0; i < masterlist.size(); i++) {
            ContentValues mastercv = new ContentValues();
            mastercv.put("ID", Integer.parseInt(masterlist.get(i).getSubid()));
            mastercv.put("SubjectName", masterlist.get(i).getSubjectname());
            mastercv.put("SubjectDescription", masterlist.get(i).getSubjectdesc());
            mastercv.put("CreatedBy", masterlist.get(i).getCreatedBy());
            mastercv.put("CreatedDate", masterlist.get(i).getCreateddate());
            mastercv.put("ModifiedBy", masterlist.get(i).getModifiedby());
            mastercv.put("ModifiedDate", masterlist.get(i).getModifieddate());
            mastercv.put("Status", masterlist.get(i).getStatus());
            mastercv.put("SchoolID", masterlist.get(i).getSchoolid());
            con.insert("Masterinfo", null, mastercv);
        }

    }

    public void UpdateMasterInfo(Masterpojo pojo){

        SQLiteDatabase con=this.getWritableDatabase();

        ContentValues mastercv = new ContentValues();
        mastercv.put("ID",Integer.parseInt(pojo.getSubid()));
        mastercv.put("SubjectName",pojo.getSubjectname());
        mastercv.put("SubjectDescription",pojo.getSubjectdesc());
        mastercv.put("CreatedBy",pojo.getCreatedBy());
        mastercv.put("CreatedDate",pojo.getCreateddate());
        mastercv.put("ModifiedBy",pojo.getModifiedby());
        mastercv.put("ModifiedDate",pojo.getModifieddate());
        mastercv.put("Status",pojo.getStatus());
        mastercv.put("SchoolID",pojo.getSchoolid());

        con.update("Masterinfo",mastercv,"ID "+ " = ?",new String[]{""+pojo.getSubid()});
        con.close();

    }




    //update Subjects
    public void UpdateSubjectFromMaster(){

        SQLiteDatabase con=this.getWritableDatabase();

       Cursor mastcur= retrive("Masterinfo");

        ArrayList<Masterpojo> masterlist = new ArrayList<Masterpojo>();

        while(mastcur.moveToNext()){
            Masterpojo masterpojo = new Masterpojo();
            masterpojo.setSubid(""+mastcur.getInt(mastcur.getColumnIndex("ID")));
            masterpojo.setSubjectname(mastcur.getString(mastcur.getColumnIndex("SubjectName")));
            masterpojo.setSubjectdesc(mastcur.getString(mastcur.getColumnIndex("SubjectDescription")));
            masterpojo.setCreatedBy(mastcur.getString(mastcur.getColumnIndex("CreatedBy")));
            masterpojo.setCreateddate(mastcur.getString(mastcur.getColumnIndex("CreatedDate")));
            masterpojo.setModifieddate(mastcur.getString(mastcur.getColumnIndex("ModifiedDate")));
            masterpojo.setStatus(mastcur.getString(mastcur.getColumnIndex("Status")));
            masterpojo.setSchoolid(mastcur.getString(mastcur.getColumnIndex("SchoolID")));

           masterlist.add(masterpojo);
        }


        ArrayList<Batchpojo> batchlist = new ArrayList<Batchpojo>();
        Cursor batcur=retrive("tblBatch");
        while(batcur.moveToNext()){
            Batchpojo batchpojo = new Batchpojo();
            batchpojo.setBatchId(""+batcur.getInt(batcur.getColumnIndex("BatchID")));
            batchpojo.setSubjects(batcur.getString(batcur.getColumnIndex("Subject")));
            batchlist.add(batchpojo);
        }

        int clid=0;
        Cursor clcur=retrive("tblClasses");
        while(clcur.moveToNext()){
            clid= clcur.getInt(clcur.getColumnIndex("ClassID"));
        }





        for(int i=0;i<batchlist.size();i++) {
            String subarray = batchlist.get(i).getSubjects();
            //  subarray=String str = "[Chrissman-@1]";
            subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
            subarray = subarray.replace("\"", "");
            String[] separated = subarray.split(",");

            for(int c=0;c<separated.length;c++){
                for(int y=0;y<masterlist.size();y++){
                    if(separated[c].equals(masterlist.get(y).getSubid())){
                        ContentValues subjectcv = new ContentValues();
                        subjectcv.put("SubjectID",Integer.parseInt(separated[c]));
                        subjectcv.put("SubjectName",masterlist.get(y).getSubjectname());
                        subjectcv.put("SchoolID",Integer.parseInt(masterlist.get(y).getSchoolid()));
                        subjectcv.put("ClassID",clid);
                        subjectcv.put("BatchID",Integer.parseInt(batchlist.get(i).getBatchId()));
                        subjectcv.put("CreatedOn",masterlist.get(y).getCreateddate());
                        subjectcv.put("ModifiedOn",masterlist.get(y).getModifieddate());
                       // con.insert("tblSubject",null,subjectcv);
                        con.update("tblSubject",subjectcv,"SubjectID "+ " = ?",new String[]{separated[c]});

                    }
                }
            }

        }

}

    //update RoomTable
    public Cursor retriveIsRoomid(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblrooms where ID="+id, null);
        return c;
    }
    public void UpdateRoom(Rooms room){
        SQLiteDatabase con=this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ID", room.getId());
        values.put("RoomName" , room.getRoomsname());
        values.put("SSID", room.getSsid());
        values.put("Password",room.getPassword());
        //values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
        con.update("tblrooms",values,"ID "+ " = ?",new String[]{""+room.getId()});
        con.close(); // Closing database connection
    }

//update CalendarType

    public Cursor retriveIsCalenderTypeId(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblcalendartype where CategoryID="+id, null);
        return c;
    }

    public void UpdateCalenderType(CalendarTypePojo pojo){

        SQLiteDatabase con=this.getWritableDatabase();
        ContentValues calendar_typecv = new ContentValues();
        calendar_typecv.put("CategoryID",pojo.getEventtypeid());
        calendar_typecv.put("Name",pojo.getEventtypename());
        calendar_typecv.put("Description",pojo.getEventtypeDescription());
        calendar_typecv.put("Image",pojo.getEventtypeimage());

        con.update("tblcalendartype",calendar_typecv,"CategoryID "+ " = ?",new String[]{""+pojo.getEventtypeid()});
    }

    public void InsertCalenderType(CalendarTypePojo pojo){

        SQLiteDatabase con=this.getWritableDatabase();
        ContentValues calendar_typecv = new ContentValues();
        calendar_typecv.put("CategoryID",pojo.getEventtypeid());
        calendar_typecv.put("Name",pojo.getEventtypename());
        calendar_typecv.put("Description",pojo.getEventtypeDescription());
        calendar_typecv.put("Image",pojo.getEventtypeimage());

        con.insert("tblcalendartype",null,calendar_typecv);
    }

    public void InsertContent(ArrayList<ContentPojo> Contentlist){
        //for content table
        SQLiteDatabase con=this.getWritableDatabase();

        for(int i=0;i<Contentlist.size();i++) {
            ContentValues contentcv = new ContentValues();
            contentcv.put("ContentID",Integer.parseInt(Contentlist.get(i).getContentid()));
            contentcv.put("Subject",Contentlist.get(i).getSubject());
            contentcv.put("ContentDescription",Contentlist.get(i).getContentDescription());
            contentcv.put("Catalog",Contentlist.get(i).getCatalog());
            contentcv.put("ContentCatalogType",Contentlist.get(i).getContentType());
            contentcv.put("ContentFilename",Contentlist.get(i).getContentFilename());
            contentcv.put("Vaporize",Contentlist.get(i).getVaporize());
            con.insert("tblContent",null,contentcv);
        }

    }

    public Cursor retriveIsContentId(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblContent where ContentID="+id, null);
        return c;
    }

    public void UpdateContentDownload(ContentPojo pojo){

    SQLiteDatabase con=this.getWritableDatabase();

    ContentValues cv=new ContentValues();
    cv.put("ContentID",Integer.parseInt(pojo.getContentid()));
    cv.put("Subject",pojo.getSubject());
    cv.put("ContentDescription",pojo.getContentDescription());
    cv.put("Catalog",pojo.getCatalog());
    cv.put("ContentCatalogType",pojo.getCatalog());
    cv.put("ContentFilename",pojo.getCatalog());
    cv.put("Vaporize",pojo.getCatalog());

    con.update("tblContent",cv,"ContentID "+ " = ?",new String[]{""+pojo.getContentid()});
    con.close();
    }
    public void InsertCalender(ArrayList<CalendarPojo> calendarlist){

        //for calendar table
        SQLiteDatabase con=this.getWritableDatabase();

        for(int i=0;i<calendarlist.size();i++) {
            ContentValues calendarcv = new ContentValues();
            calendarcv.put("EventID",Integer.parseInt(calendarlist.get(i).getEventID()));
            calendarcv.put("EventName",calendarlist.get(i).getEventName());
            calendarcv.put("EventDescription",calendarlist.get(i).getEventDescription());
            calendarcv.put("EventStartDate",calendarlist.get(i).getEventStartDate());
            calendarcv.put("EventEndDate",calendarlist.get(i).getEventEndDate());
            calendarcv.put("Category",calendarlist.get(i).getCategory());
            calendarcv.put("CategoryID",calendarlist.get(i).getCategoryID());
            calendarcv.put("CategoryIcon",calendarlist.get(i).getCategoryIcon());
            con.insert("tblCalendar",null,calendarcv);
        }

    }
    public  ArrayList<Calendarpojo>  retriveCalendar(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar", null);

        ArrayList<Calendarpojo> calendarlist=new ArrayList<>();
        while(c.moveToNext()){
            Calendarpojo cpojo=new Calendarpojo();
            cpojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
            cpojo.setEventname(c.getString(c.getColumnIndex("EventName")));
            cpojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
            cpojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
            cpojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));

            int id=c.getInt(c.getColumnIndex("CategoryID"));
            cpojo.setCategoryID(id);
            Cursor typecur=retriveCalendarType(id);
            while(typecur.moveToNext()){
                cpojo.setCategory(typecur.getString(typecur.getColumnIndex("Name")));

                 cpojo.setCategoryIcon(typecur.getString(typecur.getColumnIndex("Image")));
            }

            calendarlist.add(cpojo);

        }
        return calendarlist;
    }

    public void UpdateCalendarDownload(CalendarPojo pojo){

        SQLiteDatabase con=this.getWritableDatabase();

        ContentValues cv=new ContentValues();

        cv.put("EventID",Integer.parseInt(pojo.getEventID()));
        cv.put("EventName",pojo.getEventName());
        cv.put("EventDescription",pojo.getEventDescription());
        cv.put("EventStartDate",pojo.getEventStartDate());
        cv.put("EventEndDate",pojo.getEventEndDate());
        cv.put("Category",pojo.getCategory());
        cv.put("CategoryID",pojo.getCategoryID());
        cv.put("CategoryIcon",pojo.getCategoryIcon());

        con.update("tblCalendar",cv,"EventID "+ " = ?",new String[]{pojo.getEventID()});
        con.close();


    }
    public Cursor retriveIsCalendarEventID(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblCalendar where EventID="+id, null);
        return c;
    }

    public Cursor  retriveCalendarType(int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblcalendartype where CategoryID="+_id, null);
        return c;
    }
    public Cursor retriveCategoryType(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("SELECT * FROM tblcalendartype", null);
        return c;
    }

    public Cursor retriveStartDate(String date){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar where EventStartDate="+"'"+date+"'", null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }
    public Cursor retriveEnddate(String date){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar where EventEndDate="+"'"+date+"'", null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }

    public Cursor retrive(String typetable) {
        SQLiteDatabase con=getReadableDatabase();

        if(typetable.equals("tblStudent")) {
            Cursor c = con.rawQuery("select * from tblStudent", null);
            return c;
        }
        else if(typetable.equals("tblSchool")){
            Cursor c = con.rawQuery("select * from tblSchool", null);
            return c;
        }

        else if(typetable.equals("tblClasses")){
            Cursor c = con.rawQuery("select * from tblClasses", null);
            return c;
        }
        else if(typetable.equals("tblBatch")){
            Cursor c = con.rawQuery("select * from tblBatch", null);
            return c;
        }

        else if(typetable.equals("Masterinfo")){
            Cursor c = con.rawQuery("select * from Masterinfo", null);
            return c;
        }
        else if(typetable.equals("tblSubject")){
            Cursor c = con.rawQuery("select * from tblSubject", null);
            return c;
        }
        else if(typetable.equals("tblCalendar")){
            Cursor c = con.rawQuery("select * from tblCalendar", null);
            return c;
        }
        else if(typetable.equals("tblContent")){
            Cursor c = con.rawQuery("select * from tblContent", null);
            return c;
        }
        else if(typetable.equals("tblStaff")){
            Cursor c = con.rawQuery("select * from tblStaff", null);
            return c;
        }
        else if(typetable.equals("tblCommunication")){
            Cursor c = con.rawQuery("select * from tblCommunication", null);
            return c;
        }


        return null;
    }

    public void update(ContentValues cv, int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getWritableDatabase();
        con.update("sta",cv,null,null);
    }
    public Cursor  retriveTimeTable(int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblBatch where BatchID="+_id, null);
        return c;
    }
    public void  UpdateTimeTable(ContentValues cv,int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getWritableDatabase();
        con.update("tblBatch",cv,"BatchID"+ " = ?",new String[]{""+_id});
        con.close();
      //  return c;
    }

    public Cursor  retrivevalue(int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select AcademicYear from tblBatch where BatchID="+_id, null);
        return c;
    }
    public Cursor  retriveSubjects(int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select SubjectID,SubjectName from tblSubject where BatchID="+_id, null);
        return c;
    }
  /*  public Cursor  retrivecontent(String type) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select ContentFilename,Subject,ContentCatalogType from tblContent where ContentCatalogType="+type, null);
        return c;
    }*/

    public Cursor  retriveContentvalues(int id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select ContentFilename,Subject from tblContent where ContentID="+id, null);
        return c;
    }

    public void  deleteretriveContentvalues(int id) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        //SQLiteDatabase con=getReadableDatabase();
        db.execSQL("delete from tblContent where ContentID="+id);
        db.close();
       // return c;
    }
    public Cursor  retriveContentvaluesall(int id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblContent where ContentID="+id, null);
        return c;
    }
    public void insertNewContent(ContentValues cv){
        SQLiteDatabase con=getWritableDatabase();
        con.insert("tblContent",null,cv);
    }
    public void updateContent(ContentValues cv,int contentid){
        SQLiteDatabase con=getWritableDatabase();
        con.update("tblContent",cv,"ContentID="+contentid,null);
          }


    public Cursor retriveSubjectidFromComm(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("SELECT DISTINCT SubjectID FROM tblCommunication", null);
        return c;
    }

    public Cursor retriveSubject(int id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select SubjectName,SubjectID from tblSubject where SubjectID="+id, null);
        return c;
    }

    public Cursor retriveMessage(int id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblCommunication where SubjectID="+id, null);
        return c;
    }
    public Cursor CheckisMsg(int id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblCommunication where MsgID="+id, null);
        return c;
    }
    /*Cursor c=con.rawQuery("select ContentFilename,Subject from tblContent where ContentID="+id+",Catalog="+catalog, null);*/















    //Update All Student
  public Cursor retriveIsStudentId(int id){
     // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblStudent where StudentID="+id, null);
        return c;
    }

    public Cursor retriveIsSchool(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblSchool where SchoolID="+id, null);
        return c;
    }
    public Cursor retriveIsClass(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblClasses where ClassID="+id, null);
        return c;
    }


//for Student Table
public void UpdateStudent(ContentValues cv,String id){
    SQLiteDatabase con=this.getWritableDatabase();

    con.update("tblStudent",cv,"StudentID "+ " = ?",new String[]{id});
    con.close();
}

public void InsertStudent(ContentValues cv){
    SQLiteDatabase con=this.getWritableDatabase();
    con.insert("tblStudent",null, cv);
    con.close();
}


//for school table

public void UpdateSchool(SqliteOpen pojo){

    SQLiteDatabase con=this.getWritableDatabase();

    ContentValues schoolcv=new ContentValues();
    schoolcv.put("SchoolID",Integer.parseInt(pojo.getSchoolid()));
    schoolcv.put("SchoolName",pojo.getSchoolName());
    schoolcv.put("Acronym",pojo.getAcronym());
    schoolcv.put("SchoolLogo",pojo.getSchoolLogo());
    schoolcv.put("backgroundimage",pojo.getBackgroundimage());

    con.update("tblSchool",schoolcv,"SchoolID "+ " = ?",new String[]{pojo.getSchoolid()});
    con.close();

}
public void InsertSchool(SqliteOpen pojo){
    SQLiteDatabase con=this.getWritableDatabase();

    ContentValues schoolcv=new ContentValues();
    schoolcv.put("SchoolID",Integer.parseInt(pojo.getSchoolid()));
    schoolcv.put("SchoolName",pojo.getSchoolName());
    schoolcv.put("Acronym",pojo.getAcronym());
    schoolcv.put("SchoolLogo",pojo.getSchoolLogo());
    schoolcv.put("backgroundimage",pojo.getBackgroundimage());
    con.insert("tblSchool",null,schoolcv);
    con.close();
}

public Cursor retriveClassName(){
    SQLiteDatabase con=getReadableDatabase();
    int id=0;
    Cursor stcur=retrive("tblStudent");
    while(stcur.moveToNext()){
        id=stcur.getInt(stcur.getColumnIndex("ClassID"));
    }stcur.close();

    Cursor c=con.rawQuery("select * from tblClasses where ClassID="+id, null);
    return c;
}

 public void UpdateClass(SqliteOpen pojo){

    SQLiteDatabase con=this.getWritableDatabase();

    ContentValues classcv=new ContentValues();

   // classcv.put("ClassID",Integer.parseInt(pojo.getClassID()));   //int
    classcv.put("ClassName",pojo.getClassName());
    classcv.put("SectionName",pojo.getSectionName());
    classcv.put("Grade",pojo.getGrade());
    classcv.put("SchoolID",Integer.parseInt(pojo.getSchoolID()));  //int
    classcv.put("DepartmentID",Integer.parseInt(pojo.getDepartmentID()));  //int
    classcv.put("InternetSSID",pojo.getInternetSSID());
    classcv.put("InternetPassword",pojo.getInternetPassword());
    classcv.put("InternetType",pojo.getInternetType());
    classcv.put("PushName",pojo.getPushName());

    con.update("tblClasses",classcv,"ClassID"+ " = ?",new String[]{pojo.getClassID()});
    con.close();


}
public void InsertClass(SqliteOpen pojo){
    SQLiteDatabase con=this.getWritableDatabase();

    ContentValues classcv=new ContentValues();
    classcv.put("ClassID",Integer.parseInt(pojo.getClassID()));   //int
    classcv.put("ClassName",pojo.getClassName());
    classcv.put("SectionName",pojo.getSectionName());
    classcv.put("Grade",pojo.getGrade());
    classcv.put("SchoolID",Integer.parseInt(pojo.getSchoolID()));  //int
    classcv.put("DepartmentID",Integer.parseInt(pojo.getDepartmentID()));  //int
    classcv.put("InternetSSID",pojo.getInternetSSID());
    classcv.put("InternetPassword",pojo.getInternetPassword());
    classcv.put("InternetType",pojo.getInternetType());
    classcv.put("PushName",pojo.getPushName());
    con.insert("tblClasses",null,classcv);
    con.close();
}

    public Cursor retriveIsStaff(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblStaff where StaffID="+id, null);
        return c;
    }

    public void InsertStaff(ArrayList<Staffpojo> stafflist){

        SQLiteDatabase con=this.getWritableDatabase();
        int schid=0;
        Cursor c=retrive("tblSchool");
        while(c.moveToNext()){
            schid=c.getInt(c.getColumnIndex("SchoolID"));
        }

        for(int i=0;i<stafflist.size();i++) {
            ContentValues staffcv = new ContentValues();
            staffcv.put("StaffID",Integer.parseInt(stafflist.get(i).getStaffid()));
            staffcv.put("DOJ",stafflist.get(i).getDOJ());
            staffcv.put("FirstName",stafflist.get(i).getFirstName());
            staffcv.put("LastName",stafflist.get(i).getLastName());
            staffcv.put("Gender",stafflist.get(i).getGender());
            staffcv.put("DOB",stafflist.get(i).getDOB());
            staffcv.put("MaritalStatusID",stafflist.get(i).getMaritalStatusID());
            staffcv.put("SpouseName",stafflist.get(i).getSpouseName());
            staffcv.put("FatherName",stafflist.get(i).getFatherName());
            staffcv.put("MotherName",stafflist.get(i).getMotherName());
            staffcv.put("Phone",stafflist.get(i).getPhone());
            staffcv.put("Email",stafflist.get(i).getEmail());
            staffcv.put("SchoolID",schid);
            staffcv.put("PhotoFilename",stafflist.get(i).getPhotoFilename());
            staffcv.put("StaffCategoryID",Integer.parseInt(stafflist.get(i).getStaffCategoryID()));
            con.insert("tblStaff",null,staffcv);
        }
        con.close();
    }

    public void UpdateStaff(ArrayList<Staffpojo> stafflist) {
      SQLiteDatabase con=this.getWritableDatabase();

     int schid=0;
     Cursor c=retrive("tblSchool");
     while(c.moveToNext()){
        schid=c.getInt(c.getColumnIndex("SchoolID"));
     }
     c.close();

    for (int i = 0; i < stafflist.size(); i++) {
        ContentValues staffcv = new ContentValues();
        staffcv.put("StaffID", Integer.parseInt(stafflist.get(i).getStaffid()));
        staffcv.put("DOJ", stafflist.get(i).getDOJ());
        staffcv.put("FirstName", stafflist.get(i).getFirstName());
        staffcv.put("LastName", stafflist.get(i).getLastName());
        staffcv.put("Gender", stafflist.get(i).getGender());
        staffcv.put("DOB", stafflist.get(i).getDOB());
        staffcv.put("MaritalStatusID", stafflist.get(i).getMaritalStatusID());
        staffcv.put("SpouseName", stafflist.get(i).getSpouseName());
        staffcv.put("FatherName", stafflist.get(i).getFatherName());
        staffcv.put("MotherName", stafflist.get(i).getMotherName());
        staffcv.put("Phone", stafflist.get(i).getPhone());
        staffcv.put("Email", stafflist.get(i).getEmail());
        staffcv.put("SchoolID", schid);
        staffcv.put("PhotoFilename", stafflist.get(i).getPhotoFilename());
        staffcv.put("StaffCategoryID", Integer.parseInt(stafflist.get(i).getStaffCategoryID()));


        con.update("tblStaff",staffcv,"StaffID "+ " = ?",new String[]{stafflist.get(i).getStaffid()});
    }
        con.close();
}


    public Cursor retriveIsBatch(int id){
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblBatch where BatchID="+id, null);
        return c;
    }



    //for batch table
public void UpdateBatch(ArrayList<Batchpojo> batchlist){
    SQLiteDatabase con=this.getWritableDatabase();

    int schid=0;
    Cursor c=retrive("tblSchool");
    while(c.moveToNext()){
        schid=c.getInt(c.getColumnIndex("SchoolID"));
    }

    for(int i=0;i<batchlist.size();i++) {
        ContentValues batcv = new ContentValues();
        batcv.put("BatchID",Integer.parseInt(batchlist.get(i).getBatchId()));
        batcv.put("CoordinatingStaffID",Integer.parseInt(batchlist.get(i).getCoordinatingStaffID()));
        batcv.put("AcademicYear",batchlist.get(i).getAcademicYear());
        batcv.put("SchoolID",schid);
        batcv.put("ClassID",Integer.parseInt(batchlist.get(i).getBatchclassid()));
        batcv.put("BatchStartDate",batchlist.get(i).getBatchStartDate());
        batcv.put("BatchEndDate",batchlist.get(i).getBatchEndDate());
        batcv.put("TimeTable",batchlist.get(i).getTimeTable());
        batcv.put("Subject",batchlist.get(i).getSubjects());

        con.update("tblBatch",batcv,"BatchID "+ " = ?",new String[]{batchlist.get(i).getBatchId()});

    }
    con.close();

}

public void InsertBatch(ArrayList<Batchpojo> batchlist){

    SQLiteDatabase con=this.getWritableDatabase();


    int schid=0;
    Cursor c=retrive("tblSchool");
    while(c.moveToNext()){
        schid=c.getInt(c.getColumnIndex("SchoolID"));
    }c.close();

    for(int i=0;i<batchlist.size();i++) {
        ContentValues batcv = new ContentValues();
        batcv.put("BatchID",Integer.parseInt(batchlist.get(i).getBatchId()));
        batcv.put("CoordinatingStaffID",Integer.parseInt(batchlist.get(i).getCoordinatingStaffID()));
        batcv.put("AcademicYear",batchlist.get(i).getAcademicYear());
        batcv.put("SchoolID",schid);
        batcv.put("ClassID",Integer.parseInt(batchlist.get(i).getBatchclassid()));
        batcv.put("BatchStartDate",batchlist.get(i).getBatchStartDate());
        batcv.put("BatchEndDate",batchlist.get(i).getBatchEndDate());
        batcv.put("TimeTable",batchlist.get(i).getTimeTable());
        batcv.put("Subject",batchlist.get(i).getSubjects());
        con.insert("tblBatch",null,batcv);

    }
    con.close();
}




}