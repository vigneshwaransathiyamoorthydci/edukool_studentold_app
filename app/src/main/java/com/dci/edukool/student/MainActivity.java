package com.dci.edukool.student;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.ebookdroid.ui.viewer.IViewController;
import org.ebookdroid.ui.viewer.ViewerActivity;
import org.ebookdroid.ui.viewer.ViewerActivityController;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import Utils.Utils;
import Utils.*;
import books.BookBinActivity;
import books.ImageActivity;
import books.VideoActivity;
import books.VideoBinActivity;
import calendar.CalendarTypePojo;
import calendar.Calendaractivity;
import connection.Acknowled;
import connection.Client;
import connection.Communication;
import connection.MultiThreadChatServerSync;
import connection.Serversocket;
import connection.Violationackreceive;
import drawboard.MyDialog;
import drawboard.Whiteboard;
import exam.DatabaseHandler;
import exam.ExamDetails;
import exam.QuestionDetails;
import exam.QuizActivity;
import exam.QuizWrapper;
import exam.SelfEvaluationActivity;
import exam.StudentExamListActivity;
import helper.BroadcastMessageListener;
import helper.Broadcastlistener;
import helper.Communicationreceive;
import helper.Filemessage;
import helper.Permission;
import helper.Rooms;
import helper.RoundedImageView;
import helper.Violationmessage;
import messaging.CommunicationActivity;
import pulsequestion.PulseActivity;

import services.AppCheckServices;
import steadytate.Password;
import timetable.TimeTableActivity;
import wificonnectivity.WifiBase;
import steadytate.customviewgroup;

/*
 * Permission needed:
 * <uses-permission android:name="android.permission.INTERNET"/>
 * <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
 */

public class MainActivity extends AppCompatActivity implements WifiBase.WifiBaseListener {

    // TextView infoIp, infoPort;

    static final int SocketServerPORT = 8080;
    ServerSocket serverSocket;
    MultiThreadChatServerSync multi = null;
    Communication com = null;
    Violationackreceive vol=null;
    ArrayList<Socket> soc;
    // TextView message;
    ProgressDialog bar;
    private static String TAG = "MainActivity";
    String getclient;
    ServerSocketThread serverSocketThread;
    Serversocket server;
    Client mClient = null;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    // Button sync;
    private Broadcastlistener broadcastUtils;
    private BroadcastMessageListener broadcastMessageListener;
    private ScheduledExecutorService scheduleTaskExecutor;
    LinearLayout bookbin, whiteboard;
    LinearLayout selftest;
    LinearLayout exam;
    LinearLayout exit;
    LinearLayout multimedia;
    LinearLayout communication;
    LinearLayout timetable;
    LinearLayout calendar;
    Socket socket = null;
    int i;
    private static final int BUFFER_SIZE = 4096;

    private WifiBase mWifiBase;
    BroadcastReceiver receiver;
    RoundedImageView profileimage;
    connectTask task = null;
    TextView studentname, academic, classname;
    int reconnect;
    ImageView background, logo, back2, opendraw;
    public static String studentName;
    //dbvalues
    String clas, sec, ac, backgroundimage;
    String logourl, profile, backurl, stdname;
    int batchid;
    SqliteOpenHelperDemo obj;

    BroadcastReceiver wifilisetner;
    String ip;

    ArrayList<User> arrayOfUsers;
    UsersAdapter adapter;
    ListView listView;
    private PopupWindow pwindo, listwindo;
    String wifipass = "";
    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    StringBuilder sb = new StringBuilder();
    private final Handler handler = new Handler();
    String storagepath;

    String roomArray;
    Spinner roomSpinner;
    ArrayList<Rooms> roomlist;
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID;
    String ExamCategory;
    String ExamCode;
    int QuestionID;
    String ExamDescription;
    String BatchIDVal;
    String ExamDurationVal;
    int BatchID;
    int ExamDuration;
    int Mark;
    String SubjectIDVal;
    String Subject;
    int ObtainedMark;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    int SubjectID;
    String ExamTypeIDVal;
    int NegativeMark;
    ProgressDialog dia;
    int IsCorrect;
    String MarkForAnswer;
    String ExamType;
    DatabaseHandler db;
    String question;
    JSONArray options;
    String ExamDate;
    int AspectID;
    int ExamTypeID;
    int TopicID;
    String TopicIDVal, Topic, AspectIDVal, Aspect, QuestionNumberVal;
    int CorrectAnswerVal;
    String CorrectAnswer;
    String StudentAnswer;
    String ExamIDVal;
    int QuestionNumber;

    ArrayList<String> chklist;
    TextView roomstext;
    TextView subjectext;

    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    LinearLayout drawer;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    ImageView status;

    ArrayList<Masterpojo> sublist;
    Spinner subSpinner;
    boolean coonnectedtowifi;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    Utils utils;
    ProgressDialog diadown;
    TextView studentbirthydayText;
    String dob;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
       /* try {
            getWindow().addFlags(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
        }
        catch (Exception e)
        {

        }
*/
        setContentView(R.layout.dashboardnewdash);


        startService(new Intent(MainActivity.this, AppCheckServices.class));


        if (Build.VERSION.SDK_INT >= 23) {
            if (Settings.System.canWrite(MainActivity.this)) {
                // Do stuff here
            } else {
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        if(Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(MainActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
            //disablenotification();
        }
        else {
          // disablenotification();
        }


        coonnectedtowifi = false;
        pref = getSharedPreferences("student", MODE_PRIVATE);
        edit = pref.edit();
        studentName = pref.getString("name", "iyyappa");
        db = new DatabaseHandler(this);
        i = 0;
        reconnect = 0;
        bookbin = (LinearLayout) findViewById(R.id.bookbin);
        whiteboard = (LinearLayout) findViewById(R.id.whiteboard);
        selftest = (LinearLayout) findViewById(R.id.selfevalution);
        exam = (LinearLayout) findViewById(R.id.exams);
        exit = (LinearLayout) findViewById(R.id.exit);
        studentname = (TextView) findViewById(R.id.teachename);
        multimedia = (LinearLayout) findViewById(R.id.multimedialin);
        communication = (LinearLayout) findViewById(R.id.communicationlinear);
        calendar = (LinearLayout) findViewById(R.id.calendarlin);
        timetable = (LinearLayout) findViewById(R.id.timetablelin);
        profileimage = (RoundedImageView) findViewById(R.id.profileimage);
        background = (ImageView) findViewById(R.id.background);
        opendraw = (ImageView) findViewById(R.id.opendraw);
        logo = (ImageView) findViewById(R.id.schoollogo);
        status = (ImageView) findViewById(R.id.staus);
        roomSpinner = (Spinner) findViewById(R.id.room);
        subSpinner = (Spinner) findViewById(R.id.sub);
        roomstext = (TextView) findViewById(R.id.roomtext);
        subjectext = (TextView) findViewById(R.id.subtext);
        subjectext.setVisibility(View.GONE);
        roomstext.setText("Session Break");
        studentbirthydayText=(TextView)findViewById(R.id.studentbirthydayText);
        classname = (TextView) findViewById(R.id.classnamewithstaff);
        //   back2= (ImageView) findViewById(R.id.teacherdetaillayout);
        academic = (TextView) findViewById(R.id.acdemicyear);
        Permission.verifyStoragePermissions(this);

        status.setBackgroundResource(R.drawable.offline);
        if (pref.getString("name", "iyyappa").equalsIgnoreCase("iyyappa")) {
            profileimage.setImageResource(R.drawable.student1);
            studentname.setText("Iyyappa");
        } else {
            profileimage.setImageResource(R.drawable.student2);
            studentname.setText(pref.getString("name", "Iyyapparaj"));
        }
        obj = new SqliteOpenHelperDemo(getApplicationContext());

        utils = new Utils(MainActivity.this);
      /*  Cursor c=obj.retrive("tblSchool");
        while(c.moveToNext()){
            backgroundimage= c.getString(c.getColumnIndex("backgroundimage"));
        }*/


        coonnectedtowifi = true;
        edit.putString("wifissid", pref.getString("classwifissid", ""));
        edit.putString("wifipassword", pref.getString("classwifipassword", ""));
        edit.putBoolean("classtime", false);
        edit.commit();
        mWifiBase = new WifiBase(MainActivity.this);


        Cursor forimage = obj.retrive("tblSchool");
        while (forimage.moveToNext()) {
            logourl = forimage.getString(forimage.getColumnIndex("SchoolLogo"));
            backurl = forimage.getString(forimage.getColumnIndex("backgroundimage"));
        }

        Cursor stdcur = obj.retrive("tblStudent");
        while (stdcur.moveToNext()) {
            profile = stdcur.getString(stdcur.getColumnIndex("PhotoFilename"));
            roomArray = stdcur.getString(stdcur.getColumnIndex("Room"));
        }


        //  Toast.makeText(getApplicationContext(),""+roll,Toast.LENGTH_LONG).show();

        if (profile != null) setbackground(profileimage, profile);
        if (backurl != null) setbackground(background, backurl);
        if (logourl != null) setbackground(logo, logourl);


        Cursor batcursor = obj.retriveClassName();
        while (batcursor.moveToNext()) {
            clas = batcursor.getString(batcursor.getColumnIndex("ClassName"));
            sec = batcursor.getString(batcursor.getColumnIndex("SectionName"));

        }
        batcursor.close();

        Cursor tc = obj.retrive("tblStudent");
        while (tc.moveToNext()) {
            batchid = tc.getInt(tc.getColumnIndex("BatchID"));
            stdname = tc.getString(tc.getColumnIndex("FirstName"));
            dob=tc.getString(tc.getColumnIndex("DOB"));
            if (dob.equals("-0001-11-30"))
            {
                dob="0000-00-00";
            }

        }
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);
        SimpleDateFormat currentdate = new SimpleDateFormat("dd-MM");
        String cureentformattedDate = currentdate.format(c);

        String mytime=dob;
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-dd-MM");

        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("MM-dd");
        String finalDate = timeFormat.format(myDate);

        System.out.println(finalDate);
        if (finalDate.equals(cureentformattedDate)) {
            Log.d("VIKIs", "student birthyday" + finalDate + " " + cureentformattedDate);
            studentbirthydayText.setVisibility(View.VISIBLE);


        }

        edit.putString("violationname",stdname);
        edit.commit();


        studentname.setText(stdname.toUpperCase());

        Cursor bc = obj.retrivevalue(batchid);
        while (bc.moveToNext()) {
            ac = bc.getString(bc.getColumnIndex("AcademicYear"));
        }

        academic.setText(clas + " " + sec + " " + ac);
        classname.setText(clas + " " + sec);


        //spinnerRoom
        roomlist = new ArrayList<>();

        try {
            if (roomArray.contains(",")) {
                String SplitRoom[] = roomArray.split(",");

                for (int i = 0; i < SplitRoom.length; i++) {

                    Rooms getroom = obj.getroomdata(Integer.parseInt(SplitRoom[i]));
                    Rooms room = new Rooms();
                    room.setId(getroom.getId());
                    room.setPassword(getroom.getPassword());
                    room.setRoomsname(getroom.getRoomsname());
                    room.setSsid(getroom.getSsid());
                    roomlist.add(room);
                }
            } else {
                Rooms getroom = obj.getroomdata(Integer.parseInt(roomArray));

                Rooms room = new Rooms();
                room.setId(getroom.getId());
                room.setPassword(getroom.getPassword());
                room.setRoomsname(getroom.getRoomsname());
                room.setSsid(getroom.getSsid());
                roomlist.add(room);
            }

        } catch (Exception e) {
        }
        Roomspinner dataAdapter = new Roomspinner(MainActivity.this,
                android.R.layout.simple_spinner_item, roomlist);
        // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roomSpinner.setAdapter(dataAdapter);


        roomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //     Toast.makeText(getApplicationContext(),roomlist.get(position).getRoomsname()+","+roomlist.get(position).getId(),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //end spinner room





       /* Cursor subcursor=obj.retriveSubjects(batchid);
        while(subcursor.moveToNext()){
            Masterpojo pojo=new Masterpojo();
            pojo.setSubjectname(subcursor.getString(subcursor.getColumnIndex("SubjectName")));
            pojo.setSubid(String.valueOf(subcursor.getInt(subcursor.getColumnIndex("SubjectID"))));
            sublist.add(pojo);
        }

        Subspinner subjectadapter = new Subspinner(MainActivity.this,
                android.R.layout.simple_spinner_item, sublist);
        // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subSpinner.setAdapter(subjectadapter);


        subSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),sublist.get(position).getSubjectname()+","+sublist.get(position).getSubid(),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

*/


        multimedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //toast();

                //  Uri uri = Uri.parse(pdfpth.getText().toString());
                //File file=new File("/storage/emulated/0/cps_113/Users/S_113/Material/Academic/1498541495.pdf");

                //Uri uri = Uri.parse("file:///android_asset/" + TEST_FILE_NAME);
                   /* Intent intent = new Intent(con, MuPDFActivity.class);
                    intent.putExtra("linkhighlight", true);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(uri);

                    //if document protected with password
                    intent.putExtra("password", "encrypted PDF password");

                    //if you need highlight link boxes
                    intent.putExtra("linkhighlight", true);
                    intent.putExtra("lock", false);

                    //if you don't need device sleep on reading document
                    intent.putExtra("idleenabled", false);

                    //set true value for horizontal page scrolling, false value for vertical page scrolling
                    intent.putExtra("horizontalscrolling", true);

                    //document name
                    intent.putExtra("docname", bookname.getText().toString());

                    con.startActivity(intent);*/

              /*  Intent intent = new Intent(Intent.ACTION_VIEW,  Uri.fromFile(file));
                intent.setClass(MainActivity.this, ViewerActivity.class);
                intent.putExtra("pagenumber", 3);
                intent.putExtra("lock", false);
                intent.putExtra("bookname","");
                startActivity(intent);*/

               /* Intent intent = new Intent(Intent.ACTION_VIEW,  Uri.fromFile(file));
                intent.setClass(MainActivity.this, ViewerActivity.class);
                intent.putExtra("bookname","");
       *//* if (b != null) {
            intent.putExtra("pageIndex", "" + b.page.viewIndex);
            intent.putExtra("offsetX", "" + b.offsetX);
            intent.putExtra("offsetY", "" + b.offsetY);
        }*//*
                startActivity(intent);*/
                startActivity(new Intent(MainActivity.this, VideoBinActivity.class));
            }
        });

        communication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CommunicationActivity.class));
            }
        });
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Calendaractivity.class));
            }
        });
        timetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TimeTableActivity.class));
            }
        });

        selftest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!pref.getBoolean("classtime", false)) {
                    startActivity(new Intent(MainActivity.this, SelfEvaluationActivity.class));
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.session), Toast.LENGTH_LONG).show();
                }


            }
        });


        exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, StudentExamListActivity.class);
                startActivity(intent);
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        bookbin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(MainActivity.this, BookBinActivity.class));

            }
        });
        whiteboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Whiteboard.synwhiteboard = false;
                startActivity(new Intent(MainActivity.this, Whiteboard.class));

            }
        });

        opendraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDrawerLayout.openDrawer(Gravity.LEFT);

            }
        });


        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        drawer = (LinearLayout) findViewById(R.id.drawer);
        DataModel[] drawerItem = new DataModel[5];
        initialize();

        drawerItem[0] = new DataModel(R.drawable.wifi, "Wifi");
      /*  drawerItem[1] = new DataModel(R.drawable.pulse, "Pulse");
        drawerItem[2] = new DataModel(R.drawable.sync, "Connect");*/
        drawerItem[1] = new DataModel(R.drawable.brightness, "Brightness");
        drawerItem[2] = new DataModel(R.drawable.download, "Get Update");
        drawerItem[3] = new DataModel(R.drawable.download, "Get Software Update");
        drawerItem[4] = new DataModel(R.drawable.cancel, "Close");


        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();

   /*  Cursor c1=obj.retrive("tblContent");


      while(c1.moveToNext()) {

       Toast.makeText(getApplicationContext(), "" + c1.getString(c1.getColumnIndex("Subject")), Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(),  ""+c1.getString(c1.getColumnIndex("ContentCatalogType")), Toast.LENGTH_LONG).show();
        Toast.makeText(getApplicationContext(), "" + c1.getString(c1.getColumnIndex("ContentFilename")), Toast.LENGTH_LONG).show();
         //Toast.makeText(getApplicationContext(), "" + c1.getString(c1.getColumnIndex("LastName")), Toast.LENGTH_LONG).show();



      }
*/


       /* mWifiBase = new WifiBase(this);*///for wificonnectivity


    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }


    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                //    fragment = new ConnectFragment();
                mDrawerLayout.closeDrawer(drawer);
                if (!pref.getBoolean("classtime", false)) {
                    Listpopup();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.session), Toast.LENGTH_LONG).show();
                }
                //Toast.makeText(getApplicationContext(),"first",Toast.LENGTH_LONG).show();
                break;
            case 1:
                startActivity(new Intent(MainActivity.this, Brightness.class));
                mDrawerLayout.closeDrawer(drawer);
                //Toast.makeText(getApplicationContext(),"second",Toast.LENGTH_LONG).show();
                //     fragment = new FixturesFragment();
                break;
            case 2:

                String osversion = Build.VERSION.RELEASE;
                //   String devname = android.os.Build.MODEL;
                String appversion = BuildConfig.VERSION_NAME;

                String login_str = "UserName:" + pref.getString("username", "") + "|Password:" + pref.getString("password", "") + "|Function:StudentLogin|Update:Yes" + "|DeviceType:Android|AppVersion:" + appversion + "|MACAddress:" + "|OSVersion:" + osversion + "|GCMKey:" + "|DeviceID:" + "|AppID:" + "|IMEINumber:";
                login.clear();
                byte[] data;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        login.clear();
                        login.add(new BasicNameValuePair("WS", base64_register));

                        if (!pref.getBoolean("classtime", false)) {
                            Load_Login_WS load_plan_list = new Load_Login_WS(MainActivity.this, login);
                            load_plan_list.execute();
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.session), Toast.LENGTH_LONG).show();
                        }

                    } else {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


                mDrawerLayout.closeDrawer(drawer);
                //Toast.makeText(getApplicationContext(),"third",Toast.LENGTH_LONG).show();
                //    fragment = new TableFragment();
                break;
            case 3:
                try {
                    new DownloadNewVersion().execute();
                }
               catch (Exception e)
               {

               }
              //  mDrawerLayout.closeDrawer(drawer);
                //Toast.makeText(getApplicationContext(),"third",Toast.LENGTH_LONG).show();
                //    fragment = new TableFragment();
                break;
            case 4:
                mDrawerLayout.closeDrawer(drawer);
                //Toast.makeText(getApplicationContext(),"third",Toast.LENGTH_LONG).show();
                //    fragment = new TableFragment();
                break;

            default:
                break;
        }

      /*  if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }*/
    }

    void setupDrawerToggle() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public Load_Login_WS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(MainActivity.this);
            dia.setMessage("Downloading...");
            dia.setCancelable(false);
            dia.show();


        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service sr = new Service(MainActivity.this);
                jsonResponseString = sr.getLogin(login,
                        Url.base_url);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if (dia.isShowing())
                dia.cancel();
            new secondasync(jsonResponse).execute();

            // new LoginActivity.secondasync(jsonResponse).execute();

        }
    }


    class secondasync extends AsyncTask<String, String, String> {
        String jsonResponse;
        ProgressDialog dia;

        public secondasync(String jsonResponse) {
            this.jsonResponse = jsonResponse;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(MainActivity.this);
            dia.setMessage("Downloading..");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                String statusCode = jObj.getString("StatusCode");

                // if (status.toString().equalsIgnoreCase("Success")) {
                if (statusCode.toString().equalsIgnoreCase("200")) {


                    ArrayList<Masterpojo> masterlist = new ArrayList<Masterpojo>();

                    for (int i = 0; i < jObj.getJSONObject("masterInfo").getJSONArray("Subjects").length(); i++) {
                        int mid;
                        try {
                            mid = Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));
                        } catch (Exception e) {
                            mid = 0;
                        }

                        Cursor c = obj.retriveIsMasterID(mid);
                        Masterpojo masterpojo = new Masterpojo();
                        if (c.getCount() > 0 == false) {

                            masterpojo.setSubid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));
                            masterpojo.setSubjectname(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectName"));
                            masterpojo.setSubjectdesc(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectDescription"));
                            masterpojo.setCreatedBy(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedBy"));
                            masterpojo.setCreateddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedDate"));
                            masterpojo.setModifiedby(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedBy"));
                            masterpojo.setModifieddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedDate"));
                            masterpojo.setStatus(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("Status"));
                            masterpojo.setSchoolid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SchoolID"));

                            masterlist.add(masterpojo);

                        }

                        if (c.getCount() > 0) {

                            masterpojo.setSubid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ID"));
                            masterpojo.setSubjectname(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectName"));
                            masterpojo.setSubjectdesc(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SubjectDescription"));
                            masterpojo.setCreatedBy(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedBy"));
                            masterpojo.setCreateddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("CreatedDate"));
                            masterpojo.setModifiedby(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedBy"));
                            masterpojo.setModifieddate(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("ModifiedDate"));
                            masterpojo.setStatus(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("Status"));
                            masterpojo.setSchoolid(jObj.getJSONObject("masterInfo").getJSONArray("Subjects").getJSONObject(i).getString("SchoolID"));
                            obj.UpdateMasterInfo(masterpojo);
                        }
                    }

                    if (masterlist.size() != 0)
                        obj.InsertMasterInfo(masterlist);

                    obj.UpdateSubjectFromMaster();

                    JSONArray roomsarray = jObj.getJSONObject("masterInfo").getJSONArray("Rooms");

                    for (int r = 0; r < roomsarray.length(); r++) {

                        Rooms room = new Rooms();
                        JSONObject roomobject = roomsarray.getJSONObject(r);
                        int rid;
                        try {
                            rid = Integer.parseInt(roomobject.getString("ID"));
                        } catch (Exception e) {
                            rid = 0;
                        }

                        Cursor c = obj.retriveIsRoomid(rid);

                        if (c.getCount() > 0 == false) {
                            try {
                                room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);
                            } catch (Exception e) {
                                room.setId(0);
                            }
                            room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                            room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                            room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");
                            obj.addmasterroom(room);
                        }
                        if (c.getCount() > 0) {
                            try {
                                room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);
                            } catch (Exception e) {
                                room.setId(0);
                            }
                            room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                            room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                            room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");
                            obj.UpdateRoom(room);
                        }
                    }


                    for (int i = 0; i < jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").length(); i++) {

                        CalendarTypePojo calendarTypePojo = new CalendarTypePojo();
                        int chkid;
                        try {
                            chkid = Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID"));
                        } catch (Exception e) {
                            chkid = 0;
                        }
                        Cursor c = obj.retriveIsCalenderTypeId(chkid);

                        if (c.getCount() > 0 == false) {
                            try {
                                calendarTypePojo.setEventtypeid(Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID")));
                            } catch (Exception e) {
                                calendarTypePojo.setEventtypeid(0);
                            }

                            calendarTypePojo.setEventtypename(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Name"));
                            calendarTypePojo.setEventtypeDescription(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Description"));


                            String imagefile = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").substring(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").lastIndexOf("/") + 1, jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").length());

                            try {
                                downloadfile(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image"), pref.getString("calender", ""), imagefile);
                            } catch (Exception e) {
                            }
                            String imagename = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image");
                            String local_path = pref.getString("calender", "") + "/" + imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());
                            calendarTypePojo.setEventtypeimage(local_path);


                            obj.InsertCalenderType(calendarTypePojo);
                        }

                        if (c.getCount() > 0) {

                            try {
                                calendarTypePojo.setEventtypeid(Integer.parseInt(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("ID")));
                            } catch (Exception e) {
                                calendarTypePojo.setEventtypeid(0);
                            }

                            calendarTypePojo.setEventtypename(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Name"));
                            calendarTypePojo.setEventtypeDescription(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Description"));


                            String imagefile = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").substring(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").lastIndexOf("/") + 1, jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image").length());

                            try {
                                downloadfile(jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image"), pref.getString("calender", ""), imagefile);
                            } catch (Exception e) {
                            }
                            String imagename = jObj.getJSONObject("masterInfo").getJSONArray("CalendarType").getJSONObject(i).getString("Image");
                            String local_path = pref.getString("calender", "") + "/" + imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());
                            calendarTypePojo.setEventtypeimage(local_path);
                            obj.UpdateCalenderType(calendarTypePojo);
                        }


                    }


                    //student info


                    SqliteOpenHelperDemo obj = new SqliteOpenHelperDemo(getApplicationContext());


                    ContentValues cv = new ContentValues();
                    try {
                        cv.put("StudentID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ID")));
                    } catch (Exception e) {
                        cv.put("StudentID", 0);
                    }
                    cv.put("AdmissionNumber", jObj.getJSONObject("studentInfo").getString("AdmissionNumber"));
                    cv.put("DOA", jObj.getJSONObject("studentInfo").getString("AdmissionDate"));

                    String subarray = jObj.getJSONObject("studentInfo").getJSONArray("Room").toString();
                    subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                    subarray = subarray.replace("\"", "");
                    if (subarray.equalsIgnoreCase("")) {
                        cv.put("Room", "0");
                    } else {
                        cv.put("Room", subarray);
                    }
                    cv.put("FirstName", jObj.getJSONObject("studentInfo").getString("FirstName"));
                    cv.put("LastName", jObj.getJSONObject("studentInfo").getString("LastName"));
                    cv.put("DOB", jObj.getJSONObject("studentInfo").getString("DateOfBirth"));
                    cv.put("Gender", jObj.getJSONObject("studentInfo").getString("Gender"));
                    cv.put("Phone_1", jObj.getJSONObject("studentInfo").getString("Phone"));
                    cv.put("Email", jObj.getJSONObject("studentInfo").getString("Email"));
                    cv.put("FatherName", jObj.getJSONObject("studentInfo").getString("FatherName"));
                    cv.put("MotherName", jObj.getJSONObject("studentInfo").getString("MotherName"));
                    cv.put("GuardianMobileNumber", jObj.getJSONObject("studentInfo").getString("GuardianPhone"));
                    try {
                        String s = jObj.getJSONObject("studentInfo").getString("RollNumber");
                        if (Integer.parseInt(s) > 0) {
                            cv.put("RollNo", jObj.getJSONObject("studentInfo").getString("RollNumber"));

                        }
                    } catch (Exception e) {
                        cv.put("RollNo", "0");

                    }
                    try {
                        cv.put("ClassID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ClassId")));
                    } catch (Exception e) {
                    }
                    try {
                        cv.put("BatchID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("Batch")));
                    } catch (Exception e) {
                    }
                    try {
                        cv.put("SchoolID", Integer.parseInt(jObj.getJSONObject("studentInfo").getString("SchoolID")));
                    } catch (Exception e) {
                    }
                    //cv.put("PhotoFilename", jObj.getJSONObject("studentInfo").getString("ProfileImage"));
                    cv.put("StudentLoginID", jObj.getJSONObject("studentInfo").getString("StudentLoginID"));


                    SqliteOpen pojo = new SqliteOpen();

                    //for school table
                    if (jObj.getJSONObject("schoolInfo").getString("ID").equalsIgnoreCase("")) {
                        pojo.setSchoolid("0");
                    } else {
                        pojo.setSchoolid(jObj.getJSONObject("schoolInfo").getString("ID"));
                    }

                    pojo.setSchoolName(jObj.getJSONObject("schoolInfo").getString("Name"));
                    pojo.setAcronym(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"));

                    String acrid = jObj.getJSONObject("studentInfo").getString("ID");
                    String bac = jObj.getJSONObject("schoolInfo").getString("BackgroundImage");
                    String schoolfolder = jObj.getJSONObject("schoolInfo").getString("SchoolAcronym");
                    String namefolder = jObj.getJSONObject("studentInfo").getString("FirstName");
                    String stdid = jObj.getJSONObject("studentInfo").getString("ID");

                    File file1 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Assets");
                    file1.mkdirs();

                    // File file2 = new File(Environment.getExternalStorageDirectory(), schoolfolder+"/Users");
                    //   file2.mkdirs();
                    File file0 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid);
                    file0.mkdirs();
                    File file3 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Evaluation");
                    File file4 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material");
                    File file5 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Notes");
                    File file6 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Calendar");
                    File file7 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material" + "/Academic");
                    File file8 = new File(Environment.getExternalStorageDirectory(), schoolfolder + "_" + acrid + "/Users" + "/S_" + stdid + "/Material" + "/Reference");

                    edit.putString("academic", file7.getAbsolutePath());
                    edit.putString("reference", file8.getAbsolutePath());
                    edit.putString("notes", file5.getAbsolutePath());

                    edit.putString("calender", file6.getAbsolutePath());

                    edit.putString("Evaluation", file3.getAbsolutePath());
                    edit.commit();

                    file3.mkdirs();
                    file4.mkdirs();
                    file5.mkdirs();
                    file6.mkdirs();
                    file7.mkdirs();
                    file8.mkdirs();

                    // createFolder(schoolfolder,jObj.getJSONObject("studentInfo").getString("FirstName"),jObj.getJSONObject("schoolInfo").getString("Logo"),jObj.getJSONObject("studentInfo").getString("ProfileImage"),bac);
                    String schoollogo = jObj.getJSONObject("schoolInfo").getString("Logo");
                    String stdprofile = jObj.getJSONObject("studentInfo").getString("ProfileImage");


                    String stdimagedb = file1.getAbsolutePath() + "/" + stdprofile.substring(stdprofile.lastIndexOf("/") + 1, stdprofile.length());
                    String logoimagedb = file1.getAbsolutePath() + "/" + "logo_" + schoollogo.substring(schoollogo.lastIndexOf("/") + 1, schoollogo.length());
                    String backimagedb = file1.getAbsolutePath() + "/" + "background_" + bac.substring(bac.lastIndexOf("/") + 1, bac.length());
                    cv.put("PhotoFilename", stdimagedb);
                    pojo.setSchoolLogo(logoimagedb);
                    pojo.setBackgroundimage(backimagedb);
                    try {
                        DownloadTask1(schoollogo, file1.getAbsolutePath());
                    } catch (Exception e) {
                    }
                    try {
                        DownloadTask2(bac, file1.getAbsolutePath());
                    } catch (Exception e) {
                    }
                    try {
                        DownloadTask(stdprofile, file1.getAbsolutePath(), stdimagedb, logoimagedb, backimagedb);
                    } catch (Exception e) {

                    }

                    //for classes update
                    if (jObj.getJSONObject("Classes").getString("ID").equalsIgnoreCase("")) {
                        pojo.setClassID("0");
                    } else {
                        pojo.setClassID(jObj.getJSONObject("Classes").getString("ID"));
                    }

                    pojo.setClassName(jObj.getJSONObject("Classes").getString("ClassName"));
                    pojo.setSectionName(jObj.getJSONObject("Classes").getString("Section"));
                    pojo.setGrade(jObj.getJSONObject("Classes").getString("Grade"));

                    if (jObj.getJSONObject("Classes").getString("SchoolID").equalsIgnoreCase("")) {
                        pojo.setSchoolID("0");
                    } else {
                        pojo.setSchoolID(jObj.getJSONObject("Classes").getString("SchoolID"));
                    }

                    if (jObj.getJSONObject("Classes").getString("Department").equalsIgnoreCase("")) {
                        pojo.setDepartmentID("0");
                    } else {
                        pojo.setDepartmentID(jObj.getJSONObject("Classes").getString("Department"));
                    }

                    edit.putString("wifissid", jObj.getJSONObject("Classes").getString("InternetSSID"));
                    edit.putString("wifipassword", jObj.getJSONObject("Classes").getString("InternetPassword"));
                    edit.putString("classwifissid", jObj.getJSONObject("Classes").getString("InternetSSID"));
                    edit.putString("classwifipassword", jObj.getJSONObject("Classes").getString("InternetPassword"));
                    edit.commit();

                    pojo.setInternetSSID(jObj.getJSONObject("Classes").getString("InternetSSID"));
                    pojo.setInternetPassword(jObj.getJSONObject("Classes").getString("InternetPassword"));
                    pojo.setInternetType(jObj.getJSONObject("Classes").getString("InternetType"));
                    pojo.setPushName(jObj.getJSONObject("Classes").getString("IpushName"));


////////////for STudent
                    Cursor scur = null;
                    try {
                        scur = obj.retriveIsStudentId(Integer.parseInt(jObj.getJSONObject("studentInfo").getString("ID")));
                    } catch (Exception e) {
                        scur = obj.retriveIsStudentId(0);
                    }
                    if (scur.getCount() > 0) {
                        obj.UpdateStudent(cv, jObj.getJSONObject("studentInfo").getString("ID"));
                    } else {
                        obj.InsertStudent(cv);
                    }
                    scur.close();

////for School
                    Cursor schcur = null;
                    try {
                        schcur = obj.retriveIsSchool(Integer.parseInt(jObj.getJSONObject("schoolInfo").getString("ID")));
                    } catch (Exception e) {
                        schcur = obj.retriveIsSchool(0);
                    }
                    if (schcur.getCount() > 0) {
                        obj.UpdateSchool(pojo);
                    } else {
                        obj.InsertSchool(pojo);
                    }
                    schcur.close();

/////for Class
                    Cursor clascur = null;
                    Log.e("classid", jObj.getJSONObject("Classes").getString("ID"));
                    try {
                        clascur = obj.retriveIsClass(Integer.parseInt(jObj.getJSONObject("Classes").getString("ID")));
                    } catch (Exception e) {
                        clascur = obj.retriveIsClass(0);
                    }


                    if (clascur.getCount() > 0) {
                        obj.UpdateClass(pojo);
                    } else {
                        obj.InsertClass(pojo);
                    }
                    clascur.close();


                    //for staffInfo Update

                    ArrayList<Staffpojo> stafflist = new ArrayList<Staffpojo>();
                    for (int i = 0; i < jObj.getJSONArray("staffInfo").length(); i++) {
                        Staffpojo staffpojo = new Staffpojo();

                        Cursor stfcur = null;
                        try {
                            stfcur = obj.retriveIsStaff(Integer.parseInt(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID")));
                        } catch (Exception e) {
                            stfcur = obj.retriveIsStaff(0);
                        }
                        if (stfcur.getCount() > 0) {


                            if (jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                staffpojo.setStaffid("0");
                            } else {
                                staffpojo.setStaffid(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID"));
                            }

                            staffpojo.setDOJ(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfJoining"));
                            staffpojo.setFirstName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FirstName"));
                            staffpojo.setLastName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("LastName"));
                            staffpojo.setGender(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Gender"));
                            staffpojo.setDOB(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfBirth"));
                            staffpojo.setMaritalStatusID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MaritalStatus"));
                            staffpojo.setSpouseName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("SpouseName"));
                            staffpojo.setFatherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FatherName"));
                            staffpojo.setMotherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MotherName"));
                            staffpojo.setPhone(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("PhoneNumber"));
                            staffpojo.setEmail(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Email"));
                            staffpojo.setPhotoFilename(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ProfileImage"));
                            if (jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory").equalsIgnoreCase("")) {
                                staffpojo.setStaffCategoryID("0");
                            } else {
                                staffpojo.setStaffCategoryID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory"));
                            }

                            stafflist.add(staffpojo);

                            obj.UpdateStaff(stafflist);
                        }

                        if (stfcur.getCount() > 0 == false) {


                            if (jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                staffpojo.setStaffid("0");
                            } else {
                                staffpojo.setStaffid(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ID"));
                            }

                            staffpojo.setDOJ(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfJoining"));
                            staffpojo.setFirstName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FirstName"));
                            staffpojo.setLastName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("LastName"));
                            staffpojo.setGender(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Gender"));
                            staffpojo.setDOB(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("DateOfBirth"));
                            staffpojo.setMaritalStatusID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MaritalStatus"));
                            staffpojo.setSpouseName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("SpouseName"));
                            staffpojo.setFatherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("FatherName"));
                            staffpojo.setMotherName(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("MotherName"));
                            staffpojo.setPhone(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("PhoneNumber"));
                            staffpojo.setEmail(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("Email"));
                            staffpojo.setPhotoFilename(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("ProfileImage"));
                            if (jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory").equalsIgnoreCase("")) {
                                staffpojo.setStaffCategoryID("0");
                            } else {
                                staffpojo.setStaffCategoryID(jObj.getJSONArray("staffInfo").getJSONObject(i).getString("StaffCategory"));
                            }

                            stafflist.add(staffpojo);
                            obj.InsertStaff(stafflist);
                        }

                        stfcur.close();

                    }


                    //forBatchtable

                    ArrayList<Batchpojo> batchlist = new ArrayList<Batchpojo>();
                    for (int i = 0; i < jObj.getJSONObject("Classes").getJSONArray("Batches").length(); i++) {
                        Batchpojo batchpojo = new Batchpojo();

                        Cursor batcursor = null;
                        try {
                            batcursor = obj.retriveIsBatch(Integer.parseInt(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID")));
                        } catch (Exception e) {
                            batcursor = obj.retriveIsBatch(0);
                        }

                        if (batcursor.getCount() > 0) {


                            if (jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                batchpojo.setBatchId("0");
                            } else {
                                batchpojo.setBatchId(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID"));
                            }

                            if (jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator").equalsIgnoreCase("")) {
                                batchpojo.setCoordinatingStaffID("0");
                            } else {
                                batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            }
                            //batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            batchpojo.setAcademicYear(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("AcademicYear"));
                            batchpojo.setBatchclassid(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Class"));
                            batchpojo.setBatchStartDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("StartDate"));
                            batchpojo.setBatchEndDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("EndDate"));
                            //batchpojo.setTimeTable(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchpojo.setSubjects(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Subjects"));

                            String timetable = jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable");
                            String timedb = file0.getAbsolutePath() + "/" + timetable.substring(timetable.lastIndexOf("/") + 1, timetable.length());
                            batchpojo.setTimeTable(timedb);

                            otherTask(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"), file0.getAbsolutePath());

                            //XmltoFolder(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"),jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchlist.add(batchpojo);

                            obj.UpdateBatch(batchlist);
                        }


                        if (batcursor.getCount() > 0 == false) {


                            if (jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID").equalsIgnoreCase("")) {
                                batchpojo.setBatchId("0");
                            } else {
                                batchpojo.setBatchId(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("ID"));
                            }

                            if (jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator").equalsIgnoreCase("")) {
                                batchpojo.setCoordinatingStaffID("0");
                            } else {
                                batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            }
                            //batchpojo.setCoordinatingStaffID(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Coordinator"));
                            batchpojo.setAcademicYear(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("AcademicYear"));
                            batchpojo.setBatchclassid(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Class"));
                            batchpojo.setBatchStartDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("StartDate"));
                            batchpojo.setBatchEndDate(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("EndDate"));
                            //batchpojo.setTimeTable(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchpojo.setSubjects(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("Subjects"));

                            String timetable = jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable");
                            String timedb = file0.getAbsolutePath() + "/" + timetable.substring(timetable.lastIndexOf("/") + 1, timetable.length());
                            batchpojo.setTimeTable(timedb);

                            otherTask(jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"), file0.getAbsolutePath());

                            //XmltoFolder(jObj.getJSONObject("schoolInfo").getString("SchoolAcronym"),jObj.getJSONObject("Classes").getJSONArray("Batches").getJSONObject(i).getString("TimeTable"));
                            batchlist.add(batchpojo);
                            obj.InsertBatch(batchlist);

                        }
                        batcursor.close();
                    }//forloop


                }//200


            } catch (Exception e) {
                Log.e("Exceptionsql", e.toString());
            }

            return null;
        }


        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if (dia.isShowing())
                dia.cancel();

            SqliteOpenHelperDemo db = new SqliteOpenHelperDemo(getApplicationContext());
            Cursor forimage = db.retrive("tblSchool");
            while (forimage.moveToNext()) {
                logourl = forimage.getString(forimage.getColumnIndex("SchoolLogo"));
                backurl = forimage.getString(forimage.getColumnIndex("backgroundimage"));
            }
            forimage.close();


            Cursor stdcur = db.retrive("tblStudent");
            while (stdcur.moveToNext()) {
                profile = stdcur.getString(stdcur.getColumnIndex("PhotoFilename"));
                roomArray = stdcur.getString(stdcur.getColumnIndex("Room"));
            }
            stdcur.close();

            //  Toast.makeText(getApplicationContext(),""+roll,Toast.LENGTH_LONG).show();

            if (profile != null) setbackground(profileimage, profile);
            if (backurl != null) setbackground(background, backurl);
            if (logourl != null) setbackground(logo, logourl);

            String clasname = "", secname = "";

            Cursor batchCursor = db.retriveClassName();

            while (batchCursor.moveToNext()) {
                clasname = batchCursor.getString(batchCursor.getColumnIndex("ClassName"));
                secname = batchCursor.getString(batchCursor.getColumnIndex("SectionName"));
            }
            batchCursor.close();


            Cursor tc = db.retrive("tblStudent");
            while (tc.moveToNext()) {
                batchid = tc.getInt(tc.getColumnIndex("BatchID"));
                stdname = tc.getString(tc.getColumnIndex("FirstName"));
            }
            tc.close();


            studentname.setText(stdname.toUpperCase());

            Cursor bc = obj.retrivevalue(batchid);
            while (bc.moveToNext()) {
                ac = bc.getString(bc.getColumnIndex("AcademicYear"));
            }
            bc.close();

            academic.setText(clasname + " " + secname + " " + ac);
            classname.setText(clasname + " " + secname);

            Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_LONG).show();
        }

    }


    private void Listpopup() {
       /* LayoutInflater inflater = (LayoutInflater) MainActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_popup,
                (ViewGroup) findViewById(R.id.listparent));
        listwindo = new PopupWindow(layout, 650, 500, true);
        listwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);*/
        final MyDialog dialog = new MyDialog(MainActivity.this);
        dialog.setContentView(R.layout.list_popup);
        listView = (ListView) dialog.findViewById(R.id.list);
        final Button close = (Button) dialog.findViewById(R.id.close);

        arrayOfUsers = new ArrayList<User>();
        //   for(int i=0;i<6;i++)
        arrayOfUsers.add(new User("", ""));

        adapter = new UsersAdapter(this, arrayOfUsers);
        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(adapter);

        doInback();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = adapter.getItem(position);
                initiatePopupWindow(user.name);
                // Toast.makeText(getApplicationContext(),"position"+user.name,Toast.LENGTH_LONG).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void initiatePopupWindow(final String ssid) {
        try {
// We need to get the instance of the LayoutInflater
           /* LayoutInflater inflater = (LayoutInflater) MainActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 450, 130, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);*/
            final MyDialog dialog = new MyDialog(MainActivity.this);
            dialog.setContentView(R.layout.screen_popup);
            final EditText password = (EditText) dialog.findViewById(R.id.edit);

            Button btnconnect = (Button) dialog.findViewById(R.id.button1);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    wifipass = password.getText().toString();
                    if (!wifipass.equalsIgnoreCase("")) {
                        WifiConfiguration wc = new WifiConfiguration();
                        wc.SSID = String.format("\"%s\"", ssid);
                        wc.preSharedKey = String.format("\"%s\"", wifipass);
                        wc.status = WifiConfiguration.Status.ENABLED;
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                        int netId = mainWifi.addNetwork(wc);
                        mainWifi.disconnect();
                        mainWifi.enableNetwork(netId, true);
                        mainWifi.reconnect();
                        doInback();
                    }

                    dialog.dismiss();
                }
            });

            Button btncancel = (Button) dialog.findViewById(R.id.button2);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void doInback() {

        handler.postDelayed(new Runnable() {

            @SuppressLint("WifiManagerLeak")
            @Override
            public void run() {
                // TODO Auto-generated method stub

                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                receiverWifi = new MainActivity.WifiReceiver();
                registerReceiver(receiverWifi, new IntentFilter(
                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                if (mainWifi.isWifiEnabled() == false) {
                    mainWifi.setWifiEnabled(true);
                }

                mainWifi.startScan();
                //  String ssid=getCurrentSsid(getApplicationContext(),mainWifi);

                // doInback();
            }
        }, 1000);

    }

    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {

            arrayOfUsers.clear();

            ArrayList<String> connections = new ArrayList<String>();
            ArrayList<Float> Signal_Strenth = new ArrayList<Float>();

            sb = new StringBuilder();
            List<ScanResult> wifiList;
            wifiList = mainWifi.getScanResults();
            for (int i = 0; i < wifiList.size(); i++) {
                connections.add(wifiList.get(i).SSID);
            }

            //  adapter.clear();
            Log.d("con-size", connections.size() + "");
            if (connections.size() == 1) {
                doInback();
            }
            for (int y = 0; y < connections.size(); y++) {


                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                String mode = "Not connected";
                String compare = null;
                //   Log.d("connectedssid", ssid);
                //      Log.d("listssid",connections.get(y));
                //    Log.e("ssidiyyappa","iyyappa"+ssid);

                if (ssid != null) {
                    compare = ssid.replace("\"", "");
                }


                if (ssid != null) {
                    if (compare.equalsIgnoreCase(connections.get(y))) {
                        mode = "connected";
                    } else {
                        mode = "Not connected";
                    }
                }

                User newUser = new User("" + connections.get(y), mode);
                arrayOfUsers.add(newUser);
                //adapter.add(newUser);


            }
            for (int i = 0; i < arrayOfUsers.size(); i++) {


                //    Log.e("size",""+arrayOfUsers.size());
                if (arrayOfUsers.get(i).getHometown().equalsIgnoreCase("Connected")) {
                    int j = i;
                    if (i == 0) {
                        break;
                    } else {
                        User use = arrayOfUsers.get(i);
                        arrayOfUsers.remove(i);
                        arrayOfUsers.add(0, use);
                        break;
                    }
                }

                    /*for(int j=i+1; j<arrayOfUsers.size(); j++)
                    {

                    }*/
            }

            adapter.notifyDataSetChanged();


        }
    }

    public static String getCurrentSsid(Context context, WifiManager wifiManager) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            //wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }
        }
        return ssid;
    }

    void setbackground(ImageView view, String filepath) {

        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                // File f = new File("path-to-file/file.png");
                // Picasso.with(getActivity()).load(imgFile).into(view);
                view.setImageBitmap(myBitmap);
           /* Picasso.with(this)
                    .load(imgFile.getAbsolutePath())
                    .transform(new CircleTransrform()).fit().placeholder(R.drawable.new_pic)
                    .into(view);*/

            }
        } catch (Exception e) {

        }
    }

    void toast() {
        Toast.makeText(getApplication(), "Under Construction", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Service.isAirplaneModeOn(this)) {

            Service.Showalert(this);

        }

        float curBrightnessValue = 0;
        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        try {
            if (curBrightnessValue > 20) {
                float brightness = curBrightnessValue / (float) 255;
                WindowManager.LayoutParams lp = getWindow().getAttributes();


                lp.screenBrightness = brightness;
                getWindow().setAttributes(lp);
            }

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Toast.makeText(getApplication(), intent.getStringExtra("filename"), Toast.LENGTH_LONG).show();
                }
            };
            IntentFilter filter = new IntentFilter("file");
            registerReceiver(receiver, filter);
            wifilisetner = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    coonnectedtowifi = false;
                    if (!intent.getBooleanExtra("wifi", false)) {
                        Log.e("connection", "" + "con");
                        try {
                            connect();
                        } catch (Exception e) {

                        }
                        //  new connectTask().execute("");
                        // Toast.makeText(getApplicationContext(), "wifi", Toast.LENGTH_LONG).show();
                    } else {
                        //  Toast.makeText(getApplicationContext(), "Room Not in range", Toast.LENGTH_LONG).show();

                    }
                }
            };
            IntentFilter wififilter = new IntentFilter("wififilter");
            registerReceiver(wifilisetner, wififilter);
        } catch (Exception e) {

        }
    }



    void disablenotification()
    {
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (25 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        customviewgroup view = new customviewgroup(this);

        manager.addView(view, localLayoutParams);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(receiver);
            unregisterReceiver(wifilisetner);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            Log.e("ondestroy", "destroy");

            // this.broadcastUtils.releaseSockets();
            if (com != null) {
                com.closesocket();
                com = null;
            }
            if (vol != null) {
                vol.closesocket();
                vol = null;
            }


            if (multi != null) {
                multi.closesocket();
                multi = null;
            }

            com = null;
            multi = null;
            vol=null;


        } catch (Exception e) {

        }

        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip = inetAddress.getHostAddress();
                        edit.putString("ip", ip);
                        edit.commit();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    @Override
    public String getWifiSSID() {
        return /*return"Edimax_DCI3"*/pref.getString("wifissid", "");
    }

    // edit.putString("wifissid",room.getSsid());
    //                              edit.putString("wifipassword",room.getPassword());
    @Override
    public String getWifiPass() {
        return /*return "Dci@SecureWeb#"*/ pref.getString("wifipassword", "");
    }

    @Override
    public int getSecondsTimeout() {
        return 50;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public class ServerSocketThread extends Thread {

        @Override
        public void run() {


            try {
                serverSocket = new ServerSocket(SocketServerPORT);
                /*MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        infoPort.setText("I'm waiting here: "
                                + serverSocket.getLocalPort());
                    }});
*/
                while (true) {
                    socket = serverSocket.accept();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean alreadysocket = false;
                            for (int i = 0; i < soc.size(); i++) {
                                if (soc.get(i).getInetAddress().toString().equalsIgnoreCase(socket.getInetAddress().toString())) {

                                    soc.remove(i);
                                    soc.add(i, socket);

                                }
                            }


                        }
                    }).start();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

    }


    @Override
    public void supportInvalidateOptionsMenu() {
        super.supportInvalidateOptionsMenu();
    }

    void initialize() {

        this.broadcastUtils = new Broadcastlistener();
        this.broadcastUtils.addBroadcastMessageListener(this.broadcast);
        this.broadcastUtils.initialize();

        this.broadcastUtils.receive();
        mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
        if (ssid != null) {
            ssid = ssid.replace("\"", "");
        }
        if (ssid != null) {

            if (pref.getString("classwifissid", "").equals(ssid)) {
                connect();

                //  new connectTask().execute();
                // coonnectedtowifi=false;
            }
        }
        //connect();
        // updateConnectionOnInterval();


    }


    public void connect() {
        if (haveNetworkConnection(this) /*&& sessionId != 0*/) {
            Log.d("broad", "broadcasting teacher details");
            if (this.broadcastUtils != null) {
                // String portal_user_ids = masterDB.getAllPortalUserForSession(sessionId);
              /*  while (true) {*/
                final JSONObject jsonObj = new JSONObject();
                try {

                    jsonObj.put("Username", "iyyappa");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //  getTaskHandlerAndTeacherifNull();
                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // ip = getIpAddress();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

                        // Convert little-endian to big-endianif needed
                        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                            ipAddress = Integer.reverseBytes(ipAddress);
                        }

                        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

                        String ipAddressString;
                        try {
                            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
                        } catch (UnknownHostException ex) {
                            Log.e("WIFIIP", "Unable to get host address.");
                            ipAddressString = null;
                        }


                        ip = ipAddressString;
                        broadcastUtils.broadcastMessage("invS@" + "emppty" + "@" + ip + "@" + jsonObj.toString(), getBroadcastIp());

                    }
                }.execute();


            }
        } else if (!haveNetworkConnection(this)) {

        }
    }


    void disconnect() {
        if (haveNetworkConnection(this) /*&& sessionId != 0*/) {
            Log.d("broad", "broadcasting teacher details");
            if (this.broadcastUtils != null) {

                this.broadcastUtils.broadcastMessage("invs@Disconnect", getBroadcastIp());

            }
        } else if (!haveNetworkConnection(this)) {

        }

    }


    public void updateConnectionOnInterval() {
        this.scheduleTaskExecutor = Executors.newScheduledThreadPool(1);
        this.scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                if (reconnect < 2) {
                    reconnect = reconnect + 1;
                    MainActivity.this.connect();
                }
/*
                Log.i("info about resending the connection ", "sending connection", null);
*/
            }
        }, 0, 60, TimeUnit.SECONDS);
    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        for (NetworkInfo ni : ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo()) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI") && ni.isConnected()) {
                haveConnectedWifi = true;
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE") && ni.isConnected()) {
                haveConnectedMobile = true;
            }
        }
        if (haveConnectedWifi || haveConnectedMobile) {
            return true;
        }
        return false;
    }


    public class FileTxThread extends Thread {
        Socket socket;

        FileTxThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            if (socket.isConnected()) {
                File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "signature.png");

                byte[] bytes = new byte[(int) file.length()];
                BufferedInputStream bis;
                try {
                    bis = new BufferedInputStream(new FileInputStream(file));
                    // bis.read(bytes, 0, bytes.length);

    /*ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
    oos.writeObject(bytes);
    oos.flush();*/
                    DataInputStream dis = new DataInputStream(bis);
                    dis.readFully(bytes, 0, bytes.length);
                    OutputStream os = socket.getOutputStream();

                    //Sending file name and file size to the server
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(file.getName()
                    );
                    dos.writeLong(bytes.length);

                    dos.write(bytes, 0, bytes.length);

                    dos.flush();

                    //Sending file data to the server
                    os.write(bytes, 0, bytes.length);
                    os.flush();

                    //Closing socket
                    os.close();
                    dos.close();

                    final String sentMsg = "File sent to: " + socket.getInetAddress();
                    socket.close();

                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    sentMsg,
                                    Toast.LENGTH_LONG).show();
                        }
                    });

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            } else {
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Device disconnected",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }


    public class FileTxThreadread extends Thread {
        Socket socket;

        FileTxThreadread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            if (socket.isConnected()) {

                try {
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    Object o = ois.readObject();
                    //return o;
                } catch (Exception ex) {
                    //Log.v("Serialization Read Error : ", ex.getMessage());
                    ex.printStackTrace();
                }
            } else {
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Device disconnected",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    BroadcastMessageListener broadcast = new BroadcastMessageListener() {
        @Override
        public void onMessageReceived(String paramString) {


            if (paramString.contains("invT")) {
                //new connectTask().execute("");

                String[] connect = paramString.split("@");
                String staffid = connect[2];
                String roomid = connect[3];

                final String subjectid = connect[4];
                String roomornot = connect[5];
                String classid = connect[6];
                final String sessionbreak = connect[7];
                String batid = connect[8];


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        roomstext.setVisibility(View.GONE);

                    }
                });
                //Staffid[2]
                //roomid[3]
                //subjectid[4]
                String rmArray = "";
                edit.putString("serverip", connect[1]);
                edit.commit();
                // new connectTask().execute();//please comment these
                boolean connected = false;
                Cursor scur = obj.retrive("tblStudent");
                String studenclassid = "";
                while (scur.moveToNext()) {
                    studenclassid = "" + scur.getInt(scur.getColumnIndex("ClassID"));
                    //rmArray = scur.getString(scur.getColumnIndex("Room"));
                }

                if (Boolean.parseBoolean(roomornot)) {
                    boolean roomconnected = false;

                    Cursor scur1 = obj.retrive("tblStudent");
                    while (scur1.moveToNext()) {
                        rmArray = scur1.getString(scur1.getColumnIndex("Room"));
                    }
                    if (rmArray.contains(",")) {
                        String SplitRoom[] = rmArray.split(",");

                        for (int i = 0; i < SplitRoom.length; i++) {
                            try {
                                if (Integer.parseInt(roomid) == Integer.parseInt(SplitRoom[i])) {
                                    connected = true;
                                    roomconnected = true;
                                    edit.putBoolean("classtime", true);
                                    edit.commit();
                                    int rommid = Integer.parseInt(roomid);
                                    final Rooms room = obj.getroomdata(rommid);
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            roomstext.setText(room.getRoomsname());
                                            edit.putString("wifissid", room.getSsid());
                                            edit.putString("wifipassword", room.getPassword());

                                            edit.commit();
                                            subjectext.setVisibility(View.VISIBLE);
                                            Cursor c = obj.retriveSubject(Integer.parseInt(subjectid));
                                            while (c.moveToNext()) {
                                                subjectext.setText(c.getString(c.getColumnIndex("SubjectName")));
                                            }
                                            if (!Boolean.parseBoolean(sessionbreak)) {
                                                status.setBackgroundResource(R.drawable.online);

                                           /* if (!coonnectedtowifi) {*/
                                                //coonnectedtowifi = true;

                                                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                                                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                                                if (ssid != null) {
                                                    ssid = ssid.replace("\"", "");
                                                }
                                                if (ssid != null) {

                                                    if (pref.getString("wifissid", "").equals(ssid)) {
                                                        new connectTask().execute();
                                                        coonnectedtowifi = false;
                                                    } else {
                                                        coonnectedtowifi = true;
                                                        mWifiBase = new WifiBase(MainActivity.this);


                                                    }

                                                } else {


                                                    if (!coonnectedtowifi) {
                                                        coonnectedtowifi = true;
                                                        mWifiBase = new WifiBase(MainActivity.this);
                                                    }


                                                    // }

                                                    // new connectTask().execute("");

                                                }
                                            } else {
                                                coonnectedtowifi = false;

                                                edit.putBoolean("classtime", false);
                                                edit.commit();
                                                edit.putString("serverip", "");
                                                edit.commit();
                                                roomstext.setText("Session Break");
                                                roomstext.setVisibility(View.VISIBLE);

                                                status.setBackgroundResource(R.drawable.offline);
                                                subjectext.setVisibility(View.GONE);
                                            }

                                        }
                                    });
                                    break;
                                }
                            } catch (Exception e) {
                            }

                        }
                    } else {
                        try {
                            if (Integer.parseInt(roomid) == Integer.parseInt(rmArray)) {
                                connected = true;
                                roomconnected = true;

                                edit.putBoolean("classtime", true);
                                edit.commit();
                                int rommid = Integer.parseInt(roomid);
                                final Rooms room = obj.getroomdata(rommid);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        roomstext.setText(room.getRoomsname());
                                        subjectext.setVisibility(View.VISIBLE);
                                        edit.putString("wifissid", room.getSsid());
                                        edit.putString("wifipassword", room.getPassword());
                                        edit.commit();
                                        status.setBackgroundResource(R.drawable.online);
                                        Cursor c = obj.retriveSubject(Integer.parseInt(subjectid));
                                        while (c.moveToNext()) {
                                            subjectext.setText(c.getString(c.getColumnIndex("SubjectName")));
                                        }
                                    }
                                });
                                if (!Boolean.parseBoolean(sessionbreak)) {

                                    //  coonnectedtowifi = true;
                                    status.setBackgroundResource(R.drawable.online);

                                    mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                                    String ssid = getCurrentSsid(getApplicationContext(), mainWifi);

                                    if (ssid != null) {
                                        ssid = ssid.replace("\"", "");
                                    }
                                    if (ssid != null) {

                                        if (pref.getString("wifissid", "").equals(ssid)) {
                                            new connectTask().execute();
                                            coonnectedtowifi = false;
                                        } else {
                                            coonnectedtowifi = true;
                                            mWifiBase = new WifiBase(MainActivity.this);
                                        }

                                    } else if (!coonnectedtowifi) {

                                        coonnectedtowifi = true;

                                        mWifiBase = new WifiBase(MainActivity.this);

                                    }

                                    //  mWifiBase = new WifiBase(MainActivity.this);
                                    //new connectTask().execute("");

                                    // }
                                } else {
                                    edit.putBoolean("classtime", false);
                                    edit.commit();
                                    coonnectedtowifi = false;
                                    roomstext.setVisibility(View.VISIBLE);
                                    edit.putString("serverip", "");
                                    edit.commit();
                                    roomstext.setText("Session Break");
                                    status.setBackgroundResource(R.drawable.offline);
                                    subjectext.setVisibility(View.GONE);
                                }

                            }
                        } catch (Exception e) {
                        }


                        if (!roomconnected) {
                            edit.putBoolean("classtime", false);
                            edit.commit();
                            coonnectedtowifi = false;
                            roomstext.setVisibility(View.VISIBLE);
                            edit.putString("serverip", "");
                            edit.commit();
                            roomstext.setText("Session Break");
                            status.setBackgroundResource(R.drawable.offline);
                            subjectext.setVisibility(View.GONE);
                        }

                    }


                   /* if(connected)
                    {
                        int rommid=Integer.parseInt(roomid);
                        final Rooms room=obj.getroomdata(rommid);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                roomstext.setText(room.getRoomsname());
                                subjectext.setVisibility(View.VISIBLE);
                                Cursor c=obj.retriveSubject(Integer.parseInt(subjectid));
                                while(c.moveToNext()) {
                                    subjectext.setText(c.getString(c.getColumnIndex("SubjectName")));
                                }
                                status.setBackgroundResource(R.drawable.online);
                            }
                        });

                    }
                    else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                edit.putBoolean("classtime",false);
                                edit.commit();
                                roomstext.setText("Session Break");
                                status.setBackgroundResource(R.drawable.offline);
                                subjectext.setVisibility(View.GONE);
                            }
                        });*/

                    //  }

                } else if (classid.equals(studenclassid) && batid.equals("" + batchid)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        protected Object clone() throws CloneNotSupportedException {
                            return super.clone();
                        }

                        @Override
                        public void run() {
                            //roomstext.setText(room.getRoomsname());
                            subjectext.setVisibility(View.VISIBLE);
                            edit.putBoolean("classtime", true);
                            edit.commit();
                            Cursor c = obj.retriveSubject(Integer.parseInt(subjectid));
                            while (c.moveToNext()) {
                                subjectext.setText(c.getString(c.getColumnIndex("SubjectName")));
                            }
                            edit.putString("wifissid", pref.getString("classwifissid", ""));
                            edit.putString("wifipassword", pref.getString("classwifipassword", ""));
                            edit.commit();
                            status.setBackgroundResource(R.drawable.online);

                            if (!Boolean.parseBoolean(sessionbreak)) {
                                /*if (!coonnectedtowifi) {*/
                                //  coonnectedtowifi = true;
                                ///  new connectTask().execute();
                                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);


                                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                                if (ssid != null) {
                                    ssid = ssid.replace("\"", "");
                                }
                                if (ssid != null) {

                                    if (pref.getString("wifissid", "").equals(ssid)) {
                                        new connectTask().execute();

                                        coonnectedtowifi = false;
                                    } else {
                                        coonnectedtowifi = true;
                                        mWifiBase = new WifiBase(MainActivity.this);
                                    }


                                } else if (!coonnectedtowifi) {
                                    coonnectedtowifi = true;
                                    mWifiBase = new WifiBase(MainActivity.this);

                                }

                                // mWifiBase = new WifiBase(MainActivity.this);
                                status.setBackgroundResource(R.drawable.online);


                                //}
                            } else {
                                coonnectedtowifi = false;

                                edit.putBoolean("classtime", false);
                                edit.commit();
                                edit.putString("serverip", "");
                                edit.commit();
                                roomstext.setVisibility(View.VISIBLE);
                                subjectext.setVisibility(View.GONE);

                                roomstext.setText("Session Break");
                                status.setBackgroundResource(R.drawable.offline);
                                subjectext.setVisibility(View.GONE);
                            }
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            roomstext.setText("Session Break");
                            edit.putBoolean("classtime", false);
                            edit.commit();
                            coonnectedtowifi = false;
                            roomstext.setVisibility(View.VISIBLE);
                            edit.putString("serverip", "");
                            edit.commit();
                            status.setBackgroundResource(R.drawable.offline);
                            subjectext.setVisibility(View.GONE);
                        }
                    });

                }



               /* if (mClient == null) {
                    String[] connect = paramString.split("@");
                    edit.putString("serverip", connect[1]);
                    edit.commit();
                }*/

                if (multi == null) {
                    SqliteOpenHelperDemo obj = new SqliteOpenHelperDemo(getApplicationContext());
                    multi = new MultiThreadChatServerSync(pref, obj);
                    multi.setmsg(filemessage);
                }

                if (com == null) {
                    com = new Communication(pref, obj);
                    com.setmsg(comm_msg);

                }

                if (vol == null) {
                    vol = new Violationackreceive(pref, obj);
                    vol.setmsg(vmsg);

                }


            } else if (paramString.contains("invs")) {

                if (mClient != null) {
                    // mClient.socketclose();
                    mClient = null;
                }
            }

        }
    };

Violationmessage vmsg=new Violationmessage() {
    @Override
    public void onMessageReceived(String paramString) {

        edit.putStringSet("violationkey",new HashSet<String>());
        edit.commit();
    }
};
    Communicationreceive comm_msg = new Communicationreceive() {

        @Override
        public void onMessageReceived(final String paramString) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SqliteOpenHelperDemo obj = new SqliteOpenHelperDemo(MainActivity.this);
                    try {

                        if (paramString.startsWith("Circular")) {

                            String split[] = paramString.split("\\@");
                            JSONObject jobj = new JSONObject(split[1]);
                            ContentValues cv = new ContentValues();

                            String s = jobj.has("Action") ? jobj.getString("Action") : "";

                            if (!s.equalsIgnoreCase("RCVACK")) {
                       /* else
                        {

                            JSONObject getobj=new JSONObject(paramString);
                            if(getobj.getString("Action").equalsIgnoreCase("RCVACK"))
                            {
                                ContentValues values=new ContentValues();
                                values.put("IsAckSent",1);
                                values.put("IsSent",1);
                                obj.UpdateMap(Integer.parseInt(getobj.getString("MsgID")),values);
                            }
                            else  if(getobj.getString("Action").equalsIgnoreCase("RCVMSG"))
                            {
                                ContentValues values=new ContentValues();
                                // values.put("IsAckSent",1);
                                values.put("IsSent",1);
                                obj.UpdateMap(Integer.parseInt(getobj.getString("MsgID")),values);
                            }
                        }*/

                                Cursor chk = obj.CheckisMsg(Integer.parseInt(jobj.getString("MsgID")));

                                if (chk.getCount() <= 0) {
                                    cv.put("MsgID", Integer.parseInt(jobj.getString("MsgID")));
                                    cv.put("Receiver", Integer.parseInt(jobj.getString("ReceiverID")));
                                    cv.put("Sender", Integer.parseInt(jobj.getString("SenderID")));
                                    cv.put("DateOfCommunication", jobj.getString("DateOfCommunication"));
                                    cv.put("Title", jobj.getString("Title"));
                                    cv.put("Content", jobj.getString("Content"));
                                    cv.put("SubjectID", Integer.parseInt(jobj.getString("SubjectID")));
                                    cv.put("IsAckSent", 0);
                                    cv.put("IsSent", 1);
                                    obj.insertCommunication(cv);


                                    final JSONObject sendmessage = new JSONObject();
                                    sendmessage.put("Action", "RCVMSG");
                                    sendmessage.put("MsgID", jobj.getString("MsgID"));
                                    sendmessage.put("Status", "2");
                                    sendmessage.put("ReceiverID", jobj.getString("ReceiverID"));
                                    sendmessage.put("SubjectID", jobj.getString("SubjectID"));
                                    sendmessage.put("IPAddress",getIpAddress());

                                    new AsyncTask<Void, Void, Void>() {

                                        @Override
                                        protected Void doInBackground(Void... params) {


                                            Acknowled ack = new Acknowled(new Acknowled.OnMessageReceived() {
                                                @Override

                                                public void messageReceived(String message) {


                                                    try {


                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }


                                                }
                                            }, pref, "RCVMSG@" + sendmessage.toString());
                                            ack.run();
                                            return null;
                                        }
                                    }.execute();
                                } else {
                                    ContentValues values = new ContentValues();
                                    // values.put("IsAckSent",1);
                                    values.put("IsSent", 1);
                                    obj.UpdateMap(Integer.parseInt(jobj.getString("MsgID")), values);
                                }
                            } else {
                                ContentValues values = new ContentValues();
                                values.put("IsAckSent", 1);
                                values.put("IsSent", 1);
                                obj.UpdateMap(Integer.parseInt(jobj.getString("MsgID")), values);
                            }
                        } else {
                            String split[] = paramString.split("\\@");
                            JSONObject jobj = new JSONObject(split[1]);
                            ContentValues cv = new ContentValues();

                            String s = jobj.has("Action") ? jobj.getString("Action") : "";

                            if (s.equalsIgnoreCase("RCVACK")) {
                                ContentValues values = new ContentValues();
                                values.put("IsAckSent", 1);
                                values.put("IsSent", 1);
                                obj.UpdateMap(Integer.parseInt(jobj.getString("MsgID")), values);
                            }
                        }

                        ///  { Action = "ACK", MsgID = msgID, Status = 3, ReceiverID = receiverID, SubjectID = subjectID, IPAddress = MyIPAddress }

                    } catch (Exception e) {
                    }
                }
            });

        }
    };

    int id;
    Filemessage filemessage = new Filemessage() {
        @Override
        public void onMessageReceived(final String paramString) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    if (paramString.equalsIgnoreCase("Filesharing")) {

                        try {
                            Intent in = new Intent("pdia");
                            sendBroadcast(in);
                        } catch (Exception e) {

                        }

                        startActivity(new Intent(MainActivity.this, progressdialog.class));


                    } else if (paramString.startsWith("lock") || paramString.startsWith("unlock")) {
                        if (paramString.equalsIgnoreCase("lock")) {
                            Devicelock.backpressed = false;

                            if (!pref.getBoolean("devicelock", false)) {
                                try {
                                    Intent in = new Intent("unlock");
                                    sendBroadcast(in);
                                } catch (Exception e) {

                                }
                                startActivity(new Intent(MainActivity.this, Devicelock.class));
                                Toast.makeText(getApplicationContext(), "Your device is locked", Toast.LENGTH_SHORT).show();
                            } else {
                                try {
                                    Intent in = new Intent("unlock");
                                    sendBroadcast(in);
                                } catch (Exception e) {

                                }
                                startActivity(new Intent(MainActivity.this, Devicelock.class));
                                Toast.makeText(getApplicationContext(), "Your device is locked", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Devicelock.backpressed = true;

                            try {

                                if (Devicelock.devicelockact != null) {
                                    Devicelock.devicelockact.finish();
                                    //ViewerActivityController.lock=false;


                                }
                                //ViewerActivityController.lock=false;

                            } catch (Exception e) {

                            }
                           /* Intent in=new Intent("unlock");
                            sendBroadcast(in);*/
                            Toast.makeText(getApplicationContext(), "unlocked by your teacher.", Toast.LENGTH_SHORT).show();
                        }

                    } else if (paramString.startsWith("whiteboard")) {

                        try {
                            Intent in = new Intent("filemove");
                            //  in.putExtra("page",file[1].toString());
                            in.putExtra("close", true);
                            sendBroadcast(in);
                        } catch (Exception e) {

                        }

                        Toast.makeText(getApplicationContext(), "Your teacher has opted for whiteboard synchronization.", Toast.LENGTH_SHORT).show();
                        Whiteboard.synwhiteboard = true;
                       /* try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }*/
                        startActivity(new Intent(MainActivity.this, Whiteboard.class));
                        //startActivity(new Intent(MainActivity.this, Whiteboard.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else if (paramString.startsWith("Filemove")) {
                        String file[] = paramString.split("\\@");
                        try {
                            Intent in = new Intent("filemove");
                            in.putExtra("page", file[1].toString());
                            sendBroadcast(in);
                        } catch (Exception e) {

                        }


                    } else if (paramString.contains("ExamResult")) {

                        String exam_split[] = paramString.split("\\@");


                        DatabaseHandler db = new DatabaseHandler(MainActivity.this);

                        try {
                            JSONObject array = new JSONObject(exam_split[1]);

                            //JSONObject emp = (new JSONObject(paramString)).getJSONObject("Result");
                            String ResultStatus = array.getString("ResultStatus");
                            int ExamID = array.getInt("ExamID");

                            // Toast.makeText(getApplicationContext(), "paramString" + paramString,Toast.LENGTH_LONG).show();

                            db.updatexamdetailsByID(new ExamDetails(ExamID, 1));
                            Log.e("update ot not", "" + db.updatexamdetailsByID(new ExamDetails(ExamID, 1)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (paramString.startsWith("SelfAssesmentExamquestion")) {

                        try {
                            Intent in = new Intent("pdia");
                            sendBroadcast(in);
                        } catch (Exception e) {

                        }

                        try {


                            Log.e("examdetails,", "details");
                            //final JSONObject jObj = new JSONObject(paramString);
                            String exam_split[] = paramString.split("\\@");
                            JSONObject array = new JSONObject(exam_split[1]);
                            JSONArray Exam_arr = array.getJSONObject("ExamDetails").getJSONArray("Exam");

                            for (int i = 0; i < Exam_arr.length(); i++) {
                                JSONObject objexam = Exam_arr.getJSONObject(i);
                                ExamIDVal = objexam.getString("ExamID");

                                ExamID = Integer.parseInt(ExamIDVal);
                                ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                                ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                                ExamCategory = objexam.getString("ExamCategory");
                                ExamCode = objexam.getString("ExamCode");
                                ExamDescription = objexam.getString("ExamDescription");
                                BatchIDVal = objexam.getString("BatchID");
                                ExamDurationVal = objexam.getString("ExamDuration");
                                BatchID = Integer.parseInt(BatchIDVal);
                                ExamDuration = Integer.parseInt(ExamDurationVal);

                                SubjectIDVal = objexam.getString("SubjectID");
                                Subject = objexam.getString("Subject");

                                SubjectID = Integer.parseInt(SubjectIDVal);
                                ExamTypeIDVal = objexam.getString("ExamTypeID");

                                ExamType = objexam.getString("ExamType");

                                // ExamDate = objexam.getString("ExamDate");

                                ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   /* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*/
                                // String Questions = objexam.getString("Questions");

                                int ExamSequence = 0;
                                int SchoolID = 0;
                                int IsResultPublished = 0;
                                int ExamShelfID = 0;
                                int ClassID = 0;
                                int TimeTaken = 0;
                                int TotalScore = 0;
                                String DateAttended = "01/06/2017 10:00:00";

                                boolean isExamIdexist = db.CheckIsIDAlreadyInDBorNot(ExamIDVal);
                                if (isExamIdexist) {

                                    db.downloadUpdateExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                            BatchID, IsResultPublished,
                                            ExamShelfID,
                                            TimeTaken,
                                            DateAttended,
                                            TotalScore));

                                } else {
                                    db.addExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                            BatchID, IsResultPublished,
                                            ExamShelfID,
                                            TimeTaken,
                                            DateAttended,
                                            TotalScore));

                                }

                                List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

                                JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                                QuizWrapper newItemObject = null;

                                for (int j = 0; j < jsonArrayquestion.length(); j++) {
                                    JSONObject jsonChildNode = null;
                                    try {
                                        jsonChildNode = jsonArrayquestion.getJSONObject(j);
                                        String QuestionIDval = jsonChildNode.getString("QuestionID");
                                        QuestionID = Integer.parseInt(QuestionIDval);
                                        TopicIDVal = jsonChildNode.getString("TopicID");
                                        if (TopicIDVal.equalsIgnoreCase("")) {
                                            TopicID = 1;
                                        } else {
                                            TopicID = Integer.parseInt(TopicIDVal);

                                        }

                                        // int TopicID=1;
                                        Topic = jsonChildNode.getString("Topic");
                                        AspectIDVal = jsonChildNode.getString("AspectID");
                                        if (AspectIDVal.equalsIgnoreCase("")) {
                                            AspectID = 1;
                                        } else {
                                            AspectID = Integer.parseInt(AspectIDVal);

                                        }
                                        Aspect = jsonChildNode.getString("Aspect");
                                        QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                                        QuestionNumber = Integer.parseInt(QuestionNumberVal);
                                        question = jsonChildNode.getString("question");
                                        options = jsonChildNode.getJSONArray("options");

                                        String Evaluation = pref.getString("Evaluation", "0");
                                        File username = new File(Evaluation + "/" + ExamID);
                                        username.mkdir();
                                        if (question.contains("img")) {
                                            String questionImg = testX(question, username.getAbsolutePath());
                                            question = questionImg;
                                        }

                                        for (int z = 0; z < options.length(); z++) {
                                            String[] s = new String[options.length()];
                                            String storagepath = testX(options.get(z).toString(), username.getAbsolutePath());
                                            if (storagepath.contains(Evaluation)) {
                                                options.put(z, storagepath);
                                            }
                                        }
                                        CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                                        CorrectAnswerVal = 1;
                                        MarkVal = jsonChildNode.getString("Mark");
                                        Mark = Integer.parseInt(MarkVal);

                                        NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                                        NegativeMark = Integer.parseInt(NegativeMarkVal);
                                        String Created_on = "01/06/2017 10:00:00";
                                        String ModifiedOn = "01/06/2017 10:00:00";
                                        StudentAnswer = "";
                                        newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal, NegativeMark, Mark);

                                        if (isExamIdexist) {

                                            if (options.length() > 3) {
                                                db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(), CorrectAnswer, Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            } else if (options.length() > 2) {
                                                db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "", CorrectAnswer, Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            } else if (options.length() > 1) {
                                                db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            }


                                        } else {
                                            if (options.length() > 3) {
                                                db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(), CorrectAnswer, Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            } else if (options.length() > 2) {
                                                db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "", CorrectAnswer, Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            } else if (options.length() > 1) {
                                                db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                        NegativeMark, StudentAnswer,
                                                        IsCorrect,
                                                        ObtainedMark,
                                                        Created_on,
                                                        ModifiedOn));
                                            }

                                        }

                                        List<ExamDetails> examdetailsList = db.getAllExamsDetails();
                                        List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                                        Calendar calendar = Calendar.getInstance();
                                        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                        String strDate = mdformat.format(calendar.getTime());

                                        for (ExamDetails cn : examdetailsList) {
                                            String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID() + " ,BatchID: " + cn.getBatchID() + " ,IsResultPublished: " + cn.getIsResultPublished() + " ,ExamShelfID: " + cn.getExamShelfID() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                                            // Writing Contacts to log
                                            Log.d("Exam2: ", log);
                                        }

                                        for (QuestionDetails cn : questiondetailsList) {
                                            String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                                            // Writing Contacts to log
                                            Log.d("Question2: ", log);
                                        }

                                        jsonObject.add(newItemObject);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                Toast.makeText(getApplicationContext(), "Exam Received", Toast.LENGTH_LONG).show();

                                try {
                                    Intent in = new Intent("pdia");
                                    sendBroadcast(in);
                                } catch (Exception e) {

                                }

                                // Adding child data

                                // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                                //jsonObject.add(newItemObject);

                            }

                        } catch (Exception e) {
                            System.out.println(e.toString() + "zcx");

                        }
                    } else if (paramString.startsWith("Examquestion")) {


                        //open exam activity
                        Intent myIntent = new Intent(MainActivity.this, QuizActivity.class);
                        myIntent.putExtra("Examquestion", paramString);
                        startActivity(myIntent);

                        try {
                            Intent in = new Intent("pdia");
                            sendBroadcast(in);
                        } catch (Exception e) {

                        }

                        // startActivity(new Intent(MainActivity.this, QuizActivity.class));
                        // myIntent.putExtra("SearchText", outlet_no);
                    } else if (paramString.contains("Pulsequestion")) {


                        try {
                            Intent myIntent = new Intent(MainActivity.this, PulseActivity.class);
                            myIntent.putExtra("Pulsequestion", paramString);
                            startActivity(myIntent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                   /* else if(paramString.startsWith("Timetable"))
                    {

                        String timetable[]=paramString.split("\\@");

                    }*/

                    else if (paramString.startsWith("Calendar")) {
                        SqliteOpenHelperDemo obj = new SqliteOpenHelperDemo(MainActivity.this);
                        String getcalendardetails[] = paramString.split("\\@");
                        try {
                            JSONObject getcal = new JSONObject(getcalendardetails[1]);

                            ContentValues objcalendaer = new ContentValues();
                            objcalendaer.put("EventName", getcal.getString("EventName"));
                            objcalendaer.put("EventDescription", getcal.getString("EventDescription"));
                            objcalendaer.put("EventStartDate", getcal.getString("EventStartDate"));
                            objcalendaer.put("EventEndDate", getcal.getString("EventEndDate"));
                            objcalendaer.put("Status", 1);
                            objcalendaer.put("CategoryID", Integer.parseInt(getcal.getString("CategoryID")));

                            Cursor c = obj.retrive("tblCalendar");

                            if (c.getCount() > 0) {
                                c.moveToLast();
                                id = c.getInt(c.getColumnIndex("EventID"));
                            }
                            id = id + 1;
                            objcalendaer.put("EventID", id);
                            obj.InsertReceiveCalendar(objcalendaer);
                        } catch (Exception e) {

                        }


                    } else if (paramString.startsWith("FileClose")) {

                        // String file[]=paramString.split("\\@");

                        //  in.putExtra("page",file[1].toString());

                        try {

                            if (VideoActivity.videoact != null) {
                                VideoActivity.videoact.finish();
                            }
                        } catch (Exception e) {

                        }

                        try {

                            if (ViewerActivity.pdfact != null) {
                                ViewerActivity.pdfact.finish();
                                ViewerActivityController.lock = false;


                            }
                            ViewerActivityController.lock = false;

                        } catch (Exception e) {

                        }
                        try {

                            if (ImageActivity.imageact != null) {
                                ImageActivity.imageact.finish();

                            }
                        } catch (Exception e) {

                        }
                       /* try {
                            Intent in=new Intent("filemove");

                            in.putExtra("close", true);
                            getApplicationContext().sendBroadcast(in);
                        }
                        catch (Exception e)
                        {

                        }*/
                        Log.e("log", "log");
                        Toast.makeText(getApplicationContext(), "close", Toast.LENGTH_LONG).show();

                    } else if (paramString.startsWith("fileopen")) {
                        String split[] = paramString.split("\\@");
                        String contentid = split[5];
                        String page = split[7];
                        Cursor filecursor = obj.retriveContentvaluesall(Integer.parseInt(contentid));
                        String filename = "", catalog = "", descripe = "";


                        try {

                            if (filecursor.getCount() != 0) {
                                while (filecursor.moveToNext()) {
                                    filename = filecursor.getString(filecursor.getColumnIndex("ContentFilename"));
                                    catalog = filecursor.getString(filecursor.getColumnIndex("Catalog"));
                                    descripe = filecursor.getString(filecursor.getColumnIndex("ContentDescription"));

                                }

                                //  Toast.makeText(MainActivity.this,"Unabletoopenfile"+filename,Toast.LENGTH_LONG).show();

                                ArrayList<String> videoname = new ArrayList<String>();
                                videoname.add(".MP4");
                                videoname.add(".WAV");
                                videoname.add(".MP3");
                                videoname.add(".3GP");
                                videoname.add(".MP4");
                                videoname.add(".M4A");
                                videoname.add(".AAC");
                                videoname.add(".TS");

                                ArrayList<String> imagename = new ArrayList<String>();
                                imagename.add(".jpeg");
                                imagename.add(".bmp");
                                imagename.add(".png");
                                imagename.add(".jpg");

                                ArrayList<String> filepdf = new ArrayList<String>();
                                filepdf.add(".pdf");
                                filepdf.add(".doc");
                                filepdf.add(".ppt");
                                String filepath = filename.substring(filename.lastIndexOf("/") + 1, filename.length());
                                String filesplit[] = filepath.split("\\.");
                                if (filepdf.contains("." + filesplit[1].toLowerCase())) {

                                    Uri uri = Uri.parse(filename);
                                    File file = new File(filename);
                             /*  Intent intent = new Intent(MainActivity.this, MuPDFActivity.class);
                               intent.putExtra("linkhighlight", true);
                               //intent.putExtra("senddata",senddata);
                               intent.setAction(Intent.ACTION_VIEW);
                               intent.setData(uri);
                               intent.putExtra("pagenumber", page);
                               intent.putExtra("lock", true);

                               //if document protected with password
                               intent.putExtra("password", "encrypted PDF password");

                               //if you need highlight link boxes
                               intent.putExtra("linkhighlight", true);

                               //if you don't need device sleep on reading document
                               intent.putExtra("idleenabled", false);

                               //set true value for horizontal page scrolling, false value for vertical page scrolling
                               intent.putExtra("horizontalscrolling", true);

                               //document name
                               intent.putExtra("docname", descripe);

                                 ;
*/

                                    try {
                                        Intent in = new Intent("filemove");
                                        //  in.putExtra("page",file[1].toString());
                                        in.putExtra("close", true);
                                        sendBroadcast(in);
                                    } catch (Exception e) {

                                    }
                              /* try {
                                   Thread.sleep(2000);
                               } catch (InterruptedException e) {
                                   e.printStackTrace();
                               }*/
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromFile(file));
                                    intent.setClass(MainActivity.this, ViewerActivity.class);
                                    intent.putExtra("pagenumber", page);
                                    intent.putExtra("lock", true);
                                    intent.putExtra("bookname", descripe);
                                    startActivity(intent);
                                } else if (imagename.contains("." + filesplit[1].toLowerCase())) {
                                    try {
                                        Intent in = new Intent("filemove");
                                        //  in.putExtra("page",file[1].toString());
                                        in.putExtra("close", true);
                                        sendBroadcast(in);
                                    } catch (Exception e) {

                                    }
                              /* try {
                                   Thread.sleep(2000);
                               } catch (InterruptedException e) {
                                   e.printStackTrace();
                               }*/
                                    Intent intent = new Intent(MainActivity.this,
                                            ImageActivity.class);
                                    Bundle bun = new Bundle();
                                    Log.d("selectimage", filename);

                                    bun.putString("path", filename);
                                    intent.putExtras(bun);
                                    startActivity(intent);
                                } else {
                                    try {
                                        Intent in = new Intent("filemove");
                                        //  in.putExtra("page",file[1].toString());
                                        in.putExtra("close", true);
                                        sendBroadcast(in);
                                    } catch (Exception e) {

                                    }
                              /* Intent in=new Intent("filemove");
                               //  in.putExtra("page",file[1].toString());
                               in.putExtra("close",true);
                               sendBroadcast(in);*/
                              /* try {
                                   Thread.sleep(2000);
                               } catch (InterruptedException e) {
                                   e.printStackTrace();
                               }*/
                                    Intent intent = new Intent(MainActivity.this,
                                            VideoActivity.class);
                                    Bundle bun = new Bundle();
                                    Log.d("selectimage", filename);

                                    bun.putString("path", filename);
                                    intent.putExtras(bun);
                                    startActivity(intent);
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "No file", Toast.LENGTH_LONG).show();

                            }

                        } catch (Exception e) {
                            Toast.makeText(MainActivity.this, "Failed to open", Toast.LENGTH_LONG).show();
                        }
                        /*if(message.contains("Filesharing")) {
                            Runnable runclint = new MultiThreadChatServerSync.Receivefile(split[1], Long.parseLong(split[2]),split[3],split[4],split[5], socket.getInetAddress().toString(),split[6]);
                            Thread recf = new Thread(runclint);
                            recf.start();
                        }*/

                    }


                   /* else if(paramString.contains("lock")||paramString.contains("unlock"))
                    {
                        if(paramString.equalsIgnoreCase("lock"))
                        {
                            if(!pref.getBoolean("devicelock",false)) {
                                Intent in=new Intent("unlock");
                                sendBroadcast(in);
                                startActivity(new Intent(MainActivity.this, Devicelock.class));
                                Toast.makeText(getApplicationContext(), "Your device is locked", Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                Intent in=new Intent("unlock");
                                sendBroadcast(in);
                                startActivity(new Intent(MainActivity.this, Devicelock.class));
                                Toast.makeText(getApplicationContext(), "Your device is locked", Toast.LENGTH_LONG).show();

                            }
                        }
                        else
                        {
                            Intent in=new Intent("unlock");
                            sendBroadcast(in);
                            Toast.makeText(getApplicationContext(),"unlocked by your teacher.",Toast.LENGTH_LONG).show();
                        }

                    }*/

                    else if (paramString.equalsIgnoreCase("DND@true")) {
                        edit.putBoolean("hand", true);
                        edit.commit();
                    } else if (paramString.equalsIgnoreCase("DND@false")) {
                        edit.putBoolean("hand", false);
                        edit.commit();
                    } else if (paramString.startsWith("Quiz")) {
                        String split[] = paramString.split("\\@");
                        if (split[1].equalsIgnoreCase("open")) {
                            startActivity(new Intent(MainActivity.this, Quizzactivity.class));

                        } else {
                            try {
                                sendBroadcast(new Intent("closealert"));
                            } catch (Exception e) {

                            }
                        }
                    } else if (paramString.equalsIgnoreCase("FileFailed")) {
                        try {

                                /* if(diadown.isShowing()) {
                                     diadown.cancel();
                                 }*/
                            Intent in = new Intent("pdia");
                            sendBroadcast(in);
                                /* new AlertDialog.Builder(getApplicationContext())
                                         .setCancelable(false)
                                         .setMessage("Failed to Received")
                                         .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                             public void onClick(DialogInterface dialog, int which) {
                                                 //getActivity().finish();
                                             }
                                         })
                                         .show();*/


                        } catch (Exception e) {

                        }


                        Toast.makeText(getApplicationContext(), "Failed to Received", Toast.LENGTH_LONG).show();

                    } else if (paramString.equalsIgnoreCase("File Received")) {

                        try {

                               /*  if(diadown.isShowing()) {
                                     diadown.cancel();
                                 }
*/
                            Intent in = new Intent("pdia");
                            sendBroadcast(in);
                                 /*new AlertDialog.Builder(getApplicationContext())
                                         .setCancelable(false)
                                         .setMessage(paramString)
                                         .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                             public void onClick(DialogInterface dialog, int which) {
                                                 //getActivity().finish();
                                             }
                                         })
                                         .show();*/


                        } catch (Exception e) {

                        }
                        Toast.makeText(getApplicationContext(), paramString, Toast.LENGTH_LONG).show();

                    }


                }
            });

        }
    };


    public String testX(String hjdsf, String path) {
        Pattern p = Pattern.compile("<img.+?src=[\\\\\\\\\\\\\\\"'](.+?)[\\\\\\\\\\\\\\\"'].+?>");
        Matcher m = p.matcher(hjdsf);
        String test = "";
        if (m.find()) {

            System.out.println(m.group(1).substring(1));
            String filename = m.group(1).substring(1).substring(0, m.group(1).substring(1).lastIndexOf('.'));
            // prints http://www.01net.com/images/article/mea/150.100.790233.jpg

            String batchfile = m.group(1).substring(1).substring(m.group(1).substring(1).lastIndexOf("/") + 1, m.group(1).substring(1).length());

            downloadfile(m.group(1).substring(1), path, batchfile);
            storagepath = path + "/" + batchfile;

            test = hjdsf.replaceAll(m.group(1).substring(1), storagepath);

            System.out.println(test + "test");
            //pathArray.add(test);
        }


        return test;

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        System.out.println("....window focus changed..");
        Log.e("hia", "hia");
        try {
            super.onWindowFocusChanged(hasFocus);

            if (!hasFocus) {
                Object service = getSystemService("statusbar");
                Class<?> statusbarManager = Class.forName("android.app.StatusBarManager");
                Method collapse = statusbarManager.getMethod("collapse");
                collapse.setAccessible(true);
                collapse.invoke(service);
            }
        } catch (Exception ex) {
        }

           /* try{
                if (hasFocus) {
                    getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
              *//*  if(!hasFocus) {
                    getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE);
                }*//*
            }
            catch (Exception e)
            {

            }*/
        // }
    }

    public class connectTask extends AsyncTask<String, String, Client> {

        @Override
        protected Client doInBackground(String... message) {

            //we create a Client object and

            String rollno = "", schoolid = "", stdid = "", fname = "";
            Cursor stdcursor = obj.retrive("tblStudent");
            while (stdcursor.moveToNext()) {
                rollno = stdcursor.getString(stdcursor.getColumnIndex("RollNo"));
                schoolid = String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("SchoolID")));
                stdid = String.valueOf(stdcursor.getInt(stdcursor.getColumnIndex("StudentID")));
                fname = stdcursor.getString(stdcursor.getColumnIndex("FirstName"));

            }

            //String s="invS@Connect@IPAddress@RollNumber";
            String s = "invS@connect@" + getIpAddress() + "@" + rollno + "@" + schoolid + "@" + fname + "@" + stdid + "@" + currentdate1();
            edit.putString("studentviolationip",getIpAddress());
            edit.commit();

            mClient = new Client(new Client.OnMessageReceived() {
                @Override

                public void messageReceived(String message) {


                    try {


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }, pref, s);
            mClient.run();

            return null;
        }

    }

    @Override
    public void onBackPressed() {
    }


    public String getBroadcastIp() {
        String broadcastAddr = "255.255.255.255";
        try {
            for (InterfaceAddress addr : NetworkInterface.getByInetAddress(InetAddress.getByName(ip)).getInterfaceAddresses()) {
                if (addr.getAddress().getHostAddress().equals(getIpAddress())) {
                    broadcastAddr = addr.getBroadcast().getHostAddress();
                    break;
                }
            }
        } catch (SocketException e) {
            IOException e2 = e;
            e2.printStackTrace();
        } catch (UnknownHostException e3) {

        } catch (Exception e4) {/* e2 = e3;
            e2.printStackTrace();*/
            e4.printStackTrace();
        }
        return broadcastAddr;
    }


    public static String currentdate1() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    void downloadfile(String urls, String filepath, String filename) {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("ImageFileeError: ", e.getMessage());
        }


    }

    public void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }

        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filename;
            if (entry.getName().contains("/")) {
                filename = entry.getName();
                filename = filename.substring(filename.lastIndexOf("/") + 1, filename.length());
            } else {
                filename = entry.getName();
            }
            String filePath = destDirectory + File.separator + filename;
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }


    /**
     * Extracts a zip entry (file entry)
     *
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    void DownloadTask1(String urls, String filepath) {

        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());
            filename = "logo_" + filename;
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            //    Log.e("Error: ", e.getMessage());
        }


    }


    void DownloadTask2(String urls, String filepath) {

        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());
            filename = "background_" + filename;
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            //   Log.e("Error: ", e.getMessage());
        }


    }

    public void DownloadTask(String urls, String filepath, String stdimagedb, String logoimagedb, String backimagedb) {


        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;

                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
        }


    }


    public void otherTask(String urls, String filepath) {


        int count;
        try {
            String filename = urls.substring(urls.lastIndexOf("/") + 1, urls.length());

            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();


            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);
            // Output stream
            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
        }


    }



    class DownloadNewVersion extends AsyncTask<String,Integer,Boolean> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            bar = new ProgressDialog(MainActivity.this);
            bar.setCancelable(false);

            bar.setMessage("Downloading...");

            bar.setIndeterminate(true);
            bar.setCanceledOnTouchOutside(false);
            bar.show();

        }

        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);

            bar.setIndeterminate(false);
            bar.setMax(100);
            bar.setProgress(progress[0]);
            String msg = "";
            if(progress[0]>99){

                msg="Finishing... ";

            }else {

                msg="Downloading... "+progress[0]+"%";
            }
            bar.setMessage(msg);

        }
        @Override
        protected void onPostExecute(Boolean result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            bar.dismiss();

            if(result){

                Toast.makeText(getApplicationContext(),"Update Done",
                        Toast.LENGTH_SHORT).show();

            }else{

                Toast.makeText(getApplicationContext(),"Error: Try Again",
                        Toast.LENGTH_SHORT).show();

            }

        }


        @Override
        protected Boolean doInBackground(String... arg0) {
            Boolean flag = false;

            try {


                URL url = new URL(Url.bottomapk);


                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();


                String PATH = Environment.getExternalStorageDirectory()+"/Download/";
                File file = new File(PATH);
                file.mkdirs();

                File outputFile = new File(file,"app-debug.apk");

                if(outputFile.exists()){
                    outputFile.delete();
                }


                InputStream is = c.getInputStream();

                int total_size = 1431692;//size of apk

                byte[] buffer = new byte[1024];
                int len1 = 0;
                int per = 0;
                int downloaded=0;

                FileOutputStream fos = new FileOutputStream(outputFile);

                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                    downloaded +=len1;
                    per = (int) (downloaded * 100 / total_size);
                    publishProgress(per);
                }
                fos.close();
                is.close();

                OpenNewVersion(PATH);

                flag = true;
            } catch (Exception e) {
                Log.e(TAG, "Update Error: " + e.getMessage());
                flag = false;
            }
            return flag;


        }

    }



    void OpenNewVersion(String location) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(new File(location + "app-debug.apk")),
                "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}

/*
    void disablenotification()
    {
        WindowManager manager = ((WindowManager) getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE));

        WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        localLayoutParams.gravity = Gravity.TOP;
        localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |

                // this is to enable the notification to recieve touch events
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

                // Draws over status bar
                WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        localLayoutParams.height = (int) (25 * getResources()
                .getDisplayMetrics().scaledDensity);
        localLayoutParams.format = PixelFormat.TRANSPARENT;

        customviewgroup view = new customviewgroup(this);

        manager.addView(view, localLayoutParams);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }*/


   /* public class FileTxThread implements Runnable {
        private final Socket socket;

        FileTxThread(Socket socket){
            this.socket= socket;
        }

        @Override
        public void run() {
            if(socket.isConnected())
            {
                File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "my.mp4");

                byte[] bytes = new byte[(int) file.length()];
                BufferedInputStream bis;
                try {
                    bis = new BufferedInputStream(new FileInputStream(file));
                    bis.read(bytes, 0, bytes.length);

                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(bytes);
                    oos.flush();


                    final String sentMsg = "File sent to: " + socket.getInetAddress();
                    socket.close();

                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    sentMsg,
                                    Toast.LENGTH_LONG).show();
                        }});

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
            else
            {
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Device disconnected",
                                Toast.LENGTH_LONG).show();
                    }});
            }
        }
    }*/
